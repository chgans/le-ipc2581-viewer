#pragma once

#include <QString>
#include <QList>

enum class BomCategory
{
    Electrical = 1,
    Programmable = 2,
    Mechanical = 3,
    MAterial = 4,
    Document = 5
};

struct BomHeader
{
    QString assembly;
    QString revisioin;
    bool affecting;
    QList<QString> stepRefList;
};

struct BomDes
{
};

struct DocDes
{
    QString name;
    QString layerRef;
};

struct MatDes
{
    QString name;
    QString layerRef;
};

struct RefDes
{
    QString name;
    QString packageRef;
    bool populate;
    QString layerRef;
    // FIXME: Tunning and Firmware
};

struct ToolDes
{
    QString name;
    QString layerRef;
};

// FIXME: Measured, Ranged, Enumarated, Textual
struct Characteristic
{

};

struct Characteristics
{
    BomCategory category;
    QList<Characteristic*> characteristicList;
};

struct BomItem
{
    QString OemDesignNumberRef;
    QString quantity;
    int pinCount;
    BomCategory category;
    QString internalPartNumber;
    QString description;
    QList<BomDes*> bomDesList;
    Characteristics characteristics;
};

struct Bom
{
    QString name;
    BomHeader header;
    QList<BomItem*> itemList;
};
