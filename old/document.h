#pragma once

#include "content.h"
#include "logisitic.h"
#include "history.h"
#include "bom.h"
#include "ecad.h"
#include "avl.h"

struct Document
{
    QString revision;
    Content content;
    LogisiticHeader logisticHeader;
    HistoryRecord historyRecord;
    QList<Bom*> bomList;
    ECad eCad;
    Avl avl;
};
