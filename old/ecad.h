#pragma once

#include <QString>
#include <QList>

#include "bom.h"
#include "content.h"
#include "history.h"

// FIXME
struct NonStandardAttibute
{

};

struct Property
{
    double value;
    QString text;
    QString unit;
    double tolPlus;
    double tolMinus;
    bool tolPercent;
    QString refUnit;
    double refValue;
    QString refText;
    QString layerOrGroupRef;
    QString comment;
};

struct Specification
{

};

enum class BackdrillSpecificationType
{
    StartLayer = 1,
    MustNotCutLayer = 2,
    MaxStubLength = 3,
    Other = 4
};

struct BackdrillSpecification: public Specification
{
    BackdrillSpecificationType type;
    QString comment;
    QList<Property *> propertyList;
};

enum class ComplianceSpecificationType
{
    ROHS = 1, // Restriction of Hazardous Substances
    ConflictMinerals = 2,
    Weee = 3, // Waste Electrical and Electronic Equipment
    Reach = 4, // Registration, Evaluation, Authorisation and Restriction of Chemicals
    HalogenFree = 5,
    Other = 6
};

struct ComplianceSpecification: public Specification
{
    ComplianceSpecificationType type;
    QString comment;
    QList<Property *> propertyList;
};

enum class ConductorSpecificationType
{
    Conductivity = 1,
    SurfaceRoughnessUpfacing = 2,
    SurfaceRoughnessDownfacing = 3,
    SurfaceRoughnessTreated = 4,
    EtchFactor = 5,
    FinishedHeight = 6
};

struct ConductorSpecification: public Specification
{
    ConductorSpecificationType type;
    QString comment;
    QList<Property *> propertyList;
};

enum class DielectricSpecificationType
{
    DielectricConstant = 1,
    LossTangent = 2,
    GlassType = 3,
    GlassStyle = 4,
    ResinContent = 5,
    ProcessabilityTemperature = 6,
    Other = 7,
};

struct DielectricSpecification: public Specification
{
    DielectricSpecificationType type;
    QString comment;
    QList<Property *> propertyList;
};

enum class GeneralSpecificationType
{
    Electrical = 1,
    Thermal = 2,
    Material = 3,
    Instruction = 4,
    Standard = 5,
    Other = 6
};

struct GeneralSpecification: public Specification
{
    GeneralSpecificationType type;
    QString comment;
    QList<Property *> propertyList;
};

enum class ImpedanceSpecificationType
{
    Impedance = 1,
    LineWidth = 2,
    Spacing = 3,
    RefPlaneLayerId = 4,
    Other = 5
};

enum class ImpedanceTransmissionType
{
    SingleEnded = 1,
    EdgeCoupled = 2,
    BroadsideCoupled = 3,
    Other = 4
};

enum class ImpedanceStructureType
{
    Stripline = 1,
    PlaneLessStripline = 2,
    MicrostripEmbedded = 3,
    MicrostripNoMask = 4,
    MicrostripMaskCovered = 5,
    MicrostripDualMaskCovered = 6,
    CoplanarWaveguideStripLine = 7,
    CoplanarWaveguideEmbedded = 8,
    CoplanarWaveguideNoMask = 9,
    CoplanarWaveguideMaskCovered = 10,
    CoplanarWaveguideDualMaskCovered = 11,
};

struct ImpedanceSpecification: public Specification
{
    ImpedanceSpecificationType type;
    ImpedanceTransmissionType transmission;
    ImpedanceStructureType structure;
    QString comment;
    QList<Property *> propertyList;
};

enum class TechnologySpecificationType
{
    Rigid = 1,
    RigidFlex = 2,
    Flex = 3,
    Hdi = 4,
    EmbeddedComponent = 5,
    Other = 6
};

struct TechnologySpecification: public Specification
{
    TechnologySpecificationType type;
    QString comment;
    QList<Property *> propertyList;
};

enum class ThermalSpecificationType
{
    ThermalDelamination = 1,
    ExpansionZAxis = 2,
    ExpansionXYAxis = 3,
    Other = 4
};

struct ThermalSpecification: public Specification
{
    ThermalSpecificationType type;
    QString comment;
    QList<Property *> propertyList;
};

enum class ToolSpecificationType
{
    Carbide = 1,
    Router = 2,
    Laser = 3,
    FlatNose = 4,
    Extension = 5,
    VCutter = 6,
    Other = 7
};

enum class ToolPropertyType
{
    DrillSize = 1,
    FinishedSize = 2,
    BitAngle = 3,
    Other = 4
};

struct ToolSpecification: public Specification
{
    ToolSpecificationType type;
    ToolPropertyType toolProperty;
    QString comment;
    QList<Property *> propertyList;
};

enum class VCutSpecificationType
{
    Angle = 1,
    ThicknessRemaining = 2,
    Offset = 3,
    Other = 4
};

struct VCutSpecification: public Specification
{
    VCutSpecificationType type;
    QString comment;
    QList<Property *> propertyList;
};

struct Spec
{
    QString name;
    QList<Specification*> specificationList;
    XForm xForm;
    Location location;
    Outline outline;
};

struct CadHeader
{
    QString unit;
    QList<Spec *> specList;
    QList<ChangeRec *> changeRecList;
};

enum class LayerFunction
{
    Assembly,
    BoardFab,
    BoardOutline,
    Capacitive,
    CoatingCond,
    CoatingNonCond,
    Component,
    ComponentBottom,
    ComponenetTop,
    CondFilm,
    CondFoil,
    ConductiveAdhesive,
    Conductor,
    CourtYard,
    DielBase,
    DielCore,
    DielPreg,
    DielAdhv,
    Document,
    Drill,
    Fixture,
    Glue,
    Graphic,
    HoleFill,
    SolderBump,
    PAsteMask,
    EmbeddedComponent,
    LandPattern,
    Legend,
    Mixed,
    Other,
    Pin,
    Plane,
    Probe,
    Resistive,
    Signal,
    SilkScreen,
    SolderMask,
    SolderPaste,
    StackupComposite,
    Rework,
    Rout,
    VCut
};

enum class LayerSide
{
    Top = 1,
    Bottom = 2,
    Both = 3,
    Internal = 4,
    All = 5,
    None = 6
};

struct LayerSpan
{
    QString fromLayer; // ???
    QString toLayer; // ???
};

struct Layer
{
    QString name;
    LayerFunction function;
    LayerSide side;
    bool polarity;
    QList<QString> specRefList; // *
    LayerSpan span; // ?
};

enum class WhereMeasured
{
    Laminate = 1,
    Metal = 2,
    Mask = 3,
    Other = 4
};

struct StackupLayer
{
    QString layerOrGroupRef; // 1 LayerRef or StackupGroupRef
    double thickness; // 1
    double tolPlus; // 1
    double tolMinus; // 1
    int sequence; // ?
    QString refDes; // ?
    QString comment; // ?
    QList<QString> specRefList; // *
};

struct StackupGroup
{
    QString name; // 1
    double thickness; // 1
    double tolPlus; // 1
    double tolMinus; // 1
    QString comment; // ?
    QString refDes; // ?
    QList<QString> specRefList; // *
    QList<StackupLayer *> stackupLayerList; // *
    QList<QString> cadDataLayerRefList; // *
};

struct Stackup
{
    QString name; // 1
    double overallThickness; // 1
    double tolPlus; // 1
    double tolMinus; // 1
    WhereMeasured whereMeasured;  // 1
    QString comment; // ?
    QString refDes; // ?
    QList<QString> specRefList; // *
    QList<StackupGroup *> stackupGroupList; // *
};

enum class PlatingStatus
{
    Plated = 1,
    NonPlated = 2,
    Via = 3
};

// FIXME
struct PadStack
{

};

struct PadStackHoleDef
{
    QString name;
    double diameter;
    PlatingStatus platingStatus;
    double tolPlus;
    double tolMinus;
    double x;
    double y;
};

enum class PadUse
{
    Regular = 1,
    AntiPad = 2,
    Thermal = 3,
    Other = 4
};

struct PadStackPadDef
{
    QString layerRef;
    PadUse padUse;
    QString comment;
    XForm xForm;
    Location location;
    Feature feature;
};

struct PadStackDef
{
    QString name;
    QList<PadStackHoleDef *> padStaskHoleDefList;
    QList<PadStackPadDef *> padStackPadDefList;
};

enum class Polarity
{
    Positive = 1,
    Negative = 2
};

enum class PadUsage
{
    Termination = 1,
    Via = 2,
    Plane = 3,
    ToolingHole = 4,
    Mask = 5,
    None = 6
};

struct Set
{
    QString net;
    Polarity polarity;
    PadUsage padUsage;
    bool testPoint;
    QString geometry;
    bool plate;
    QString componentRef;
    // FIXME: add ordered choice
};

struct LayerFeature
{
    QString layerRef;
    QList<Set *> setList;
};

struct Route
{
    QString net;
    QList<LayerFeature *> layerFeatureList;
};

// FIXME
struct StepRepeat
{

};

// FIXME
struct Pad
{

};

// FIXME
struct Target
{

};

// FIXME
struct Marking
{

};

struct LandPattern
{
    QList<Pad *> padList;
    QList<Target *> targetList;
};

struct SilkScreen
{
    QList<Outline *> outlineList;
    QList<Marking *> markingList;
};

struct AssemblyDrawing
{
    QList<Outline *> outlineList;
    QList<Marking *> markingList;
};

enum class CadPinType
{
    Thru = 1,
    Blind = 2,
    Surface = 3
};

enum class PinElectricalType
{
    Electrical = 1,
    Mechanical = 2,
    Undefined = 3
};

enum class PinMountType
{
    SurfaceMountPin = 1,
    SurfaceMountPad = 2,
    ThroughHolePin = 3,
    ThroughHoleHole = 4,
    Pressfit = 5,
    NonBoard = 6,
    Hole = 7,
    Undefined = 8
};

struct Pin
{
    QString number;
    QString name;
    CadPinType type;
    PinElectricalType electricalType;
    PinMountType mountType;
    XForm xform;
    Location location;
    StandardShape *standardShape;
};

enum class PackageType
{
    AxialLeaded = 1,
    BareDie = 2,
    CeramicBga = 3,
    CeramicDip = 4,
    CeramicFlatPAck = 5,
    CeramicQuadFlatPack = 6,
    CeramicSip = 7,
    Chip = 8,
    ChipScale = 9,
    ChokeSwitchSm = 10,
    ConnectorTh = 11,
    Embedded = 12,
    FlipChip = 13,
    HermeticHybrid = 14,
    LeadlessCeramicChip = 15,
    Mcm = 16,
    Melf = 17,
    FinePitchBga = 18,
    Molded = 19,
    Network = 20,
    Pga = 21,
    PlasticBga = 22,
    PlasticChipCarrier = 23,
    PlasticDip = 24,
    PlasticSip = 25,
    PoserTransistor = 26,
    RadialLeaded = 27,
    RectangularQuadFlatPack = 28,
    RealySm = 29,
    RelayTh = 30,
    Sod123 = 31,
    Soic = 32,
    Soj = 33,
    Sopic = 34,
    Sot143 = 35,
    Sot23 = 36,
    Sot52 = 37,
    Sot89 = 38,
    SquareQuadFlatPack = 39,
    Ssoic = 40,
    SwitchTh = 41,
    Tantalum = 42,
    ToType = 43,
    Transformer = 44,
    TrimpotSm = 45,
    TrimpotTh = 46,
    Other = 47
};

enum class PinOneOrientationType
{
    LowerLeft = 1,
    UpperLeft = 2,
    Left = 3,
    UpperCenter = 4,
    LeftCenter = 5,
    Other = 6
};

struct Package
{
    QString name;
    PackageType type;
    QString pinOne;
    PinOneOrientationType pinOrientation;
    double height;
    QString comment;
    Outline outline;
    Location pickupPoint;
    LandPattern landPattern;
    SilkScreen silkScreen;
    AssemblyDrawing assemblyDrawing;
    QList<Pin *> pinList;
};

enum class MountType
{
    Smt = 1,
    Thmt = 2,
    Other = 3
};

enum class NetClass
{
    Clk = 1,
    Fixed = 2,
    Ground = 3,
    Signal = 4,
    Power = 5,
    Unused = 6
};

struct PinRef
{
    QString componentRef;
    QString pin;
    QString title;
};

struct LogicalNet
{
    QString name;
    NetClass netClass;
    QList<NonStandardAttibute *> nonStandardAttributeList;
    QList<PinRef> pinRefList;
};

struct Component
{
    QString refDes;
    QString packageRef;
    QString part;
    QString layerRef;
    MountType mountType;
    double weight;
    double height;
    double standoff;
    QList<NonStandardAttibute *> nonStandardAttributeList;
    XForm xForm;
    Location location;
};

enum class NetPointType
{
    End = 1,
    Middle =2
};

enum class Exposure
{
    Exposed = 1,
    CoveredPrimary = 2,
    CoveredSecondary = 3,
    Covered = 4
};

struct PhyNetPoint
{
    double x;
    double y;
    QString layerRef;
    QString secondaryLayerRef;
    NetPointType netNode;
    Exposure exposure;
    QString layerIndex;
    QString comment;
    bool via;
    bool fiducial;
    bool test;
    double staggerX;
    double staggerY;
    double staggerRadius;
    XForm xForm;
    Feature *feature;
};

struct PhyNet
{
    QString name;
    QList<PhyNetPoint *> phyNetPointList;
};

struct PhyNetGroup
{
    QString name;
    bool optimised;
    QList<PhyNet *> phyNetList;
};

// FIXME
struct DfxMeasurementGroup
{

};

struct Step
{
    QString name;
    Location datum;
    Contour profile;
    QList<NonStandardAttibute> nonStandardAttributeList;
    QList<PadStack> padStaskList;
    QList<PadStackDef *> padStackDefList;
    QList<Route *> routeList;
    QList<StepRepeat *> stepRepeatList;
    QList<Package *> packageList;
    QList<Component> componentList;
    QList<LogicalNet *> logicalNetList;
    QList<PhyNetGroup *> phyNetGroupList;
    QList<LayerFeature *> layerFeatureList;
    QList<DfxMeasurementGroup> dfxMeasurementGroupList;
};

struct CadData
{
    QList<Layer *> layerList;
    QList<Stackup *> stackupList;
    QList<Step *> stepList;
};

struct ECad
{
    QString name;
    CadHeader header;
    CadData data;
};
