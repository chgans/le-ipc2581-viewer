#!/bin/sh

XSD=IPC-2581B_V3.0
XSDROOT=/c/Programs/xsd-4.1.0.a9-i686-windows

export PATH=$XSDROOT/bin:$PATH

xsd cxx-parser \
	--root-element IPC-2581 \
	--generate-polymorphic \
	--namespace-map http://webstds.ipc.org/2581=ipc2581 \
	--generate-print-impl --generate-test-driver \
	--file-per-type --output-dir xsdcxx \
	--std c++11 \
	$XSD.xsd