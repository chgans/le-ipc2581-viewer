#!/bin/sh

XSD=IPC-2581B_V3.0
#XSDROOT=/c/Programs/xsd-4.1.0.a9-i686-windows
XSDROOT=/home/krys/Projects/xsd-4.1.0.a8-x86_64-linux-gnu

export PATH=$XSDROOT/bin:$PATH

xsd cxx-tree \
	--root-element IPC-2581 \
	--generate-polymorphic \
	--type-naming ucc \
	--function-naming lcc \
	--namespace-map http://webstds.ipc.org/2581=ipc2581 \
	$XSD.xsd
