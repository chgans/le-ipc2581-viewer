#!/usr/bin/env python
# -*- coding: utf-8 -*-

from enum import Enum
import sys
import re


class ClassDecl:
	def __init__(self, className):
		self._className = className
		self._members = []

		# TODO:
		# Class name, storage (always *), member map attr/el name -> attr/el Decl

	def attributes(self):
		results = []
		for member in self._members:
			if type(member) is ClassAttributeDecl:
				results.append(member)
		return results

	def elements(self):
		results = []
		for member in self._members:
			if type(member) is ClassElementDecl:
				results.append(member)
		return results

	def toCPlusPlus(self):
		print("struct %s {" % (self._className))
		for member in self._members:
			member.toClassMember();
		print("};")

	def toCPlusPlusForward(self):
		print("class %s;" % (self._className))

class ClassAttributeDecl:
	def __init__(self, name, type, method, mul):
		self._memberName = name
		self._typeName = type
		self._passMethod = method
		self._multiplicity = mul

	def toClassMember(self):
		typeSig = self._typeName;
		if self._passMethod == "*":
			typeSig += self._passMethod
		if self._multiplicity == "*":
			print("  QList<%s> %sList; // %s" % (typeSig, self._memberName, self._multiplicity))
		elif self._multiplicity == "+":
			print("  QList<%s> %sList; // %s" % (typeSig, self._memberName, self._multiplicity))
		elif self._multiplicity == "?":
			print("  Optional<%s> %sOptional; // %s" % (typeSig, self._memberName, self._multiplicity))
		else:
			print("  %s %s; // %s" % (typeSig, self._memberName, self._multiplicity))

class ClassElementDecl:
	def __init__(self, name, type, method, mul):
		self._memberName = name
		self._typeName = type
		self._passMethod = method
		self._multiplicity = mul

	def toClassMember(self):
		typeSig = self._typeName;
		if self._passMethod == "*":
			typeSig += self._passMethod
		if self._multiplicity == "*":
			print("  QList<%s> %sList; // %s" % (typeSig, self._memberName, self._multiplicity))
		elif self._multiplicity == "+":
			print("  QList<%s> %sList; // %s" % (typeSig, self._memberName, self._multiplicity))
		elif self._multiplicity == "?":
			print("  Optional<%s> %sOptional; // %s" % (typeSig, self._memberName, self._multiplicity))
		else:
			print("  %s %s; // %s" % (typeSig, self._memberName, self._multiplicity))


class ClassMemberDeclParser:
	def __init__(self):
		pass

	def parse(self, data):
		regexp = r"(?P<attr>@?)(?P<name>[a-zA-Z0-9]+)(?P<mul>[*+?]?)\s*((?P<type>[a-zA-Z0-9]*)(?P<method>[&*]?))?"
		tokens = re.search(regexp, data).groupdict()

		if len(tokens['type']) == 0:
			tokens['type'] = tokens['name'][0].upper() + tokens['name'][1:]

		tokens['name'] = tokens['name'][0].lower() + tokens['name'][1:]

		if len(tokens['mul']) == 0:
			tokens['mul'] = "1"

		if len(tokens['attr']) != 0:
			return ClassAttributeDecl(tokens['name'], tokens['type'], tokens['method'], tokens['mul'])
		else:
			return ClassElementDecl(tokens['name'], tokens['type'], tokens['method'], tokens['mul'])
		pass

		return None


class ClassDeclParser:
	def __init__(self):
		pass

	def parse(self, data):
		tokens = data.split(" ")
		className = tokens[0]
		return ClassDecl(className), ClassMemberDeclParser()


class EnumDecl:
	def __init__(self, enumName):
		self._enumName = enumName
		self._members = []

	def toCPlusPlus(self):
		print("enum class %s {" % (self._enumName))
		for member in self._members:
			member.toEnumMember();
		print("};")

class EnumMemberDecl:
	def __init__(self, xml, value, desc):
		self._xml = xml
		self._value = value
		self._desc = desc

	def toEnumMember(self):
		print("  %s, // %s: %s" %(self._value, self._xml, self._desc))

class EnumMemberDeclParser:
	def __init__(self):
		pass

	def parse(self, data):
		tokens = data.split(" ")
		xml = tokens[0]
		value = tokens[1]
		desc = " ".join(tokens[2:])
		return EnumMemberDecl(xml, value, desc)

class EnumDeclParser:
	def __init__(self):
		pass

	def parse(self, data):
		tokens = data.split(" ")
		enumName = tokens[0]
		return EnumDecl(enumName), EnumMemberDeclParser()



def nextStatement():
	with open("ipc2581b30.md") as input:
		for line in input:
			yield line.strip()


classDeclParser = ClassDeclParser()
classDeclList = []
enumDeclParser = EnumDeclParser()
enumDeclList = []
currentDecl = None
currentParser = None
for statement in nextStatement():
	tokens = statement.split(" ")
	if tokens[0] == '#':
		if tokens[1] == "Class":
			currentDecl, currentParser = classDeclParser.parse(" ".join(tokens[2:]))
			classDeclList.append(currentDecl)
		elif tokens[1] == "Enum":
			currentDecl, currentParser = enumDeclParser.parse(" ".join(tokens[2:]))
			enumDeclList.append(currentDecl)
		else:
			raise
	elif statement.startswith('-'):
		member = currentParser.parse(statement[1:].strip())
		currentDecl._members.append(member)
	elif len(statement) != 0:
		sys.stderr.write('Warning: ignoring garbage (' + statement + ')\n')

def genPreamble():
	print("""
#pragma once

#include <QString>
#include <QScopedPointer>

template<class T>
struct Optional
{
	Optional(const T &value):
	    value(value)
	{}
	bool hasValue() const;
	T value;
};

""")

def genEnumDecl(tab, enumDecl):
	enumDecl.toCPlusPlus()

def genClassForwardDecl(tab, classDecl):
	classDecl.toCPlusPlusForward()

def genClassDecl(tab, classDecl):
	classDecl.toCPlusPlus()

def genParseAttributes(tab, classDecl):
	print(tab + "/* Attributes */")
	print()
	for attrDecl in classDecl.attributes():
		if attrDecl._multiplicity == "?":
			print(tab + "if (reader->attributes.hasAttribute(\"%s\"))" % attrDecl._memberName)
			print(tab + "  object->%sOptional = Optional<%s>(parse%s(reader->attributes.value(\"%s\")));" % (attrDecl._memberName, attrDecl._typeName, attrDecl._typeName, attrDecl._memberName))
		else:
			print(tab + "object->%s = parse%s(reader->attributes.value(\"%s\"));" % (attrDecl._memberName, attrDecl._typeName, attrDecl._memberName))

def genParseElements(tab, classDecl):
	print(tab + "/* Elements */")
	print()
	print(tab + "while (reader->readNextStartElement()) {")
	first = True
	for elDecl  in classDecl.elements():
		print(tab + "  %sif (reader->name() == \"%s\")" % ("" if first else "else " , elDecl._memberName))
		if elDecl._multiplicity == "*" or elDecl._multiplicity == "+":
			print(tab + "    object->%sList.append(parse%s());" % (elDecl._memberName, elDecl._typeName))
		else:
			print(tab + "    object->%s = parse%s();" % (elDecl._memberName, elDecl._typeName))
		first = False
	print(tab + "  %sreader->skipCurrentElement();" % ("" if first else "else "))
	print(tab + "}")

def genClassDecl(tab, classDecl):
	print(tab + "class %sParser" % (classDecl._className))
	print(tab + "{")
	print(tab + "public:")
	print(tab + "  %sParser::%sParser();" % (classDecl._className, classDecl._className))
	print()
	print(tab + "  %s %sParser::parse();" % (classDecl._className, classDecl._className))
	print(tab + "};")
	print()

def genClassImpl(tab, classDecl):
	print(tab + "%sParser::%sParser()" % (classDecl._className, classDecl._className))
	print(tab + "{}")
	print()
	print(tab + "%s %sParser::parse()" % (classDecl._className, classDecl._className))
	print(tab + "{")
	print(tab + "  /* Pre */")
	print()
	print(tab + "  QScopedPointer<%s> object = new %s();" % (classDecl._className, classDecl._className))
	print()
	genParseAttributes(tab + "  ", classDecl)
	print()
	genParseElements(tab + "  ", classDecl)
	print()
	print(tab + "  /* Post */\n")
	print(tab + "  return object.take();")
	print(tab + "}")


genPreamble()

for classDecl in classDeclList:
	genClassForwardDecl("", classDecl)

for enumDecl in enumDeclList:
	genEnumDecl("", enumDecl)

for classDecl in classDeclList:
	genClassDecl("", classDecl)

for classDecl in classDeclList:
	genClassImpl("", classDecl)

