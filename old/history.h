#pragma once

#include <QString>
#include <QList>

enum class CertificationStatus
{
    Alpha = 1,
    Beta = 2,
    Certified = 3,
    SelfTest = 4
};

enum class CertificationCategory
{
    AssemblyDrawing = 1,
    AssemblyFixtureGeneration = 2,
    AssemblyPanel = 3,
    AssemblyPrepTools = 4,
    AssemblyTestFixtureGeneration = 5,
    AssemblyTestGeneration = 6,
    BoardFabrication = 7,
    BoardFixtureGeneration = 8,
    BoardPanel = 9,
    BoardTestGeneration = 10,
    ComponentPlacement = 11,
    DetailedDrawing = 12,
    FabricationDrawing = 13,
    GeneralAssembly = 14,
    GlueDot = 15,
    MechanicalHardware = 16,
    MultiBoardPartList = 17,
    PhotoTools = 18,
    SchematicDrawings = 19,
    SingleBoardPartList = 20,
    SolderPencilPaste = 21,
    SpecSourceControlDrawing = 22,
    EmbeddedComponent = 23,
    Other = 24
};

struct Certification
{
    CertificationStatus status;
    CertificationCategory category;
};

struct SoftwarePackage
{
    QString name;
    QString vendor;
    QString revision;
    QString model;
    QList<Certification> certificationList;
};

struct FileRevision
{
    QString revisionId;
    QString comment;
    QString label;
    SoftwarePackage softwarePackage;
};

struct Approval
{
    QString dateTime;
    QString personRef;
};

struct ChangeRec
{
    QString dateTime;
    QString personRef;
    QString application;
    QString change;
    QList<Approval> approvalList;
};

struct HistoryRecord
{
    QString number;
    QString origination;
    QString software;
    QString lastChange;
    QString externalConfigurationEntryPoint;
    FileRevision fileRevision;
    QList<ChangeRec> changeRecList;
};
