#include "ipcreader.h"

#include <QXmlStreamReader>
#include <QDebug>

IpcReader::IpcReader(QObject *parent) :
    QObject(parent)
{

}

bool IpcReader::read(QIODevice *device)
{
    m_document = new Document();

    m_xml.setDevice(device);

    if (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "IPC-2581" && m_xml.attributes().value("revision") == "B")
        {
            readIPC();
        }
        else
        {
            m_xml.raiseError(QObject::tr("The file is not an IPC-2581 revision B file."));
        }
    }

    return !m_xml.error();
}

QString IpcReader::errorString() const
{
    return tr("%1\nLine %2, column %3")
            .arg(m_xml.errorString())
            .arg(m_xml.lineNumber())
            .arg(m_xml.columnNumber());
}

Document *IpcReader::document() const
{
    return m_document;
}

void IpcReader::readIPC()
{
    /* Attributes */

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Content")
        {
            readContent();
        }
        // FIXME: LogisticHeader, HistoryRecord, Bom and Ecad
        else
        {
            skipCurrentElement(__FUNCTION__);
        }
    }
}

void IpcReader::readContent()
{
    /* Attributes */

    // FIXME: roleRef

    /* Elements */
    while (readNextStartElement(__FUNCTION__))
    {
        auto name = m_xml.name();
        // FIXME: FunctionMode, StepRef, LAyerRef, BomRef, AvlRef
        if (m_xml.name() == "DictionaryLineDesc")
        {
            readDictionaryLineDesc();
        }
        else if (m_xml.name() == "DictionaryFillDesc")
        {
            readDictionaryFillDesc();
        }
        else if (m_xml.name() == "DictionaryColor")
        {
            readDictionaryColor();
        }
        else if (m_xml.name() == "DictionaryStandard")
        {
            readDictionaryStandard();
        }
        else if (m_xml.name() == "DictionaryUser")
        {
            readDictionaryUser();
        }
        // FIXME: DictionaryFont, DictionaryFirmware
        else
        {
            skipCurrentElement(__FUNCTION__);
        }
    }
}

void IpcReader::readDictionaryLineDesc()
{
    /* Attributes */

    // TODO: Read units attribute

    /* Elements */
    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "EntryLineDesc")
            readEntryLineDesc();
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readEntryLineDesc()
{
    /* Attributes */

    if (!fetchRequiredAttribute("id"))
        return;
    if (!parseIdAttributeValue())
        return;
    const QString id = idAttributeValue();
    if (m_document->content.dictionaryLineDesc.contains(id))
    {
        m_xml.raiseError("Duplicated ID");
        return;
    }

    /* Elements */
    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "LineDesc")
        {
            readLineDesc();
            m_document->content.dictionaryLineDesc.insert(id, m_lineDesc);
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readLineDesc()
{
    static const QMap<QString, LineEnd> endMap =
    {
        {"NONE", LineEnd::None},
        {"ROUND", LineEnd::Round},
        {"SQUARE", LineEnd::Square}
    };
    static const QMap<QString, LineProperty> propMap =
    {
        {"SOLID", LineProperty::Solid},
        {"DOTTED", LineProperty::Dotted},
        {"DASHED", LineProperty::Dashed},
        {"CENTER", LineProperty::Center},
        {"PHANTOM", LineProperty::Phantom},
        {"ERASE", LineProperty::Erase},
    };

    m_lineDesc = new LineDesc;

    /* Attributes */

    if (!fetchRequiredAttribute("lineEnd"))
        return;
    if (!validateEnumAttributeValue<LineEnd>(endMap))
        return;
    m_lineDesc->end = enumAttributeValue<LineEnd>(endMap);

    if (!fetchRequiredAttribute("lineWidth"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    m_lineDesc->width = nonNegativeDoubleAttributeValue();

    fetchOptionalAttribute("lineProperty", "SOLID");
    if (!validateEnumAttributeValue<LineProperty>(propMap))
        return;
    m_lineDesc->property = enumAttributeValue<LineProperty>(propMap);

    /* Elements */
    while (readNextStartElement(__FUNCTION__))
    {
        skipCurrentElement(__FUNCTION__);
    }
}

bool IpcReader::isLineDescGroup(const QStringRef &name)
{
    return name =="LineDesc" || name == "LineDescRef";
}

void IpcReader::readLineDescGroup()
{
    if (m_xml.name() == "LineDesc")
        readLineDesc();
    else if (m_xml.name() == "LineDescRef")
        readLineDescRef();
}

void IpcReader::readLineDescRef()
{
    /* Attributes */

    if (!fetchRequiredAttribute("id"))
        return;
    if (!parseIdAttributeValue())
        return;
    auto id = idAttributeValue();
    if (!m_document->content.dictionaryLineDesc.contains(id))
        return;
    m_lineDesc = m_document->content.dictionaryLineDesc.value(id);

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readDictionaryFillDesc()
{
    /* Attributes */

    // TODO: Read units attribute

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        auto name = m_xml.name();
        if (name == "EntryFillDesc")
            readEntryFillDesc();
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readEntryFillDesc()
{
    /* Attributes */

    if (!fetchRequiredAttribute("id"))
        return;
    if (!parseIdAttributeValue())
        return;
    const QString id = idAttributeValue();
    if (m_document->content.dictionaryFillDesc.contains(id))
    {
        m_xml.raiseError("Duplicated ID");
        return;
    }

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        auto name = m_xml.name();
        if (name == "FillDesc")
        {
            readFillDesc();
            m_document->content.dictionaryFillDesc.insert(id, m_fillDesc);
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readFillDesc()
{
    static const QMap<QString, FillProperty> propMap =
    {
        {"HOLLOW", FillProperty::Hollow},
        {"HATCH", FillProperty::Hatch},
        {"MESH", FillProperty::Mesh},
        {"FILL", FillProperty::Fill},
        {"VOID", FillProperty::Void},
    };

    /* Attributes */

    auto m_fillDesc = new FillDesc;

    fetchOptionalAttribute("fillProperty", "HOLLOW");
    if (!validateEnumAttributeValue<FillProperty>(propMap))
        return;
    m_fillDesc->property = enumAttributeValue<FillProperty>(propMap);

    if (m_fillDesc->property == FillProperty::Hatch || m_fillDesc->property == FillProperty::Mesh)
    {
        if (!fetchRequiredAttribute("lineWidth"))
            return;
        if (!parseNonNegativeDoubleAttributeValue())
            return;
        m_fillDesc->lineWidth = nonNegativeDoubleAttributeValue();

        fetchOptionalAttribute("pitch1", QString::number(4*m_fillDesc->lineWidth));
        if (!parseNonNegativeDoubleAttributeValue())
            return;
        m_fillDesc->pitch1 = nonNegativeDoubleAttributeValue();

        fetchOptionalAttribute("angle1", "45.0");
        if (!parseAngleAttributeValue(0.0, 180.0))
            return;
        m_fillDesc->angle1 = angleAttributeValue();

        if (m_fillDesc->property == FillProperty::Mesh)
        {
            fetchOptionalAttribute("pitch2", QString::number(4*m_fillDesc->lineWidth));
            if (!parseNonNegativeDoubleAttributeValue())
                return;
            m_fillDesc->pitch2 = nonNegativeDoubleAttributeValue();

            fetchOptionalAttribute("angle2", "135.0");
            if (!parseAngleAttributeValue(90.0, 180.0))
                return;
            m_fillDesc->angle2 = angleAttributeValue();
        }
    }

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (isColorGroup(m_xml.name()))
        {
            readColorGroup();
            m_fillDesc->color = *m_color;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

bool IpcReader::isFillDescGroup(const QStringRef &name)
{
    return name == "FillDesc" || name == "FillDescRef";
}

void IpcReader::readFillDescGroup()
{
    if (m_xml.name() == "FillDesc")
        readFillDesc();
    else if (m_xml.name() == "FillDescRef")
        readFillDescRef();
}

void IpcReader::readFillDescRef()
{
    /* Attributes */

    if (!fetchRequiredAttribute("id"))
        return;
    if (!parseIdAttributeValue())
        return;
    auto id = idAttributeValue();
    if (!m_document->content.dictionaryFillDesc.contains(id))
        return;
    m_fillDesc = m_document->content.dictionaryFillDesc.value(id);

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readDictionaryStandard()
{
    /* Attributes */

    // TODO: Read units attribute

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "EntryStandard")
        {
            readEntryStandard();
        }
        else
        {
            skipCurrentElement(__FUNCTION__);
        }
    }
}

// TODO use a QMap<QString, StandardPrimitive *(*IpcReader::func)(id)
void IpcReader::readEntryStandard()
{
    auto dict = &m_document->content.dictionaryStandard;

    /* Attributes */

    if (!fetchRequiredAttribute("id"))
        return;
    if (!parseIdAttributeValue())
        return;
    const QString id = idAttributeValue();
    if (dict->contains(id))
    {
        m_xml.raiseError("Duplicated ID");
        return;
    }

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        auto name = m_xml.name();
        if (m_xml.name() == "Butterfly")
        {
            readButterfly();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "Circle")
        {
            readCircle();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "Contour")
        {
            readContour();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "Diamond")
        {
            readDiamond();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "Donut")
        {
            readDonut();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "Ellipse")
        {
            readEllipse();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "Hexagon")
        {
            readHexagon();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "Moire")
        {
            readMoire();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "Octagon")
        {
            readOctagon();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "Oval")
        {
            readOval();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "RectCenter")
        {
            readRectCenter();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "RectCham")
        {
            readRectCham();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "RectCorner")
        {
            readRectCorner();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "RectRound")
        {
            readRectRound();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "Thermal")
        {
            readThermal();
            dict->insert(id, m_standardPrimitive);
        }
        else if (m_xml.name() == "Triangle")
        {
            readTriangle();
            dict->insert(id, m_standardPrimitive);
        }
        else
        {
            skipCurrentElement(__FUNCTION__);
        }
    }
}

void IpcReader::readButterfly()
{
    static const QMap<QString, ButterflyShape> shapeMap =
    {
        {"ROUND", ButterflyShape::Round},
        {"SQUARE", ButterflyShape::Square}
    };

    auto primitive = new Butterfly();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("shape"))
        return;
    if (!validateEnumAttributeValue<ButterflyShape>(shapeMap))
        return;
    primitive->shape = enumAttributeValue<ButterflyShape>(shapeMap);

    // TODO: Either Diameter or Side
    fetchOptionalAttribute("diameter", "0.0");
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->diameter = nonNegativeDoubleAttributeValue();

    fetchOptionalAttribute("side", "0.0");
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->side = nonNegativeDoubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readCircle()
{
    auto primitive = new Circle();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("diameter"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->diameter = nonNegativeDoubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readContour()
{
    /* pre */

    auto primitive = new Contour();
    m_standardPrimitive = primitive;

    /* Attributes */

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Polygon")
        {
            readPolygon();
            primitive->polygon = m_polygon;
        }
        else if (m_xml.name() == "Cutout")
        {
            readPolygon();
            primitive->cutout = m_polygon;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readDiamond()
{
    /* pre */

    auto primitive = new Diamond();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("width"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->width = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("height"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->height = nonNegativeDoubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readDonut()
{
    static const QMap<QString, DonutShape> shapeMap =
    {
        {"ROUND", DonutShape::Round},
        {"SQUARE", DonutShape::Square},
        {"HEXAGON", DonutShape::Hexagon},
        {"OCTOGON", DonutShape::Octogon}
    };

    auto primitive = new Donut();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("shape"))
        return;
    if (!validateEnumAttributeValue<DonutShape>(shapeMap))
        return;
    primitive->shape = enumAttributeValue<DonutShape>(shapeMap);

    if (!fetchRequiredAttribute("outerDiameter"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->outerDiameter = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("innerDiameter"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->innerDiameter = nonNegativeDoubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readEllipse()
{
    auto primitive = new Ellipse();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("width"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->width = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("height"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->height = nonNegativeDoubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readHexagon()
{
    auto primitive = new Hexagon();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("length"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->length = nonNegativeDoubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readMoire()
{
    auto primitive = new Moire();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("diameter"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->diameter = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("ringWidth"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->ringWidth = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("ringGap"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->ringGap = nonNegativeDoubleAttributeValue(); // NonNegativeDouble

    if (!fetchRequiredAttribute("ringNumber"))
        return;
    if (!parseNonNegativeIntegerAttributeValue())
        return;
    primitive->ringNumber = nonNegativeIntegerAttributeValue();

    fetchOptionalAttribute("lineWidth", "0.0");
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->lineWidth = nonNegativeDoubleAttributeValue();

    fetchOptionalAttribute("lineLength", QString::number(primitive->diameter));
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->lineLength = nonNegativeDoubleAttributeValue();

    fetchOptionalAttribute("lineAngle", "0.0");
    if (!parseAngleAttributeValue(0.0, 90.0))
        return;
    primitive->lineAngle = angleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readOctagon()
{
    auto primitive = new Octagon();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("length"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->length = nonNegativeDoubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readOval()
{
    auto primitive = new Oval();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("width"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->width = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("height"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->height = nonNegativeDoubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readRectCenter()
{
    auto primitive = new RectCenter();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("width"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->width = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("height"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->height = nonNegativeDoubleAttributeValue();

    /* Elements */
    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readRectCham()
{
    auto primitive = new RectCham();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("width"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->width = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("height"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->height = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("chamfer"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->chamfer = nonNegativeDoubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readRectCorner()
{
    auto primitive = new RectCorner();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("lowerLeftX"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    primitive->lowerLeftX = doubleAttributeValue();
    if (!fetchRequiredAttribute("lowerLeftY"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    primitive->lowerLeftY = doubleAttributeValue();
    if (!fetchRequiredAttribute("upperRightX"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    primitive->upperRightX = doubleAttributeValue();
    if (!fetchRequiredAttribute("upperRightY"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    primitive->upperRightY = doubleAttributeValue();


    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readRectRound()
{
    auto primitive = new RectRound();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("width"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->width = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("height"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->height = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("radius"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->radius = nonNegativeDoubleAttributeValue();


    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readThermal()
{
    static const QMap<QString, ThermalShape> shapeMap =
    {
        {"ROUND", ThermalShape::Round},
        {"SQUARE", ThermalShape::Square},
        {"HEXAGON", ThermalShape::Hexagon},
        {"OCTOGON", ThermalShape::Octogon}
    };

    auto primitive = new Thermal();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("shape"))
        return;
    if (!validateEnumAttributeValue<ThermalShape>(shapeMap))
        return;
    primitive->shape = enumAttributeValue<ThermalShape>(shapeMap);

    if (!fetchRequiredAttribute("outerDiameter"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->outerDiameter = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("innerDiameter"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->innerDiameter = nonNegativeDoubleAttributeValue();

    QString defaultSpokeCount;
    int spokeCountEnd;
    switch (primitive->shape)
    {
        case ThermalShape::Round:
        case ThermalShape::Square:
        case ThermalShape::Octogon:
            defaultSpokeCount = "4";
            spokeCountEnd = 5;
            break;
        case ThermalShape::Hexagon:
            defaultSpokeCount = "3";
            spokeCountEnd = 4;
            break;
    }
    if (!parseIntegerAttributeValue(0, spokeCountEnd))
        return;
    if (integerAttributeValue() == 1)
    {
        m_xml.raiseError("Invalid value"); // FIXME
        return;
    }
    if (primitive->shape == ThermalShape::Square &&
            integerAttributeValue() == 3)
    {
        m_xml.raiseError("Invalid value"); // FIXME
        return;
    }
    primitive->spokeCount = integerAttributeValue();

    if (primitive->spokeCount != 0)
    {
        const QString defaultGap = QString::number(primitive->outerDiameter -
                                                   primitive->innerDiameter);
        fetchOptionalAttribute("gap", defaultGap);
        if (!parseNonNegativeDoubleAttributeValue())
            return;
        primitive->gap = nonNegativeDoubleAttributeValue();
        fetchOptionalAttribute("spokeStartAngle", "45.0");
        if (!parseAngleAttributeValue())
            return;
        primitive->spokeStartAngle = angleAttributeValue();
    }

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readTriangle()
{
    auto primitive = new Triangle();
    m_standardPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("base"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->base = nonNegativeDoubleAttributeValue();

    if (!fetchRequiredAttribute("height"))
        return;
    if (!parseNonNegativeDoubleAttributeValue())
        return;
    primitive->height = nonNegativeDoubleAttributeValue();


    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Xform")
        {
            readXForm();
            primitive->xForm = m_xForm;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            primitive->fillDesc = *m_fillDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readDictionaryUser()
{
    /* Attributes */

    // TODO: Read units attribute

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "EntryUser")
        {
            readEntryUser();
        }
        else
        {
            skipCurrentElement(__FUNCTION__);
        }
    }
}

void IpcReader::readEntryUser()
{
    /* pre */

    auto dict = &m_document->content.dictionaryUser;

    /* Attributes */

    if (!fetchRequiredAttribute("id"))
        return;
    if (!parseIdAttributeValue())
        return;
    const QString id = idAttributeValue();
    if (dict->contains(id))
    {
        m_xml.raiseError("Duplicated ID");
        return;
    }

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        auto name = m_xml.name();
        if (name == "Arc")
        {
            readArc();
            dict->insert(id, m_userPrimitive);
        }
        else if (name == "Line")
        {
            readLine();
            dict->insert(id, m_userPrimitive);
        }
        else if (name == "Outline")
        {
            readOutline();
            dict->insert(id, m_userPrimitive);
        }
        else if (name == "Polyline")
        {
            readPolyline();
            dict->insert(id, m_userPrimitive);
        }
        else if (name == "Text")
        {
            readText();
            dict->insert(id, m_userPrimitive);
        }
        else if (name == "UserSpecial")
        {
            readUserSpecial();
            dict->insert(id, m_userPrimitive);
        }
        else
            m_xml.skipCurrentElement();
    }

    /* post */
}

void IpcReader::readArc()
{
    /* pre */
    auto primitive = new Arc();
    m_userPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("startX"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    primitive->startX = doubleAttributeValue();

    if (!fetchRequiredAttribute("startY"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    primitive->startY = doubleAttributeValue();

    if (!fetchRequiredAttribute("endX"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    primitive->endX = doubleAttributeValue();

    if (!fetchRequiredAttribute("endY"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    primitive->endY = doubleAttributeValue();

    fetchOptionalAttribute("clockwise", "false");
    if (!parseBooleanAttributeValue())
        return;
    primitive->clockwise = booleanAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }

    /* post */

}

void IpcReader::readLine()
{
    /* pre */
    auto primitive = new Line();
    m_userPrimitive = primitive;

    /* Attributes */

    if (!fetchRequiredAttribute("startX"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    primitive->startX = doubleAttributeValue();

    if (!fetchRequiredAttribute("startY"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    primitive->startY = doubleAttributeValue();

    if (!fetchRequiredAttribute("endX"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    primitive->endX = doubleAttributeValue();

    if (!fetchRequiredAttribute("endY"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    primitive->endY = doubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }

    /* post */

}

void IpcReader::readOutline()
{
    /* pre */
    auto primitive = new Outline();
    m_userPrimitive = primitive;

    /* Attributes */

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Polygon")
        {
            readPolygon();
            primitive->polygon = m_polygon;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }

    /* post */

}

void IpcReader::readPolyline()
{
    /* pre */
    auto primitive = new Polyline();
    m_userPrimitive = primitive;

    /* Attributes */

    /* Elements */
    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "PolyBegin")
        {
            readPolyBegin<Polyline>(*primitive);
        }
        else if (m_xml.name() == "PolyStepSegment")
        {
            readPolyStepSegment<Polyline>(*primitive);
        }
        else if (m_xml.name() == "PolyStepCurve")
        {
            readPolyStepCurve<Polyline>(*primitive);
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            primitive->lineDesc = *m_lineDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }

    /* post */
}

// FIXME
void IpcReader::readText()
{

    while (readNextStartElement(__FUNCTION__))
    {
        skipCurrentElement(__FUNCTION__);
    }
}

// FIXME
void IpcReader::readUserSpecial()
{
    while (readNextStartElement(__FUNCTION__))
    {
        skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readXForm()
{
    /* pre */

    m_xForm = XForm();

    /* Attributes */

    fetchOptionalAttribute("xOffset", "0.0");
    if (!parseDoubleAttributeValue())
        return;
    m_xForm.xOffset = doubleAttributeValue();

    fetchOptionalAttribute("yOffset", "0.0");
    if (!parseDoubleAttributeValue())
        return;
    m_xForm.yOffset = doubleAttributeValue();

    fetchOptionalAttribute("rotation", "0.0");
    if (!parseAngleAttributeValue())
        return;
    m_xForm.rotation = angleAttributeValue();

    fetchOptionalAttribute("mirror", "false");
    if (!parseBooleanAttributeValue())
        return;
    m_xForm.mirrored = booleanAttributeValue();

    fetchOptionalAttribute("scale", "1.0");
    if (!parsePositiveDoubleAttributeValue())
        return;
    m_xForm.scale = positiveDoubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        skipCurrentElement(__FUNCTION__);
    }

    /* post */
}

void IpcReader::readPolygon()
{
    /* pre */

    m_polygon = Polygon();

    /* Attributes */

    /* Elements */
    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "PolyBegin")
        {
            readPolyBegin<Polygon>(m_polygon);
        }
        else if (m_xml.name() == "PolyStepSegment")
        {
            readPolyStepSegment<Polygon>(m_polygon);
        }
        else if (m_xml.name() == "PolyStepCurve")
        {
            readPolyStepCurve<Polygon>(m_polygon);
        }
        else if (m_xml.name() == "Xform")
        {
            readXForm();
            m_polygon.xForm = m_xForm;
        }
        else if (isFillDescGroup(m_xml.name()))
        {
            readFillDescGroup();
            m_polygon.fillDesc = *m_fillDesc;
        }
        else if (isLineDescGroup(m_xml.name()))
        {
            readLineDescGroup();
            m_polygon.lineDesc = *m_lineDesc;
        }
        else
            skipCurrentElement(__FUNCTION__);
    }

    /* post */
}

template<class T>
void IpcReader::readPolyBegin(T &poly)
{
    /* Attributes */

    if (!fetchRequiredAttribute("x"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    poly.x = doubleAttributeValue();

    if (!fetchRequiredAttribute("y"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    poly.y = doubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        skipCurrentElement(__FUNCTION__);
    }
}

template<class T>
void IpcReader::readPolyStepSegment(T &poly)
{
    /* pre */
    PolyStep step;
    step.type = PolyStep::Segment;

    /* Attributes */

    if (!fetchRequiredAttribute("x"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    step.x = doubleAttributeValue();

    if (!fetchRequiredAttribute("y"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    step.y = doubleAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        skipCurrentElement(__FUNCTION__);
    }

    /* post */

    poly.steps.append(step);
}

template<class T>
void IpcReader::readPolyStepCurve(T &poly)
{
    /* pre */

    PolyStep step;
    step.type = PolyStep::Curve;

    /* Attributes */

    if (!fetchRequiredAttribute("x"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    step.x = doubleAttributeValue();

    if (!fetchRequiredAttribute("y"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    step.y = doubleAttributeValue();

    if (!fetchRequiredAttribute("centerX"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    step.centerX = doubleAttributeValue();

    if (!fetchRequiredAttribute("centerY"))
        return;
    if (!parseDoubleAttributeValue())
        return;
    step.centerY = doubleAttributeValue();

    fetchOptionalAttribute("clockwise", "true");
    if (!parseBooleanAttributeValue())
        return;
    step.clockwise = booleanAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        skipCurrentElement(__FUNCTION__);
    }

    /* Post */
    poly.steps.append(step);
}

void IpcReader::readDictionaryColor()
{
    /* Attributes */

    /* Elements */
    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "EntryColor")
            readEntryColor();
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readEntryColor()
{
    auto dict = &m_document->content.dictionaryColor;

    /* Attributes */

    if (!fetchRequiredAttribute("id"))
        return;
    if (!parseIdAttributeValue())
        return;
    const QString id = idAttributeValue();
    if (dict->contains(id))
    {
        m_xml.raiseError("Duplicated ID");
        return;
    }

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        if (m_xml.name() == "Color")
        {
            readColor();
            dict->insert(id, m_color);
        }
        else
            skipCurrentElement(__FUNCTION__);
    }
}

void IpcReader::readColor()
{
    m_color = new Color();

    /* Attributes */

    if (!fetchRequiredAttribute("r"))
        return;
    if (!parseUnsignedByteAttributeValue())
        return;
    m_color->red = unsignedByteAttributeValue();

    if (!fetchRequiredAttribute("g"))
        return;
    if (!parseUnsignedByteAttributeValue())
        return;
    m_color->green = unsignedByteAttributeValue();

    if (!fetchRequiredAttribute("b"))
        return;
    if (!parseUnsignedByteAttributeValue())
        return;
    m_color->blue = unsignedByteAttributeValue();

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        skipCurrentElement(__FUNCTION__);
    }
}

bool IpcReader::isColorGroup(const QStringRef &name)
{
    return name == "Color" || name == "ColorRef";
}

void IpcReader::readColorGroup()
{
    if (m_xml.name() == "Color")
        readColor();
    else if (m_xml.name() == "ColorRef")
        readColorRef();
}

void IpcReader::readColorRef()
{
    /* Attributes */

    if (!fetchRequiredAttribute("id"))
        return;
    if (!parseIdAttributeValue())
        return;
    auto id = idAttributeValue();
    if (!m_document->content.dictionaryColor.contains(id))
        return;

    /* Elements */

    while (readNextStartElement(__FUNCTION__))
    {
        skipCurrentElement(__FUNCTION__);
    }

    /* Post */

    m_color = m_document->content.dictionaryColor.value(id);
}



bool IpcReader::fetchRequiredAttribute(const QString &name)
{
    m_attributeName = name;
    if (m_xml.attributes().hasAttribute(name))
    {
        m_attributeValue = m_xml.attributes().value(name).toString();
        return true;
    }
    m_xml.raiseError(QString("Missing '%1' attribute").arg(name));
    return false;
}

void IpcReader::fetchOptionalAttribute(const QString &name, const QString &defaultValue)
{
    m_attributeName = name;
    if (m_xml.attributes().hasAttribute(name))
    {
        m_attributeValue = m_xml.attributes().value(name).toString();
        return;
    }
    m_attributeValue = defaultValue;
}

bool IpcReader::parseIdAttributeValue()
{
    return !m_attributeValue.isEmpty();
}

QString IpcReader::idAttributeValue()
{
    return m_attributeValue;
}

bool IpcReader::parseNonNegativeDoubleAttributeValue()
{
    bool ok;
    m_doubleValue = m_attributeValue.toDouble(&ok);
    return ok && !(m_doubleValue < 0);
}

qreal IpcReader::nonNegativeDoubleAttributeValue()
{
    return m_doubleValue;
}

bool IpcReader::parsePositiveDoubleAttributeValue()
{
    bool ok;
    m_doubleValue = m_attributeValue.toDouble(&ok);
    return ok && (m_doubleValue > 0);
}

qreal IpcReader::positiveDoubleAttributeValue()
{
    return m_doubleValue;
}

bool IpcReader::parseAngleAttributeValue(qreal begin, qreal end)
{
    bool ok;
    m_doubleValue = m_attributeValue.toDouble(&ok);
    return ok && (m_doubleValue >= begin) && (m_doubleValue < end);
}

qreal IpcReader::angleAttributeValue()
{
    return m_doubleValue;
}

bool IpcReader::parseNonNegativeIntegerAttributeValue()
{
    bool ok;
    m_intValue = m_attributeValue.toInt(&ok);
    return ok && (m_intValue >= 0);
}

int IpcReader::nonNegativeIntegerAttributeValue()
{
    return m_intValue;
}

bool IpcReader::parseUnsignedByteAttributeValue()
{
    bool ok;
    m_intValue = m_attributeValue.toInt(&ok);
    return ok && m_intValue >= 0 && m_intValue <= 255;
}

quint8 IpcReader::unsignedByteAttributeValue()
{
    return static_cast<quint8>(m_intValue);
}

bool IpcReader::parseIntegerAttributeValue(int begin, int end)
{
    bool ok;
    m_intValue = m_attributeValue.toInt(&ok);
    return ok && m_intValue >= begin && m_intValue < end;
}

int IpcReader::integerAttributeValue()
{
    return  m_intValue;
}

bool IpcReader::parseDoubleAttributeValue()
{
    bool ok;
    m_doubleValue = m_attributeValue.toDouble(&ok);
    return ok;
}

double IpcReader::doubleAttributeValue()
{
    return m_doubleValue;
}

bool IpcReader::parseBooleanAttributeValue()
{
    if (m_attributeValue.toLower() == "true")
        m_boolean = true;
    else if (m_attributeValue.toLower() == "false")
        m_boolean = false;
    else
        return false;
    return true;
}

bool IpcReader::booleanAttributeValue()
{
    return m_boolean;
}

bool IpcReader::readNextStartElement(const char *prefix)
{
    bool ok = m_xml.readNextStartElement();
//    if (ok)
//        qDebug() << prefix << "Next" << m_xml.name();
//    else
//        qDebug() << prefix << "Next" << "<none>";
    return ok;
}

void IpcReader::skipCurrentElement(const char *prefix)
{
    qDebug() << prefix << "Skipping" << m_xml.name();
    m_xml.skipCurrentElement();
}
