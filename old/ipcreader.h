#ifndef IPCREADER_H
#define IPCREADER_H

#include "document.h"

#include <QObject>
#include <QString>
#include <QXmlStreamReader>

class QIODevice;

/*
 * FooBarReader();
 * bool read(const QString &);
 * bool read(QIODevice *);
 * bool read(QXmlStreamReader *)
 * QString errorString();
 * FooBar *fooBar();
 */
class IpcReader : public QObject
{
    Q_OBJECT

public:
    explicit IpcReader(QObject *parent = 0);

    bool read(QIODevice *device);
    QString errorString() const;
    Document *document() const;

protected:
    void readIPC();
    void readContent();

    void readDictionaryColor();
    void readEntryColor();
    Color *m_color;
    void readColor();

    bool isColorGroup(const QStringRef &name);
    void readColorGroup();
    void readColorRef();

    void readDictionaryLineDesc();
    void readEntryLineDesc();
    LineDesc *m_lineDesc;
    void readLineDesc();

    bool isLineDescGroup(const QStringRef &name);
    void readLineDescGroup();
    void readLineDescRef();

    void readDictionaryFillDesc();
    void readEntryFillDesc();
    FillDesc *m_fillDesc;
    void readFillDesc();

    bool isFillDescGroup(const QStringRef &name);
    void readFillDescGroup();
    void readFillDescRef();

    void readDictionaryStandard();
    void readEntryStandard();
    StandardPrimitive *m_standardPrimitive;
    void readButterfly();
    void readCircle();
    void readContour();
    void readDiamond();
    void readDonut();
    void readEllipse();
    void readHexagon();
    void readMoire();
    void readOctagon();
    void readOval();
    void readRectCenter();
    void readRectCham();
    void readRectCorner();
    void readRectRound();
    void readThermal();
    void readTriangle();

    void readDictionaryUser();
    void readEntryUser();
    UserPrimitive *m_userPrimitive;
    void readArc();
    void readLine();
    void readOutline();
    void readPolyline();
    void readText();
    void readUserSpecial();

    XForm m_xForm;
    void readXForm();

    Polygon m_polygon;
    void readPolygon();
    template<class T>
    void readPolyBegin(T &poly);
    template<class T>
    void readPolyStepSegment(T &poly);
    template<class T>
    void readPolyStepCurve(T &poly);


    bool fetchRequiredAttribute(const QString &name);
    void fetchOptionalAttribute(const QString &name, const QString &defaultValue);
    QString m_attributeName;
    QString m_attributeValue;

    bool parseIdAttributeValue();
    QString idAttributeValue();

    template<class T>
    bool validateEnumAttributeValue(const QMap<QString, T> &map)
    {
        if (!map.contains(m_attributeValue))
        {
            m_xml.raiseError(QString("'%1': Invalid value for '%2'")
                             .arg(m_attributeValue).arg(m_attributeName));
            return false;
        }
        return true;
    }
    template<class T>
    T enumAttributeValue(const QMap<QString, T> &map)
    {
        return map.value(m_attributeValue);
    }

    qreal m_doubleValue;
    bool parseNonNegativeDoubleAttributeValue();
    qreal nonNegativeDoubleAttributeValue();
    bool parsePositiveDoubleAttributeValue();
    qreal positiveDoubleAttributeValue();
    bool parseAngleAttributeValue(qreal begin = 0.0, qreal end = 360.0);
    qreal angleAttributeValue();
    int m_intValue;
    bool parseNonNegativeIntegerAttributeValue();
    int nonNegativeIntegerAttributeValue();
    bool parseUnsignedByteAttributeValue();
    quint8 unsignedByteAttributeValue();
    bool parseIntegerAttributeValue(int begin, int end);
    int integerAttributeValue();
    bool parseDoubleAttributeValue();
    double doubleAttributeValue();
    bool m_boolean;
    bool parseBooleanAttributeValue();
    bool booleanAttributeValue();

    bool readNextStartElement(const char *prefix);
    void skipCurrentElement(const char *prefix);
    void raiseError(const char *prefix, const QString &msg);

private:
    QXmlStreamReader m_xml;
    Document *m_document;
};

#endif // IPCREADER_H
