#pragma once

#include <QString>
#include <QList>

enum class RoleFunction
{
    Sender = 1,
    Owner = 2,
    Receiver = 3,
    Designer = 4,
    Enginner = 5,
    Byer = 6,
    CustomerService = 7,
    DeliverTo = 8,
    BillTo = 9,
    Other = 10
};

enum class EnterpriseCodeType
{
    DUNS = 1,
    CAGE = 2
};

struct Role
{
    QString id;
    RoleFunction function;
    QString description;
    QByteArray publicKey;
    QString authority;
};

struct Enterprise
{
    QString id;
    QString name;
    QString code;
    EnterpriseCodeType codeType;
    QString address1;
    QString address2;
    QString city;
    QString stateOrProvince;
    QString country;
    QString postalCode;
    QString phone;
    QString fax;
    QString email;
    QString url;
};

struct Person
{
    QString name;
    QString enterpriseRef;
    QString title;
    QString email;
    QString phone;
    QString fax;
};

struct LogisiticHeader
{
    QList<Role> roleList;
    QList<Enterprise> enterpriseList;
    QList<Person> personList;
};
