#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "ipcreader.h"

class StackupModel;
class QGraphicsScene;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setDocument(Document *doc);

private:
    Ui::MainWindow *m_ui;
    Document *m_doc;
    StackupModel *m_stackupModel;
    QGraphicsScene *m_stackupScene;

    // QWidget interface
    void setupStackupModel();

    void setupStackupTreeView();

    void setupStackupGraphicsScene();

    void setupStackupGraphicsView();

protected:
    virtual void resizeEvent(QResizeEvent *event) override;
};

#endif // MAINWINDOW_H
