#pragma once

#include <QList>

enum class LineEnd
{
    //Undefined = 0,
    Round = 1,
    Square = 2,
    None = 3
};

enum class LineProperty
{
    //Undefined = 0,
    Solid = 1,
    Dotted = 2,
    Dashed = 3,
    Center = 4,
    Phantom = 5,
    Erase = 6
};

enum class FillProperty
{
    //Undefined = 0,
    Hollow = 1,
    Fill = 2,
    Mesh = 3,
    Hatch = 4,
    Void = 5
};

struct Color
{
    int red = 0;
    int green = 0;
    int blue = 0;
};

struct LineDesc
{
    LineEnd end = LineEnd::Round;
    double width = 0.0;
    LineProperty property = LineProperty::Solid;
};

struct FillDesc
{
    FillProperty property = FillProperty::Fill;
    double lineWidth = 0.0;
    double pitch1 = 0.0;
    double pitch2 = 0.0;
    double angle1 = 0.0;
    double angle2 = 0.0;
    Color color;
};

struct PolyStep
{
    enum
    {
        Segment = 0,
        Curve = 1
    };
    int type = Segment;
    double x = 0.0;
    double y = 0.0;
    double centerX = 0.0;
    double centerY = 0.0;
    bool clockwise = true;
};

struct XForm
{
    double xOffset = 0.0;
    double yOffset = 0.0;
    double rotation = 0.0;
    bool mirrored = false;
    double scale = 1.0;
};

struct Polygon
{
    double x;
    double y;
    QList<PolyStep> steps;
    XForm xForm;
    FillDesc fillDesc;
    LineDesc lineDesc;
};

enum class ButterflyShape
{
    //Undefined = 0,
    Round = 1,
    Square = 2
};

enum class DonutShape
{
    //Undefined = 0,
    Round = 1,
    Square = 2,
    Hexagon = 3,
    Octogon = 4
};

enum class ThermalShape
{
    //Undefined = 0,
    Round = 1,
    Square = 2,
    Hexagon = 3,
    Octogon = 4
};

struct Feature
{

};

struct StandardShape: public Feature
{

};

struct UserShape: public Feature
{

};

struct StandardPrimitive: public StandardShape
{

};

struct UserPrimitive: public UserShape
{

};

struct Butterfly: public StandardPrimitive
{
    ButterflyShape shape = ButterflyShape::Round;
    double diameter = 0.0;
    double side = 0.0;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct Circle: public StandardPrimitive
{
    double diameter = 0.0;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct Contour: public StandardPrimitive
{
    Polygon polygon;
    Polygon cutout;
};

struct Diamond: public StandardPrimitive
{
    double width = 0.0;
    double height = 0.0;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct Donut: public StandardPrimitive
{
    DonutShape shape = DonutShape::Round;
    double outerDiameter = 0.0;
    double innerDiameter = 0.0;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct Ellipse: public StandardPrimitive
{
    double width = 0.0;
    double height = 0.0;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct Hexagon: public StandardPrimitive
{
    double length = 0.0;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct Moire: public StandardPrimitive
{
    double diameter = 0.0;
    double ringWidth = 0.0;
    double ringGap = 0.0;
    int ringNumber = 0;
    double lineWidth = 0;
    double lineLength = 0;
    double lineAngle = 0;
    XForm xForm;
    // No Line/Fill desc
};

struct Octagon: public StandardPrimitive
{
    double length = 0.0;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct Oval: public StandardPrimitive
{
    double width = 0.0;
    double height = 0.0;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct RectCenter: public StandardPrimitive
{
    double width = 0.0;
    double height = 0.0;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct RectCham: public StandardPrimitive
{
    double width = 0.0;
    double height = 0.0;
    double chamfer = 0.0;
    bool upperRight = false;
    bool upperLeft = false;
    bool lowerRight = false;
    bool lowerLeft = false;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct RectCorner: public StandardPrimitive
{
    double lowerLeftX = 0.0;
    double lowerLeftY = 0.0;
    double upperRightX = 0.0;
    double upperRightY = 0.0;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct RectRound: public StandardPrimitive
{
    double width = 0.0;
    double height = 0.0;
    double radius = 0.0;
    bool upperRight = false;
    bool upperLeft = false;
    bool lowerRight = false;
    bool lowerLeft = false;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct Thermal: public StandardPrimitive
{
    ThermalShape shape = ThermalShape::Round;
    double outerDiameter = 0.0;
    double innerDiameter = 0.0;
    int spokeCount = 0;
    double gap = 0.0;
    double spokeStartAngle = 0;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct Triangle: public StandardPrimitive
{
    double base = 0.0;
    double height = 0.0;
    XForm xForm;
    LineDesc lineDesc;
    FillDesc fillDesc;
};

struct Arc: public UserPrimitive
{
    double startX = 0.0;
    double startY = 0.0;
    double endX = 0.0;
    double endY = 0.0;
    double centerX = 0.0;
    double centerY = 0.0;
    bool clockwise = false;
    LineDesc lineDesc;
};

struct Line: public UserPrimitive
{
    double startX = 0.0;
    double startY = 0.0;
    double endX = 0.0;
    double endY = 0.0;
    LineDesc lineDesc;
};

struct Outline: public UserPrimitive
{
    Polygon polygon;
    LineDesc lineDesc;
};

struct Polyline: public UserPrimitive
{
    double x;
    double y;
    QList<PolyStep> steps;
    LineDesc lineDesc;
};

struct Text: public UserPrimitive
{

};

struct UserSpecial: public UserPrimitive
{

};

enum class ModeType
{
    UserDef = 1,
    Design = 2,
    Fabrication = 3,
    Asembly = 4,
    Test = 5
};

struct FunctionMode
{
    ModeType mode;
    int level;
    QString comment;
};

struct Content
{
    QString roleRef;
    QList<FunctionMode> functionModeList;
    QList<QString> stepRefList;
    QList<QString> layerRefList;
    QList<QString> bomRefList;
    QList<QString> avlRefList;
};

#include <QMap>

class IpcDocument
{
public:
    typedef QMap<QString, Color*> ColorDictionary;
    typedef QMap<QString, LineDesc*> LineDescDictionary;
    typedef QMap<QString, FillDesc*> FillDescDictionary;
    typedef QMap<QString, StandardPrimitive*> StandardPrimitiveDictionary;
    typedef QMap<QString, UserPrimitive*> UserPrimitiveDictionary;

    const ColorDictionary *colorDictionary() const
    {
        return  &m_colorDictionary;
    }

    ColorDictionary *colorDictionary()
    {
        return  &m_colorDictionary;
    }

    const LineDescDictionary *lineDescDictionary() const
    {
        return  &m_lineDescDictionary;
    }

    LineDescDictionary *lineDescDictionary()
    {
        return  &m_lineDescDictionary;
    }

    const FillDescDictionary *fillDescDictionary() const
    {
        return  &m_fillDescDictionary;
    }

    FillDescDictionary *fillDescDictionary()
    {
        return  &m_fillDescDictionary;
    }

    const StandardPrimitiveDictionary *standardPrimitiveDictionary() const
    {
        return  &m_standardPrimitiveDictionary;
    }

    StandardPrimitiveDictionary *standardPrimitiveDictionary()
    {
        return  &m_standardPrimitiveDictionary;
    }

    const UserPrimitiveDictionary *userPrimitiveDictionary() const
    {
        return  &m_userPrimitiveDictionary;
    }

    UserPrimitiveDictionary *userPrimitiveDictionary()
    {
        return  &m_userPrimitiveDictionary;
    }


private:
    ColorDictionary m_colorDictionary;
    LineDescDictionary m_lineDescDictionary;
    FillDescDictionary m_fillDescDictionary;
    StandardPrimitiveDictionary m_standardPrimitiveDictionary;
    UserPrimitiveDictionary m_userPrimitiveDictionary;
};
