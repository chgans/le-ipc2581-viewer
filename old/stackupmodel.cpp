#include "stackupmodel.h"
#include "stackupnode.h"

StackupModel::StackupModel(QObject *parent):
    QAbstractItemModel (parent),
    m_root(nullptr)
{
}

StackupModel::~StackupModel()
{
    delete m_root;
}

void StackupModel::setRootNode(StackupNode *node)
{
    beginResetModel();
    m_root = node;
    endResetModel();
}

StackupNode *StackupModel::rootNode() const
{
    return m_root;
}


QModelIndex StackupModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
    {
        return QModelIndex();
    }

    auto parentNode = nodeForIndex(parent);
    auto childNode = parentNode->children.value(row);
    return createIndex(row, column, childNode);
}

QModelIndex StackupModel::parent(const QModelIndex &child) const
{
    auto childNode = nodeForIndex(child);
    auto parentNode = childNode->parent;

    if (parentNode == m_root)
    {
        return QModelIndex();
    }

    int row = rowForNode(parentNode);
    int column = 0;
    return createIndex(row, column, parentNode);
}

int StackupModel::rowCount(const QModelIndex &parent) const
{
    return nodeForIndex(parent)->children.count();
}

int StackupModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 2;
}

QVariant StackupModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    switch (role)
    {
        case Qt::DisplayRole:
            switch (index.column())
            {
                case 0:
                    return nodeForIndex(index)->name;
                case 1:
                    return nodeForIndex(index)->nominalThickness;
                case 2:
                    return nodeForIndex(index)->material;
                default:
                    return QVariant();
            }
        default:
            return QVariant();
    }
}

QModelIndex StackupModel::indexForNode(StackupNode *node) const
{
    if (node == m_root)
    {
        return QModelIndex();
    }

    int row = rowForNode(node);
    int column = 0;
    return createIndex(row, column, node);
}

StackupNode *StackupModel::nodeForIndex(const QModelIndex &index) const
{
    if (index.isValid())
    {
        return static_cast<StackupNode *>(index.internalPointer());
    }

    return m_root;
}

int StackupModel::rowForNode(StackupNode *node) const
{
    return node->parent->children.indexOf(node);
}


QVariant StackupModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal)
        return QVariant();
    if (role != Qt::DisplayRole)
        return QVariant();

    switch (section)
    {
        case 0: return "Name";
        case 1: return "Thickness";
        case 2: return "Material";
        default: return QVariant();
    }
}
