#ifndef STACKUPMODEL_H
#define STACKUPMODEL_H

#include <QAbstractItemModel>

class StackupNode;

class StackupModel : public QAbstractItemModel
{
public:
    StackupModel(QObject *parent = nullptr);
    ~StackupModel();

    void setRootNode(StackupNode *node);
    StackupNode *rootNode() const;

    // QAbstractItemModel interface
public:
    virtual QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    virtual QModelIndex parent(const QModelIndex &child) const override;
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    // Helper methods
protected:
    QModelIndex indexForNode(StackupNode *node) const;
    StackupNode *nodeForIndex(const QModelIndex &index) const;
    int rowForNode(StackupNode *node) const;

private:
    StackupNode *m_root;

};

#endif // STACKUPMODEL_H
