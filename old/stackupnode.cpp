#include "stackupnode.h"

StackupNode::StackupNode(const QString &name, double thickness, const QString &material, StackupNode *parent):
    parent(parent),
    name(name),
    nominalThickness(thickness),
    material(material)
{
    if (parent != nullptr)
    {
        parent->children.append(this);
    }
}

StackupNode::~StackupNode()
{
    qDeleteAll(children);
}
