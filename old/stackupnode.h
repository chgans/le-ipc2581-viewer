#ifndef STACKUPNODE_H
#define STACKUPNODE_H

#include <QString>
#include <QList>
/*
 * Backdrill
 *  StartLayer = <unit>=<value> [+/-<tolUnit>=<tolValue>] @ <refUnit>=<refValue>
 *  MustNotCutLayer
 *  MaxStubLength
 *  Other
 *
 */
class StackupNode
{
public:
    StackupNode(const QString &name, double nominalThickness = 0.0,
                const QString &material = QString(), StackupNode *parent = nullptr);
    ~StackupNode();

    StackupNode *parent;
    QList<StackupNode *> children;

    // Common data
    QString name;
    double nominalThickness;
    double thicknessPositiveTolerance = 0.0;
    double thicknessNegativeTolerance = 0.0;
    QString bomReferenceDesignator;
    QString comment;
    QString material; // from spec General.Material if exists
    //QList<Specification> specifications;

    // Root only (from ipc2851::Stackup)
    // whereMeasured

    // Group only (from ipc8251::StackupGroup and StackupLayer)
    //

    // Leaf only (from Ipc2851::StakupLayer and Layer])
    // function
    // side
    // polarity
    // span?

};

#endif // STACKUPNODE_H
