#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# TODO
# Parser generator: Allow for specialised templates (template name = Element.typeName)
# Generate Formater
# Add multiplicity checks

# TBD
# double
#  - [min:max] ]min:max[
#  - total digits, fraction digits
# string
#  - pattern
# qname, id and idref eg., '# IdRef <type-name> references <type-name>' and use first attribute
#
# $ sed -ne 's,^.*type="xsd:\([^"]*\)".*$,\1,gp' ../IPC-2581B_V3.0.xsd | sort |uniq -c | sort -nr
# 98 string
# 58 double
# 25 boolean
#  5 nonNegativeInteger
#  5 dateTime
#  3 positiveInteger
#  2 base64Binary
#  2 anyURI
#  1 hexBinary

from jinja2 import Environment, PackageLoader
import sys, re


class AbstractTypeDecl:
	def __init__(self, typeName, inheritsTypeName = None):
		self.typeName = typeName
		self.inheritsTypeName = inheritsTypeName
		# For pass 2
		self.inherits = None
		self.inheritedBy = []

	def lccTypeName(self):
		return self.typeName[0].lower() + self.typeName[1:]

# Class <name> is <name>
class ClassDecl(AbstractTypeDecl):
	def __init__(self, typeName, inheritsTypeName = None):
		AbstractTypeDecl.__init__(self, typeName, inheritsTypeName)
		self.declType = "Class"
		self.attributes = []
		self.elements = []

	def members(self):
		return self.attributes + self.elements

	def memberTypeNames(self):
		return list(set(m.typeName for m in self.attributes + self.elements))

# Group <name> is <name>
class GroupDecl(ClassDecl):
	def __init__(self, typeName, inheritsTypeName = None):
		ClassDecl.__init__(self, typeName, inheritsTypeName)
		sys.stdout.write("Group %s inherits %s\n" % (self.typeName, self.inheritsTypeName if self.inheritsTypeName is not None else "<none>"))
		self.declType = "Group"

class PodDecl(AbstractTypeDecl):
		def __init__(self, typeName):
			AbstractTypeDecl.__init__(self, typeName)
			self.declType = "Pod"

		def members(self):
			return []

		def memberTypeNames(self):
			return []

# Enum <name>
class EnumDecl(AbstractTypeDecl):
	def __init__(self, typeName):
		AbstractTypeDecl.__init__(self, typeName)
		self.declType = "Enum"
		self.nameMap = {} # xml <-> C++

	def addName(self, xmlName, cppName = None):
		if cppName is None:
			cppName = xmlName
		self.nameMap[xmlName] = cppName

	def cppNames(self):
		return self.nameMap.values()

	def members(self):
		return []

	def memberTypeNames(self):
		return []

# @?<name>[*+?]? as <name> is <name>
class InstanceDecl:
	def __init__(self, xmlName, multiplicity = "", varName = None, typeName = None):
		self.xmlName = xmlName
		self.multiplicity = multiplicity
		self.varName = varName
		if self.varName is None:
			self.varName = self.xmlName[0].lower() + self.xmlName[1:]
		self.typeName = typeName
		if self.typeName is None:
			self.typeName = self.xmlName[0].upper() + self.xmlName[1:]
		# For pass 2
		self.instanceOf = None # resolved typeName

	def isAbstract(self):
		return len(self.instanceOf.inheritedBy) != 0

	def typeStorage(self):
		if self.instanceOf.declType == "Class":
			return "*";
		if self.instanceOf.declType == "Group":
			return "*";
		return ""


class ClassParser():
	def __init__(self):
		pass

	def parse(self, data):
		regexp = r"(?P<name>[a-zA-Z0-9_]+)(\s+(?P<inherits>[a-zA-Z0-9_]+))?"
		res = re.search(regexp, data.strip()).groupdict()
		return ClassDecl(res["name"], res["inherits"]), InstanceParser()

class GroupParser():
	def __init__(self):
		pass

	def parse(self, data):
		regexp = r"(?P<name>[a-zA-Z0-9_]+)(\s+(?P<inherits>[a-zA-Z0-9_]+))?"
		res = re.search(regexp, data.strip()).groupdict()
		return GroupDecl(res["name"], res["inherits"]), InstanceParser()

class EnumParser():
	def __init__(self):
		pass

	def parse(self, data):
		regexp = r"(?P<name>[a-zA-Z0-9_]+)"
		res = re.search(regexp, data.strip()).groupdict()
		return EnumDecl(res["name"]), EnumValueParser()

class EnumValueParser():
	def __init__(self):
		pass

	def normalise(self, str):
		res = str.replace('/', '_Per_').replace('_', ' ').title().replace(' ', '')
		if (res[0].isdigit()):
			return '_' + res
		return res

	def parse(self, enum, data):
		regexp = r"(?P<xmlName>[a-zA-Z0-9_/]+)(\s+(?P<cppName>[a-zA-Z0-9_]+))?"
		res = re.search(regexp, data.strip()).groupdict()
		if res["cppName"] is not None:
			enum.nameMap[res["xmlName"]] = res["cppName"]
		else:
			enum.nameMap[res["xmlName"]] = self.normalise(res["xmlName"]) if enum.typeName != "IsoCode" else res["xmlName"]

class InstanceParser():
	def __init__(self):
		pass

	def parse(self, cls, data):
		regexp = r"(?P<isAttr>@?)(?P<xmlName>[a-zA-Z0-9_]+)(?P<multiplicity>[*+?]?)(\s+(?P<typeName>[a-zA-Z0-9_]+))?"
		res = re.search(regexp, data.strip()).groupdict()
		if res["typeName"] is None:
			res["typeName"] = res["xmlName"]
		res["varName"] = None # FIXME
		instance = InstanceDecl(res["xmlName"], res["multiplicity"], res["varName"], res["typeName"])
		if res["isAttr"] == "@":
			cls.attributes.append(instance)
		else:
			cls.elements.append(instance)

all = []
map = {}
def addDecl(decl):
	all.append(decl)
	map[decl.typeName] = decl

parserMap = {"Enum": EnumParser(), "Class": ClassParser(), "Group": GroupParser()}
currentDecl = None
currentParser = None


def nextStatement():
	with open("ipc2581b30.md") as input:
		for line in input:
			yield line.strip()

for statement in nextStatement():
	if statement.startswith("#"):
		regexp = r"#+\s*(?P<kind>(Class|Enum|Group))(?P<data>\s+.*)"
		res = re.search(regexp, statement.strip())
		if res is None:
			currentDecl = None
			continue
		tokens = re.search(regexp, statement).groupdict()
		kind = tokens["kind"]
		data = tokens["data"]
		currentDecl, currentParser = parserMap[kind].parse(data)
		addDecl(currentDecl)
	elif statement.startswith('-') and currentDecl is not None:
		currentParser.parse(currentDecl, statement[1:])

builtins = ["Double", "Int", "Bool", "QString"]
for builtin in builtins:
	addDecl(PodDecl(builtin))

# Resolve inheritance tree
for decl in all:
	if decl.inheritsTypeName is not None:
		decl.inherits = map[decl.inheritsTypeName]
		decl.inherits.inheritedBy.append(decl)

# Resolve attribute instance types
for decl in all:
	if (type(decl) == ClassDecl) or (type(decl) == GroupDecl): # FIXME: group shouldn't have to declare this
		for i in decl.attributes + decl.elements:
			i.instanceOf = map[i.typeName]

def dumpDeclMap(map):
	for decl in map.values():
		sys.stdout.write(decl.typeName+"\n")
		if type(decl) == ClassDecl:
			if decl.inherits is not None:
				sys.stdout.write("  Inherits: " + decl.inherits.typeName + "\n")
			if len(decl.inheritedBy):
				sys.stdout.write("  Inherited by: " + ", ".join([ h.typeName for h in decl.inheritedBy]) + "\n")
			sys.stdout.write("  Attributes:\n")
			for attr in decl.attributes:
				sys.stdout.write("   - %s, as %s, is %s %s\n" % (attr.xmlName, attr.varName, attr.typeName, "(abstract)" if attr.isAbstract() else ""))
			sys.stdout.write("  Elements:\n")
			for el in decl.elements:
				sys.stdout.write("   - %s, as %s, is %s %s\n" % (el.xmlName, el.varName, el.typeName, "(abstract)" if el.isAbstract() else ""))
		elif type(decl) == EnumDecl:
			sys.stdout.write("  Values:\n")
			for xml, cpp in decl.nameMap.items():
				sys.stdout.write("   - %s as %s\n" %(xml, cpp))

# dumpDeclMap(map)

out="../src/lib/LeIpc2581"
env = Environment(
	loader=PackageLoader('ipc', 'templates'),
	block_start_string="/*{",
	block_end_string="}*/",
	variable_start_string="/*[",
    variable_end_string="]*/",
    trim_blocks = True,
    lstrip_blocks = True
	)

def generate(**kwargs):
	item=kwargs["item"]
	sys.stderr.write("Generating code for %s %s ...\n" % (item.declType, item.typeName) )
	template = env.get_template("%s.h" % item.declType)
	content=template.render(kwargs)
	with open("%s/%s.h" % (out, item.typeName), '+w') as file:
		file.write(content)

	template = env.get_template("%sParser.h" % item.declType)
	content=template.render(kwargs)
	with open("%s/%sParser.h" % (out, item.typeName), '+w') as file:
		file.write(content)

	template = env.get_template("%sParser.cpp" % item.declType)
	content=template.render(kwargs)
	with open("%s/%sParser.cpp" % (out, item.typeName), '+w') as file:
		file.write(content)

for decl in all:
	if not decl.typeName in builtins:
		generate(item=decl)

doc = ClassDecl("Document")
doc.declType = "Document"
doc.elements.append(map["Ipc2581"])
generate(item=doc, all=all)

sys.exit(0)
