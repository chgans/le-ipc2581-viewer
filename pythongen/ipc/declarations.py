#!/usr/bin/env python
# -*- coding: utf-8 -*-

class ClassDecl:
        def __init__(self, className, extends = None):
                self.name = className
                self.typeName = className
                self.members = []
                self.extends = extends

        def attributes(self):
                results = []
                for member in self.members:
                        if type(member) is ClassAttributeDecl:
                                results.append(member)
                return results

        def elements(self):
                results = []
                for member in self.members:
                        if type(member) is ClassElementDecl:
                                results.append(member)
                return results

        def subTypeNames(self):
            return list(set(m.typeName for m in self.members))

class ClassAttributeDecl:
        def __init__(self, xml, name, type, method, mul):
                self.xml = xml
                self.name = name
                self.typeName = type
                self.typeStorage = ""
                self.passMethod = method
                self.multiplicity = mul

class ClassElementDecl:
        def __init__(self, xml, name, type, method, mul):
                self.xml = xml
                self.name = name
                self.typeName = type
                self.typeStorage = "*"
                self.passMethod = method
                self.multiplicity = mul
                self.group = None

class GroupDecl:
        def __init__(self, className):
                self.name = className
                self.typeName = className
                self.members = []

        def subTypeNames(self):
            return list(set(m.typeName for m in self.members))

        def isSubstitutes(self, name):
            return name in [m.name for m in self.members]

class GroupMemberDecl:
        def __init__(self, name, type):
                self.name = name
                self.typeName = type
                self.typeStorage = "*"

class EnumDecl:
        def __init__(self, enumName):
                self.name = enumName
                self.members = []

class EnumMemberDecl:
        def __init__(self, xml, value, desc):
                self.xml = xml
                self.value = value
                self.desc = desc
                self.typeName = None


