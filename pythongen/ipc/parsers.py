# -*- coding: utf-8 -*-

from . import declarations as decl

import re

class ClassMemberDeclParser:
        def __init__(self):
                pass

        def parse(self, data):
                regexp = r"(?P<attr>@?)(?P<name>[a-zA-Z0-9]+)(?P<mul>[*+?]?)\s*((?P<type>[a-zA-Z0-9]*)(?P<method>[&*]?))?"
                tokens = re.search(regexp, data).groupdict()

                if len(tokens['type']) == 0:
                        tokens['type'] = tokens['name'][0].upper() + tokens['name'][1:]

                xml = tokens['name']
                tokens['name'] = tokens['name'][0].lower() + tokens['name'][1:]

                if len(tokens['mul']) == 0:
                        tokens['mul'] = "1"

                if len(tokens['attr']) != 0:
                        return decl.ClassAttributeDecl(xml, tokens['name'], tokens['type'], tokens['method'], tokens['mul'])
                else:
                        return decl.ClassElementDecl(xml, tokens['name'], tokens['type'], tokens['method'], tokens['mul'])
                pass

                return None

class ClassDeclParser:
        def __init__(self):
                pass

        def parse(self, data):
                tokens = data.split(" ")
                className = tokens[0]
                extends = None
                if len(tokens) > 1:
                    extends = tokens[1]
                return decl.ClassDecl(className, extends), ClassMemberDeclParser()


class EnumMemberDeclParser:
        def __init__(self):
                pass

        def parse(self, data):
                tokens = data.split(" ")
                xml = tokens[0]
                value = tokens[1]
                desc = " ".join(tokens[2:])
                return decl.EnumMemberDecl(xml, value, desc)

class EnumDeclParser:
        def __init__(self):
                pass

        def parse(self, data):
                tokens = data.split(" ")
                enumName = tokens[0]
                return decl.EnumDecl(enumName), EnumMemberDeclParser()


class GroupMemberDeclParser:
        def __init__(self):
            pass

        def parse(self, data):
                tokens = data.split(" ")
                name = tokens[0]
                if len(tokens) >= 2:
                    type = tokens[1]
                else:
                    type = name
                return decl.GroupMemberDecl(name, type)


class GroupDeclParser:
        def __init__(self):
                pass

        def parse(self, data):
                tokens = data.split(" ")
                name = tokens[0]
                return decl.GroupDecl(name), GroupMemberDeclParser()

