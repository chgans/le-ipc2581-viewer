
#pragma once

#include <QList>
#include "Optional.h"

/*{ if item.inherits }*/
#include "/*[ item.inherits.typeName ]*/.h"
/*{ endif }*/

/*{ for name in item.memberTypeNames() }*/
#include "/*[ name ]*/.h"
/*{ endfor }*/

namespace Ipc2581b
{

class /*[ item.typeName ]*//*{ if item.inherits }*/: public /*[ item.inherits.typeName ]*//*{ endif }*/

{
public:
	virtual ~/*[ item.typeName ]*/() {}

    /*{ for member in item.members() }*/
    /*{ if member.multiplicity == "*" or member.multiplicity == "+" }*/
    QList</*[ member.typeName ]*//*[ member.typeStorage() ]*/> /*[ member.varName ]*/List;
    /*{ elif member.multiplicity == "?" }*/
    Optional</*[ member.typeName ]*//*[ member.typeStorage() ]*/> /*[ member.varName ]*/Optional;
    /*{ else }*/
    /*[ member.typeName ]*/ /*[ member.typeStorage() ]*//*[ member.varName ]*/;
    /*{ endif }*/
    /*{ endfor }*/

    /*{ if item.inherits }*/
    virtual /*[ item.inherits.typeName ]*/Type /*[ item.inherits.lccTypeName() ]*/Type() const override
    {
        return /*[ item.inherits.typeName ]*/Type::/*[ item.typeName ]*/;
    }
    /*{ endif }*/
};

}