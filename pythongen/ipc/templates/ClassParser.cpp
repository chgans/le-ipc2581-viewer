#include "/*[ item.typeName ]*/Parser.h"

/*{ for name in item.memberTypeNames() }*/
#include "/*[ name ]*/Parser.h"
/*{ endfor }*/

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

/*{ set comma = joiner(', ') }*/
/*[ item.typeName ]*/Parser::/*[ item.typeName ]*/Parser(
    /*{ for member in item.members() }*/
    /*[ comma() ]*//*[ member.typeName ]*/Parser *&_/*[ member.varName ]*/Parser
    /*{ endfor }*/)/*{ if item.members()|count() }*/:/*{ endif }*/
    /*{ set comma = joiner(', ') }*/
    /*{ for member in item.members() }*/
    /*[ comma() ]*/m_/*[ member.varName ]*/Parser(_/*[ member.varName ]*/Parser)
    /*{ endfor }*/
{

}

bool /*[ item.typeName ]*/Parser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new /*[ item.typeName ]*/());

    /* Attributes */

    QStringRef data;
    /*{ for attr in item.attributes }*/
    /*{ if attr.multiplicity == "?" }*/
    if (reader->attributes().hasAttribute(QStringLiteral("/*[ attr.xmlName ]*/")))
    {
        data = reader->attributes().value(QStringLiteral("/*[ attr.xmlName ]*/"));
        if (!m_/*[ attr.varName ]*/Parser->parse(reader, data))
            return false;
        m_result->/*[ attr.varName ]*/Optional = Optional</*[ attr.typeName ]*/>(m_/*[ attr.varName ]*/Parser->result());
    }
    /*{ else }*/
    if (reader->attributes().hasAttribute(QStringLiteral("/*[ attr.xmlName ]*/")))
    {
        data = reader->attributes().value(QStringLiteral("/*[ attr.xmlName ]*/"));
        if (!m_/*[ attr.varName ]*/Parser->parse(reader, data))
            return false;
        m_result->/*[ attr.varName ]*/ = m_/*[ attr.varName ]*/Parser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("/*[ attr.xmlName ]*/: Attribute is required"));
        return false;
    }
    /*{ endif }*/
    /*{ endfor }*/

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        /*{ set elseToken="" }*/
        /*{ for el  in item.elements }*/
        /*[  elseToken  ]*/if (/*{ if el.isAbstract() }*/m_/*[ el.varName ]*/Parser->isSubstitution(name)/*{ else }*/name == QStringLiteral("/*[  el.xmlName  ]*/")/*{ endif }*/)
        {
            if (!m_/*[ el.varName ]*/Parser->parse(reader))
                return false;
            auto result = m_/*[ el.varName ]*/Parser->result();
            /*{ if el.multiplicity == "*" or el.multiplicity == "+" }*/
            m_result->/*[ el.varName ]*/List.append(result);
            /*{ elif el.multiplicity == "?" }*/
            m_result->/*[ el.varName ]*/Optional = Optional</*[ el.typeName ]*//*[ el.typeStorage() ]*/>(result);
            /*{ else }*/
            m_result->/*[ el.varName ]*/ = result;
            /*{ endif }*/
            /*{ set elseToken="else " }*/
        }
        /*{ endfor }*/
        /*{ if item.elements|count != 0 }*/
        else
        /*{ endif }*/
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

/*[ item.typeName ]*/ */*[ item.typeName ]*/Parser::result()
{
    return m_result.take();
}

}