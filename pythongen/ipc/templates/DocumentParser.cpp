
#include <QXmlStreamReader>
#include "DocumentParser.h"

/*{ for decl in all }*/
#include "/*[ decl.typeName ]*/Parser.h"
/*{ endfor }*/

namespace Ipc2581b
{

DocumentParser::DocumentParser()
{
    /*{ for decl in all }*/
    m_/*[ decl.typeName ]*/Parser = new /*[ decl.typeName ]*/Parser(
                /*{ set comma = joiner(', ') }*/
                /*{ for member in decl.members() }*/
                /*[ comma() ]*/m_/*[ member.typeName ]*/Parser
                /*{ endfor }*/
                );
    /*{ endfor }*/
}



bool DocumentParser::parse(QXmlStreamReader *reader)
{
    return m_Ipc2581Parser->parse(reader);
}

Ipc2581 *DocumentParser::result()
{
    return m_Ipc2581Parser->result();
}

}