#pragma once

#include <QXmlStreamReader>

namespace Ipc2581b
{

/*{ for decl in all }*/
class /*[ decl.typeName ]*/Parser;
/*{ endfor }*/

class Ipc2581;

class DocumentParser
{
public:
    DocumentParser();

    bool parse(QXmlStreamReader *reader);
    Ipc2581 *result();

private:

    /*{ for decl in all }*/
    /*[ decl.typeName ]*/Parser *m_/*[ decl.typeName ]*/Parser;
    /*{ endfor }*/

};

}