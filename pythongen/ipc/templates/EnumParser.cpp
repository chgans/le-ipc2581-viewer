#include "/*[ item.typeName ]*/Parser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

/*[ item.typeName ]*/Parser::/*[ item.typeName ]*/Parser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool /*[ item.typeName ]*/Parser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, /*[ item.typeName ]*/> map =
    {
        /*{ for xml, cpp in item.nameMap.items() }*/
        {QStringLiteral("/*[ xml ]*/"), /*[ item.typeName ]*/::/*[ cpp ]*/},
        /*{ endfor }*/
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum /*[ item.typeName ]*/").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

/*[ item.typeName ]*/ /*[ item.typeName ]*/Parser::result()
{
    return m_result;
}

}