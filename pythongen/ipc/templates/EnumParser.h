
#pragma once

#include <QString>

class QXmlStreamReader;

#include "/*[ item.typeName ]*/.h"

namespace Ipc2581b
{

class /*[ item.typeName ]*/Parser
{
public:
    /*[ item.typeName ]*/Parser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    /*[ item.typeName ]*/ result();

private:
    /*[ item.typeName ]*/ m_result;
};

}