
#pragma once

/*{ if item.inherits }*/
#include "/*[ item.inherits.typeName ]*/.h"
/*{ endif }*/

namespace Ipc2581b
{

/*{ for inherited in item.inheritedBy }*/
class /*[ inherited.typeName ]*/;
/*{ endfor }*/

class /*[ item.typeName ]*//*{ if item.inherits }*/: public /*[ item.inherits.typeName ]*//*{ endif }*/

{
public:
	virtual ~/*[ item.typeName ]*/() {}

    /*{ set comma = joiner(',') }*/
    /*{ if item.inheritedBy|count }*/
    enum class /*[ item.typeName ]*/Type
    {
        /*{ for inherited in item.inheritedBy }*/
        /*[ comma() ]*//*[ inherited.typeName ]*/
        /*{ endfor }*/
    };

    virtual /*[ item.typeName ]*/Type /*[ item.lccTypeName() ]*/Type() const = 0;
    /*{ for inherited in item.inheritedBy }*/

    inline bool is/*[ inherited.typeName ]*/() const
    {
        return /*[ item.lccTypeName() ]*/Type() == /*[ item.typeName ]*/Type::/*[ inherited.typeName ]*/;
    }

    inline /*[ inherited.typeName ]*/ *to/*[ inherited.typeName ]*/()
    {
        return reinterpret_cast</*[ inherited.typeName ]*/*>(this);
    }

    inline const /*[ inherited.typeName ]*/ *to/*[ inherited.typeName ]*/() const
    {
        return reinterpret_cast<const /*[ inherited.typeName ]*/*>(this);
    }
    /*{ endfor }*/
    /*{ endif }*/

    /*{ if item.inherits }*/
    virtual /*[ item.inherits.typeName ]*/Type /*[ item.inherits.lccTypeName() ]*/Type() const override
    {
        return /*[ item.inherits.typeName ]*/Type::/*[ item.typeName ]*/;
    }
    /*{ endif }*/

};

}

// For user convenience
/*{ for inherited in item.inheritedBy }*/
#include "/*[ inherited.typeName ]*/.h"
/*{ endfor }*/
