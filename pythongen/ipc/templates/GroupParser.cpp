#include "/*[ item.typeName ]*/Parser.h"

/*{ for name in item.memberTypeNames() }*/
#include "/*[ name ]*/Parser.h"
/*{ endfor }*/

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

/*{ set comma = joiner(', ') }*/
/*[ item.typeName ]*/Parser::/*[ item.typeName ]*/Parser(
    /*{ for member in item.members() }*/
    /*[ comma() ]*//*[ member.typeName ]*/Parser *&_/*[ member.varName ]*/Parser
    /*{ endfor }*/)/*{ if item.members()|count() }*/:/*{ endif }*/
    /*{ set comma = joiner(', ') }*/
    /*{ for member in item.members() }*/
    /*[ comma() ]*/m_/*[ member.varName ]*/Parser(_/*[ member.varName ]*/Parser)
    /*{ endfor }*/
{

}

bool /*[ item.typeName ]*/Parser::parse(QXmlStreamReader *reader)
{
    /*{ for member in item.members() }*/
    /*{ if member.isAbstract() }*/
    if (m_/*[ member.varName ]*/Parser->isSubstitution(reader->name()))
    /*{ else }*/
    if (reader->name() == QStringLiteral("/*[ member.xmlName ]*/"))
    /*{ endif }*/
    {
        if(m_/*[ member.varName ]*/Parser->parse(reader))
        {
            m_result = m_/*[ member.varName ]*/Parser->result();
            return true;
        }
        else
            return false;
    }
    /*{ endfor }*/
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"/*[ item.typeName ]*/\"").arg(reader->name().toString()));
    return false;
}

/*[ item.typeName ]*/ */*[ item.typeName ]*/Parser::result()
{
    return m_result;
}

bool /*[ item.typeName ]*/Parser::isSubstitution(const QStringRef &name) const
{
    /*{ for member in item.members() }*/
    /*{ if member.isAbstract() }*/
    if (m_/*[ member.varName ]*/Parser->isSubstitution(name))
        return true;
    /*{ else }*/
    if (name == QStringLiteral("/*[ member.xmlName ]*/"))
        return true;
    /*{ endif }*/
    /*{ endfor }*/
    return false;
}

}