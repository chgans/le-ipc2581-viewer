
#pragma once

#include <QString>
class QXmlStreamReader;

#include "/*[ item.typeName ]*/.h"

namespace Ipc2581b
{

/*{ for name in item.memberTypeNames() }*/
class /*[ name ]*/Parser;
/*{ endfor }*/

class /*[ item.typeName ]*/Parser
{
public:
    /*{ set comma = joiner(', ') }*/
    /*[ item.typeName ]*/Parser(
            /*{ for member in item.members() }*/
            /*[ comma() ]*//*[ member.typeName ]*/Parser*&
            /*{ endfor }*/
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    /*[ item.typeName ]*/ *result();

private:
    /*{ for member in item.members() }*/
    /*[ member.typeName ]*/Parser *&m_/*[ member.varName ]*/Parser;
    /*{ endfor }*/
    /*[ item.typeName ]*/ *m_result;
};

}