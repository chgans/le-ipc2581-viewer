#pragma once

#include <qabstractitemmodel.h>

namespace ItemModel
{
    enum
    {
        UserAttributeModelRole = Qt::UserRole + 0x1976,
        SpecificationModelRole,
        DocumentRole
    };
}
