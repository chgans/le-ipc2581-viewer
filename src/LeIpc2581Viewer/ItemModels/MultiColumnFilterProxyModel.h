#pragma once

#include <QSortFilterProxyModel>

class MultiColumnFilterProxyModelPrivate;
class MultiColumnFilterProxyModel: public QSortFilterProxyModel
{
    Q_OBJECT

    Q_DECLARE_PRIVATE(MultiColumnFilterProxyModel)
    QScopedPointer<MultiColumnFilterProxyModelPrivate> const d_ptr;

    Q_PROPERTY(QList<int> filterKeyColumns READ filterKeyColumns WRITE setFilterKeyColumns)

public:
    explicit MultiColumnFilterProxyModel(QObject *parent = nullptr);
    ~MultiColumnFilterProxyModel();

    QList<int> filterKeyColumns() const;

public slots:
    void setFilterKeyColumns(QList<int> columns);
    void setFilterKeyColumns(int first, int last);

    // QSortFilterProxyModel interface
protected:
    virtual bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
};
