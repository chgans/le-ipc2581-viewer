#pragma once

#include <QAbstractTableModel>

namespace Ipc2581b
{
    class LogicalNet;
    class PhyNet;
}

class NetListModelPrivate;
class NetListModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(NetListModel)
    QScopedPointer<NetListModelPrivate> const d_ptr;

public:
    enum
    {
        NameColumn = 0,
        NetClassColumn,
        PinCountColumn,
        ComponentCountColumn,
        NodeCountColumn,
        ColumnCount
    };

    explicit NetListModel(QObject *parent = 0);
    ~NetListModel();

    void addLogicalNet(const Ipc2581b::LogicalNet *net);
    void addPhysicalNet(const Ipc2581b::PhyNet *net);

public slots:
    void clear();

    // QAbstractTableModel Interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
};
