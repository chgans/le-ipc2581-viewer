#ifndef PADUSELISTMODEL_H
#define PADUSELISTMODEL_H

#include <QAbstractListModel>

#include <PadUse.h>

class PadUseItem;

class PadUseListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    PadUseListModel(QObject *parent = nullptr);

    Ipc2581b::PadUse padUse(const QModelIndex &index) const;

signals:
    void padUseStateChanged(Ipc2581b::PadUse use, bool state);

public slots:
    void setAllStates(bool state);
    void setPadUseState(Ipc2581b::PadUse use, bool state);

private:
    QList<PadUseItem *> m_padUseItemList;
    QMap<Ipc2581b::PadUse, int> m_padUseMap;

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex & index) const override;
};

#endif // PADUSELISTMODEL_H