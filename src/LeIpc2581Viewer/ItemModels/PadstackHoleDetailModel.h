#pragma once

#include <QAbstractTableModel>

namespace Ipc2581b
{
    class PadstackHoleDef;
}

class PadstackHoleDetailModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    PadstackHoleDetailModel(QObject *parent = nullptr);

    void setPadstackHoleDef(Ipc2581b::PadstackHoleDef *holeDef);

private:
    Ipc2581b::PadstackHoleDef *m_holeDef;

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
};
