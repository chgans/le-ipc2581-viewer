#ifndef PINLISTMODEL_H
#define PINLISTMODEL_H

#include <QAbstractTableModel>

namespace Ipc2581b
{
    class Pin;
}

class PinListModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    PinListModel(QObject *parent = nullptr);

    void setPinList(const QList<Ipc2581b::Pin *> &list);

    Ipc2581b::Pin *pin(const QModelIndex &index);

private:
    QList<Ipc2581b::Pin *> m_pinList;

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
};

#endif // PINLISTMODEL_H
