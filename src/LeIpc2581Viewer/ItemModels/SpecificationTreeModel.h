#ifndef SPECIFICATIONTREEMODEL_H
#define SPECIFICATIONTREEMODEL_H

#include <QAbstractItemModel>

namespace Ipc2581b
{
    class Spec;
}

class SpecificationTreeModelPrivate;
class SpecificationTreeModel : public QAbstractItemModel
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(SpecificationTreeModel)
    QScopedPointer<SpecificationTreeModelPrivate> const d_ptr;

public:
    enum
    {
        NameColumn = 0,
        ValueColumn,
        ColumnCount
    };

    explicit SpecificationTreeModel(const QList<Ipc2581b::Spec *> specList, QObject *parent = 0);
    ~SpecificationTreeModel();

    QModelIndex index(const QString &specificationKey) const;

    // QAbstractItemModel interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
};

#endif // SPECIFICATIONTREEMODEL_H
