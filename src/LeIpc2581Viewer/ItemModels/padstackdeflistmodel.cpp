#include "padstackdeflistmodel.h"

#include "PadstackDef.h"

PadstackDefListModel::PadstackDefListModel(QObject *parent):
    QAbstractListModel(parent)
{

}

void PadstackDefListModel::setPadstackDefList(const QList<Ipc2581b::PadstackDef *> &list)
{
    beginResetModel();
    m_padstackDefList = list;
    endResetModel();
}

Ipc2581b::PadstackDef *PadstackDefListModel::padstackDef(const QModelIndex &index) const
{
    return m_padstackDefList.value(index.row());
}

int PadstackDefListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_padstackDefList.count();
}

QVariant PadstackDefListModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();
    if (m_padstackDefList.value(index.row())->nameOptional.hasValue)
        return m_padstackDefList.value(index.row())->nameOptional.value;
    return QString("Pad stack #%1").arg(index.row()+1);
}
