#ifndef LAYERLISTWIDGET_H
#define LAYERLISTWIDGET_H

#include <QWidget>

#include "LeGraphicsView/LeGraphicsView.h" // For orientation

class QCheckBox;
class QComboBox;
class QDoubleSpinBox;
class QSlider;
class QTableView;
class QToolBar;
class QSortFilterProxyModel;

class LayerSetModel;

class LayerListWidget : public QWidget
{
    Q_OBJECT
public:
    explicit LayerListWidget(QWidget *parent = 0);
    ~LayerListWidget();

signals:
    void graphicsViewOrientationChanged(LeGraphicsView::Orientation orientation);
    void graphicsViewRotationChanged(qreal angle);
    void currentLayerChanged(const QModelIndex &index);
    void configureLayerSetRequested();

public slots:
    void setLayerSetModel(LayerSetModel *model);
    void setGraphicsViewOrientation(LeGraphicsView::Orientation orientation);
    void setGraphicsViewRotation(qreal angle);
    void setCurrentLayer(const QModelIndex &index);

private slots:
    void moveCurrentLayerUp();
    void moveCurrentLayerToTop();
    void moveCurrentLayerDown();
    void moveCurrentLayerToBottom();

private:
    QIcon orientationIcon(LeGraphicsView::Orientation orientation);
    QString orientationText(LeGraphicsView::Orientation orientation);
    QVariant orientationData(LeGraphicsView::Orientation orientation);

    QTableView *m_view;
    LayerSetModel *m_model;
    QSortFilterProxyModel *m_proxyModel;
    QToolBar *m_topToolBar;
    QToolBar *m_bottomToolBar;
    QComboBox *m_orientationComboBox;
    QDoubleSpinBox *m_rotationSpinBox;
    QSlider *m_opacitySlider;
};

#endif // LAYERLISTWIDGET_H
