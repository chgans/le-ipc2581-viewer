#pragma once

#include <QtGlobal>

#include <QPointF>
#include <QStringList>
#include <QTransform>

#include "LeIpc2581/Xform.h"
#include "LeIpc2581/Location.h"

static inline QTransform createTransform(const Ipc2581b::Xform *xform)
{
    QTransform transform;
    if (xform->xOffsetOptional.hasValue)
        transform.translate(xform->xOffsetOptional.value, 0);
    if (xform->yOffsetOptional.hasValue)
        transform.translate(0, xform->yOffsetOptional.value);
    if (xform->mirrorOptional.hasValue && xform->mirrorOptional.value)
        transform.scale(-1.0, 1.0);
    if (xform->rotationOptional.hasValue)
        transform.rotate(xform->rotationOptional.value);
    if (xform->scaleOptional.hasValue)
        transform.scale(xform->scaleOptional.value, xform->scaleOptional.value);
    return transform;
}

class StringFormatter
{
public:
    static inline QString pointF(const QPointF &point)
    {
        return pointF(point.x(), point.y());
    }

    static inline QString pointF(qreal x, qreal y)
    {
        return QString("(%1, %2)").arg(x).arg(y);
    }

    static inline QString pointF(const Ipc2581b::Location *location)
    {
        return pointF(location->x, location->y);
    }

    static inline QString transform(const Ipc2581b::Xform *xform)
    {
        bool mirrored = xform->mirrorOptional.hasValue ? xform->mirrorOptional.value : false;
        return QString("(%1, %2, %3, %4, %5)")
                .arg(xform->xOffsetOptional.hasValue ? xform->xOffsetOptional.value : 0.0)
                .arg(xform->yOffsetOptional.hasValue ? xform->yOffsetOptional.value : 0.0)
                .arg(xform->rotationOptional.hasValue ? xform->rotationOptional.value : 0.0)
                .arg(mirrored ? "True" : "False")
                .arg(xform->scaleOptional.hasValue ? xform->scaleOptional.value : 1.0);
//        QStringList operations;
//        if ((xform->xOffsetOptional.hasValue && !qFuzzyCompare(xform->xOffsetOptional.value, 0.0)) ||
//                (xform->yOffsetOptional.hasValue && !qFuzzyCompare(xform->yOffsetOptional.value, 0.0)))
//            operations += "T";
//        if (xform->mirrorOptional.hasValue && xform->mirrorOptional.value)
//            operations += "M";
//        if (xform->rotationOptional.hasValue && !qFuzzyCompare(xform->rotationOptional.value, 0.0))
//            operations += "R";
//        if (xform->scaleOptional.hasValue && !qFuzzyCompare(xform->scaleOptional.value, 1.0))
//            operations += "S";
//        return QString("(%1)").arg(operations.join(", "));
    }

    static inline QString tolerance(qreal plus, qreal minus)
    {
        if (qFuzzyCompare(plus, minus))
            return QString("±%1").arg(plus);
        else
            return QString("+%1/-%2").arg(plus).arg(minus);
    }
};
