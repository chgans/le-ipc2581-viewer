#include "brushstylepickerwidget.h"
#include "brushrenderframe.h"

#include <QBrush>
#include <QDebug>
#include <QMouseEvent>
#include <QPainter>
#include <QVBoxLayout>

const QSize BrushStylePickerWidget::g_iconSize = QSize(32, 16);

BrushStylePickerWidget::BrushStylePickerWidget(QWidget *parent):
    QWidget(parent),
    m_selectedBrushStyle(Qt::NoBrush),
    m_hoveredBrushStyle(Qt::NoBrush)
{
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);

    setMouseTracking(true);
    //setContentsMargins(3, 3, 3, 3);
    setLayout(new QVBoxLayout);
    layout()->setContentsMargins(0, 0, 0, 0);
    layout()->setSpacing(1);
    layout()->addWidget(createFrame(Qt::NoBrush));
    layout()->addWidget(createFrame(Qt::SolidPattern));
    layout()->addWidget(createFrame(Qt::CrossPattern));
    layout()->addWidget(createFrame(Qt::FDiagPattern));
    layout()->addWidget(createFrame(Qt::BDiagPattern));
    layout()->addWidget(createFrame(Qt::DiagCrossPattern));

    m_styleToFrame.value(m_selectedBrushStyle)->setFrameShadow(QFrame::Sunken);
    m_styleToFrame.value(m_hoveredBrushStyle)->setFrameShadow(QFrame::Raised);
}

void BrushStylePickerWidget::setSelectedBrushStyle(Qt::BrushStyle style)
{
    m_styleToFrame.value(m_selectedBrushStyle)->setFrameShadow(QFrame::Plain);
    m_selectedBrushStyle = style;
    m_styleToFrame.value(m_selectedBrushStyle)->setFrameShadow(QFrame::Sunken);
}

Qt::BrushStyle BrushStylePickerWidget::selectedBrushStyle() const
{
    return m_selectedBrushStyle;
}

void BrushStylePickerWidget::setHoveredBrushStyle(Qt::BrushStyle style)
{
    if (m_hoveredBrushStyle == style)
        return;

    m_styleToFrame.value(m_hoveredBrushStyle)->setFrameShadow(QFrame::Plain);
    m_hoveredBrushStyle = style;
    m_styleToFrame.value(m_hoveredBrushStyle)->setFrameShadow(QFrame::Raised);
    m_styleToFrame.value(m_selectedBrushStyle)->setFrameShadow(QFrame::Sunken);

    emit brushStyleHovered(style);
}

Qt::BrushStyle BrushStylePickerWidget::hoveredBrushStyle() const
{
    return m_hoveredBrushStyle;
}

void BrushStylePickerWidget::setBrushColor(const QColor &color)
{
    m_brushColor = color;
    for (auto style: m_styleToFrame.keys())
        m_styleToFrame.value(style)->setBrush(QBrush(m_brushColor, style));
}

BrushRenderFrame *BrushStylePickerWidget::createFrame(Qt::BrushStyle style)
{
    auto frame = new BrushRenderFrame();
    frame->setFrameShape(QFrame::Panel);
    frame->setLineWidth(1);
    frame->setMinimumSize(g_iconSize);
    frame->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    frame->setAttribute(Qt::WA_TransparentForMouseEvents);
    frame->setBrush(QBrush(m_brushColor, style));
    //frame->setToolTip(toolTip);

    m_styleToFrame.insert(style, frame);

    return frame;
}


void BrushStylePickerWidget::mousePressEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
    event->accept();
    setSelectedBrushStyle(m_hoveredBrushStyle);
    emit brushStyleClicked(m_hoveredBrushStyle);
}

void BrushStylePickerWidget::mouseMoveEvent(QMouseEvent *event)
{
    for (auto style: m_styleToFrame.keys())
    {
        if (m_styleToFrame.value(style)->geometry().contains(event->pos()))
        {
            setHoveredBrushStyle(style);
            return;
        }
    }
}

void BrushStylePickerWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
    for (auto style: m_styleToFrame.keys())
    {
        if (m_styleToFrame.value(style)->geometry().contains(event->pos()))
        {
            emit brushStyleDoubleClicked(m_selectedBrushStyle);
            return;
        }
    }
}

void BrushStylePickerWidget::enterEvent(QEvent *event)
{
    Q_UNUSED(event)
    m_styleToFrame.value(m_hoveredBrushStyle)->setFrameShadow(QFrame::Raised);
    m_styleToFrame.value(m_selectedBrushStyle)->setFrameShadow(QFrame::Sunken);
    setHoveredBrushStyle(m_selectedBrushStyle);
}

void BrushStylePickerWidget::leaveEvent(QEvent *event)
{
    Q_UNUSED(event)
    m_styleToFrame.value(m_hoveredBrushStyle)->setFrameShadow(QFrame::Plain);
    m_styleToFrame.value(m_selectedBrushStyle)->setFrameShadow(QFrame::Sunken);
    setHoveredBrushStyle(m_selectedBrushStyle);
}
