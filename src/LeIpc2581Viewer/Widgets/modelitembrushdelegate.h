#ifndef MODELITEMBRUSHDELEGATE_H
#define MODELITEMBRUSHDELEGATE_H

#include <QStyledItemDelegate>

class ModelItemBrushDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    ModelItemBrushDelegate(const QPainterPath &path, QObject *parent = 0);

private slots:
    void editorAccepted();
    void editorRejected();

private:
    QPainterPath m_path;

    // QStyledItemDelegate Interface
public:
    virtual void paint(QPainter *painter, const QStyleOptionViewItem &option,
                       const QModelIndex &index) const override;
    virtual QSize sizeHint(const QStyleOptionViewItem &option,
                           const QModelIndex &index) const override;
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const override;
    virtual void destroyEditor(QWidget *editor, const QModelIndex &index) const override;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model,
                              const QModelIndex &index) const override;
};

#endif // MODELITEMBRUSHDELEGATE_H
