#include "modelitembrusheditor.h"
#include "brushstylepickerwidget.h"
#include "ColorPickerWidget.h"

#include <QHBoxLayout>
#include <QMouseEvent>
#include <QPainter>
#include <QPushButton>
#include <QStyle>
#include <QStyleOption>
#include <QVBoxLayout>

ModelItemBrushEditor::ModelItemBrushEditor(QWidget *parent):
    QWidget(parent),
    m_accepted(false),
    m_okButton(new QPushButton("&Ok")),
    m_cancelButton(new QPushButton("&Cancel")),
    m_brushPicker(new BrushStylePickerWidget()),
    m_colorPicker(new ColorPickerWidget()),
    m_popUpWidget(new QWidget(this))
{
    setupWidget();

    connect(m_brushPicker, &BrushStylePickerWidget::brushStyleHovered,
            this, &ModelItemBrushEditor::setCurrentBrushStyle);
    connect(m_colorPicker, &ColorPickerWidget::colorHovered,
            this, &ModelItemBrushEditor::setCurrentBrushColor);

    connect(m_colorPicker, &ColorPickerWidget::colorDoubleClicked,
            this, &ModelItemBrushEditor::accept);
    connect(m_brushPicker, &BrushStylePickerWidget::brushStyleDoubleClicked,
            this, &ModelItemBrushEditor::accept);
    connect(m_okButton, &QPushButton::clicked,
            this, &ModelItemBrushEditor::accept);

    connect(m_cancelButton, &QPushButton::clicked,
            this, &ModelItemBrushEditor::rejected);
}

QBrush ModelItemBrushEditor::selectedBrush() const
{
    QBrush result;
    result.setColor(m_colorPicker->selectedColor());
    result.setStyle(m_brushPicker->selectedBrushStyle());
    return result;
}

QBrush ModelItemBrushEditor::brush() const
{
    return m_brush;
}

void ModelItemBrushEditor::setBrush(const QBrush &brush)
{
    m_brush = brush;
    m_currentBrush = m_brush;
}

void ModelItemBrushEditor::setPath(const QPainterPath &path)
{
    m_path = path;
}

void ModelItemBrushEditor::accept()
{
    m_brush = selectedBrush();
    m_currentBrush = m_brush;
    emit accepted();
}

void ModelItemBrushEditor::setCurrentBrushStyle(Qt::BrushStyle style)
{
    m_currentBrush.setStyle(style);
    update();
}

void ModelItemBrushEditor::setCurrentBrushColor(const QColor &color)
{
    m_currentBrush.setColor(color);
    update();
}

void ModelItemBrushEditor::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);
    m_popUpWidget->show();
}

void ModelItemBrushEditor::hideEvent(QHideEvent *event)
{
    QWidget::hideEvent(event);
    m_popUpWidget->hide();
}

void ModelItemBrushEditor::moveEvent(QMoveEvent *event)
{
    QWidget::moveEvent(event);
    auto globalPos = parentWidget()->mapToGlobal(geometry().bottomLeft());
    m_popUpWidget->move(m_popUpWidget->mapFromGlobal(globalPos));
}

void ModelItemBrushEditor::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter painter(this);
    painter.setClipRect(rect());

    QRect r = rect().adjusted(2, 2, -2, -2);
    painter.setClipRect(r);

    // Draw the chess pattern for transparency
    const int cellSize = 8;
    QRect cell(0, 0, cellSize, cellSize);
    cell.translate(r.topLeft());
    for (int col = 0; col < (r.width()/cellSize+1); col++)
    {
        for (int row = 0; row < (r.height()/cellSize+1); row++)
        {
            if ((row%2 == 0 && col%2 == 0) || (row%2 == 1 && col%2 == 1))
                painter.fillRect(cell.translated(col*cellSize, row*cellSize), QColor("#ffffff"));
            else
                painter.fillRect(cell.translated(col*cellSize, row*cellSize), QColor("#dddddd"));
        }
    }

    painter.save();
    painter.translate(r.center());
    painter.setBrush(m_currentBrush);
    painter.setPen(Qt::NoPen);
    painter.drawPath(m_path);
    painter.restore();

}

void ModelItemBrushEditor::setupWidget()
{
    m_popUpWidget->setWindowFlags(Qt::Popup);

    auto hLayout = new QHBoxLayout();
    hLayout->addWidget(m_brushPicker);
    hLayout->addWidget(m_colorPicker);
    m_brushPicker->setContentsMargins(0, 0, 0, 0);
    m_brushPicker->layout()->setContentsMargins(0, 0, 0, 0);
    m_brushPicker->layout()->setSpacing(1);
    m_colorPicker->setContentsMargins(0, 0, 0, 0);
    m_colorPicker->layout()->setContentsMargins(0, 0, 0, 0);
    m_colorPicker->layout()->setSpacing(1);
    hLayout->setContentsMargins(0, 0, 0, 0);
    hLayout->setSpacing(1);

    auto vLayout = new QVBoxLayout();
    vLayout->setSpacing(1);
    vLayout->setContentsMargins(3, 3, 3, 3);
    m_popUpWidget->setLayout(vLayout);
    vLayout->addLayout(hLayout);

    hLayout = new QHBoxLayout();
    hLayout->addStretch(5);
    hLayout->addWidget(m_cancelButton);
    hLayout->addWidget(m_okButton);
    vLayout->addLayout(hLayout);
}

