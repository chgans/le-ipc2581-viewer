#ifndef PACKAGEVIEWER_H
#define PACKAGEVIEWER_H

#include "iviewer.h"

#include <QMap>

class QListView;
class QTableView;

namespace Ipc2581b
{
    class Ipc2581;
    class Package;
    class Pin;
    class Layer;
}

class PackageListModel;
class PackageDetailModel;
class LayerListModel;
class LayerListWidget;
class PinListModel;
class PinDetailModel;
class GraphicsItemFactory;
class GraphicsScene;
class GraphicsView;
class GraphicsItem;

class PackageViewer: public IViewer
{
    Q_OBJECT
public:
    explicit PackageViewer(QObject *parent = 0);

signals:

private slots:
    void setCurrentPackage(Ipc2581b::Package *package);
    void setCurrentPin(Ipc2581b::Pin *pin);

private:
    void clear();

    static QList<Ipc2581b::Layer *> createLayerList();

    void populateOutlineLayer();
    void populateLandPatternLayer();
    void populatePinLayer();
    void populateSilkscreenLayer();
    void populateAssemblyDrawingLayer();

    Ipc2581b::Ipc2581 *m_ipc;
    GraphicsItemFactory *m_graphicsItemFactory;

    GraphicsScene *m_graphicsScene;
    GraphicsView *m_graphicsView;

    QListView *m_packageListView;
    PackageListModel *m_packageListModel;
    QList<Ipc2581b::Package *> m_packageList;

    QTableView *m_packageDetailView;
    PackageDetailModel *m_packageDetailModel;
    Ipc2581b::Package *m_currentPackage;

    LayerListWidget *m_layerListWidget;
    LayerListModel *m_layerListModel;
    QList<Ipc2581b::Layer *> m_layerList;

    QTableView *m_pinListView;
    PinListModel *m_pinListModel;
    QList<Ipc2581b::Pin *> m_pinList;
    QMap<Ipc2581b::Pin*, GraphicsItem*> m_pinToGraphicsItem; // TODO: use item->data(IpcDataKey)

    QTableView *m_pinDetailView;
    PinDetailModel *m_pinDetailModel;
    Ipc2581b::Pin *m_currentPin;

    QWidget *m_navigationWidget;
    QWidget *m_auxiliaryWidget;

    static const QList<const QColor*> g_layerBgColorList;

    // IViewer interface
public:
    virtual void setDocument(Ipc2581b::Ipc2581 *doc) override;
    virtual QWidget *centralWidget() const override;
    virtual QWidget *navigationWidget() const override;
    virtual QWidget *auxiliaryWidget() const override;
    virtual void enableAntiAlias(bool enabled) override;
    virtual void enableTransparentLayers(bool enabled) override;
    virtual void enableDropShadowEffect(bool enabled) override;
    virtual void enableOpenGL(bool enabled) override;
    virtual void setLevelOfDetails(LevelOfDetails lod) override;
};

#endif // PACKAGEVIEWER_H
