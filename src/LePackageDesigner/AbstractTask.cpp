#include "AbstractTask.h"

#include "LeIpc7351/Document.h"
#include "LeIpc7351/IDocumentObject.h"

#include "LeGraphicsView/LeGraphicsView.h"
#include "LeGraphicsView/LeGraphicsScene.h"

#include <QAction>

AbstractTask::AbstractTask(QObject *parent)
    : QObject(parent)
    , m_action(new QAction(this))
{
}

QIcon AbstractTask::icon() const
{
    return m_icon;
}

void AbstractTask::setIcon(const QIcon &icon)
{
    m_icon = icon;
}

QString AbstractTask::title() const
{
    return m_title;
}

void AbstractTask::setTile(const QString &title)
{
    m_title = title;
}

QString AbstractTask::description() const
{
    return m_description;
}

void AbstractTask::setDescription(const QString &description)
{
    m_description = description;
}

QKeySequence AbstractTask::shortcut() const
{
    return m_shortcut;
}

void AbstractTask::setShortcut(const QKeySequence &keySequence)
{
    m_shortcut = keySequence;
}

QAction *AbstractTask::action() const
{
    m_action->setText(m_title);
    if (m_shortcut.isEmpty())
        m_action->setToolTip(m_description);
    else
        m_action->setToolTip(QString("%1 <i>%2</i>").arg(m_description, m_shortcut.toString()));
    m_action->setShortcut(m_shortcut);
    m_action->setIcon(m_icon);
    return m_action;
}

//void AbstractTask::setView(LeGraphicsView *view)
//{
//    m_view = view;
//}

//LeGraphicsView *AbstractTask::view() const
//{
//    return m_view;
//}

//LeGraphicsScene *AbstractTask::scene() const
//{
//    if (m_view == nullptr)
//        return nullptr;
//    return m_view->graphicsScene();
//}

//void AbstractTask::setDocument(Document *document)
//{
//    m_document = document;
//}

//Document *AbstractTask::document() const
//{
//    return m_document;
//}

//void AbstractTask::setDocumentObject(IDocumentObject *object)
//{
//    m_documentObject = object;
//}

//IDocumentObject *AbstractTask::object() const
//{
//    return m_documentObject;
//}

//void AbstractTask::setPropertyBrowser(QtTreePropertyBrowser *propertyBrowser)
//{
//    m_propertyBrowser = propertyBrowser;
//}

//QtTreePropertyBrowser *AbstractTask::propertyBrowser() const
//{
//    return m_propertyBrowser;
//}
