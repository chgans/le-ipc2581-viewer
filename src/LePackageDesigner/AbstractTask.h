#ifndef ABSTRACTTASK_H
#define ABSTRACTTASK_H

#include <QObject>
#include <QIcon>
#include <QKeySequence>

class Document;
class IDocumentObject;

class LeGraphicsScene;
class LeGraphicsView;

class QtTreePropertyBrowser;

class QAction;

class AbstractTask : public QObject
{
    Q_OBJECT
public:
    explicit AbstractTask(QObject *parent = 0);

    QIcon icon() const;
    void setIcon(const QIcon &icon);
    QString title() const;
    void setTile(const QString &title);
    QString description() const;
    void setDescription(const QString &description);
    QKeySequence shortcut() const;
    void setShortcut(const QKeySequence &keySequence);

    QAction *action() const;

//    void setView(LeGraphicsView *view);
//    LeGraphicsView *view() const;
//    LeGraphicsScene *scene() const;
//    void setDocument(Document *document);
//    Document *document() const;
//    void setDocumentObject(IDocumentObject *object);
//    IDocumentObject *object() const;

//    void setPropertyBrowser(QtTreePropertyBrowser *propertyBrowser);
//    QtTreePropertyBrowser *propertyBrowser() const;

signals:
    void started(AbstractTask *task);
    void accepted(AbstractTask *task);
    void rejected(AbstractTask *task);

public slots:
    virtual void start() = 0;
    virtual void accept() = 0;
    virtual void reject() = 0;

private:
    QIcon m_icon;
    QString m_title;
    QString m_description;
    QKeySequence m_shortcut;
    // cppcheck-suppress unsafeClassCanLeak
    QAction *m_action = nullptr;
//    LeGraphicsView *m_view = nullptr;
//    Document *m_document = nullptr;
//    IDocumentObject *m_documentObject = nullptr;
//    QtTreePropertyBrowser *m_propertyBrowser = nullptr;
};

Q_DECLARE_METATYPE(AbstractTask*)

#endif // ABSTRACTTASK_H
