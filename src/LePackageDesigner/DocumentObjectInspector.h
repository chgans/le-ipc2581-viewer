#pragma once

#include <QWidget>

class QItemSelection;
class QTreeView;

class Document;
class DocumentObjectTreeModel;
class IDocumentObject;
class ObjectPropertyBrowser;

class DocumentObjectInspector : public QWidget
{
    Q_OBJECT
public:
    explicit DocumentObjectInspector(QWidget *parent = nullptr);
    ~DocumentObjectInspector();

    void setDocumentObject(Document *document, IDocumentObject *object);
    const IDocumentObject *documentObject() const;

public slots:

signals:

private slots:
    void applyDocumentSelection(const QList<IDocumentObject*> &current,
                                     const QList<IDocumentObject*> &previous);
    void applyViewSelection(const QItemSelection &selected,
                            const QItemSelection &deselected);

private:
    Document *m_document = nullptr;
    const IDocumentObject *m_object = nullptr;
    DocumentObjectTreeModel *m_model = nullptr;
    QTreeView *m_view = nullptr;
    ObjectPropertyBrowser *m_browser = nullptr;
};

