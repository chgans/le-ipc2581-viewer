#include "DocumentObjectTreeModel.h"
#include "LeIpc7351/Document.h"

#include "Gui.h"

class DocumentObjectTreeModelPrivate
{
    Q_DISABLE_COPY(DocumentObjectTreeModelPrivate)
    Q_DECLARE_PUBLIC(DocumentObjectTreeModel)
    DocumentObjectTreeModel * const q_ptr;

public:
    explicit DocumentObjectTreeModelPrivate(DocumentObjectTreeModel *model):
        q_ptr(model)
    {

    }

    ~DocumentObjectTreeModelPrivate()
    {
    }

    inline IDocumentObject *object(const QModelIndex &index) const
    {
        return static_cast<IDocumentObject*>(index.internalPointer());
    }

    inline IDocumentObject *objectOrRoot(const QModelIndex &index) const
    {
        if (!index.isValid())
            return rootObject;
        else
            return object(index);
    }

    Document *document = nullptr;
    IDocumentObject *rootObject = nullptr;
};

DocumentObjectTreeModel::DocumentObjectTreeModel(QObject *parent)
    : QAbstractItemModel(parent)
    , IDocumentObjectListener()
    , d_ptr(new DocumentObjectTreeModelPrivate(this))
{
}

DocumentObjectTreeModel::~DocumentObjectTreeModel()
{

}

void DocumentObjectTreeModel::setRootObject(Document *document, IDocumentObject *object)
{
    Q_D(DocumentObjectTreeModel);

    if (d->rootObject != nullptr)
        stopListeningRecursive(d->rootObject);

    beginResetModel();
    d->document = document;
    d->rootObject = object;
    endResetModel();

    if (d->rootObject != nullptr)
        startListeningRecursive(d->rootObject);
}

IDocumentObject *DocumentObjectTreeModel::rootObject() const
{
    Q_D(const DocumentObjectTreeModel);
    return d->rootObject;
}

IDocumentObject *DocumentObjectTreeModel::documentObject(const QModelIndex &index) const
{
    Q_D(const DocumentObjectTreeModel);
    return d->object(index);
}

QModelIndex DocumentObjectTreeModel::index(const IDocumentObject *object) const
{
    Q_D(const DocumentObjectTreeModel);
    QModelIndex modelIndex;
    if (object != d->rootObject)
        modelIndex = createIndex(object->parentObjectIndex(), 0, const_cast<IDocumentObject*>(object));
    return modelIndex;
}

QModelIndex DocumentObjectTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_D(const DocumentObjectTreeModel);

    if (!hasIndex(row, column, parent))
        return QModelIndex();

    if (d->rootObject == nullptr)
        return QModelIndex();

    const IDocumentObject *parentItem = d->objectOrRoot(parent);

    IDocumentObject *childItem = parentItem->childObject(row);
    if (childItem != nullptr)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex DocumentObjectTreeModel::parent(const QModelIndex &index) const
{
    Q_D(const DocumentObjectTreeModel);

    if (!index.isValid())
        return QModelIndex();

    IDocumentObject *childItem = d->object(index);
    IDocumentObject *parentItem = childItem->parentObject();

    if (parentItem == d->rootObject)
        return QModelIndex();

    return createIndex(parentItem->parentObjectIndex(), 0, parentItem);
}

int DocumentObjectTreeModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const DocumentObjectTreeModel);

    if (parent.column() > 0)
        return 0;

    if (d->rootObject == nullptr)
        return 0;

    const IDocumentObject *parentItem = d->objectOrRoot(parent);
    return parentItem->childObjectCount();
}

int DocumentObjectTreeModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return ColumnCount;
}

QVariant DocumentObjectTreeModel::data(const QModelIndex &index, int role) const
{
    Q_D(const DocumentObjectTreeModel);

    if (!index.isValid())
        return QVariant();

    const IDocumentObject *item = d->object(index);

    switch (role) {
        case Qt::DisplayRole:
        case Qt::EditRole:
            switch (index.column())
            {
                case NameColumn:
                    return item->objectUserName();
                default:
                    return QVariant();
            }
            break;
        case Qt::DecorationRole:
            switch (index.column())
            {
                case NameColumn:
                {
//                    const QString name = d->rootObject->iconName(item);
//                    return icon(name);
                    return QVariant();
                }
                default:
                    return QVariant();
            }
        default:
            break;
    }

    return QVariant();
}


QVariant DocumentObjectTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal)
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    switch (section)
    {
        case NameColumn:
            return "Name";
        default:
            return QVariant();
    }
}

Qt::ItemFlags DocumentObjectTreeModel::flags(const QModelIndex &index) const
{
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}


bool DocumentObjectTreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_D(const DocumentObjectTreeModel);

    if (!index.isValid())
        return false;

    if (role != Qt::EditRole)
        return false;

    if (index.column() != NameColumn)
        return false;

    if (!value.canConvert<QString>())
        return false;

    d->object(index)->setObjectUserName(value.toString());

    return true;
}

void DocumentObjectTreeModel::documentObjectAboutToBeInserted(IDocumentObject *parent,
                                                              IDocumentObject *child,
                                                              int index)
{
    Q_UNUSED(child)

    Q_D(const DocumentObjectTreeModel);

    QModelIndex modelIndex;
    if (parent != d->rootObject)
        modelIndex = createIndex(parent->parentObjectIndex(), 0, const_cast<IDocumentObject*>(parent));
    emit beginInsertRows(modelIndex, index, index);
}

void DocumentObjectTreeModel::documentObjectInserted(IDocumentObject *parent,
                                                     IDocumentObject *child,
                                                     int index)
{
    Q_UNUSED(parent)
    Q_UNUSED(child)
    Q_UNUSED(index)

    emit endInsertRows();
    startListeningRecursive(child);
}

void DocumentObjectTreeModel::documentObjectAboutToBeRemoved(IDocumentObject *parent,
                                                             IDocumentObject *child,
                                                             int index)
{
    Q_UNUSED(child)

    Q_D(const DocumentObjectTreeModel);

    QModelIndex modelIndex;
    if (parent != d->rootObject)
        modelIndex = createIndex(parent->parentObjectIndex(), 0, const_cast<IDocumentObject*>(parent));
    emit beginRemoveRows(modelIndex, index, index);
}

void DocumentObjectTreeModel::documentObjectRemoved(IDocumentObject *parent,
                                                    IDocumentObject *child,
                                                    int index)
{
    Q_UNUSED(parent)
    Q_UNUSED(child)
    Q_UNUSED(index)

    emit endRemoveRows();
    stopListeningRecursive(child);
}

void DocumentObjectTreeModel::documentObjectAboutToChangeProperty(const IDocumentObject *object,
                                                                  const QString &name,
                                                                  const QVariant &oldValue)
{
    Q_UNUSED(object)
    Q_UNUSED(name)
    Q_UNUSED(oldValue)
}

void DocumentObjectTreeModel::documentObjectPropertyChanged(const IDocumentObject *object,
                                                            const QString &name, const
                                                            QVariant &newValue)
{
    Q_UNUSED(name)
    Q_UNUSED(newValue)

    Q_D(const DocumentObjectTreeModel);

    QModelIndex modelIndex;
    if (object != d->rootObject)
        modelIndex = createIndex(object->parentObjectIndex(), 0, const_cast<IDocumentObject*>(object));
    emit dataChanged(modelIndex, modelIndex);
}

void DocumentObjectTreeModel::startListeningRecursive(IDocumentObject *object)
{
    if (object == nullptr)
        return;

    beginListeningToDocumentObject(object);
    for (int i = 0; i < object->childObjectCount(); i++)
        startListeningRecursive(object->childObject(i));
}

void DocumentObjectTreeModel::stopListeningRecursive(IDocumentObject *object)
{
    if (object == nullptr)
        return;

    for (int i = 0; i < object->childObjectCount(); i++)
        stopListeningRecursive(object->childObject(i));
    endListeningToDocumentObject(object);
}
