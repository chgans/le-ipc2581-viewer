#include "DocumentWidget.h"

#include "LeIpc7351/Document.h"

#include "DocumentTreeModel.h"
#include "ObjectPropertyBrowser.h"
#include "MainWindow.h"

#include <QAction>
#include <QApplication>
#include <QDebug>
#include <QMenu>
#include <QMetaObject>
#include <QMetaProperty>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QSplitter>
#include <QTreeView>
#include <QVBoxLayout>

DocumentWidget::DocumentWidget(QWidget *parent)
    : QWidget(parent)
{
    m_model = new DocumentTreeModel(this);
    m_view = new QTreeView();
    m_view->setModel(m_model);
    m_view->setEditTriggers(QAbstractItemView::EditKeyPressed);

    m_view->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_view, &QTreeView::customContextMenuRequested,
            this, &DocumentWidget::executeItemContextMenu);

    connect(m_view, &QTreeView::doubleClicked,
            this, [this](const QModelIndex &clicked) {
        editObject(m_model->documentObject(clicked));
    });

#if 0
    m_browser = new ObjectPropertyBrowser();
    connect(m_view->selectionModel(), &QItemSelectionModel::currentRowChanged,
            [this](const QModelIndex &current, const QModelIndex &/*previous*/) {
        m_browser->setObject(m_model->documentObject(current));
    });

    auto splitter = new QSplitter(Qt::Vertical);
    splitter->addWidget(m_view);
    splitter->addWidget(m_browser);
    setLayout(new QVBoxLayout);
    layout()->addWidget(splitter);
#else
    setLayout(new QVBoxLayout);
    layout()->addWidget(m_view);
#endif
    layout()->setMargin(0);
    layout()->setSpacing(0);
}

void DocumentWidget::setDocument(MainWindow *gui, Document *document)
{
    m_gui = gui;
    //m_browser->setObject(nullptr);
    m_document = document;
    m_model->setDocument(document);
}

void DocumentWidget::executeItemContextMenu(const QPoint &pos)
{
    if (m_document == nullptr)
        return;

    QModelIndex index = m_view->indexAt(pos);

    QMenu menu;

    if (!index.isValid())
    {
        menu.addAction("Add a Pad Stack", this, &DocumentWidget::addPadStackObject);
        menu.addAction("Add a Land Pattern", this, &DocumentWidget::addLandPatternObject);
    }
    else
    {
        // FIXME: the code triggered by the action should get the object(s)
        // From the "current selection"
        IDocumentObject *object = m_model->documentObject(index);
        QVariant variant = QVariant::fromValue<IDocumentObject*>(object);
        QAction *action;
        action = m_gui->action("edit-object");
        action->setData(variant);
        menu.addAction(action);
        action = m_gui->action("edit-delete");
        action->setData(variant);
        menu.addAction(action);
        action = m_gui->action("edit-cut");
        action->setData(variant);
        menu.addAction(action);
        action = m_gui->action("edit-copy");
        action->setData(variant);
        menu.addAction(action);
        action = m_gui->action("edit-paste");
        action->setData(variant);
        menu.addAction(action);
    }

    menu.exec(m_view->mapToGlobal(pos));
}

// TODO: implement this
void DocumentWidget::editObject(IDocumentObject *object)
{
    m_gui->openEditor(object);
}

// TODO: implement this
void DocumentWidget::renameObject(IDocumentObject *object)
{
    m_view->edit(m_model->index(object));
}

// TODO: implement this
void DocumentWidget::deleteObject(IDocumentObject *object)
{
    object->parentObject()->removeChildObject(object);
}

// TODO: implement this
void DocumentWidget::cutObject(IDocumentObject *object)
{
    Q_UNUSED(object)
}

// TODO: implement this
void DocumentWidget::copyObject(IDocumentObject *object)
{
    Q_UNUSED(object)
}

// TODO: implement this
void DocumentWidget::pasteObject(IDocumentObject *object)
{
    Q_UNUSED(object)
}

void DocumentWidget::addPadStackObject()
{
    IDocumentObject *object = m_document->addObject("PadStack");
    QModelIndex index = m_model->index(object);
    m_view->setCurrentIndex(index);
    editObject(object);
}

void DocumentWidget::addLandPatternObject()
{
    IDocumentObject *object = m_document->addObject("LandPattern");
    QModelIndex index = m_model->index(object);
    m_view->setCurrentIndex(index);
    editObject(object);
}
