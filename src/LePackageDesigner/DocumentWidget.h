#ifndef DOCUMENTWIDGET_H
#define DOCUMENTWIDGET_H

#include <QWidget>
#include <QMap>

class Document;
class DocumentTreeModel;
class IDocumentObject;
class ObjectPropertyBrowser;
class MainWindow;

class QTreeView;

class DocumentWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DocumentWidget(QWidget *parent = 0);

    void setDocument(MainWindow *gui, Document *document);

private:
    void executeItemContextMenu(const QPoint &pos);
    void editObject(IDocumentObject *object);
    void renameObject(IDocumentObject *object);
    void deleteObject(IDocumentObject *object);
    void cutObject(IDocumentObject *object);
    void copyObject(IDocumentObject *object);
    void pasteObject(IDocumentObject *object);
    void addPadStackObject();
    void addLandPatternObject();

private:
    MainWindow *m_gui;
    Document *m_document = nullptr;
    DocumentTreeModel *m_model = nullptr;
    QTreeView *m_view = nullptr;
    IDocumentObject *m_object = nullptr;
    ObjectPropertyBrowser *m_browser = nullptr;
};

#endif // DOCUMENTWIDGET_H
