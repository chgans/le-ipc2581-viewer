#ifndef EDITORWIDGET_H
#define EDITORWIDGET_H

#include <QWidget>

class LeGraphicsView;

class MainWindow;
class Document;
class IDocumentObject;
class AbstractTask;

class EditorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit EditorWidget(QWidget *parent = 0);

    virtual void edit(MainWindow *app, Document *document, IDocumentObject *object) = 0;

    virtual IDocumentObject *object() const = 0;
    virtual Document *document() const = 0;

    virtual QList<AbstractTask*> tasks() const = 0;
    virtual QString title() const = 0;

signals:
    void taskStarted(AbstractTask *task);
    void taskFinished(AbstractTask *task);

public slots:
};

#endif // EDITORWIDGET_H
