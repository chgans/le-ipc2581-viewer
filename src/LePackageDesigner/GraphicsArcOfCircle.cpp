#include "GraphicsArcOfCircle.h"

#include "LeIpc7351/Primitives/ArcOfCircle.h"

#include "LeGraphicsView/LeGraphicsHandleItem.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/LeGraphicsScene.h"

#include <QStyleOptionGraphicsItem>


GraphicsArcOfCircle::GraphicsArcOfCircle(ArcOfCircle *primitive)
    : LeGraphicsItem(GftPad)
    , IDocumentObjectListener()
    , m_primitive(primitive)
{
    addHandles();
    updateGeometry();
    setPos(primitive->position());
    setRotation(primitive->rotation());
    setTransform(QTransform().scale(primitive->isMirrored() ? -1 : 1, 1));
    beginListeningToDocumentObject(primitive);
}

GraphicsArcOfCircle::~GraphicsArcOfCircle()
{
    endListeningToDocumentObject(m_primitive);
}

void GraphicsArcOfCircle::updateGeometry()
{
    const qreal radius = m_primitive->diameter()/2.0;
    QRectF bRect(QPointF(-radius, -radius),
                 QPointF(radius, radius));
    handle(TopRightHandleRole)->setPos(bRect.bottomRight());
    handle(TopHandleRole)->setPos(QPointF(bRect.center().x(),
                                          bRect.bottom()));
    handle(TopLeftHandleRole)->setPos(bRect.bottomLeft());
    handle(LeftHandleRole)->setPos(QPointF(bRect.left(),
                                           bRect.center().y()));
    handle(RightHandleRole)->setPos(QPointF(bRect.right(),
                                            bRect.center().y()));
    handle(BottomRightHandleRole)->setPos(bRect.topRight());
    handle(BottomHandleRole)->setPos(QPointF(bRect.center().x(),
                                             bRect.top()));
    handle(BottomLeftHandleRole)->setPos(bRect.topLeft());
}

QRectF GraphicsArcOfCircle::boundingRect() const
{
    const qreal penWidth = 1.0;
    const qreal radius = (m_primitive->diameter() + penWidth)/2.0;
    return QRectF(QPointF(-radius, -radius),
                  QPointF(radius, radius));
}

void GraphicsArcOfCircle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget)

    GraphicsStyleOption itemOption;
    itemOption.exposedRect = option->exposedRect;
    itemOption.palette = layer()->graphicsPalette(featureType());
    itemOption.state = state();

    QPainterPath path;
    const qreal radius = m_primitive->diameter()/2.0;
    path.arcMoveTo(QRectF(-radius, -radius, m_primitive->diameter(), m_primitive->diameter()),
                   360-m_primitive->startAngle());
    path.arcTo(QRectF(-radius, -radius, m_primitive->diameter(), m_primitive->diameter()),
               360-m_primitive->startAngle(), -m_primitive->spanAngle());
    LeGraphicsStyle *style = graphicsScene()->graphicsStyle();
    style->strokePath(&itemOption, painter, path, QPainterPath(), 1.0);
}

LeGraphicsItem *GraphicsArcOfCircle::clone() const
{
    return nullptr;
}

void GraphicsArcOfCircle::documentObjectAboutToChangeProperty(const IDocumentObject *object, const QString &name, const QVariant &oldValue)
{
    Q_UNUSED(object)
    Q_UNUSED(oldValue)

    if (name == "diameter" || name == "startAngle" || name == "spanAngle")
        prepareGeometryChange();

}

void GraphicsArcOfCircle::documentObjectPropertyChanged(const IDocumentObject *object, const QString &name, const QVariant &newValue)
{
    Q_UNUSED(object)

    if (name == "diameter" || name == "startAngle" || name == "spanAngle")
        updateGeometry();
    else if (name == "position")
        setPos(newValue.toPointF());
    else if (name == "rotation")
        setRotation(newValue.toDouble());
    else if (name == "mirrored")
        setTransform(QTransform().scale(newValue.toBool() ? -1 : 1, 1));
}

void GraphicsArcOfCircle::addHandles()
{
    addHandle(TopRightHandleRole);
    addHandle(TopHandleRole);
    addHandle(TopLeftHandleRole);
    addHandle(BottomRightHandleRole);
    addHandle(BottomHandleRole);
    addHandle(BottomLeftHandleRole);
    addHandle(RightHandleRole);
    addHandle(LeftHandleRole);
    addHandle(CenterHandleRole);
}

QPointF GraphicsArcOfCircle::handlePositionChangeFilter(LeGraphicsHandleRole role,
                                                        const QPointF &value)
{
    switch (role)
    {
        case TopHandleRole:
        {
            const qreal radius = qAbs(value.y());
            m_primitive->setDiameter(2*radius);
            return QPointF(0, radius);
        }
        case BottomHandleRole:
        {
            const qreal radius = qAbs(value.y());
            m_primitive->setDiameter(2*qAbs(value.y()));
            return QPointF(0, -radius);
        }
        case LeftHandleRole:
        {
            const qreal radius = qAbs(value.x());
            m_primitive->setDiameter(2*radius);
            return QPointF(-radius, 0);
        }
        case RightHandleRole:
        {
            const qreal radius = qAbs(value.x());
            m_primitive->setDiameter(2*radius);
            return QPointF(radius, 0);
        }
        case TopRightHandleRole:
        {
            const qreal radius = qMax(qAbs(value.x()), qAbs(value.y()));
            m_primitive->setDiameter(2*radius);
            return QPointF(radius, radius);
        }
        case TopLeftHandleRole:
        {
            const qreal radius = qMax(qAbs(value.x()), qAbs(value.y()));
            m_primitive->setDiameter(2*radius);
            return QPointF(-radius, radius);
        }
        case BottomRightHandleRole:
        {
            const qreal radius = qMax(qAbs(value.x()), qAbs(value.y()));
            m_primitive->setDiameter(2*radius);
            return QPointF(radius, -radius);
        }
        case BottomLeftHandleRole:
        {
            const qreal radius = qMax(qAbs(value.x()), qAbs(value.y()));
            m_primitive->setDiameter(2*radius);
            return QPointF(-radius, -radius);
        }
        case CenterHandleRole:
        {
            // Move the parent to where the handle wants to go...
            m_primitive->setPosition(mapToParent(value));
            // And leave the handle where it should be
            return QPointF(0, 0);
        }
        default:
            return value;
    }
}
