#include "GraphicsButterflyPrimitive.h"

#include "LeIpc7351/Primitives/ButterflyPrimitive.h"

#include "LeGraphicsView/LeGraphicsHandleItem.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/LeGraphicsScene.h"

#include <QStyleOptionGraphicsItem>


GraphicsButterflyPrimitive::GraphicsButterflyPrimitive(ButterflyPrimitive *primitive)
    : LeGraphicsItem(GftPad)
    , IDocumentObjectListener()
    , m_primitive(primitive)
{
    addHandles();
    updateGeometry();
    setPos(primitive->position());
    setRotation(primitive->rotation());
    setPos(primitive->position());
    setTransform(QTransform().scale(primitive->isMirrored() ? -1 : 1, 1));
    beginListeningToDocumentObject(primitive);
}

GraphicsButterflyPrimitive::~GraphicsButterflyPrimitive()
{
    endListeningToDocumentObject(m_primitive);
}

void GraphicsButterflyPrimitive::updateGeometry()
{
    // Handles ignore view transfrom, so top is actually bottom ...

    handle(TopRightHandleRole)->setPos(boundingRect().bottomRight());
    handle(TopHandleRole)->setPos(QPointF(boundingRect().center().x(),
                                          boundingRect().bottom()));
    handle(TopLeftHandleRole)->setPos(boundingRect().bottomLeft());
    handle(LeftHandleRole)->setPos(QPointF(boundingRect().left(),
                                           boundingRect().center().y()));
    handle(RightHandleRole)->setPos(QPointF(boundingRect().right(),
                                            boundingRect().center().y()));
    handle(BottomRightHandleRole)->setPos(boundingRect().topRight());
    handle(BottomHandleRole)->setPos(QPointF(boundingRect().center().x(),
                                             boundingRect().top()));
    handle(BottomLeftHandleRole)->setPos(boundingRect().topLeft());
}

QRectF GraphicsButterflyPrimitive::boundingRect() const
{
    return QRectF(QPointF(-m_primitive->size()/2.0,
                          -m_primitive->size()/2.0),
                  QSizeF(m_primitive->size(), m_primitive->size()));
}

void GraphicsButterflyPrimitive::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget)

    GraphicsStyleOption itemOption;
    itemOption.exposedRect = option->exposedRect;
    itemOption.palette = layer()->graphicsPalette(featureType());
    itemOption.state = state();

    LeGraphicsStyle *style = graphicsScene()->graphicsStyle();

    if (m_primitive->shape() == ButterflyPrimitive::Round)
    {
        QPainterPath path;
        path.addEllipse(boundingRect());
        QPainterPath cut;
        cut.addRect(QRectF(boundingRect().topLeft(), boundingRect().center()));
        cut.addRect(QRectF(boundingRect().center(), boundingRect().bottomRight()));
        style->fillPath(&itemOption, painter, path - cut);
    }
    else
    {
        style->drawRect(&itemOption, painter, QRectF(boundingRect().topRight(), boundingRect().center()));
        style->drawRect(&itemOption, painter, QRectF(boundingRect().center(), boundingRect().bottomLeft()));
    }
}

void GraphicsButterflyPrimitive::addHandles()
{
    addHandle(TopRightHandleRole);
    addHandle(TopHandleRole);
    addHandle(TopLeftHandleRole);
    addHandle(BottomRightHandleRole);
    addHandle(BottomHandleRole);
    addHandle(BottomLeftHandleRole);
    addHandle(RightHandleRole);
    addHandle(LeftHandleRole);
    addHandle(CenterHandleRole);
}

QPointF GraphicsButterflyPrimitive::handlePositionChangeFilter(LeGraphicsHandleRole role,
                                                               const QPointF &value)
{
    switch (role)
    {
        case TopHandleRole:
        {
            const qreal radius = qAbs(value.y());
            m_primitive->setSize(2*radius);
            return QPointF(0, radius);
        }
        case BottomHandleRole:
        {
            const qreal radius = qAbs(value.y());
            m_primitive->setSize(2*qAbs(value.y()));
            return QPointF(0, -radius);
        }
        case LeftHandleRole:
        {
            const qreal radius = qAbs(value.x());
            m_primitive->setSize(2*radius);
            return QPointF(-radius, 0);
        }
        case RightHandleRole:
        {
            const qreal radius = qAbs(value.x());
            m_primitive->setSize(2*radius);
            return QPointF(radius, 0);
        }
        case TopRightHandleRole:
        {
            const qreal radius = qMax(qAbs(value.x()), qAbs(value.y()));
            m_primitive->setSize(2*radius);
            return QPointF(radius, radius);
        }
        case TopLeftHandleRole:
        {
            const qreal radius = qMax(qAbs(value.x()), qAbs(value.y()));
            m_primitive->setSize(2*radius);
            return QPointF(-radius, radius);
        }
        case BottomRightHandleRole:
        {
            const qreal radius = qMax(qAbs(value.x()), qAbs(value.y()));
            m_primitive->setSize(2*radius);
            return QPointF(radius, -radius);
        }
        case BottomLeftHandleRole:
        {
            const qreal radius = qMax(qAbs(value.x()), qAbs(value.y()));
            m_primitive->setSize(2*radius);
            return QPointF(-radius, -radius);
        }
        case CenterHandleRole:
        {
            m_primitive->setPosition(mapToParent(value));
            return QPointF(0, 0);
        }
        default:
            return value;
    }
}

LeGraphicsItem *GraphicsButterflyPrimitive::clone() const
{
    return nullptr;
}


void GraphicsButterflyPrimitive::documentObjectAboutToChangeProperty(const IDocumentObject *object, const QString &name, const QVariant &oldValue)
{
    Q_UNUSED(object)
    Q_UNUSED(oldValue)

    if (name == "size" || name == "shape")
        prepareGeometryChange();
}

void GraphicsButterflyPrimitive::documentObjectPropertyChanged(const IDocumentObject *object, const QString &name, const QVariant &newValue)
{
    Q_UNUSED(object)

    if (name == "size" || name == "shape")
        updateGeometry();
    else if (name == "position")
        setPos(newValue.toPointF());
    else if (name == "rotation")
        setRotation(newValue.toDouble());
    else if (name == "mirrored")
        setTransform(QTransform().scale(newValue.toBool() ? -1 : 1, 1));
}
