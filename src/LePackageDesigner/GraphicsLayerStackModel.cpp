#include "GraphicsLayerStackModel.h"

#include "Gui.h"

#include "LeGraphicsView/LeGraphicsItem.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/LeGraphicsScene.h"

#include <QDebug>

class GraphicsLayerStackItem
{
    GraphicsLayerStackItem(LeGraphicsItemLayer *aLayer)
        : layer(aLayer)
    {}

    LeGraphicsItemLayer *layer;
};

class GraphicsLayerStackModelPrivate
{
    Q_DISABLE_COPY(GraphicsLayerStackModelPrivate)
    Q_DECLARE_PUBLIC(GraphicsLayerStackModel)
    GraphicsLayerStackModel * const q_ptr;

    explicit GraphicsLayerStackModelPrivate(GraphicsLayerStackModel *model):
        q_ptr(model)
    {}

    LeGraphicsScene *m_scene = nullptr;
};

GraphicsLayerStackModel::GraphicsLayerStackModel(QObject *parent):
    QAbstractTableModel(parent),
    d_ptr(new GraphicsLayerStackModelPrivate(this))
{
}

GraphicsLayerStackModel::~GraphicsLayerStackModel()
{

}

void GraphicsLayerStackModel::setScene(LeGraphicsScene *scene)
{
    Q_D(GraphicsLayerStackModel);

    if (d->m_scene != nullptr)
        d->m_scene->disconnect(this);

    beginResetModel();
    d->m_scene = scene;
    endResetModel();

    if (d->m_scene != nullptr)
    {
        connect(d->m_scene, &LeGraphicsScene::aboutToAddLayer,
                this, &GraphicsLayerStackModel::beginResetModel);
        connect(d->m_scene, &LeGraphicsScene::layerAdded,
                this, &GraphicsLayerStackModel::endResetModel);
        connect(d->m_scene, &LeGraphicsScene::aboutToRemoveLayer,
                this, &GraphicsLayerStackModel::beginResetModel);
        connect(d->m_scene, &LeGraphicsScene::layerRemoved,
                this, &GraphicsLayerStackModel::endResetModel);
        connect(d->m_scene, &LeGraphicsScene::aboutToChangeLayerOrdering,
                this, &GraphicsLayerStackModel::beginResetModel);
        connect(d->m_scene, &LeGraphicsScene::layerOrderingChanged,
                this, &GraphicsLayerStackModel::endResetModel);
    }
}

LeGraphicsItemLayer *GraphicsLayerStackModel::graphicsLayer(const QString &name) const
{
    Q_D(const GraphicsLayerStackModel);

    if (d->m_scene == nullptr)
        return nullptr;

    for (LeGraphicsItemLayer *layer: d->m_scene->featureLayers())
        if (layer->fullName() == name)
            return layer;

    return nullptr;
}

LeGraphicsItemLayer *GraphicsLayerStackModel::graphicsLayer(const QModelIndex &index) const
{
    Q_D(const GraphicsLayerStackModel);

    if (!index.isValid())
        return nullptr;

    if (d->m_scene == nullptr)
        return nullptr;

    return d->m_scene->featureLayers().value(index.row(), nullptr);
}

QModelIndex GraphicsLayerStackModel::moveToTop(const QModelIndex &index)
{
    if (moveRow(QModelIndex(), index.row(), QModelIndex(), 0))
        return createIndex(0, 0);
    return index;
}

QModelIndex GraphicsLayerStackModel::moveToBottom(const QModelIndex &index)
{
    if (moveRow(QModelIndex(), index.row(), QModelIndex(), rowCount()))
        return createIndex(rowCount() - 1, 0);
    return index;
}

QModelIndex GraphicsLayerStackModel::moveUp(const QModelIndex &index)
{
    if (moveRow(QModelIndex(), index.row(), QModelIndex(), index.row() - 1))
        return createIndex(index.row() - 1, 0);
    return index;
}

QModelIndex GraphicsLayerStackModel::moveDown(const QModelIndex &index)
{
    if (moveRow(QModelIndex(), index.row(), QModelIndex(), index.row() + 2))
        return createIndex(index.row() + 1, 0);
    return index;
}

QVariant GraphicsLayerStackModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal)
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    switch (section)
    {
        case VisibleColumn: return "Vis.";
        case SequenceNumberColumn: return "Seq";
        case NameColumn: return "Name";
        default: return "?";
    }
}


int GraphicsLayerStackModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const GraphicsLayerStackModel);
    Q_UNUSED(parent);

    if (d->m_scene != nullptr)
        return d->m_scene->featureLayers().count();
    return 0;
}

int GraphicsLayerStackModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return ColumnCount;
}

QVariant GraphicsLayerStackModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    Q_D(const GraphicsLayerStackModel);

    const LeGraphicsItemLayer *layer = d->m_scene->featureLayers().at(index.row());
    switch (role)
    {
        case Qt::DisplayRole:
            switch (index.column())
            {
                case SequenceNumberColumn:
                    return 0;
                case NameColumn:
                    return layer->shortName();
                case CompactColumn:
                    return layer->tinyName();
                default:
                    return QVariant();
            }
            break;
        case Qt::DecorationRole:
            switch (index.column())
            {
                case VisibleColumn:
                    return layer->isVisible() ? Gui::icon("layer-visible-on") :
                                                Gui::icon("layer-visible-off");
                    break;
                default:
                    break;
            }
            break;
        case Qt::ToolTipRole:
            return layer->fullName();
        case Qt::BackgroundRole:
            return layer->featureOption().featureBrush(GftPad);
        case Qt::CheckStateRole:
            switch (index.column())
            {
                case VisibleColumn:
                case CompactColumn:
                    return QVariant::fromValue<Qt::CheckState>(layer->isVisible() ?
                                                                   Qt::Checked: Qt::Unchecked);
                default:
                    break;
            }
            break;
        default:
            break;
    }
    return QVariant();
}

bool GraphicsLayerStackModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_D(GraphicsLayerStackModel);

    if (data(index, role) == value)
        return false;

    LeGraphicsItemLayer *layer = d->m_scene->featureLayers().at(index.row());
    switch (role)
    {
        case Qt::CheckStateRole:
            switch (index.column())
            {
                case VisibleColumn:
                case CompactColumn:
                {
                    Qt::CheckState state = value.value<Qt::CheckState>();
                    if (state == Qt::Checked)
                        layer->setVisible(true);
                    else
                        layer->setVisible(false);
                    emit dataChanged(index, index);
                    return true;
                }
                default:
                    break;
            }
        default:
            break;
    }

    return false;
}

Qt::ItemFlags GraphicsLayerStackModel::flags(const QModelIndex &index) const
{
    //Q_D(const GraphicsLayerStackModel);

    if (!index.isValid())
        return Qt::NoItemFlags;

    Qt::ItemFlags itemFlags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;

    switch (index.column())
    {
        case VisibleColumn:
        case CompactColumn:
            itemFlags.setFlag(Qt::ItemIsUserCheckable);
            break;
        default:
            break;
    }

    return itemFlags;
}

bool GraphicsLayerStackModel::moveRows(const QModelIndex &sourceParent, int sourceRow, int count,
                                     const QModelIndex &destinationParent, int destinationChild)
{
    Q_D(GraphicsLayerStackModel);

    // Table model, parent is always invalid
    QModelIndex parent;
    if (sourceParent.isValid() || destinationParent.isValid())
        return false;

    // More than one row is not supported (yet)
    if (count > 1)
        return false;

    if (sourceRow < 0 || sourceRow >= rowCount())
        return false;

    if (destinationChild < 0 || destinationChild > rowCount())
        return false;

    // Cannot move X above X
    if (destinationChild == sourceRow)
        return false;

    // Cannot move X above X+1
    if (destinationChild == sourceRow + 1)
        return false;

    int destinationRow = destinationChild;

    // move X above X+N+1 = move X to X+N
    if (destinationRow > (sourceRow + 1 ))
    {
        destinationRow--;
    }

    beginMoveRows(parent, sourceRow, sourceRow,
                  parent, destinationChild);
    QList<LeGraphicsItemLayer*> layers = d->m_scene->featureLayers();
    layers.move(sourceRow, destinationRow);
    d->m_scene->changeLayerOrdering(layers);
    endMoveRows();

    return true;
}
