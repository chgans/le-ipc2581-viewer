#pragma once

#include <QAbstractTableModel>

class LeGraphicsItemLayer;
class LeGraphicsScene;

class GraphicsLayerStackModelPrivate;
class GraphicsLayerStackModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(GraphicsLayerStackModel)
    QScopedPointer<GraphicsLayerStackModelPrivate> const d_ptr;

public:
    enum
    {
        CompactColumn = 0,
        VisibleColumn,
        SequenceNumberColumn,
        NameColumn,
        ColumnCount
    };

    explicit GraphicsLayerStackModel(QObject *parent = 0);
    ~GraphicsLayerStackModel();

    void setScene(LeGraphicsScene *scene);

    QList<LeGraphicsItemLayer*> graphicsLayers() const;
    LeGraphicsItemLayer *graphicsLayer(const QString &name) const;
    LeGraphicsItemLayer *graphicsLayer(const QModelIndex &index) const;

    QModelIndex moveToTop(const QModelIndex &index);
    QModelIndex moveToBottom(const QModelIndex &index);
    QModelIndex moveUp(const QModelIndex &index);
    QModelIndex moveDown(const QModelIndex &index);

    // QAbstractTableModel Interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // QAbstractItemModel interface
public:
    virtual bool moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild) override;
};
