#include "GraphicsRectangularShapeItem.h"

#include "LeIpc7351/LeIpc7351.h"
#include "LeIpc7351/Primitives/RectanglePrimitive.h"

#include "LeGraphicsView/LeGraphicsHandleItem.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/LeGraphicsScene.h"

#include <QDebug>
#include <QStyleOptionGraphicsItem>

GraphicsRectangleShapeItem::GraphicsRectangleShapeItem(RectanglePrimitive *primitive)
    : LeGraphicsItem(GftPad)
    , m_primitive(primitive)
{
    addHandles();
    updateGeometry();
    setPos(primitive->position());
    setRotation(primitive->rotation());
    setPos(primitive->position());
    setTransform(QTransform().scale(primitive->isMirrored() ? -1 : 1, 1));
    beginListeningToDocumentObject(primitive);
}

GraphicsRectangleShapeItem::~GraphicsRectangleShapeItem()
{
    endListeningToDocumentObject(primitive());
}

RectanglePrimitive *GraphicsRectangleShapeItem::primitive() const
{
    return m_primitive;
}

void GraphicsRectangleShapeItem::updateGeometry()
{
    // Handles ignore view transfrom, so top is actually bottom ...

    handle(TopRightHandleRole)->setPos(boundingRect().bottomRight());
    handle(TopHandleRole)->setPos(QPointF(boundingRect().center().x(),
                                          boundingRect().bottom()));
    handle(TopLeftHandleRole)->setPos(boundingRect().bottomLeft());
    handle(LeftHandleRole)->setPos(QPointF(boundingRect().left(),
                                           boundingRect().center().y()));
    handle(RightHandleRole)->setPos(QPointF(boundingRect().right(),
                                            boundingRect().center().y()));
    handle(BottomRightHandleRole)->setPos(boundingRect().topRight());
    handle(BottomHandleRole)->setPos(QPointF(boundingRect().center().x(),
                                             boundingRect().top()));
    handle(BottomLeftHandleRole)->setPos(boundingRect().topLeft());
}

QRectF GraphicsRectangleShapeItem::boundingRect() const
{
    return QRectF(QPointF(-m_primitive->size().width()/2.0,
                          -m_primitive->size().height()/2.0),
                  m_primitive->size());
}

void GraphicsRectangleShapeItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget)

    GraphicsStyleOption itemOption;
    itemOption.exposedRect = option->exposedRect;
    itemOption.palette = layer()->graphicsPalette(featureType());
    itemOption.state = state();

    LeGraphicsStyle *style = graphicsScene()->graphicsStyle();
    QPainterPath path;
    const qreal top = m_primitive->size().height()/2.0;
    const qreal left = -m_primitive->size().width()/2.0;
    const qreal bottom = -m_primitive->size().height()/2.0;
    const qreal right = m_primitive->size().width()/2.0;
    const qreal chamfer = m_primitive->chamferSize();
    const qreal radius = m_primitive->cornerRadius();
    if (m_primitive->chamferedCorners().testFlag(L7::TopLeftCorner))
    {
        path.moveTo(left, top - chamfer);
        path.lineTo(left + chamfer, top);
    }
    else if (m_primitive->roundedCorners().testFlag(L7::TopLeftCorner))
    {
        path.moveTo(left, top - radius);
        path.arcTo(QRectF(QPointF(left, top - 2*radius),
                          QPointF(left + 2*radius, top)),
                   180, 90);
    }
    else
    {
        path.moveTo(left, top);
    }

    if (m_primitive->chamferedCorners().testFlag(L7::TopRightCorner))
    {
        path.lineTo(right - chamfer, top);
        path.lineTo(right, top - chamfer);
    }
    else if (m_primitive->roundedCorners().testFlag(L7::TopRightCorner))
    {
        path.lineTo(right - radius, top);
        path.arcTo(QRectF(QPointF(right - 2*radius, top),
                          QPointF(right, top - 2*radius)),
                   90, -90);
    }
    else
    {
        path.lineTo(right, top);
    }

    if (m_primitive->chamferedCorners().testFlag(L7::BottomRightCorner))
    {
        path.lineTo(right, bottom + chamfer);
        path.lineTo(right - chamfer, bottom);
    }
    else if (m_primitive->roundedCorners().testFlag(L7::BottomRightCorner))
    {
        path.lineTo(right, bottom + radius);
        path.arcTo(QRectF(QPointF(right - 2*radius, bottom  + 2*radius),
                          QPointF(right, bottom)),
                   0, -90);
    }
    else
    {
        path.lineTo(right, bottom);
    }

    if (m_primitive->chamferedCorners().testFlag(L7::BottomLeftCorner))
    {
        path.lineTo(left + chamfer, bottom);
        path.lineTo(left, bottom + chamfer);
    }
    else if (m_primitive->roundedCorners().testFlag(L7::BottomLeftCorner))
    {
        path.lineTo(left + radius, bottom);
        path.arcTo(QRectF(QPointF(left, bottom  + 2*radius),
                          QPointF(left + 2*radius, bottom)),
                   270, -90);
    }
    else
    {
        path.lineTo(left, bottom);
    }

    path.closeSubpath();
    style->fillPath(&itemOption, painter, path);
}

LeGraphicsItem *GraphicsRectangleShapeItem::clone() const
{
    return nullptr;
}

void GraphicsRectangleShapeItem::addHandles()
{
    addHandle(TopRightHandleRole);
    addHandle(TopHandleRole);
    addHandle(TopLeftHandleRole);
    addHandle(BottomRightHandleRole);
    addHandle(BottomHandleRole);
    addHandle(BottomLeftHandleRole);
    addHandle(RightHandleRole);
    addHandle(LeftHandleRole);
    addHandle(CenterHandleRole);
}

void GraphicsRectangleShapeItem::documentObjectAboutToChangeProperty(const IDocumentObject *object,
                                                                     const QString &name,
                                                                     const QVariant &oldValue)
{
    Q_UNUSED(object)
    Q_UNUSED(oldValue)

    if (name == "size" || name == "cornerRadius" || name == "chamferSize"
            || name == "roundedCorners" || name == "chamferedCorners")
        prepareGeometryChange();
}

void GraphicsRectangleShapeItem::documentObjectPropertyChanged(const IDocumentObject *object,
                                                               const QString &name,
                                                               const QVariant &newValue)
{
    Q_UNUSED(object)

    if (name == "size" || name == "cornerRadius" || name == "chamferSize"
            || name == "roundedCorners" || name == "chamferedCorners")
        updateGeometry();
    else if (name == "position")
        setPos(newValue.toPointF());
    else if (name == "rotation")
        setRotation(newValue.toDouble());
    else if (name == "mirrored")
        setTransform(QTransform().scale(newValue.toBool() ? -1 : 1, 1));
}

QPointF GraphicsRectangleShapeItem::handlePositionChangeFilter(LeGraphicsHandleRole role,
                                                               const QPointF &value)
{
    switch (role)
    {

        case TopHandleRole:
        {
            m_primitive->setSize(QSizeF(m_primitive->size().width(), 2*qAbs(value.y())));
            return QPointF(0, qAbs(value.y()));
        }
        case BottomHandleRole:
        {
            m_primitive->setSize(QSizeF(m_primitive->size().width(), 2*qAbs(value.y())));
            return QPointF(0, -qAbs(value.y()));
        }
        case LeftHandleRole:
        {
            m_primitive->setSize(QSizeF(2*qAbs(value.x()), m_primitive->size().height()));
            return QPointF(-qAbs(value.x()), 0);
        }
        case RightHandleRole:
        {
            m_primitive->setSize(QSizeF(2*qAbs(value.x()), m_primitive->size().height()));
            return QPointF(qAbs(value.x()), 0);
        }
        case TopRightHandleRole:
        {
            m_primitive->setSize(QSizeF(2*qAbs(value.x()), 2*qAbs(value.y())));
            return QPointF(qAbs(value.x()), qAbs(value.y()));
        }
        case TopLeftHandleRole:
        {
            m_primitive->setSize(QSizeF(2*qAbs(value.x()), 2*qAbs(value.y())));
            return QPointF(-qAbs(value.x()), qAbs(value.y()));
        }
        case BottomRightHandleRole:
        {
            m_primitive->setSize(QSizeF(2*qAbs(value.x()), 2*qAbs(value.y())));
            return QPointF(qAbs(value.x()), -qAbs(value.y()));
        }
        case BottomLeftHandleRole:
        {
            m_primitive->setSize(QSizeF(2*qAbs(value.x()), 2*qAbs(value.y())));
            return QPointF(-qAbs(value.x()), -qAbs(value.y()));
        }
        case CenterHandleRole:
        {
            m_primitive->setPosition(mapToParent(value));
            return QPointF(0, 0);
        }
        default:
            return value;
    }
}
