#pragma once

#include "LeGraphicsView/LeGraphicsItem.h"

#include "LeIpc7351/IDocumentObjectListener.h"

class RegularPolygonPrimitive;
class LeGraphicsHandleItem;


class GraphicsRegularPolygonPrimitive : public LeGraphicsItem, public IDocumentObjectListener
{
    Q_OBJECT

public:
    GraphicsRegularPolygonPrimitive(RegularPolygonPrimitive *primitive);
    ~GraphicsRegularPolygonPrimitive();


private:
    void updateGeometry();
    void addHandles();
    RegularPolygonPrimitive *m_primitive;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // LeGraphicsItem interface
public:
    virtual LeGraphicsItem *clone() const override;
protected:
    virtual QPointF handlePositionChangeFilter(LeGraphicsHandleRole role, const QPointF &value) override;

    // IDocumentObjectListener interface
public:
    virtual void documentObjectAboutToChangeProperty(const IDocumentObject *object, const QString &name, const QVariant &oldValue) override;
    virtual void documentObjectPropertyChanged(const IDocumentObject *object, const QString &name, const QVariant &newValue) override;

};

