#include "GraphicsTrianglePrimitive.h"

#include "LeIpc7351/Primitives/TrianglePrimitive.h"

#include "LeGraphicsView/LeGraphicsHandleItem.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/LeGraphicsScene.h"

#include <QStyleOptionGraphicsItem>

GraphicsTrianglePrimitive::GraphicsTrianglePrimitive(TrianglePrimitive *primitive)
    : LeGraphicsItem(GftPad)
    , IDocumentObjectListener()
    , m_primitive(primitive)
{
    addHandles();
    updateGeometry();
    setPos(primitive->position());
    setRotation(primitive->rotation());
    setPos(primitive->position());
    setTransform(QTransform().scale(primitive->isMirrored() ? -1 : 1, 1));
    beginListeningToDocumentObject(primitive);
}

GraphicsTrianglePrimitive::~GraphicsTrianglePrimitive()
{
    endListeningToDocumentObject(m_primitive);
}

void GraphicsTrianglePrimitive::updateGeometry()
{
    handle(TopRightHandleRole)->setPos(boundingRect().bottomRight());
    handle(TopHandleRole)->setPos(QPointF(boundingRect().center().x(),
                                          boundingRect().bottom()));
    handle(TopLeftHandleRole)->setPos(boundingRect().bottomLeft());
    handle(LeftHandleRole)->setPos(QPointF(boundingRect().left(),
                                           boundingRect().center().y()));
    handle(RightHandleRole)->setPos(QPointF(boundingRect().right(),
                                            boundingRect().center().y()));
    handle(BottomRightHandleRole)->setPos(boundingRect().topRight());
    handle(BottomHandleRole)->setPos(QPointF(boundingRect().center().x(),
                                             boundingRect().top()));
    handle(BottomLeftHandleRole)->setPos(boundingRect().topLeft());
}

void GraphicsTrianglePrimitive::addHandles()
{
    addHandle(TopRightHandleRole);
    addHandle(TopHandleRole);
    addHandle(TopLeftHandleRole);
    addHandle(BottomRightHandleRole);
    addHandle(BottomHandleRole);
    addHandle(BottomLeftHandleRole);
    addHandle(RightHandleRole);
    addHandle(LeftHandleRole);
    addHandle(CenterHandleRole);
}

QRectF GraphicsTrianglePrimitive::boundingRect() const
{
    return QRectF(QPointF(-m_primitive->size().width()/2.0,
                          -m_primitive->size().height()/2.0),
                  m_primitive->size());
}

void GraphicsTrianglePrimitive::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget)

    GraphicsStyleOption itemOption;
    itemOption.exposedRect = option->exposedRect;
    itemOption.palette = layer()->graphicsPalette(featureType());
    itemOption.state = state();

    LeGraphicsStyle *style = graphicsScene()->graphicsStyle();

    QPainterPath path;
    const qreal halfWidth = m_primitive->size().width()/2.0;
    const qreal halfHeight = m_primitive->size().height()/2.0;
    path.moveTo(-halfWidth, -halfHeight);
    path.lineTo(0, halfHeight);
    path.lineTo(halfWidth, -halfHeight);
    path.closeSubpath();

    style->fillPath(&itemOption, painter, path);
}

LeGraphicsItem *GraphicsTrianglePrimitive::clone() const
{
    return nullptr;
}


void GraphicsTrianglePrimitive::documentObjectAboutToChangeProperty(const IDocumentObject *object, const QString &name, const QVariant &oldValue)
{
    Q_UNUSED(object)
    Q_UNUSED(oldValue)

    if (name == "size")
        prepareGeometryChange();
}

void GraphicsTrianglePrimitive::documentObjectPropertyChanged(const IDocumentObject *object, const QString &name, const QVariant &newValue)
{
    Q_UNUSED(object)

    if (name == "size")
        updateGeometry();
    else if (name == "position")
        setPos(newValue.toPointF());
    else if (name == "rotation")
        setRotation(newValue.toDouble());
    else if (name == "mirrored")
        setTransform(QTransform().scale(newValue.toBool() ? -1 : 1, 1));
}

QPointF GraphicsTrianglePrimitive::handlePositionChangeFilter(LeGraphicsHandleRole role, const QPointF &value)
{
    switch (role)
    {
        case TopHandleRole:
        {
            m_primitive->setSize(QSizeF(m_primitive->size().width(), 2*qAbs(value.y())));
            return QPointF(0, qAbs(value.y()));
        }
        case BottomHandleRole:
        {
            m_primitive->setSize(QSizeF(m_primitive->size().width(), 2*qAbs(value.y())));
            return QPointF(0, -qAbs(value.y()));
        }
        case LeftHandleRole:
        {
            m_primitive->setSize(QSizeF(2*qAbs(value.x()), m_primitive->size().height()));
            return QPointF(-qAbs(value.x()), 0);
        }
        case RightHandleRole:
        {
            m_primitive->setSize(QSizeF(2*qAbs(value.x()), m_primitive->size().height()));
            return QPointF(qAbs(value.x()), 0);
        }
        case TopRightHandleRole:
        {
            m_primitive->setSize(QSizeF(2*qAbs(value.x()), 2*qAbs(value.y())));
            return QPointF(qAbs(value.x()), qAbs(value.y()));
        }
        case TopLeftHandleRole:
        {
            m_primitive->setSize(QSizeF(2*qAbs(value.x()), 2*qAbs(value.y())));
            return QPointF(-qAbs(value.x()), qAbs(value.y()));
        }
        case BottomRightHandleRole:
        {
            m_primitive->setSize(QSizeF(2*qAbs(value.x()), 2*qAbs(value.y())));
            return QPointF(qAbs(value.x()), -qAbs(value.y()));
        }
        case BottomLeftHandleRole:
        {
            m_primitive->setSize(QSizeF(2*qAbs(value.x()), 2*qAbs(value.y())));
            return QPointF(-qAbs(value.x()), -qAbs(value.y()));
        }
        case CenterHandleRole:
        {
            // Move the parent to where the handle wants to go...
            m_primitive->setPosition(mapToParent(value));
            // And leave the handle where it should be
            return QPointF(0, 0);
        }
        default:
            return value;
    }
}
