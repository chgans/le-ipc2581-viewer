#pragma once

#include <QIcon>

namespace Gui
{

    static inline QIcon icon(const QString &name)
    {
        if (name.isEmpty())
            return QIcon();
        return QIcon::fromTheme(name, QIcon(QString(":/icons/%1.svg").arg(name)));
    }

}
