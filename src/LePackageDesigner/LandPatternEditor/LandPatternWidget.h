#pragma once

#include "EditorWidget.h"

#include "LeIpc7351/IDocumentObjectListener.h"

class AbstractTask;
class Document;
class LandPattern;

class LeGraphicsView;
class LeGraphicsScene;
class LeGraphicsItemLayer;

class QSettings;

class LandPatternWidget : public EditorWidget, public IDocumentObjectListener
{
    Q_OBJECT
public:
    explicit LandPatternWidget(QWidget *parent = 0);

signals:

public slots:
    void startTask(AbstractTask *task);
    void restoreGeometryAndState(QSettings &settings);
    void saveGeometryAndState(QSettings &settings);

    LeGraphicsScene *scene() const;
    LeGraphicsView *view() const;

private:
    void setupScene();
    void setupView();
    void addObject(IDocumentObject *object);
    void removeObject(IDocumentObject *object);

private:
    LeGraphicsScene *m_scene = nullptr;
    LeGraphicsView *m_view = nullptr;
    LeGraphicsItemLayer *m_mcadLayer = nullptr;

    MainWindow *m_gui = nullptr;
    Document *m_document = nullptr;
    LandPattern *m_landPattern = nullptr;

    // EditorWidget interface
public:
    virtual void edit(MainWindow *gui, Document *document, IDocumentObject *object) override;
    virtual IDocumentObject *object() const override;
    virtual Document *document() const override;
    virtual QList<AbstractTask *> tasks() const override;
    virtual QString title() const override;

    // IDocumentObjectListener interface
public:
    virtual void documentObjectAboutToBeInserted(IDocumentObject *parent,
                                                 IDocumentObject *child,
                                                 int index) override;
    virtual void documentObjectInserted(IDocumentObject *parent,
                                        IDocumentObject *child,
                                        int index) override;
    virtual void documentObjectAboutToBeRemoved(IDocumentObject *parent,
                                                IDocumentObject *child,
                                                int index) override;
    virtual void documentObjectRemoved(IDocumentObject *parent,
                                       IDocumentObject *child,
                                       int index) override;
    virtual void documentObjectPropertyChanged(const IDocumentObject *object,
                                               const QString &name,
                                               const QVariant &value) override;
};
