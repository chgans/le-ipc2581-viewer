#include "PlaceArrangementTask.h"

#include "QuadInLineArrangementItem.h"
#include "Gui.h"

#include "LeIpc7351/LeIpc7351.h"
#include "LeIpc7351/Document.h"

#include "LeIpc7351/Arrangements/QuadInLineArrangement.h"

#include "LeGraphicsView/LeGraphicsItem.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/LeGraphicsScene.h"
#include "LeGraphicsView/LeGraphicsView.h"

#include "qtpropertybrowser/qtvariantproperty.h"
#include "qtpropertybrowser/qttreepropertybrowser.h"


#include <QAction>
#include <QDebug>
#include <QGraphicsSceneEvent>
#include <QMouseEvent>

PlaceArrangementTask::PlaceArrangementTask(QObject *parent)
    : AbstractTask(parent)
    , LeGraphicsSceneEventFilter()
{
    setTile("XYZ arrangement");
    setDescription("Place an XYZ arrangement");
    setIcon(Gui::icon("arrangement-array"));
    setShortcut(QKeySequence("p,a,a"));
}

void PlaceArrangementTask::start()
{    
    setVCount(0);
    setVPitch(0);
    setVSpan(0.0);
    setHCount(0);
    setHPitch(0.0);
    setHSpan(0.0);
    setNumberingPrefix("");
    //scene()->setEventFilter(this);
    emit started(this);
}

void PlaceArrangementTask::accept()
{
//    if (m_item != nullptr)
//    {
//        scene()->activeFeatureLayer()->removeFromLayer(m_item);
//        m_arrangement = m_item->arrangement();
//        // get the
//        delete m_item;
//    }
//    m_item = nullptr;
//    scene()->setEventFilter(nullptr);
    emit accepted(this);
}

void PlaceArrangementTask::reject()
{
    //scene()->setEventFilter(nullptr);
    //delete m_arrangementItem;
//    m_arrangementItem = nullptr;
//    delete m_arrangement;
//    m_arrangement = nullptr;
    emit rejected(this);
}

void PlaceArrangementTask::setVSpan(qreal vSpan)
{
    m_arrangement->setSize(QSizeF(m_arrangement->size().width(),
                                  vSpan));
    //m_arrangement->calculate();
    //m_arrangementItem->synchronise();
    emit vSpanChanged(m_arrangement->size().height());
}

void PlaceArrangementTask::setVCount(int vCount)
{
    m_arrangement->setTerminalCount(vCount);
    //m_arrangement->calculate();
    //m_arrangementItem->synchronise();
    emit vCountChanged(m_arrangement->terminalCount());
}

void PlaceArrangementTask::setVPitch(qreal vPitch)
{
    m_arrangement->setTerminalPitch(vPitch);
    //m_arrangement->calculate();
    //m_arrangementItem->synchronise();
    emit vPitchChanged(m_arrangement->terminalPitch());
}

void PlaceArrangementTask::setHSpan(qreal hSpan)
{
    m_arrangement->setSize(QSizeF(hSpan,
                                  m_arrangement->size().height()));
    //m_arrangement->calculate();
    ///m_arrangementItem->synchronise();
    emit hSpanChanged(m_arrangement->size().width());
}

void PlaceArrangementTask::setHCount(int hCount)
{
    m_arrangement->setTerminalCount(hCount);
    //m_arrangement->calculate();
    //m_arrangementItem->synchronise();
    emit hCountChanged(m_arrangement->terminalCount());
}

void PlaceArrangementTask::setHPitch(qreal hPitch)
{
    m_arrangement->setTerminalPitch(hPitch);
    //m_arrangement->calculate();
    //m_arrangementItem->synchronise();
    emit hPitchChanged(m_arrangement->terminalPitch());
}

void PlaceArrangementTask::setNumberingPrefix(QString numberingPrefix)
{
    m_arrangement->setNamingPrefix(numberingPrefix);
    //m_arrangement->calculate();
    //m_arrangementItem->synchronise();
    emit numberingPrefixChanged(m_arrangement->namingPrefix());
}

void PlaceArrangementTask::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(painter)
    Q_UNUSED(rect)
}

void PlaceArrangementTask::drawForeground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect)

//    QLineF line(m_arrangementItem->pos(),
//                QPointF(qMax(m_lastMouseScenePos.x(), m_arrangement->size().width()/2.0),
//                        qMax(m_lastMouseScenePos.y(), m_arrangement->size().height()/2.0)));

//    painter->setBrush(Qt::NoBrush);
//    painter->setPen(QPen(scene()->graphicsPalette().color(LeGraphicsPalette::Highlight), 0));
//    painter->drawLine(line);
}

bool PlaceArrangementTask::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
    if (event->button() != Qt::LeftButton)
        return false;
    event->accept();
    accept();
    return true;
}

bool PlaceArrangementTask::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_arrangement == nullptr)
        return false;

//    m_lastMouseScenePos = event->scenePos();

//    QLineF line(m_arrangementItem->pos(), m_lastMouseScenePos);
//    setHSpan(qAbs(2*line.dx()));
//    setVSpan(qAbs(2*line.dy()));

//    m_arrangement->calculate();
//    m_arrangementItem->synchronise();

    return true;
}

bool PlaceArrangementTask::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
    return true;
}

bool PlaceArrangementTask::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
    return true;
}

qreal PlaceArrangementTask::vSpan() const
{
    return m_arrangement->size().height();
}

int PlaceArrangementTask::vCount() const
{
    return m_arrangement->terminalCount();
}

qreal PlaceArrangementTask::vPitch() const
{
    return m_arrangement->terminalPitch();
}

qreal PlaceArrangementTask::hSpan() const
{
    return m_arrangement->size().width();
}

int PlaceArrangementTask::hCount() const
{
    return m_arrangement->terminalCount();
}

qreal PlaceArrangementTask::hPitch() const
{
    return m_arrangement->terminalPitch();
}

QString PlaceArrangementTask::numberingPrefix() const
{
    return m_arrangement->namingPrefix();
}

#define DEBUG() qDebug() << __PRETTY_FUNCTION__

bool PlaceArrangementTask::keyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event)
    DEBUG() << event->key();
    return false;
}


bool PlaceArrangementTask::keyReleaseEvent(QKeyEvent *event)
{
    Q_UNUSED(event)
    DEBUG() << event->key();
    if (event->key() == Qt::Key_Cancel)
    {
        reject();
        return true;
    }
    return false;
}
