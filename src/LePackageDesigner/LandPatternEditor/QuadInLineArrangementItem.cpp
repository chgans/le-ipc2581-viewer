#include "QuadInLineArrangementItem.h"

#include "LeIpc7351/Arrangements/QuadInLineArrangement.h"

#include "LeGraphicsView/LeGraphicsHandleItem.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/LeGraphicsScene.h"

#include <QDebug>
#include <QStyleOptionGraphicsItem>

QuadInLineArrangementItem::QuadInLineArrangementItem(QuadInLineArrangement *arrangement)
    : LeGraphicsItem(GftNone)
    , m_arrangement(arrangement)
{
    beginListeningToDocumentObject(arrangement);
    //setFlag(ItemHasNoContents);
    setEnabled(true);
    updateGeometry();
}

void QuadInLineArrangementItem::updateGeometry()
{

}

void QuadInLineArrangementItem::addHandles()
{
    addHandle(TopRightHandleRole);
    addHandle(TopHandleRole);
    addHandle(TopLeftHandleRole);
    addHandle(BottomRightHandleRole);
    addHandle(BottomHandleRole);
    addHandle(BottomLeftHandleRole);
    addHandle(RightHandleRole);
    addHandle(LeftHandleRole);
    addHandle(CenterHandleRole);
}


QRectF QuadInLineArrangementItem::boundingRect() const
{
    return QRectF(-m_arrangement->size().width()/2.0, -m_arrangement->size().height()/2.0,
                  m_arrangement->size().width(), m_arrangement->size().height());
}

void QuadInLineArrangementItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget)

    GraphicsStyleOption itemOption;
    itemOption.exposedRect = option->exposedRect;
    itemOption.palette = layer()->graphicsPalette(featureType());
    itemOption.state = state();
    LeGraphicsStyle *style = graphicsScene()->graphicsStyle();
    style->drawRect(&itemOption, painter, boundingRect());
}

void QuadInLineArrangementItem::documentObjectAboutToChangeProperty(const IDocumentObject *object, const QString &name, const QVariant &oldValue)
{
    Q_UNUSED(object)
    Q_UNUSED(name)
    Q_UNUSED(oldValue)
}

void QuadInLineArrangementItem::documentObjectPropertyChanged(const IDocumentObject *object, const QString &name, const QVariant &newValue)
{
    Q_UNUSED(object)
    Q_UNUSED(name)
    Q_UNUSED(newValue)
}

LeGraphicsItem *QuadInLineArrangementItem::clone() const
{
    return nullptr;
}

QPointF QuadInLineArrangementItem::handlePositionChangeFilter(LeGraphicsHandleRole role, const QPointF &value)
{
    switch (role)
    {
        case TopHandleRole:
        {
            m_arrangement->setSize(QSizeF(m_arrangement->size().width(), 2*qAbs(value.y())));
            return QPointF(0, qAbs(value.y()));
        }
        case BottomHandleRole:
        {
            m_arrangement->setSize(QSizeF(m_arrangement->size().width(), 2*qAbs(value.y())));
            return QPointF(0, -qAbs(value.y()));
        }
        case LeftHandleRole:
        {
            m_arrangement->setSize(QSizeF(2*qAbs(value.x()), m_arrangement->size().height()));
            return QPointF(-qAbs(value.x()), 0);
        }
        case RightHandleRole:
        {
            m_arrangement->setSize(QSizeF(2*qAbs(value.x()), m_arrangement->size().height()));
            return QPointF(qAbs(value.x()), 0);
        }
        case TopRightHandleRole:
        {
            m_arrangement->setSize(QSizeF(2*qAbs(value.x()), 2*qAbs(value.y())));
            return QPointF(qAbs(value.x()), qAbs(value.y()));
        }
        case TopLeftHandleRole:
        {
            m_arrangement->setSize(QSizeF(2*qAbs(value.x()), 2*qAbs(value.y())));
            return QPointF(-qAbs(value.x()), qAbs(value.y()));
        }
        case BottomRightHandleRole:
        {
            m_arrangement->setSize(QSizeF(2*qAbs(value.x()), 2*qAbs(value.y())));
            return QPointF(qAbs(value.x()), -qAbs(value.y()));
        }
        case BottomLeftHandleRole:
        {
            m_arrangement->setSize(QSizeF(2*qAbs(value.x()), 2*qAbs(value.y())));
            return QPointF(-qAbs(value.x()), -qAbs(value.y()));
        }
        case CenterHandleRole:
        {
            // Move the parent to where the handle wants to go...
            // FIXME: m_arrangement->place(mapToParent(value));
            setPos(mapToParent(value));
            // And leave the handle where it should be
            return QPointF(0, 0);
        }
        default:
            return value;
    }
}
