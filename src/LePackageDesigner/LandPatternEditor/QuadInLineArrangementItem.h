#pragma once

#include "LeIpc7351/IDocumentObjectListener.h"

#include "LeGraphicsView/LeGraphicsItem.h"

class QuadInLineArrangement;

class QuadInLineArrangementItem: public LeGraphicsItem, public IDocumentObjectListener
{
    Q_OBJECT

public:
    enum
    {
        Type = UserType + 0x2001
    };

    explicit QuadInLineArrangementItem(QuadInLineArrangement *arrangement);
    QuadInLineArrangement *arrangement() const;

private:
    void updateGeometry();
    void addHandles();

private:
    QuadInLineArrangement *m_arrangement;
    LeGraphicsItem *m_item;

    // QGraphicsItem interface
public:
    virtual int type() const override { return Type; }
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // IDocumentObjectListener interface
public:
    virtual void documentObjectAboutToChangeProperty(const IDocumentObject *object, const QString &name, const QVariant &oldValue) override;
    virtual void documentObjectPropertyChanged(const IDocumentObject *object, const QString &name, const QVariant &newValue) override;

    // LeGraphicsItem interface
public:
    virtual LeGraphicsItem *clone() const override;
protected:
    virtual QPointF handlePositionChangeFilter(LeGraphicsHandleRole role, const QPointF &value) override;
};
