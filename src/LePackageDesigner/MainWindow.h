#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QMap>

class IDocumentObject;

class AbstractTask;
class Document;
class DocumentWidget;
class DocumentObjectInspector;
class EditorWidget;
class GraphicsLayerStackModel;
class LandPatternWidget;
class PadStackWidget;
class TaskWidget;

class QAction;
class QActionGroup;
class QStackedWidget;
class QTableView;
class QToolBar;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


    QAction *action(const QString &id) const;
    QMenu *menu(const QString &id) const;
    DocumentObjectInspector* objectInspector() const;

public slots:
    void openEditor(IDocumentObject *object);
    void enableAction(const QString &id, bool enable = true);
    void disableAction(const QString &id, bool disable = true);

private:
    void createMenus();
    void createActions();
    void createToolBars();
    void createDummyDocument();
    void createDockWidgets();
    void createCentralWidget();
    void populateMenus();
    void connectActions();
    void registerMetaTypes();

    Document *currentDocument() const;
    void setCurrentDocument(Document *doc);

    EditorWidget *currentEditor() const;
    IDocumentObject *currentObject() const;

    void setCurrentEditor(EditorWidget *editor);
    void activateEditor(EditorWidget *editor);
    void desactivateEditor(EditorWidget *editor);

    void onTaskStarted(AbstractTask *task);
    void onTaskFinished(AbstractTask *task);

private slots:
    void restoreGui();
    void restoreGeometryAndState();
    void saveGeometryAndState();

    void newDocumentRequested();
    void openDocumentRequested();
    void saveDocumentRequested();
    void saveDocumentAsRequested();
    void closeDocumentRequested();

private:
    QMap<QString, QMenu*> m_menuMap;
    QMap<QString, QAction*> m_actionMap;

    QToolBar *m_taskToolBar = nullptr;

    QStackedWidget *m_editorStackedWidget = nullptr;
    QMap<IDocumentObject*, EditorWidget*> m_editorForObject;
    Document *m_document = nullptr;

    QDockWidget *m_taskDockWidget = nullptr;
    TaskWidget *m_taskWidget = nullptr;
    QDockWidget *m_documentDockWidget = nullptr;
    DocumentWidget *m_documentWidget = nullptr;
    QDockWidget *m_documentObjectInspectorDockWidget = nullptr;
    DocumentObjectInspector *m_documentObjectInspector = nullptr;

    // QWidget interface
    void updateDocumentActions();

protected:
    virtual void keyPressEvent(QKeyEvent *event) override;
    virtual void keyReleaseEvent(QKeyEvent *event) override;
    virtual void focusInEvent(QFocusEvent *event) override;
    virtual void focusOutEvent(QFocusEvent *event) override;
    virtual void enterEvent(QEvent *event) override;
    virtual void leaveEvent(QEvent *event) override;
    virtual void closeEvent(QCloseEvent *event) override;
};

#endif // MAINWINDOW_H
