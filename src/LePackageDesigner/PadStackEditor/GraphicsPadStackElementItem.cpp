#include "GraphicsPadStackElementItem.h"

#include "GraphicsRectangularShapeItem.h"
#include "GraphicsCircularShapeItem.h"

#include "LeIpc7351/LeIpc7351.h"
#include "LeIpc7351/PadStackElement.h"
#include "LeIpc7351/Primitives/CirclePrimitive.h"
#include "LeIpc7351/Primitives/RectanglePrimitive.h"

#include "LeGraphicsView/LeGraphicsHandleItem.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/LeGraphicsScene.h"

#include <QDebug>
#include <QStyleOptionGraphicsItem>

GraphicsPadStackElementItem::GraphicsPadStackElementItem(PadStackElement *element)
    : LeGraphicsItem(GftPad)
    , m_element(element)
{
    beginListeningToDocumentObject(element);
    setFlag(ItemHasNoContents);
    setEnabled(true);
    updateGeometry();
}

PadStackElement *GraphicsPadStackElementItem::element() const
{
    return m_element;
}

void GraphicsPadStackElementItem::updateGeometry()
{
    if (m_element->primitive() == nullptr)
        return;

    switch (m_element->primitive()->objectType())
    {
        case CirclePrimitive::Type:
        {
            auto primitive = qobject_cast<CirclePrimitive*>(m_element->primitive());
            m_item = new GraphicsCircularShapeItem(primitive);
            m_item->setParentItem(this);
            break;
        }
        case RectanglePrimitive::Type:
        {
            auto primitive = qobject_cast<RectanglePrimitive*>(m_element->primitive());
            m_item = new GraphicsRectangleShapeItem(primitive);
            m_item->setParentItem(this);
            break;
        }
        default:
            qWarning() << "GraphicsPadStackElementItem::updateGeometry: Unknown primitive type: "
                       <<  m_element->primitive()->objectType();
    }
}

LeGraphicsItem *GraphicsPadStackElementItem::clone() const
{
    return nullptr;
}

void GraphicsPadStackElementItem::documentObjectAboutToChangeProperty(const IDocumentObject *object,
                                                                      const QString &name,
                                                                      const QVariant &oldValue)
{
    Q_UNUSED(object)
    Q_UNUSED(name)
    Q_UNUSED(oldValue)

//    if (name =="primitive")
//        delete m_item;
}

void GraphicsPadStackElementItem::documentObjectPropertyChanged(const IDocumentObject *object,
                                                                const QString &name,
                                                                const QVariant &newValue)
{
    Q_UNUSED(object)
    Q_UNUSED(newValue)

    if (name == "primitive")
    {
        updateGeometry();
    }
}


QRectF GraphicsPadStackElementItem::boundingRect() const
{
    return QRectF();
}

void GraphicsPadStackElementItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter)
    Q_UNUSED(option)
    Q_UNUSED(widget)
}


QVariant GraphicsPadStackElementItem::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemSelectedHasChanged && m_item != nullptr)
        m_item->setSelected(isSelected());
    return LeGraphicsItem::itemChange(change, value);
}
