#pragma once

#include "LeGraphicsView/LeGraphicsItem.h"

#include "LeIpc7351/IDocumentObjectListener.h"

class LeGraphicsHandleItem;

class PadStackElement;

class GraphicsPadStackElementItem: public LeGraphicsItem, public IDocumentObjectListener
{
    Q_OBJECT

public:
    enum
    {
        Type = UserType + 0x1001
    };

    explicit GraphicsPadStackElementItem(PadStackElement *element);
    PadStackElement *element() const;

private:
    void updateGeometry();

private:
    PadStackElement *m_element;
    LeGraphicsItem *m_item;

    // QGraphicsItem interface
public:
    virtual int type() const override { return Type; }
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;    
protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

    // LeGraphicsItem interface
public:
    virtual LeGraphicsItem *clone() const override;

    // IDocumentObjectListener interface
public:
    virtual void documentObjectAboutToChangeProperty(const IDocumentObject *object, const QString &name, const QVariant &oldValue) override;
    virtual void documentObjectPropertyChanged(const IDocumentObject *object, const QString &name, const QVariant &newValue) override;   

};
