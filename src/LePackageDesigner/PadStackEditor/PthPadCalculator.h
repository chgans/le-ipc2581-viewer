#ifndef PTHPADCALCULATOR_H
#define PTHPADCALCULATOR_H

#include <QObject>

class PthPadCalculator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qreal leadThicknessMax READ leadThicknessMax WRITE setLeadThicknessMax NOTIFY leadThicknessMaxChanged)
    Q_PROPERTY(qreal holeOverLead READ holeOverLead WRITE setHoleOverLead NOTIFY holeOverLeadChanged)
    Q_PROPERTY(qreal padToHoleRatio READ padToHoleRatio WRITE setPadToHoleRatio NOTIFY padToHoleRatioChanged)
    Q_PROPERTY(qreal minimalAnularRing READ minimalAnularRing WRITE setMinimalAnularRing NOTIFY minimalAnularRingChanged)
    Q_PROPERTY(qreal thermalIdOverHole READ thermalIdOverHole WRITE setThermalIdOverHole NOTIFY thermalIdOverHoleChanged)
    Q_PROPERTY(qreal minimalThermalOdOverId READ minimalThermalOdOverId WRITE setMinimalThermalOdOverId NOTIFY minimalThermalOdOverIdChanged)
    Q_PROPERTY(qreal thermalOdToHoleRatio READ thermalOdToHoleRatio WRITE setThermalOdToHoleRatio NOTIFY ThermalOdToHoleRatioChanged)
    Q_PROPERTY(qreal spokeWidthToQuarterOdPercent READ spokeWidthToQuarterOdPercent WRITE setSpokeWidthToQuarterOdPercent NOTIFY spokeWidthToQuarterOdPercentChanged)
    Q_PROPERTY(qreal roundOff READ roundOff WRITE setRoundOff NOTIFY roundOffChanged)

    Q_PROPERTY(qreal holeDiameter READ holeDiameter NOTIFY holeDiameterChanged)
    Q_PROPERTY(qreal regularPadDiameter READ regularPadDiameter NOTIFY regularPadDiameterChanged)
    Q_PROPERTY(qreal thermalPadInnderDiameter READ thermalPadInnderDiameter NOTIFY thermalPadInnderDiameterChanged)
    Q_PROPERTY(qreal thermalPadOuterDiameter READ thermalPadOuterDiameter NOTIFY thermalPadOuterDiameterChanged)
    Q_PROPERTY(qreal thermalPadSpokeWidth READ thermalPadSpokeWidth NOTIFY thermalPadSpokeWidthChanged)
    Q_PROPERTY(qreal antiPadDiameter READ antiPadDiameter NOTIFY antiPadDiameterChanged)

    qreal m_leadThicknessMax;
    qreal m_holeOverLead;
    qreal m_padToHoleRatio;
    qreal m_minimalAnularRing;
    qreal m_thermalIdOverHole;
    qreal m_minimalThermalOdOverId;
    qreal m_thermalOdToHoleRatio;
    qreal m_spokeWidthToQuarterOdPercent;
    qreal m_roundOff;

    qreal m_holeDiameter;
    qreal m_regularPadDiameter;
    qreal m_thermalPadInnderDiameter;
    qreal m_thermalPadOuterDiameter;
    qreal m_thermalPadSpokeWidth;
    qreal m_antiPadDiameter;

public:
    explicit PthPadCalculator(QObject *parent = 0);
    ~PthPadCalculator();

    qreal leadThicknessMax() const;
    qreal holeOverLead() const;
    qreal padToHoleRatio() const;
    qreal minimalAnularRing() const;
    qreal thermalIdOverHole() const;
    qreal minimalThermalOdOverId() const;
    qreal thermalOdToHoleRatio() const;
    qreal spokeWidthToQuarterOdPercent() const;
    qreal roundOff() const;

    qreal holeDiameter() const;
    qreal regularPadDiameter() const;
    qreal thermalPadInnderDiameter() const;
    qreal thermalPadOuterDiameter() const;
    qreal thermalPadSpokeWidth() const;
    qreal antiPadDiameter() const;

signals:
    void leadThicknessMaxChanged(qreal leadThicknessMax);
    void holeOverLeadChanged(qreal holeOverLead);
    void padToHoleRatioChanged(qreal padToHoleRatio);
    void minimalAnularRingChanged(qreal minimalAnularRing);
    void thermalIdOverHoleChanged(qreal thermalIdOverHole);
    void minimalThermalOdOverIdChanged(qreal minimalThermalOdOverId);
    void ThermalOdToHoleRatioChanged(qreal thermalOdToHoleRatio);
    void spokeWidthToQuarterOdPercentChanged(qreal spokeWidthToQuarterOdPercent);
    void roundOffChanged(qreal roundOff);

    void holeDiameterChanged(qreal holeDiameter);
    void regularPadDiameterChanged(qreal regularPadDiameter);
    void thermalPadInnderDiameterChanged(qreal thermalPadInnderDiameter);
    void thermalPadOuterDiameterChanged(qreal thermalPadOuterDiameter);
    void thermalPadSpokeWidthChanged(qreal thermalPadSpokeWidth);
    void antiPadDiameterChanged(qreal antiPadDiameter);

public slots:
    void setLeadThicknessMax(qreal leadThicknessMax);
    void setHoleOverLead(qreal holeOverLead);
    void setPadToHoleRatio(qreal padToHoleRatio);
    void setMinimalAnularRing(qreal minimalAnularRing);
    void setThermalIdOverHole(qreal thermalIdOverHole);
    void setMinimalThermalOdOverId(qreal minimalThermalOdOverId);
    void setThermalOdToHoleRatio(qreal thermalOdToHoleRatio);
    void setSpokeWidthToQuarterOdPercent(qreal spokeWidthToQuarterOdPercent);
    void setRoundOff(qreal roundOff);

    void clear();
    bool calculate();

private:
    void setHoleDiameter(qreal holeDiameter);
    void setRegularPadDiameter(qreal regularPadDiameter);
    void setThermalPadInnderDiameter(qreal thermalPadInnderDiameter);
    void setThermalPadOuterDiameter(qreal thermalPadOuterDiameter);
    void setThermalPadSpokeWidth(qreal thermalPadSpokeWidth);
    void setAntiPadDiameter(qreal antiPadDiameter);
    static inline qreal roundTo(qreal value, qreal round)
    {
        return qRound(value/round)*round;
    }
};

#endif // PTHPADCALCULATOR_H
