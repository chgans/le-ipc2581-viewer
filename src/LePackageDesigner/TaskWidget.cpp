#include "TaskWidget.h"

#include "AbstractTask.h"
#include "ObjectPropertyBrowser.h"
#include "Gui.h"

#include "qtpropertybrowser/qttreepropertybrowser.h"
#include "qtpropertybrowser/qtvariantproperty.h"

#include <QDebug>
#include <QMetaObject>
#include <QMetaProperty>
#include <QTreeView>
#include <QPushButton>
#include <QVBoxLayout>

TaskWidget::TaskWidget(QWidget *parent) : QWidget(parent)
{
    m_acceptButton = new QPushButton(Gui::icon("dialog-accept"), "&Ok");
    m_rejectButton = new QPushButton(Gui::icon("dialog-cancel"), "&Cancel");
    auto buttonsLayout = new QHBoxLayout();
    buttonsLayout->addStretch();
    buttonsLayout->addWidget(m_acceptButton);
    buttonsLayout->addWidget(m_rejectButton);
    buttonsLayout->addStretch();
    auto buttonBox = new QWidget();
    buttonBox->setLayout(buttonsLayout);

    setLayout(new QVBoxLayout);
    layout()->addWidget(buttonBox);
    m_propertyBrowser = new ObjectPropertyBrowser();
    layout()->addWidget(m_propertyBrowser);

    setTabOrder(m_propertyBrowser, m_acceptButton);
    setTabOrder(m_acceptButton, m_rejectButton);
}

void TaskWidget::setTask(AbstractTask *task)
{
    m_task = task;

    if (m_task == nullptr)
        return;

    connect(m_task, &AbstractTask::accepted,
            this, &TaskWidget::clearCurrentTask);
    connect(m_task, &AbstractTask::rejected,
            this, &TaskWidget::clearCurrentTask);

    connect(m_acceptButton, &QPushButton::clicked,
            m_task, &AbstractTask::accept);
    connect(m_rejectButton, &QPushButton::clicked,
            m_task, &AbstractTask::reject);

    m_propertyBrowser->setObject(m_task);
}

void TaskWidget::clearCurrentTask()
{
    m_acceptButton->disconnect(m_task);
    m_rejectButton->disconnect(m_task);
    m_propertyBrowser->setObject(nullptr);
    m_task = nullptr;
}

#include <QDebug>
#include <QKeyEvent>
#include <QFocusEvent>
#define DEBUG() qDebug() << __PRETTY_FUNCTION__

void TaskWidget::keyPressEvent(QKeyEvent *event)
{
    DEBUG() << event->key();
    QWidget::keyPressEvent(event);
}

void TaskWidget::keyReleaseEvent(QKeyEvent *event)
{
    DEBUG() << event->key();
    QWidget::keyReleaseEvent(event);
}

void TaskWidget::focusInEvent(QFocusEvent *event)
{
    DEBUG() << event->reason();
    //QWidget::focusInEvent(event); // what about this?
    m_propertyBrowser->setFocus();
}

void TaskWidget::focusOutEvent(QFocusEvent *event)
{
    DEBUG();
    QWidget::focusOutEvent(event);
}

void TaskWidget::enterEvent(QEvent *event)
{
    DEBUG();
    QWidget::enterEvent(event);
}

void TaskWidget::leaveEvent(QEvent *event)
{
    DEBUG();
    QWidget::leaveEvent(event);
}

void TaskWidget::actionEvent(QActionEvent *event)
{
    DEBUG();
    QWidget::actionEvent(event);
}
