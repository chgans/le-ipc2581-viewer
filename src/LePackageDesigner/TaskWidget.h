#ifndef TASKWIDGET_H
#define TASKWIDGET_H

#include <QWidget>

#include <QMap>

class AbstractTask;
class ObjectPropertyBrowser;

class QPushButton;

class TaskWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TaskWidget(QWidget *parent = 0);

public slots:
    void setTask(AbstractTask *task);

private:
    void clearCurrentTask();

private:
    AbstractTask *m_task = nullptr;
    QPushButton *m_acceptButton = nullptr;
    QPushButton *m_rejectButton = nullptr;
    ObjectPropertyBrowser *m_propertyBrowser = nullptr;

    // QWidget interface
protected:
    virtual void keyPressEvent(QKeyEvent *event) override;
    virtual void keyReleaseEvent(QKeyEvent *event) override;
    virtual void focusInEvent(QFocusEvent *event) override;
    virtual void focusOutEvent(QFocusEvent *event) override;
    virtual void enterEvent(QEvent *event) override;
    virtual void leaveEvent(QEvent *event) override;
    virtual void actionEvent(QActionEvent *event) override;
};

#endif // TASKWIDGET_H
