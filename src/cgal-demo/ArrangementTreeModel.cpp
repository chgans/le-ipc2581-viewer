#include "ArrangementTreeModel.h"

using namespace LGA;

ArrangementTreeModel::ArrangementTreeModel(QObject *parent)
    : QAbstractItemModel(parent)
    , ArrangementObserver()
{
}

void ArrangementTreeModel::setArrangement(Arrangement *arrangement)
{
    beginResetModel();

    if (m_arrangementItem != nullptr)
    {
        detach();
        delete m_arrangementItem;
    }
    if (arrangement != nullptr)
    {
        m_arrangementItem = new ArrangementItem(arrangement);
        attach(*arrangement);
    }

    endResetModel();
}

QVariant ArrangementTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section);

    if (orientation != Qt::Horizontal)
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    return "Label";
}

QModelIndex ArrangementTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    ArrangementTreeItemBase *parentItem;

    if (!parent.isValid())
        parentItem = m_arrangementItem;
    else
        parentItem = static_cast<ArrangementTreeItemBase*>(parent.internalPointer());

    ArrangementTreeItemBase *childItem = parentItem->child(row);
    if (childItem != nullptr)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex ArrangementTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    ArrangementTreeItemBase *childItem = static_cast<ArrangementTreeItemBase*>(index.internalPointer());
    ArrangementTreeItemBase *parentItem = childItem->parent();

    if (parentItem == m_arrangementItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int ArrangementTreeModel::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0)
        return 0;

    ArrangementTreeItemBase *parentItem;
    if (!parent.isValid())
        parentItem = m_arrangementItem;
    else
        parentItem = static_cast<ArrangementTreeItemBase*>(parent.internalPointer());

    return parentItem->childCount();
}

int ArrangementTreeModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 1;
}

QVariant ArrangementTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    ArrangementTreeItemBase *item = static_cast<ArrangementTreeItemBase*>(index.internalPointer());
    return item->label();
}

void ArrangementTreeModel::before_create_vertex(const Point &p)
{
    Q_UNUSED(p);
    auto parent = m_arrangementItem->verticesItem;
    const QModelIndex modelIndex = createIndex(parent->row(), 0,
                                               parent);
    beginInsertRows(modelIndex,
                    parent->childCount(),
                    parent->childCount());
}

void ArrangementTreeModel::after_create_vertex(Vertex_handle v)
{
    auto parent = m_arrangementItem->verticesItem;
    const QModelIndex modelIndex = createIndex(parent->row(), 0,
                                               parent);
    parent->addVertex(v);
    endInsertRows();
    dataChanged(modelIndex, modelIndex);
}

void ArrangementTreeModel::before_create_edge(const Segment &c, Vertex_handle v1, Vertex_handle v2)
{
    Q_UNUSED(c);
    Q_UNUSED(v1);
    Q_UNUSED(v2);

    auto parent = m_arrangementItem->edgesItem;
    const QModelIndex modelIndex = createIndex(parent->row(), 0,
                                               parent);
    beginInsertRows(modelIndex,
                    parent->childCount(),
                    parent->childCount()+1);
}

void ArrangementTreeModel::after_create_edge(Halfedge_handle e)
{
    auto parent = m_arrangementItem->edgesItem;
    const QModelIndex modelIndex = createIndex(parent->row(), 0,
                                               parent);
    parent->addEdge(e);
    parent->addEdge(e->twin());
    endInsertRows();
    dataChanged(modelIndex, modelIndex);
}

void ArrangementTreeModel::before_split_edge(Halfedge_handle e, Vertex_handle v, const Segment &c1, const Segment &c2)
{
    Q_UNUSED(v);
    Q_UNUSED(c1);
    Q_UNUSED(c2);

    auto parent = m_arrangementItem->edgesItem;
    const QModelIndex modelIndex = createIndex(parent->row(), 0,
                                               parent);
    int row = parent->childRow(e);
    row -= row % 2;
    beginRemoveRows(modelIndex,
                    row+0,
                    row+1);
    parent->removeEdge(e);
    parent->removeEdge(e->twin());
}

void ArrangementTreeModel::after_split_edge(Halfedge_handle e1, Halfedge_handle e2)
{
    endRemoveRows();

    auto parent = m_arrangementItem->edgesItem;
    const QModelIndex modelIndex = createIndex(parent->row(), 0,
                                               parent);
    beginInsertRows(modelIndex,
                    parent->childCount()+0,
                    parent->childCount()+3);
    parent->addEdge(e1);
    parent->addEdge(e1->twin());
    parent->addEdge(e2);
    parent->addEdge(e2->twin());
    endInsertRows();
    dataChanged(modelIndex, modelIndex);
}

void ArrangementTreeModel::before_split_face(Face_handle f, Halfedge_handle e)
{
    Q_UNUSED(f);
    Q_UNUSED(e);

    auto parent = m_arrangementItem->facesItem;
    const QModelIndex modelIndex = createIndex(parent->row(), 0,
                                               parent);
    beginInsertRows(modelIndex,
                    parent->childCount(),
                    parent->childCount());
}

void ArrangementTreeModel::after_split_face(Face_handle f1, Face_handle f2, bool is_hole)
{
    Q_UNUSED(f1);
    Q_UNUSED(is_hole);

    auto parent = m_arrangementItem->facesItem;
    const QModelIndex modelIndex = createIndex(parent->row(), 0,
                                               parent);
    parent->addFace(f2);
    endInsertRows();
    dataChanged(modelIndex, modelIndex);
}

void ArrangementTreeModel::before_merge_edge(Halfedge_handle e1, Halfedge_handle e2, const Segment &c)
{
    Q_UNUSED(e1);
    Q_UNUSED(e2);
    Q_UNUSED(c);
    Q_UNIMPLEMENTED();
}

void ArrangementTreeModel::after_merge_edge(Halfedge_handle e)
{
    Q_UNUSED(e);
    Q_UNIMPLEMENTED();
}

void ArrangementTreeModel::before_merge_face(Face_handle f1, Face_handle f2, Halfedge_handle e)
{
    auto parent = m_arrangementItem->facesItem;
    const QModelIndex modelIndex = createIndex(parent->row(), 0,
                                               parent);
    int row = parent->childRow(f1);
    beginRemoveRows(modelIndex,
                    row,
                    row);
    parent->removeFace(f1);
    endInsertRows();
    row = parent->childRow(f2);
    beginRemoveRows(modelIndex,
                    row,
                    row);
    parent->removeFace(f2);
    endInsertRows();    dataChanged(modelIndex, modelIndex);
}

void ArrangementTreeModel::after_merge_face(Face_handle f)
{
    auto parent = m_arrangementItem->facesItem;
    const QModelIndex modelIndex = createIndex(parent->row(), 0,
                                               parent);
    beginInsertRows(modelIndex,
                    parent->childCount(),
                    parent->childCount());
    parent->addFace(f);
    endInsertRows();
}

void ArrangementTreeModel::before_remove_vertex(Vertex_handle v)
{
    auto parent = m_arrangementItem->verticesItem;
    QModelIndex modelIndex = createIndex(parent->row(), 0,
                                         parent);
    int row = parent->childRow(v);
    beginRemoveRows(modelIndex,
                    row,
                    row);
    parent->removeVertex(v);
}

void ArrangementTreeModel::after_remove_vertex()
{
    endRemoveRows();
    auto parent = m_arrangementItem->verticesItem;
    QModelIndex modelIndex = createIndex(parent->row(), 0,
                                         parent);
    dataChanged(modelIndex, modelIndex);
}

void ArrangementTreeModel::before_remove_edge(Halfedge_handle e)
{
    Q_ASSERT(!m_removingAnEdge);
    m_removingAnEdge = true;
    m_halfedgeBeingRemoved = e;
    m_twinHalfedgeBeingRemoved = e->twin();
}

void ArrangementTreeModel::after_remove_edge()
{
    Q_ASSERT(m_removingAnEdge);
    m_removingAnEdge = false;

    auto parent = m_arrangementItem->edgesItem;
    QModelIndex modelIndex = createIndex(parent->row(), 0,
                                         parent);
    int row = parent->childRow(m_halfedgeBeingRemoved);
    row -= row % 2;
    beginRemoveRows(modelIndex,
                    row,
                    row+1);
    parent->removeEdge(m_halfedgeBeingRemoved);
    parent->removeEdge(m_twinHalfedgeBeingRemoved);
    endRemoveRows();
    dataChanged(modelIndex, modelIndex);
}

/******************************************************************************
 * ArrangementTreeItemBase
 *****************************************************************************/

ArrangementTreeItemBase::ArrangementTreeItemBase(const QString &aLabel)
    : m_label(aLabel)
{
}

ArrangementTreeItemBase::~ArrangementTreeItemBase()
{
}

/******************************************************************************
 * ArrangementItem
 *****************************************************************************/

int ArrangementItem::indexCounter = 0;

ArrangementItem::ArrangementItem(Arrangement *arr)
    : ArrangementTreeItemBase(QString("Arrangement%1").arg(indexCounter++))
    , arrangement(arr)
    , verticesItem(new ArrangementVerticesItem(this))
    , edgesItem(new ArrangementEdgesItem(this))
    , facesItem(new ArrangementFacesItem(this))
{
}

ArrangementItem::~ArrangementItem()
{
}

int ArrangementItem::childCount() const
{
    return 3;
}

ArrangementTreeItemBase *ArrangementItem::child(int row) const
{
    switch (row)
    {
        case 0: return verticesItem;
        case 1: return edgesItem;
        case 2: return facesItem;
        default: return nullptr;
    }
}

int ArrangementItem::row() const
{
    Q_UNREACHABLE();
}

ArrangementTreeItemBase *ArrangementItem::parent() const
{
    return nullptr;
}

/******************************************************************************
 * ArrangementVerticesItem
 *****************************************************************************/

ArrangementVerticesItem::ArrangementVerticesItem(ArrangementItem *aParent)
    : ArrangementTreeItemBase()
    , m_id("Vertices")
    , m_parent(aParent)
{
    setLabel(m_id);
    Vertex_iterator vit;
    for (vit = m_parent->arrangement->vertices_begin(); vit != m_parent->arrangement->vertices_end(); ++vit)
        addVertex(Vertex_handle(vit));
}

ArrangementVerticesItem::~ArrangementVerticesItem()
{
    qDeleteAll(m_vertices);
}

int ArrangementVerticesItem::childCount() const
{
    return m_vertices.count();
}

ArrangementTreeItemBase *ArrangementVerticesItem::child(int row) const
{
    return m_vertices.value(row);
}

int ArrangementVerticesItem::row() const
{
    return 0;
}

ArrangementTreeItemBase *ArrangementVerticesItem::parent() const
{
    return m_parent;
}

void ArrangementVerticesItem::addVertex(Vertex_handle v)
{
    m_vertices.append(new ArrangementVertexItem(this, v));
    setLabel(QString("%1 (%2)").arg(m_id).arg(m_vertices.count()));
}

void ArrangementVerticesItem::removeVertex(Vertex_handle v)
{
    delete m_vertices.takeAt(childRow(v));
    setLabel(QString("%1 (%2)").arg(m_id).arg(m_vertices.count()));
}

int ArrangementVerticesItem::childRow(Vertex_handle v) const
{
    for (int i=0; i<m_vertices.count(); i++)
        if (m_vertices.value(i)->vertex() == v)
            return i;
    Q_UNREACHABLE();
}

/******************************************************************************
 * ArrangementVertexItem
 *****************************************************************************/

int ArrangementVertexItem::indexCounter = 0;

ArrangementVertexItem::ArrangementVertexItem(ArrangementVerticesItem *aParent, Vertex_handle v)
    : ArrangementTreeItemBase(QString("Vertex%1").arg(v->data()))
    , m_parent(aParent)
    , m_vertex(v)
{

}

ArrangementVertexItem::~ArrangementVertexItem() {}

int ArrangementVertexItem::childCount() const
{
    return 0;
}

ArrangementTreeItemBase *ArrangementVertexItem::child(int row) const
{
    Q_UNUSED(row);
    return nullptr;
}

int ArrangementVertexItem::row() const
{
    return m_parent->vertices().indexOf(const_cast<ArrangementVertexItem*>(this));
}

ArrangementTreeItemBase *ArrangementVertexItem::parent() const
{
    return m_parent;
}


/******************************************************************************
 * ArrangementEdgeItem
 *****************************************************************************/

int ArrangementEdgeItem::indexCounter = 0;

ArrangementEdgeItem::ArrangementEdgeItem(ArrangementEdgesItem *aParent, Halfedge_handle e)
    : ArrangementTreeItemBase(QString("Edge%1").arg(e->data()))
    , m_parent(aParent)
    , m_halfEdge(e)
{

}

ArrangementEdgeItem::~ArrangementEdgeItem()
{

}

int ArrangementEdgeItem::childCount() const
{
    return 0;
}

ArrangementTreeItemBase *ArrangementEdgeItem::child(int) const
{
    return nullptr;
}

int ArrangementEdgeItem::row() const
{
    return m_parent->halfEdges().indexOf(const_cast<ArrangementEdgeItem*>(this));
}

ArrangementTreeItemBase *ArrangementEdgeItem::parent() const
{
    return m_parent;
}

/******************************************************************************
 * ArrangementEdgesItem
 *****************************************************************************/

ArrangementEdgesItem::ArrangementEdgesItem(ArrangementItem *aParent)
    : ArrangementTreeItemBase()
    , m_id("Edges")
    , m_parent(aParent)
{
    setLabel(m_id);
    Edge_iterator eit;
    for (eit = m_parent->arrangement->edges_begin(); eit != m_parent->arrangement->edges_end(); ++eit)
    {
        addEdge(Halfedge_handle(eit));
        addEdge(Halfedge_handle(eit->twin()));
    }
}

ArrangementEdgesItem::~ArrangementEdgesItem()
{
    qDeleteAll(m_halfEdges);
}

int ArrangementEdgesItem::childCount() const
{
    return m_halfEdges.count();
}

ArrangementTreeItemBase *ArrangementEdgesItem::child(int row) const
{
    return m_halfEdges.value(row);
}

int ArrangementEdgesItem::row() const
{
    return 1;
}

ArrangementTreeItemBase *ArrangementEdgesItem::parent() const
{
    return m_parent;
}

void ArrangementEdgesItem::addEdge(Halfedge_handle h)
{
    m_halfEdges.append(new ArrangementEdgeItem(this, h));
    setLabel(QString("%1 (%2)").arg(m_id).arg(m_halfEdges.count()));
}

void ArrangementEdgesItem::removeEdge(Halfedge_handle h)
{
    delete m_halfEdges.takeAt(childRow(h));
    setLabel(QString("%1 (%2)").arg(m_id).arg(m_halfEdges.count()));
}

int ArrangementEdgesItem::childRow(Halfedge_handle h) const
{
    for (int i=0; i<m_halfEdges.count(); i++)
        if (m_halfEdges.value(i)->halfEdge() == h)
            return i;
    Q_UNREACHABLE();
}

/******************************************************************************
 * ArrangementFaceItem
 *****************************************************************************/

int ArrangementFaceItem::indexCounter = 0;

ArrangementFaceItem::ArrangementFaceItem(ArrangementFacesItem *aParent, Face_handle f)
    : ArrangementTreeItemBase(QString("Face%1").arg(f->data()))
    , m_parent(aParent)
    , m_face(f)
{

}

ArrangementFaceItem::~ArrangementFaceItem()
{

}

int ArrangementFaceItem::childCount() const
{
    return 0;
}

ArrangementTreeItemBase *ArrangementFaceItem::child(int) const
{
    return nullptr;
}

int ArrangementFaceItem::row() const
{
    return m_parent->faces().indexOf(const_cast<ArrangementFaceItem*>(this));
}

ArrangementTreeItemBase *ArrangementFaceItem::parent() const
{
    return m_parent;
}

/******************************************************************************
 * ArrangementFacesItem
 *****************************************************************************/

ArrangementFacesItem::ArrangementFacesItem(ArrangementItem *aParent)
    : ArrangementTreeItemBase()
    , m_id("Faces")
    , m_parent(aParent)
{
    setLabel(m_id);
    Face_iterator eit;
    for (eit = m_parent->arrangement->faces_begin(); eit != m_parent->arrangement->faces_end(); ++eit)
        addFace(Face_handle(eit));
}

ArrangementFacesItem::~ArrangementFacesItem()
{
    qDeleteAll(m_faces);
}

int ArrangementFacesItem::childCount() const
{
    return m_faces.count();
}

ArrangementTreeItemBase *ArrangementFacesItem::child(int row) const
{
    return m_faces.value(row);
}

int ArrangementFacesItem::row() const
{
    return 2;
}

ArrangementTreeItemBase *ArrangementFacesItem::parent() const
{
    return m_parent;
}

void ArrangementFacesItem::addFace(Face_handle h)
{
    m_faces.append(new ArrangementFaceItem(this, h));
    setLabel(QString("%1 (%2)").arg(m_id).arg(m_faces.count()));
}

void ArrangementFacesItem::removeFace(Face_handle f)
{
    delete m_faces.takeAt(childRow(f));
    setLabel(QString("%1 (%2)").arg(m_id).arg(m_faces.count()));
}

int ArrangementFacesItem::childRow(Face_handle f) const
{
    for (int i=0; i<m_faces.count(); i++)
        if (m_faces.value(i)->face() == f)
            return i;
    Q_UNREACHABLE();
}
