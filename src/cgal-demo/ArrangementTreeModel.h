#ifndef ARRANGEMENTTREEMODEL_H
#define ARRANGEMENTTREEMODEL_H

#include <QAbstractItemModel>

#include <LeGeometricArrangement/LeGeometricArrangement.h>


struct ArrangementItem;
struct ArrangementVertexItem;
struct ArrangementVerticesItem;
struct ArrangementEdgeItem;
struct ArrangementEdgesItem;
struct ArrangementFaceItem;
struct ArrangementFacesItem;

struct ArrangementTreeItemBase
{
    explicit ArrangementTreeItemBase(const QString &aLabel = QString());
    virtual ~ArrangementTreeItemBase();

    virtual int childCount() const = 0;
    virtual ArrangementTreeItemBase *child(int childRow) const = 0;
    virtual int row() const = 0;
    virtual ArrangementTreeItemBase *parent() const  = 0;

    QString label() const { return m_label; }
    void setLabel(const QString &label) { m_label = label; }

private:
    Q_DISABLE_COPY(ArrangementTreeItemBase)
    QString m_label;
};

struct ArrangementVertexItem: public ArrangementTreeItemBase
{

    ArrangementVertexItem(ArrangementVerticesItem *aParent, LGA::Vertex_handle v);
    virtual ~ArrangementVertexItem();

    virtual int childCount() const override;
    virtual ArrangementTreeItemBase *child(int /*row*/) const override;
    virtual int row() const override;
    virtual ArrangementTreeItemBase *parent() const override;

    LGA::Vertex_handle vertex() const { return m_vertex; }

private:
    Q_DISABLE_COPY(ArrangementVertexItem)
    static int indexCounter;
    ArrangementVerticesItem *m_parent = nullptr;
    LGA::Vertex_handle m_vertex;
};

struct ArrangementVerticesItem: public ArrangementTreeItemBase
{
    ArrangementVerticesItem(ArrangementItem *aParent);
    virtual ~ArrangementVerticesItem();

    virtual int childCount() const override;
    virtual ArrangementTreeItemBase *child(int row) const override;
    virtual int row() const override;
    virtual ArrangementTreeItemBase *parent() const override;

    void addVertex(LGA::Vertex_handle v);
    void removeVertex(LGA::Vertex_handle v);
    int childRow(LGA::Vertex_handle v) const;

    QList<ArrangementVertexItem*> vertices() const { return m_vertices; }

private:
    Q_DISABLE_COPY(ArrangementVerticesItem)
    QString m_id;
    ArrangementItem *m_parent = nullptr;
    QList<ArrangementVertexItem*> m_vertices;
};

struct ArrangementEdgeItem: public ArrangementTreeItemBase
{

    ArrangementEdgeItem(ArrangementEdgesItem *aParent, LGA::Halfedge_handle e);
    virtual ~ArrangementEdgeItem();

    virtual int childCount() const override;
    virtual ArrangementTreeItemBase *child(int /*row*/) const override;
    virtual int row() const override;
    virtual ArrangementTreeItemBase *parent() const override;

    LGA::Halfedge_handle halfEdge() const { return m_halfEdge; }

private:
    Q_DISABLE_COPY(ArrangementEdgeItem)
    static int indexCounter;
    ArrangementEdgesItem *m_parent = nullptr;
    LGA::Halfedge_handle m_halfEdge;
};

struct ArrangementEdgesItem: public ArrangementTreeItemBase
{
    ArrangementEdgesItem(ArrangementItem *aParent);
    virtual ~ArrangementEdgesItem();

    virtual int childCount() const override;
    virtual ArrangementTreeItemBase *child(int row) const override;
    virtual int row() const override;
    virtual ArrangementTreeItemBase *parent() const override;

    void addEdge(LGA::Halfedge_handle h);
    void removeEdge(LGA::Halfedge_handle h);
    int childRow(LGA::Halfedge_handle h) const;

    QList<ArrangementEdgeItem*> halfEdges() const { return m_halfEdges; }

private:
    Q_DISABLE_COPY(ArrangementEdgesItem)
    QString m_id;
    ArrangementItem *m_parent = nullptr;
    QList<ArrangementEdgeItem*> m_halfEdges;
};

struct ArrangementFaceItem: public ArrangementTreeItemBase
{

    ArrangementFaceItem(ArrangementFacesItem *aParent, LGA::Face_handle f);
    virtual ~ArrangementFaceItem();

    virtual int childCount() const override;
    virtual ArrangementTreeItemBase *child(int /*row*/) const override;
    virtual int row() const override;
    virtual ArrangementTreeItemBase *parent() const override;

    LGA::Face_handle face() const { return m_face; }

private:
    Q_DISABLE_COPY(ArrangementFaceItem)
    static int indexCounter;
    ArrangementFacesItem *m_parent = nullptr;
    LGA::Face_handle m_face;
};

struct ArrangementFacesItem: public ArrangementTreeItemBase
{
    ArrangementFacesItem(ArrangementItem *aParent);
    virtual ~ArrangementFacesItem();

    virtual int childCount() const override;
    virtual ArrangementTreeItemBase *child(int row) const override;
    virtual int row() const override;
    virtual ArrangementTreeItemBase *parent() const override;

    void addFace(LGA::Face_handle f);
    void removeFace(LGA::Face_handle f);
    int childRow(LGA::Face_handle f) const;

    QList<ArrangementFaceItem*> faces() const { return m_faces; }

private:
    Q_DISABLE_COPY(ArrangementFacesItem)
    QString m_id;
    ArrangementItem *m_parent = nullptr;
    QList<ArrangementFaceItem*> m_faces;
};

struct ArrangementItem: public ArrangementTreeItemBase
{
    static int indexCounter;

    ArrangementItem(LGA::Arrangement *arr);
    virtual ~ArrangementItem();

    virtual int childCount() const override;
    virtual ArrangementTreeItemBase *child(int /*row*/) const override;
    virtual int row() const override;
    virtual ArrangementTreeItemBase *parent() const override;

    LGA::Arrangement *arrangement = nullptr;
    ArrangementVerticesItem *verticesItem = nullptr;
    ArrangementEdgesItem *edgesItem = nullptr;
    ArrangementFacesItem *facesItem = nullptr;

private:
    Q_DISABLE_COPY(ArrangementItem)
};


class ArrangementTreeModel : public QAbstractItemModel, public LGA::ArrangementObserver
{
    Q_OBJECT

public:
    explicit ArrangementTreeModel(QObject *parent = 0);

    void setArrangement(LGA::Arrangement *arrangement);

private:
    ArrangementItem *m_arrangementItem = nullptr;
    bool m_removingAnEdge = false;
    LGA::Halfedge_handle m_halfedgeBeingRemoved;
    LGA::Halfedge_handle m_twinHalfedgeBeingRemoved;

    // QAbstractItemModel interface
public:
    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Arr_observer interface
public:
    virtual void before_create_vertex(const LGA::Point &p) override;
    virtual void after_create_vertex(LGA::Vertex_handle v) override;
    virtual void before_create_edge(const LGA::Segment &c, LGA::Vertex_handle v1, LGA::Vertex_handle v2) override;
    virtual void after_create_edge(LGA::Halfedge_handle e) override;
    virtual void before_split_edge(LGA::Halfedge_handle e, LGA::Vertex_handle v, const LGA::Segment &c1, const LGA::Segment &c2) override;
    virtual void after_split_edge(LGA::Halfedge_handle e1, LGA::Halfedge_handle e2) override;
    virtual void before_split_face(LGA::Face_handle f, LGA::Halfedge_handle e) override;
    virtual void after_split_face(LGA::Face_handle f1, LGA::Face_handle f2, bool is_hole) override;
    virtual void before_merge_edge(LGA::Halfedge_handle e1, LGA::Halfedge_handle e2, const LGA::Segment &c) override;
    virtual void after_merge_edge(LGA::Halfedge_handle e) override;
    virtual void before_merge_face(LGA::Face_handle f1, LGA::Face_handle f2, LGA::Halfedge_handle e) override;
    virtual void after_merge_face(LGA::Face_handle f) override;
    virtual void before_remove_vertex(LGA::Vertex_handle v) override;
    virtual void after_remove_vertex() override;
    virtual void before_remove_edge(LGA::Halfedge_handle e) override;
    virtual void after_remove_edge() override;
};

#endif // ARRANGEMENTTREEMODEL_H
