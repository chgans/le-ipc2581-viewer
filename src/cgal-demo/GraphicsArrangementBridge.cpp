#include "GraphicsArrangementBridge.h"

#include "LeGraphicsView/LeGraphicsItem.h"
#include "LeGraphicsView/LeGraphicsScene.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/LeGraphicsView.h"

#include <QCursor>
#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QKeyEvent>
#include <QPointF>
#include <QVector>

using namespace LGA;

//#define NO_GRAPHICS_ITEM_FOR_FACE

unsigned int GraphicsArrangementBridge::g_index = 0;

GraphicsArrangementBridge::GraphicsArrangementBridge(QObject *parent)
    : QObject(parent)
    , m_locator(new TrapezoidLocator())
{

}

void GraphicsArrangementBridge::setup(Arrangement *arrangement, LeGraphicsScene *graphicsScene)
{
    if (m_arrangement != nullptr)
    {
        m_locator->detach();
        detach();
    }

    m_scene = graphicsScene;
    m_scene->setEventFilter(this);
    m_arrangement = arrangement;

    if (m_arrangement != nullptr)
    {
        attach(*m_arrangement);
        m_locator->attach(*m_arrangement);
    }
}

bool GraphicsArrangementBridge::isVertex(const LeGraphicsItem *item)
{
    bool ok;
    int i = item->data(0).toInt(&ok);
    return ok && m_vertexHandles.contains(i);
}

Vertex_handle GraphicsArrangementBridge::vertex(const LeGraphicsItem *item) const
{
    int i = item->data(0).toInt();
    return m_vertexHandles.value(i);
}

bool GraphicsArrangementBridge::isHalfedge(const LeGraphicsItem *item)
{
    bool ok;
    int i = item->data(0).toInt(&ok);
    return ok && m_halfEdgeHandles.contains(i);
}

Halfedge_handle GraphicsArrangementBridge::halfedge(const LeGraphicsItem *item) const
{
    int i = item->data(0).toInt();
    return m_halfEdgeHandles.value(i);
}

bool GraphicsArrangementBridge::isFace(const LeGraphicsItem *item)
{
    bool ok;
    int i = item->data(0).toInt(&ok);
    return ok && m_faceHandles.contains(i);
}

Face_handle GraphicsArrangementBridge::face(const LeGraphicsItem *item) const
{
    int i = item->data(0).toInt();
    return m_faceHandles.value(i);
}

Face_handle GraphicsArrangementBridge::unboundedFace() const
{
    return m_arrangement->unbounded_face();
}

TrapezoidLocator *GraphicsArrangementBridge::locator() const
{
    return m_locator;
}

QPointF GraphicsArrangementBridge::qPointF(const Point &p)
{
    return QPointF(CGAL::to_double(p.x()), CGAL::to_double(p.y()));
}

QLineF GraphicsArrangementBridge::qLineF(const Point &p1, const Point &p2)
{
    return QLineF(qPointF(p1), qPointF(p2));
}

//#define DEBUG

#ifdef DEBUG

#define TRACE(i) qDebug() << QString("%1%2").arg(' ', i).arg(__FUNCTION__)

#define SANITY_CHECK() do { \
    CGAL_assertion(int(m_arrangement->number_of_halfedges()) == m_halfEdgeHandles.count()); \
    CGAL_assertion(m_halfEdgeHandles.count() == m_halfEdgeItems.count()); \
    CGAL_assertion(int(m_arrangement->number_of_vertices()) == m_vertexHandles.count()); \
    CGAL_assertion(m_vertexHandles.count() == m_vertexItems.count()); \
    } while (0)
#if 0
CGAL_assertion(int(m_arrangement->number_of_faces()) == (m_faceHandles.count() + 1)); \
CGAL_assertion(m_faceHandles.count() == m_faceItems.count());
#endif

#else
#define TRACE(a) do {} while(0)
#define DONE(a) do {} while(0)
#define SANITY_CHECK() do {} while(0)
#endif
static int indent = 0;
#define BEFORE_VERTEX() SANITY_CHECK(); TRACE(indent++)
#define BEFORE_EDGE() SANITY_CHECK(); TRACE(indent++)
#define BEFORE_CCB() SANITY_CHECK(); TRACE(indent++)
#define BEFORE_FACE() SANITY_CHECK(); TRACE(indent++)
#define AFTER_VERTEX() TRACE(--indent); SANITY_CHECK()
#define AFTER_EDGE() TRACE(--indent); SANITY_CHECK()
#define AFTER_CCB() TRACE(--indent); SANITY_CHECK()
#define AFTER_FACE() TRACE(--indent); SANITY_CHECK()

/******************************************************************************
 *  Notifications on Global Arrangement Operations
 *****************************************************************************/

/*
 * issued just before the attached arrangement is assigned with the contents of another arrangement arr.
 */
void GraphicsArrangementBridge::before_assign(const Arrangement_2 &)
{
}

/*
 * issued immediately after the attached arrangement has been assigned with the contents of another arrangement.
 */
void GraphicsArrangementBridge::after_assign()
{
}

/*
 * issued just before the attached arrangement is cleared.
 */
void GraphicsArrangementBridge::before_clear()
{
    m_scene->clear();
    m_faceHandles.clear();
    m_faceItems.clear();
    m_halfEdgeHandles.clear();
    m_halfEdgeItems.clear();
    m_vertexHandles.clear();
    m_vertexItems.clear();
}

/*
 * issued immediately after the attached arrangement has been cleared, so it now consists only of a the unbounded face uf.
 */
void GraphicsArrangementBridge::after_clear()
{
}

/*
 * issued just before a global function starts to modify the attached arrangement.
 * It is guaranteed that no queries (especially no point-location queries) are issued until
 * the termination of the global function is indicated by after_global_change().
 */
void GraphicsArrangementBridge::before_global_change()
{
    qDebug() << __PRETTY_FUNCTION__;
    SANITY_CHECK();
}

/*
 * issued immediately after a global function has stopped modifying the attached arrangement.
 */
void GraphicsArrangementBridge::after_global_change()
{
    qDebug() << __PRETTY_FUNCTION__;
    SANITY_CHECK();
}

/******************************************************************************
 *  Notifications on Attachment or Detachment
 *****************************************************************************/

/*
 * issued just before the observer is attached to the arrangement instance arr.
 */
void GraphicsArrangementBridge::before_attach(const Arrangement_2 &/*arr*/)
{
}

/*
 * issued immediately after the observer has been attached to an arrangement instance.
 */
void GraphicsArrangementBridge::after_attach()
{
    Vertex_iterator vit;
    for (vit = m_arrangement->vertices_begin(); vit != m_arrangement->vertices_end(); ++vit)
        addVertexToScene(vit);
    Edge_iterator eit;
    for (eit = m_arrangement->edges_begin(); eit != m_arrangement->edges_end(); ++eit)
    {
        addEdgeToScene(eit);
    }
    Face_iterator fit;
    for (fit = m_arrangement->faces_begin(); fit != m_arrangement->faces_end(); ++fit)
        addFaceToScene(fit);
    SANITY_CHECK();
}

/*
 * issued just before the observer is detached from its arrangement instance.
 */
void GraphicsArrangementBridge::before_detach()
{
}

/*
 * issued immediately after the observer has been detached from its arrangement instance.
 */
void GraphicsArrangementBridge::after_detach()
{
}

/******************************************************************************
 * Notifications on Local Changes in the Arrangement
 *****************************************************************************/

/*
 * issued just before a new vertex that corresponds to the point p is created.
 */
void GraphicsArrangementBridge::before_create_vertex(const Point_2 &)
{
    BEFORE_VERTEX();
}

/*
 * issued immediately after a new vertex v has been created.
 * Note that the vertex still has no incident edges and is not connected to any other vertex.
 */
void GraphicsArrangementBridge::after_create_vertex(Vertex_handle vh)
{
    addVertexToScene(vh);
    AFTER_VERTEX();
}

/*
 * issued just before a new vertex at infinity is created, cv is the curve incident to the surface boundary,
 * ind is the relevant curve-end, ps_x is the boundary condition of the vertex in x and ps_y is the boundary condition of the vertex in y.
 * */
void GraphicsArrangementBridge::before_create_boundary_vertex(const X_monotone_curve_2 &/*cv*/, CGAL::Arr_curve_end /*ind*/,
                                                              CGAL::Arr_parameter_space /*ps_x*/, CGAL::Arr_parameter_space /*ps_y*/)
{
    BEFORE_VERTEX();
    Q_UNREACHABLE();
}

/*
 * issued immediately after a new vertex v has been created.
 * Note that the vertex still has no incident edges and is not connected to any other vertex.
 */
void GraphicsArrangementBridge::after_create_boundary_vertex(Vertex_handle /*v*/)
{
    AFTER_VERTEX();
    Q_UNREACHABLE();
}
/*
 * issued just before a new edge that corresponds to the x-monotone curve c and connects the vertices v1 and v2 is created.
 */
void GraphicsArrangementBridge::before_create_edge(const X_monotone_curve_2 &/*c*/, Vertex_handle /*v1*/, Vertex_handle /*v2*/)
{
    BEFORE_EDGE();
}

/*
 * issued immediately after a new edge e has been created.
 * The halfedge that is sent to this function is always directed from v1 to v2 (see above).
 */
void GraphicsArrangementBridge::after_create_edge(Halfedge_handle eh)
{
    addEdgeToScene(eh);
    AFTER_EDGE();
}

/*
 * issued just before a vertex v is modified to be associated with the point p.
 */
void GraphicsArrangementBridge::before_modify_vertex(Vertex_handle v, const Point_2 &p)
{
    BEFORE_VERTEX();
    //Q_UNREACHABLE();
}

/*
 * issued immediately after an existing vertex v has been modified.
 */
void GraphicsArrangementBridge::after_modify_vertex(Vertex_handle v)
{
    AFTER_VERTEX();
    //Q_UNREACHABLE();
}

/*
 * issued just before an edge e is modified to be associated with the x-monotone curve c.
 */
void GraphicsArrangementBridge::before_modify_edge(Halfedge_handle e, const X_monotone_curve_2 &c)
{
    BEFORE_EDGE();
    //Q_UNREACHABLE();
}

/*
 * issued immediately after an existing edge e has been modified.
 */
void GraphicsArrangementBridge::after_modify_edge(Halfedge_handle e)
{
    AFTER_EDGE();
    //Q_UNREACHABLE();
}

/*
 * issued just before an edge e is split into two edges that should be associated with the x-monotone curves c1 and c2.
 * The vertex v corresponds to the split point, and will be used to separate the two resulting edges.
 */
void GraphicsArrangementBridge::before_split_edge(Halfedge_handle e, Vertex_handle v,
                                                  const X_monotone_curve_2 &c1, const X_monotone_curve_2 &c2)
{
    BEFORE_EDGE();
    removeEdgeFromScene(e);
}

/*
 * issued immediately after an existing edge has been split into the two given edges e1 and e2.
 */
void GraphicsArrangementBridge::after_split_edge(Halfedge_handle e1, Halfedge_handle e2)
{
    addEdgeToScene(e1);
    addEdgeToScene(e2);
    AFTER_EDGE();
}
#if 0
/*
 * issued just before a fictitious edge e is split into two.
 * The vertex at infinity v corresponds to the split point, and will be used to separate the two resulting edges.
 */
void GraphicsArrangementBridge::before_split_fictitious_edge(Halfedge_handle e, Vertex_handle v)
{
    BEFORE_EDGE();
}

/*
 * issued immediately after an existing fictitious edge has been split into the two given fictitious edges e1 and e2.
 */
void GraphicsArrangementBridge::after_split_fictitious_edge(Halfedge_handle e1, Halfedge_handle e2)
{
    AFTER_EDGE();
}
#endif
/*
 * issued just before a face f is split into two, as a result of the insertion of the edge e into the arrangement.
 */
void GraphicsArrangementBridge::before_split_face(Face_handle f, Halfedge_handle e)
{
    BEFORE_FACE();
    removeFaceFromScene(f); // TODO: instead update f1 in after()
}

/*
 * issued immediately after the existing face f1 has been split, such that a portion of it now forms a new face f2.
 * The flag is_hole designates whether f2 forms a hole inside f1.
 */
void GraphicsArrangementBridge::after_split_face(Face_handle f1, Face_handle f2, bool is_hole)
{
    addFaceToScene(f1);
    addFaceToScene(f2);
    AFTER_FACE();
}
#if 0
/*
 * issued just before outer ccb h inside a face f is split into two, as a result of the removal of the edge e from the arrangement.
 */
void GraphicsArrangementBridge::before_split_outer_ccb(Face_handle f, Ccb_halfedge_circulator h, Halfedge_handle e)
{
    BEFORE_CCB();
}

/*
 * issued immediately after outer ccb the face f has been split, resulting in the two holes h1 and h2.
 */
void GraphicsArrangementBridge::after_split_outer_ccb(Face_handle f, Ccb_halfedge_circulator h1, Ccb_halfedge_circulator h2)
{
    AFTER_CCB();
}

/*
 * issued just before inner ccb h inside a face f is split into two, as a result of the removal of the edge e from the arrangement.
 */
void GraphicsArrangementBridge::before_split_inner_ccb(Face_handle f, Ccb_halfedge_circulator h, Halfedge_handle e)
{
    BEFORE_CCB();
    //removeFaceFromScene(f);
}

/*
 * issued immediately after inner ccb the face f has been split, resulting in the two holes h1 and h2.
 */
void GraphicsArrangementBridge::after_split_inner_ccb(Face_handle f, Ccb_halfedge_circulator h1, Ccb_halfedge_circulator h2)
{
    AFTER_CCB();
    //addFaceToScene(f);
}

/*
 * issued just before the edge e is inserted as a new outer ccb inside the face f.
 */
void GraphicsArrangementBridge::before_add_outer_ccb(Face_handle f, Halfedge_handle e)
{
    BEFORE_CCB();
}

/*
 * issued immediately after a new outer ccb h has been created.
 * The outer ccb always consists of a single pair of twin halfedges.
 */
void GraphicsArrangementBridge::after_add_outer_ccb(Ccb_halfedge_circulator h)
{
    AFTER_CCB();
}

/*
 * issued just before the edge e is inserted as a new inner ccb inside the face f.
 */
void GraphicsArrangementBridge::before_add_inner_ccb(Face_handle f, Halfedge_handle e)
{
    BEFORE_CCB();
}

/*
 * issued immediately after a new inner ccb h has been created.
 * The inner ccb always consists of a single pair of twin halfedges.
 */
void GraphicsArrangementBridge::after_add_inner_ccb(Ccb_halfedge_circulator h)
{
    AFTER_CCB();
}

/*
 * issued just before the vertex v is inserted as an isolated vertex inside the face f.
 */
void GraphicsArrangementBridge::before_add_isolated_vertex(Face_handle f, Vertex_handle v)
{
    BEFORE_VERTEX();
    //Q_UNREACHABLE();
}

/*
 * issued immediately after the vertex v has been set as an isolated vertex.
 */
void GraphicsArrangementBridge::after_add_isolated_vertex(Vertex_handle /*v*/)
{
    AFTER_VERTEX();
    //Q_UNREACHABLE();
}
#endif

/*
 * issued just before the two edges e1 and e2 are merged to form a single edge that will be associated with the x-monotone curve c.
 */
void GraphicsArrangementBridge::before_merge_edge(Halfedge_handle e1, Halfedge_handle e2, const X_monotone_curve_2 &)
{
    BEFORE_EDGE();
    removeEdgeFromScene(e1);
    removeEdgeFromScene(e2);

}

/*
 * issued immediately after two edges have been merged to form the edge e.
 */
void GraphicsArrangementBridge::after_merge_edge(Halfedge_handle e)
{
    addEdgeToScene(e);
    //    e->face()->outer_ccb();
    //    e->twin()->face()->outer_ccb();
    AFTER_EDGE();
}

/*
 * issued just before the two fictitious edges e1 and e2 are merged to form a single fictitious edge.
 */
void GraphicsArrangementBridge::before_merge_fictitious_edge(Halfedge_handle /*e1*/, Halfedge_handle /*e2*/)
{
    BEFORE_EDGE();
    Q_UNREACHABLE();
}

/*
 * issued immediately after two fictitious edges have been merged to form the fictitious edge e.
 */
void GraphicsArrangementBridge::after_merge_fictitious_edge(Halfedge_handle e)
{
    AFTER_EDGE();
    Q_UNREACHABLE();
}

/*
 * issued just before the two edges f1 and f2 are merged to form a single face, following the removal of the edge e from the arrangement.
 */
void GraphicsArrangementBridge::before_merge_face(Face_handle f1, Face_handle f2, Halfedge_handle e)
{
    BEFORE_FACE();
    removeFaceFromScene(f1);
    removeFaceFromScene(f2);
}

/*
 * issued immediately after two faces have been merged to form the face f.
 */
void GraphicsArrangementBridge::after_merge_face(Face_handle f)
{
    addFaceToScene(f);
    AFTER_FACE();
}
#if 0
/*
 * issued just before two outer ccbs h1 and h2 inside the face f are merged to form a single connected component,
 * following the insertion of the edge e into the arrangement.
 */
void GraphicsArrangementBridge::before_merge_outer_ccb(Face_handle f, Ccb_halfedge_circulator h1, Ccb_halfedge_circulator h2, Halfedge_handle e)
{
    BEFORE_CCB();
}

/*
 * issued immediately after two outer ccbs have been merged to form a single outer ccb h inside the face f.
 */
void GraphicsArrangementBridge::after_merge_outer_ccb(Face_handle f, Ccb_halfedge_circulator h)
{
    AFTER_CCB();
}

/*
 * issued just before two inner ccbs h1 and h2 inside the face f are merged to form a single connected component,
 * following the insertion of the edge e into the arrangement.
 */
void GraphicsArrangementBridge::before_merge_inner_ccb(Face_handle f, Ccb_halfedge_circulator h1, Ccb_halfedge_circulator h2, Halfedge_handle e)
{
    BEFORE_CCB();
}

/*
 * issued immediately after two inner ccbs have been merged to form a single inner ccb h inside the face f.
 */
void GraphicsArrangementBridge::after_merge_inner_ccb(Face_handle, Ccb_halfedge_circulator)
{
    AFTER_CCB();
}

/*
 * issued just before the outer ccb h is moved from one face to another.
 * This can happen if the face to_f containing the outer ccb has just been split from from_f.
 */
void GraphicsArrangementBridge::before_move_outer_ccb(Face_handle from_f, Face_handle to_f, Ccb_halfedge_circulator h)
{
    BEFORE_CCB();
}

/*
 * issued immediately after the outer ccb h has been moved to a new face.
 */
void GraphicsArrangementBridge::after_move_outer_ccb(Ccb_halfedge_circulator h)
{
    AFTER_CCB();
}

/*
 * issued just before the inner ccb h is moved from one face to another.
 * This can happen if the face to_f containing the inner ccb has just been split from from_f.
 */
void GraphicsArrangementBridge::before_move_inner_ccb(Face_handle from_f, Face_handle to_f, Ccb_halfedge_circulator h)
{
    //removeFaceFromScene(to_f);
    BEFORE_CCB();
}

/*
 * issued immediately after the inner ccb h has been moved to a new face.
 */
void GraphicsArrangementBridge::after_move_inner_ccb(Ccb_halfedge_circulator h)
{
    //addFaceToScene(h->face());
    AFTER_CCB();
}

/*
 * issued just before the isolated vertex v is moved from one face to another.
 * This can happen if the face to_f containing the isolated vertex has just been split from from_f.
 */
void GraphicsArrangementBridge::before_move_isolated_vertex(Face_handle from_f, Face_handle to_f, Vertex_handle v)
{
    BEFORE_VERTEX();
}

/*
 * issued immediately after the isolated vertex v has been moved to a new face.
 */
void GraphicsArrangementBridge::after_move_isolated_vertex(Vertex_handle v)
{
    AFTER_VERTEX();
}
#endif
/*
 * issued just before the vertex v is removed from the arrangement.
 */
void GraphicsArrangementBridge::before_remove_vertex(Vertex_handle v)
{
    BEFORE_VERTEX();
    removeVertexFromScene(v);
}

/*
 * issued immediately after a vertex has been removed (and deleted) from the arrangement.
 */
void GraphicsArrangementBridge::after_remove_vertex()
{
    AFTER_VERTEX();
}

/*
 * issued just before the edge e is removed from the arrangement.
 */
void GraphicsArrangementBridge::before_remove_edge(Halfedge_handle e)
{
    BEFORE_EDGE();
    removeEdgeFromScene(e);
}

/*
 * issued immediately after an edge has been removed (and deleted) from the arrangement.
 */
void GraphicsArrangementBridge::after_remove_edge()
{
    AFTER_EDGE();
}
#if 0
/*
 * issued just before the outer ccb f is removed from inside the face f.
 */
void GraphicsArrangementBridge::before_remove_outer_ccb(Face_handle f, Ccb_halfedge_circulator h)
{
    BEFORE_CCB();
}

/*
 * issued immediately after a outer ccb has been removed (and deleted) from inside the face f.
 */
void GraphicsArrangementBridge::after_remove_outer_ccb(Face_handle f)
{
    AFTER_CCB();
}

/*
 * issued just before the inner ccb f is removed from inside the face f.
 */
void GraphicsArrangementBridge::before_remove_inner_ccb(Face_handle f, Ccb_halfedge_circulator h)
{
    BEFORE_CCB();
}

/*
 * issued immediately after a inner ccb has been removed (and deleted) from inside the face f.
 */
void GraphicsArrangementBridge::after_remove_inner_ccb(Face_handle)
{
    AFTER_CCB();
}
#endif
/******************************************************************************
 * Convenience function to keep the scene up to date
 *****************************************************************************/

void GraphicsArrangementBridge::addVertexToScene(Vertex_handle vh)
{
    g_index++;
    auto item = new LeGraphicsCircleItem(GftPad);
    item->setFlag(QGraphicsItem::ItemIgnoresTransformations);
    item->setDiameter(VertexDiameter); // pixels
    item->setPos(qPointF(vh->point()));
    m_scene->currentLayer()->addToLayer(item);
    m_vertexItems.insert(g_index, item);
    m_vertexHandles.insert(g_index, vh);
    vh->set_data(g_index);
    item->setData(0, g_index);
}

void GraphicsArrangementBridge::removeVertexFromScene(Vertex_handle vh)
{
    auto item = m_vertexItems.take(vh->data());
    m_vertexHandles.take(vh->data());
    m_scene->currentLayer()->removeFromLayer(item);
    delete item;
}

void GraphicsArrangementBridge::addEdgeToScene(Halfedge_handle eh)
{
    QLineF line = qLineF(eh->source()->point(), eh->target()->point());

    auto item = new LeGraphicsStrokedPathItem(GftTrace);
    QPainterPath path;
    path.moveTo(line.p1());
    path.lineTo(line.p2());
    item->setPath(path, 0); // pixels

    addHalfedgeToScene(eh);
    addHalfedgeToScene(eh->twin());

    item->setData(0, g_index); // edge item has same data as twin item from above
    m_scene->currentLayer()->addToLayer(item);
    m_edgeItems.insert(g_index, item);
}

void GraphicsArrangementBridge::addHalfedgeToScene(Halfedge_handle eh)
{
    g_index++;

    QLineF line = qLineF(eh->source()->point(), eh->target()->point());

    auto mark = new LeGraphicsFilledPathItem(GftTrace);
    auto path = QPainterPath();
    path.moveTo(-HalfedgePullBack, 0+HalfedgeSide);
    path.lineTo(-HalfedgeLength-HalfedgePullBack, HalfedgeWidth+HalfedgeSide);
    path.lineTo(-HalfedgeLength-HalfedgePullBack, 0+HalfedgeSide);
    path.closeSubpath();
    mark->setPath(path);
    mark->setFlag(QGraphicsItem::ItemIgnoresTransformations);
    //mark->setParentItem(item);
    mark->setPos(line.center());
    mark->setRotation(line.angle());

    m_scene->currentLayer()->addToLayer(mark);
    m_halfEdgeItems.insert(g_index, mark);
    m_halfEdgeHandles.insert(g_index, eh);
    eh->set_data(g_index);
    mark->setData(0, g_index);
}

void GraphicsArrangementBridge::removeEdgeFromScene(Halfedge_handle eh)
{
    removeHalfedgeFromScene(eh);
    removeHalfedgeFromScene(eh->twin());
    if (m_edgeItems.contains(eh->data()))
    {
        LeGraphicsItem *item = m_edgeItems.take(eh->data());
        m_scene->currentLayer()->removeFromLayer(item);
        delete item;
    }
    else if (m_edgeItems.contains(eh->twin()->data()))
    {
        LeGraphicsItem *item = m_edgeItems.take(eh->twin()->data());
        m_scene->currentLayer()->removeFromLayer(item);
        delete item;
    }
    else
    {
        qWarning() << "Edge item" << eh->data() << "/" << eh->twin()->data() << "not found";
    }
}

void GraphicsArrangementBridge::removeHalfedgeFromScene(Halfedge_handle eh)
{
    if (m_halfEdgeHandles.contains(eh->data()))
        m_halfEdgeHandles.take(eh->data());
    else
    {
        qWarning() << "Halfedge handle" << eh->data() << "not found";
        for (Edge_iterator eit = m_arrangement->edges_begin(); eit != m_arrangement->edges_end(); ++eit)
            if (eit->data() == eh->data())
                qWarning() << "Halfedge handle still in arrangment";
    }
    if (m_halfEdgeItems.contains(eh->data()))
    {
        LeGraphicsItem *item = m_halfEdgeItems.take(eh->data());
        m_scene->currentLayer()->removeFromLayer(item);
        delete item;
    }
    else
    {
        qWarning() << "Halfedge item" << eh->data() << "not found";
        for (Edge_iterator eit = m_arrangement->edges_begin(); eit != m_arrangement->edges_end(); ++eit)
            if (eit->data() == eh->data())
                qWarning() << "Halfedge handle still in arrangment";
    }
}

QPainterPath GraphicsArrangementBridge::createPath(Ccb_halfedge_circulator circ)
{
    QPainterPath path;
    auto curr = circ;
    Halfedge_const_handle he = curr;
    path.moveTo(qPointF(he->source()->point()));
    do {
        he = curr;
        path.lineTo(qPointF(he->source()->point()));
    } while (++curr != circ);
    path.closeSubpath();
    return path;
}

void GraphicsArrangementBridge::addFaceToScene(Face_handle f)
{
    g_index++;

    if (!f->has_outer_ccb())
    {
        f->set_data(g_index);
        return;
    }

    auto item = new LeGraphicsFilledPathItem(GftPad);
#ifndef NO_GRAPHICS_ITEM_FOR_FACE
    m_scene->currentLayer()->addToLayer(item);
#endif
    m_faceHandles.insert(g_index, f);
    m_faceItems.insert(g_index, item);
    f->set_data(g_index);
    item->setData(0, g_index);

    QPainterPath outline = createPath(f->outer_ccb());
    QPainterPath cut;
    for (auto curr = f->holes_begin(); curr != f->holes_end(); ++curr)
        cut += createPath(*curr);
    item->setPath(outline - cut);
    item->setOpacity(0.25);
}

void GraphicsArrangementBridge::removeFaceFromScene(Face_handle f)
{
    if (!f->has_outer_ccb())
        return;
    if (m_faceHandles.contains(f->data()))
        m_faceHandles.take(f->data());
    else
    {
        qWarning() << "Face item" << f->data() << "not found";
        for (Face_iterator eit = m_arrangement->faces_begin(); eit != m_arrangement->faces_end(); ++eit)
            if (eit->data() == f->data())
                qWarning() << "Face handle still in arrangment";
    }
    if (m_faceItems.contains(f->data()))
    {
#ifndef NO_GRAPHICS_ITEM_FOR_FACE
        delete m_scene->currentLayer()->removeFromLayer(m_faceItems.take(f->data()));
#else
        m_faceItems.take(f->data());
#endif
    }
    else
    {
        qWarning() << "Face item" << f->data() << "not found";
        for (Face_iterator eit = m_arrangement->faces_begin(); eit != m_arrangement->faces_end(); ++eit)
            if (eit->data() == f->data())
                qWarning() << "Face handle still in arrangment";
    }
}

/******************************************************************************
 * Event handling from the scene
 *****************************************************************************/

bool GraphicsArrangementBridge::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    return false;
}

bool GraphicsArrangementBridge::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    switch (m_state) {
        case WaitingForPoint1:
            break;
        case WaitingForPoint2:
        {
            QLineF line = m_lineItem->line();
            line.setP2(event->scenePos());
            m_lineItem->setLine(line);
            break;
        }
        default:
            break;
    }
    return false;
}

void GraphicsArrangementBridge::setP1(const QPointF &pos)
{
    auto items = m_scene->items(pos,
                                Qt::IntersectsItemShape, Qt::DescendingOrder,
                                m_scene->views().first()->transform());
    Vertex_handle invalid_v;
    m_p1 = m_p2 = pos;
    m_v1 = m_v2 = invalid_v;
    for (auto item: items)
    {
        const QVariant data = item->data(0);
        if (!data.canConvert<int>())
            continue;
        m_v1 = m_v2 = m_vertexHandles.value(data.toInt(), invalid_v);
        qDebug() << "P1" << data.toInt();
        if (m_v1 != invalid_v)
        {
            qDebug() << "P1" << data.toInt() << "picked";
            m_p1 = m_p2 = qPointF(m_v1->point());
            return;
        }
    }
}

void GraphicsArrangementBridge::setP2(const QPointF &pos)
{
    auto items = m_scene->items(pos,
                                Qt::IntersectsItemShape, Qt::DescendingOrder,
                                m_scene->views().first()->transform());
    Vertex_handle invalid_v;
    m_p2 = pos;
    m_v2 = invalid_v;
    for (auto item: items)
    {
        const QVariant data = item->data(0);
        if (!data.canConvert<int>())
            continue;
        m_v2 = m_vertexHandles.value(data.toInt(), invalid_v);
        qDebug() << "P2" << data.toInt();
        if (m_v2 != invalid_v)
        {
            qDebug() << "P2" << data.toInt() << "picked";
            m_p2 = qPointF(m_v2->point());
            return;
        }
    }
}

bool GraphicsArrangementBridge::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    switch (m_state) {
        case WaitingForPoint1:
        {
            setP1(event->scenePos());
            m_lineItem = m_scene->addLine(QLineF(m_p1, m_p2), QPen(Qt::white, 0));
            m_state = WaitingForPoint2;
            break;
        }
        case WaitingForPoint2:
        {
            setP2(event->scenePos());

            Point p1(m_p1.x(), m_p1.y());
            Point p2(m_p2.x(), m_p2.y());
            if (m_v1 != Vertex_handle())
                p1 = m_v1->point();
            else if (m_v2 != Vertex_handle())
                p2 = m_v2->point();

            CGAL::insert(*m_arrangement,
                         Curve(p1, p2));
            delete m_lineItem;
            m_state = Disabled;
            m_v1 = m_v2 = Vertex_handle();
            break;
        }
        default:
            break;
    }
    return false;
}

bool GraphicsArrangementBridge::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    return false;
}


bool GraphicsArrangementBridge::removeVertex(int index)
{
    Vertex_handle v = m_vertexHandles.value(index);
    // If it cannot be removed in the simple case (isolated or
    // only 2 incident edges in which case the edge will be
    // merged), then remove all incident edges and all the
    // isolated vertices left behind
    //    if (!CGAL::remove_vertex(*m_arrangement, v))
    //    {
    //        Halfedge_around_vertex_circulator first, curr;
    //        first = curr = v->incident_halfedges();
    //        QList<Halfedge_handle> edges;
    //        do
    //            edges.append(Halfedge_handle(curr));
    //        while (++curr != first);
    //        for (Halfedge_handle h: edges)
    //            if (m_halfEdgeHandles.contains(h->data()))
    //                CGAL::remove_edge(*m_arrangement, h);
    //    }
    switch (v->degree()) {
        case 0: // 0 edge: Isolated
            m_arrangement->remove_isolated_vertex(v);
            return true;
        case 1: // 1 edge: Antenna => remove antenna
            m_arrangement->remove_edge(Halfedge_handle(v->incident_halfedges()), false, false);
            m_arrangement->remove_isolated_vertex(v);
            return true;
        case 2: // 2 edges: MidPoint => replace the vertex with a segment
        {
            auto circ = v->incident_halfedges();
            auto e1 = Halfedge_handle(circ);
            auto e2 = Halfedge_handle(++circ);
            auto v1 = e1->source();
            auto v2 = e2->source();
            m_arrangement->remove_edge(e1, false, false);
            m_arrangement->remove_edge(e2, false, false);
            m_arrangement->remove_isolated_vertex(v);
            CGAL::insert(*m_arrangement, Segment(v1->point(), v2->point())/*, v1, v2*/);

            return true;
        }
        default: // 3 and more edges: Barycenter => replace the vertex with a hole
        {
            auto circ = v->incident_halfedges();
            auto curr = circ;
            QList<Halfedge_handle> toBeRemoved;
            QList<Vertex_handle> toBeConnected;
            do
            {
                auto e = Halfedge_handle(curr);
                toBeRemoved.append(e);
                toBeConnected.append(e->source());
            }
            while (++curr != circ);

            for (auto e: toBeRemoved)
                m_arrangement->remove_edge(e, false, false);

            Vertex_handle v1 = toBeConnected.first();
            for (int i=1; i<toBeConnected.count(); i++)
            {
                Vertex_handle v2 = toBeConnected.at(i);
                CGAL::insert(*m_arrangement, Segment(v1->point(), v2->point()));
                v1 = v2;
            }
            CGAL::insert(*m_arrangement, Segment(v1->point(), toBeConnected.first()->point()));

            // At this point, v could belong to a new CCB if an edge has "landed" on it
            removeVertex(v->data());

            return true;
        }
    }
}

bool GraphicsArrangementBridge::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
        case Qt::Key_Delete:
        {
            if (m_scene->selectedItems().isEmpty())
                return false;

            while (!m_scene->selectedItems().isEmpty())
            {
                auto item = m_scene->selectedItems().first();
                auto index = item->data(0).toInt();
                if (m_vertexHandles.contains(index))
                {
                    if (!removeVertex(index))
                        item->setSelected(false);
                }
                else if (m_halfEdgeHandles.contains(index))
                {
                    //                    Halfedge_handle eh1 = m_halfEdgeHandles.value(index);
                    //                    CGAL::remove_edge(*m_arrangement, eh1);
                    item->setSelected(false);
                }
                else if (m_faceHandles.contains(index))
                {
                    //                    auto f = m_faceHandles.value(index);
                    //                    Ccb_halfedge_circulator circ = f->outer_ccb();
                    //                    QList<Halfedge_handle> edges;
                    //                    do
                    //                        edges.append(Halfedge_handle(circ));
                    //                    while (++circ != f->outer_ccb());
                    //                    for (Halfedge_handle e: edges)
                    //                        if (m_halfEdgeHandles.contains(e->data()))
                    //                            CGAL::remove_edge(*m_arrangement, e);
                    item->setSelected(false);
                }
                else
                {
                    qWarning() << "Seleted item is of unknown origin";
                    break;
                }
            }
            return true;
        }
        case Qt::Key_I:
            if (m_state == Disabled)
                m_state = WaitingForPoint1;
            return true;
        default:
            return false;
    }
}
