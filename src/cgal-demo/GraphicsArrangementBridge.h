#pragma once

#include <LeGeometricArrangement/LeGeometricArrangement.h>

#include <LeGraphicsView/LeGraphicsSceneEventFilter.h>

#include <QObject>
#include <QHash>
#include <QPointF>
#include <QPainterPath>

class LeGraphicsScene;
class LeGraphicsItem;
class LeGraphicsFilledPathItem;

class QGraphicsLineItem;

class GraphicsArrangementBridge : public QObject, public LGA::ArrangementObserver, public LeGraphicsSceneEventFilter
{
    Q_OBJECT
public:
    explicit GraphicsArrangementBridge(QObject *parent = 0);

    void setup(LGA::Arrangement *arrangement, LeGraphicsScene *graphicsScene);
    LeGraphicsScene *scene() const { return m_scene; }

    bool isVertex(const LeGraphicsItem *item);
    LGA::Vertex_handle vertex(const LeGraphicsItem *item) const;
    bool isHalfedge(const LeGraphicsItem *item);
    LGA::Halfedge_handle halfedge(const LeGraphicsItem *item) const;
    bool isFace(const LeGraphicsItem *item);
    LGA::Face_handle face(const LeGraphicsItem *item) const;
    LGA::Face_handle unboundedFace() const;

    LGA::TrapezoidLocator *locator() const;

signals:

public slots:

private:
    static QPointF qPointF(const LGA::Point &p);
    static QLineF qLineF(const LGA::Point &p1, const LGA::Point &p2);
    QPainterPath createPath(LGA::Ccb_halfedge_circulator circ);

    LGA::Arrangement *m_arrangement = nullptr;
    LeGraphicsScene *m_scene = nullptr;
    LGA::TrapezoidLocator *m_locator = nullptr;
    QHash<unsigned int, LeGraphicsItem*> m_vertexItems;
    QHash<unsigned int, Vertex_handle> m_vertexHandles;
    QHash<unsigned int, LeGraphicsItem*> m_edgeItems;
    QHash<unsigned int, LeGraphicsItem*> m_halfEdgeItems;
    QHash<unsigned int, Halfedge_handle> m_halfEdgeHandles;
    QHash<unsigned int, LeGraphicsFilledPathItem*> m_faceItems;
    QHash<unsigned int, Face_handle> m_faceHandles;


    enum
    {
        Disabled,
        WaitingForPoint1,
        WaitingForPoint2,
    };
    int m_state = Disabled;
    QGraphicsLineItem *m_lineItem;
    LGA::Vertex_handle m_v1;
    QPointF m_p1;
    LGA::Vertex_handle m_v2;
    QPointF m_p2;
    void setP1(const QPointF &pos);
    void setP2(const QPointF &pos);

    static unsigned int g_index;
    enum // Unit: pixels
    {
        VertexDiameter = 8,
        HalfedgePullBack = -20,
        HalfedgeSide = 2,
        HalfedgeLength = 15,
        HalfedgeWidth = 8
    };
    void addEdgeToScene(LGA::Halfedge_handle eh);
    void addHalfedgeToScene(LGA::Halfedge_handle eh);
    void removeEdgeFromScene(LGA::Halfedge_handle eh);
    void removeHalfedgeFromScene(LGA::Halfedge_handle eh);
    void addFaceToScene(LGA::Face_handle f);
    void removeFaceFromScene(LGA::Face_handle f);
    void addVertexToScene(LGA::Vertex_handle vh);
    void removeVertexFromScene(LGA::Vertex_handle vh);

    // Arr_observer interface
public:
    virtual void before_assign(const LGA::Arrangement &) override;
    virtual void after_assign() override;
    virtual void before_clear() override;
    virtual void after_clear() override;
    virtual void before_global_change() override;
    virtual void after_global_change() override;
    virtual void before_attach(const LGA::Arrangement &arr) override;
    virtual void after_attach() override;
    virtual void before_detach() override;
    virtual void after_detach() override;

    virtual void before_create_vertex(const LGA::Point &) override;
    virtual void after_create_vertex(LGA::Vertex_handle vh) override;
    virtual void before_create_boundary_vertex(const LGA::Segment &, CGAL::Arr_curve_end, CGAL::Arr_parameter_space, CGAL::Arr_parameter_space) override;
    virtual void after_create_boundary_vertex(LGA::Vertex_handle) override;
    virtual void before_create_edge(const LGA::Segment &, LGA::Vertex_handle, LGA::Vertex_handle) override;
    virtual void after_create_edge(LGA::Halfedge_handle eh) override;
    virtual void before_modify_vertex(LGA::Vertex_handle, const LGA::Point &) override;
    virtual void after_modify_vertex(LGA::Vertex_handle) override;
    virtual void before_modify_edge(LGA::Halfedge_handle, const LGA::Segment &) override;
    virtual void after_modify_edge(LGA::Halfedge_handle) override;
    virtual void before_split_edge(LGA::Halfedge_handle e, LGA::Vertex_handle v, const LGA::Segment &c1, const LGA::Segment &c2) override;
    virtual void after_split_edge(LGA::Halfedge_handle e1, LGA::Halfedge_handle e2) override;
#if 0
    virtual void before_split_fictitious_edge(Halfedge_handle e, Vertex_handle v) override;
    virtual void after_split_fictitious_edge(Halfedge_handle e1, Halfedge_handle e2) override;
#endif
    virtual void before_split_face(LGA::Face_handle f, LGA::Halfedge_handle e) override;
    virtual void after_split_face(LGA::Face_handle f1, LGA::Face_handle f2, bool is_hole) override;
#if 0
    virtual void before_split_outer_ccb(Face_handle f, Ccb_halfedge_circulator h, Halfedge_handle e) override;
    virtual void after_split_outer_ccb(Face_handle f, Ccb_halfedge_circulator h1, Ccb_halfedge_circulator h2) override;
    virtual void before_split_inner_ccb(Face_handle f, Ccb_halfedge_circulator h, Halfedge_handle e) override;
    virtual void after_split_inner_ccb(Face_handle, Ccb_halfedge_circulator h1, Ccb_halfedge_circulator h2) override;
    virtual void before_add_outer_ccb(Face_handle f, Halfedge_handle e) override;
    virtual void after_add_outer_ccb(Ccb_halfedge_circulator h) override;
    virtual void before_add_inner_ccb(Face_handle f, Halfedge_handle e) override;
    virtual void after_add_inner_ccb(Ccb_halfedge_circulator h) override;
    virtual void before_add_isolated_vertex(Face_handle f, Vertex_handle v) override;
    virtual void after_add_isolated_vertex(Vertex_handle v) override;
#endif
    virtual void before_merge_edge(LGA::Halfedge_handle e1, LGA::Halfedge_handle e2, const LGA::Segment &) override;
    virtual void after_merge_edge(LGA::Halfedge_handle e) override;
    virtual void before_merge_fictitious_edge(LGA::Halfedge_handle e1, LGA::Halfedge_handle e2) override;
    virtual void after_merge_fictitious_edge(LGA::Halfedge_handle e) override;
    virtual void before_merge_face(LGA::Face_handle f1, LGA::Face_handle f2, LGA::Halfedge_handle) override;
    virtual void after_merge_face(LGA::Face_handle f) override;
#if 0
    virtual void before_merge_outer_ccb(Face_handle f, Ccb_halfedge_circulator h1, Ccb_halfedge_circulator h2, Halfedge_handle e) override;
    virtual void after_merge_outer_ccb(Face_handle f, Ccb_halfedge_circulator h) override;
    virtual void before_merge_inner_ccb(Face_handle f, Ccb_halfedge_circulator h1, Ccb_halfedge_circulator h2, Halfedge_handle e) override;
    virtual void after_merge_inner_ccb(Face_handle, Ccb_halfedge_circulator) override;
    virtual void before_move_outer_ccb(Face_handle from_f, Face_handle to_f, Ccb_halfedge_circulator h) override;
    virtual void after_move_outer_ccb(Ccb_halfedge_circulator) override;
    virtual void before_move_inner_ccb(Face_handle from_f, Face_handle to_f, Ccb_halfedge_circulator h) override;
    virtual void after_move_inner_ccb(Ccb_halfedge_circulator) override;
    virtual void before_move_isolated_vertex(Face_handle from_f, Face_handle to_f, Vertex_handle v) override;
    virtual void after_move_isolated_vertex(Vertex_handle v) override;
#endif
    virtual void before_remove_vertex(LGA::Vertex_handle v) override;
    virtual void after_remove_vertex() override;
    virtual void before_remove_edge(LGA::Halfedge_handle e) override;
    virtual void after_remove_edge() override;
#if 0
    virtual void before_remove_outer_ccb(Face_handle f, Ccb_halfedge_circulator h) override;
    virtual void after_remove_outer_ccb(Face_handle f) override;
    virtual void before_remove_inner_ccb(Face_handle f, Ccb_halfedge_circulator h) override;
    virtual void after_remove_inner_ccb(Face_handle) override;
#endif

    // LeGraphicsSceneEventFilter interface
public:
    virtual bool mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;

    // LeGraphicsSceneEventFilter interface
public:
    virtual bool keyPressEvent(QKeyEvent *event) override;
    bool removeVertex(int index);
};
