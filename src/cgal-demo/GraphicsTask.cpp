#include "GraphicsTask.h"

#include "GraphicsArrangementBridge.h"

#include <LeGraphicsView/LeGraphicsItem.h>
#include <LeGraphicsView/LeGraphicsItemLayer.h>
#include <LeGraphicsView/LeGraphicsScene.h>
#include <LeGraphicsView/LeGraphicsView.h>
#include <LeGraphicsView/LeGraphicsStyle.h>

#include <QDebug>
#include <QFinalState>
#include <QGraphicsLineItem>
#include <QGraphicsSceneMouseEvent>
#include <QSignalTransition>
#include <QStateMachine>
#include <QState>

using namespace LGA;

GraphicsTask::GraphicsTask(QObject *parent)
    : QObject(parent)
    , LeGraphicsSceneEventFilter()
    , m_stateMachine(new QStateMachine(this))
    , m_finalState(new QFinalState())
{
    m_stateMachine->addState(m_finalState);

    connect(m_stateMachine, &QStateMachine::started,
            this, [this]() {
        qDebug() << "Started";
        clearConstraints();
        m_bridge->scene()->setEventFilter(this);
    });
    connect(m_stateMachine, &QStateMachine::finished,
            this, [this]() {
        qDebug() << "Finished";
        m_bridge->scene()->setEventFilter(nullptr);
        emit finished();
    });

}

void GraphicsTask::start(GraphicsArrangementBridge *bridge, LeGraphicsView *view)
{
    m_bridge = bridge;
    m_view = view;
    m_stateMachine->start();
}

void GraphicsTask::addInitialState(QAbstractState *state)
{
    m_stateMachine->addState(state);
    m_stateMachine->setInitialState(state);
}

void GraphicsTask::addState(QAbstractState *state)
{
    m_stateMachine->addState(state);
}

QAbstractState *GraphicsTask::finalState() const
{
    return m_finalState;
}

Arrangement *GraphicsTask::arrangement() const
{
    return m_bridge->arrangement();
}

LeGraphicsScene *GraphicsTask::scene() const
{
    return m_bridge->scene();
}

/**************************************************************************************
 * FIXME: re-iplment this whole 'Clicked' stuff in a proper way
 *************************************************************************************/
void GraphicsTask::setClickGoal(GraphicsTask::ClickGoal goal)
{
    m_clickGoal = goal;
    m_point = Point();
    m_vertex = Vertex_handle();
    m_halfedge = Halfedge_handle();
    m_face = Face_handle();
}

void GraphicsTask::addHalfedgeClickedTransition(const QString &text, Halfedge_handle &he,
                                                QState *source, QAbstractState *target)
{
    source->addTransition(this, &GraphicsTask::halfedgeClicked, target);

    connect(source, &QAbstractState::entered,
            this, [this, text]() {
        qDebug() << QString("Select %1 half-edge").arg(text);
        setClickGoal(ClickHalfedge);
    });

    connect(source, &QAbstractState::exited,
            this, [this, &he]() {
        he = m_halfedge;
    });
}

void GraphicsTask::addVertexClickedTransition(const QString &text, Vertex_handle &v,
                                              QState *source, QAbstractState *target)
{
    source->addTransition(this, &GraphicsTask::vertexClicked, target);

    connect(source, &QAbstractState::entered,
            this, [this, text]() {
        qDebug() << QString("Select %1 vertex").arg(text);
        setClickGoal(ClickVertex);
    });

    connect(source, &QAbstractState::exited,
            this, [this, &v]() {
        v = m_vertex;
    });
}

void GraphicsTask::addFaceClickedTransition(const QString &text, Face_handle &f,
                                            QState *source, QAbstractState *target)
{
    source->addTransition(this, &GraphicsTask::faceClicked, target);

    connect(source, &QAbstractState::entered,
            this, [this, text]() {
        qDebug() << QString("Select %1 face").arg(text);
        setClickGoal(ClickFace);
    });

    connect(source, &QAbstractState::exited,
            this, [this, &f]() {
        f = m_face;
    });
}

void GraphicsTask::addPointClickedTransition(const QString &text, Point &p,
                                             QState *source, QAbstractState *target)
{
    source->addTransition(this, &GraphicsTask::pointClicked, target);

    connect(source, &QAbstractState::entered,
            this, [this, text]() {
        qDebug() << QString("Select %1 point").arg(text);
        setClickGoal(ClickPoint);
    });

    connect(source, &QAbstractState::exited,
            this, [this, &p]() {
        p = m_point;
    });
}

void GraphicsTask::clearConstraints()
{
    m_inFace = Face_handle();
    m_noIntersectVertex = Vertex_handle();
    m_haveNoIntersectPoint = false;
    m_noIntersectPoint = Point();
    m_leftTurnHalfedge =Halfedge_handle();
    m_rightTurnHalfedge = Halfedge_handle();
    m_haveNoDegenerateSegment = false;
    m_noDegenerateSegment = Point();
    m_haveCcwBetween = false;
    m_ccwBetweenD1 = Segment();
    m_ccwBetweenD2 = Segment();
    m_haveLeftToRightPoint = false;
    m_leftToRightPoint = Point();
    m_haveRightToLeftPoint = false;
    m_rightToLeftPoint = Point();
    setConstrained(true);
}

void GraphicsTask::setInFaceConstraint(Face_handle ref)
{
    m_inFace = ref;
}

void GraphicsTask::setNoIntersectVertexConstraint(Vertex_handle ref)
{
    m_noIntersectVertex = ref;
}

void GraphicsTask::setNoIntersectPointConstraint(Point ref)
{
    m_haveNoIntersectPoint = true;
    m_noIntersectPoint = ref;
}

void GraphicsTask::setLeftTurnConstraint(Halfedge_handle ref)
{
    m_leftTurnHalfedge = ref;
}

void GraphicsTask::setRightTurnConstraint(Halfedge_handle ref)
{
    m_rightTurnHalfedge = ref;
}

void GraphicsTask::setNoDegenerateSegmentConstraint(Point ref)
{
    m_haveNoDegenerateSegment = true;
    m_noDegenerateSegment = ref;
}

void GraphicsTask::setCounterclockwiseBetweenConstraint(const Segment &d1, const Segment &d2)
{
    m_haveCcwBetween = true;
    m_ccwBetweenD1 = d1;
    m_ccwBetweenD2 = d2;
}

void GraphicsTask::setLeftToRightConstraint(Point ref)
{
    m_haveLeftToRightPoint = true;
    m_leftToRightPoint = ref;
}

void GraphicsTask::setRightToLeftConstraint(Point ref)
{
    m_haveRightToLeftPoint = true;
    m_rightToLeftPoint = ref;
}

void GraphicsTask::checkConstraints(const Point &target)
{
    // TODO: Need a mix of scene->itemAt and locator

    bool noDegenerateSegmentViolated = false;
    if (arrangement()->traits()->equal_2_object()(m_noDegenerateSegment,
                                                  target))
        noDegenerateSegmentViolated = true;

    bool inFaceViolated = false;
    if (m_inFace != Face_handle())
    {
        LocatorResult result = m_bridge->locator()->locate(target);
        const Halfedge_const_handle *h = boost::get<Halfedge_const_handle>(&result);
        const Face_const_handle *f = boost::get<Face_const_handle>(&result);
        const Vertex_const_handle *v = boost::get<Vertex_const_handle>(&result);
        if (f != nullptr)
        {
            if ((*f) != m_inFace)
                inFaceViolated = true;
        }
        else if (v != nullptr)
        {
            if ((*v)->is_isolated())
            {
                if ((*v)->face() != m_face)
                    inFaceViolated = true;
            }
            else
            {
                inFaceViolated = true;
            }
        }
        else if (h != nullptr)
        {
            inFaceViolated = true;
        }
    }

    // TODO: No need to compute the whole zone, stop the process as soon
    // as we hit something else that is not the 'final destination'
    bool noIntersectVertexViolated = false;
    if (m_noIntersectVertex != Vertex_handle())
    {
        if (arrangement()->traits()->equal_2_object()(m_noIntersectVertex->point(),
                                                      target))
            noIntersectVertexViolated = true;
        else
        {
            Segment segment(m_noIntersectVertex->point(), target);

            QList<CGAL::Object> elements;
            CGAL::zone(*arrangement(), segment, std::back_inserter(elements));
            Q_ASSERT(elements.count() >= 2);
            Vertex_handle v1;
            Face_handle f1;
            if (elements.count() > 2)
                noIntersectVertexViolated = true;
            else if (segment.is_directed_right())
            {
                Q_ASSERT(CGAL::assign(v1, elements.value(0)));
                if (!CGAL::assign(f1, elements.value(1)))
                    noIntersectVertexViolated = true;
            }
            else
            {
                Q_ASSERT(CGAL::assign(v1, elements.value(1)));
                if (!CGAL::assign(f1, elements.value(0)))
                    noIntersectVertexViolated = true;
            }
        }
    }

    // TODO: merge with above
    bool noIntersectPointViolated = false;
    if (m_haveNoIntersectPoint)
    {
        if (arrangement()->traits()->equal_2_object()(m_noIntersectPoint,
                                                      target))
            noIntersectPointViolated = true;
        else
        {
            // The zone should only contains a face
            Segment segment(m_noIntersectPoint, target);

            QList<CGAL::Object> elements;
            CGAL::zone(*arrangement(), segment, std::back_inserter(elements));
            Face_handle f1;
            if (elements.count() > 1)
                noIntersectPointViolated = true;
            else if (!CGAL::assign(f1, elements.value(0)))
                noIntersectPointViolated = true;
        }
    }

    bool leftTurnViolated = false;
    if (m_leftTurnHalfedge != Halfedge_handle())
    {
        if (m_leftTurnHalfedge->target()->degree() > 1)
            leftTurnViolated = !CGAL::left_turn(m_leftTurnHalfedge->source()->point(),
                                                 m_leftTurnHalfedge->target()->point(),
                                                 target);
    }

    bool rightTurnViolated = false;
    if (m_rightTurnHalfedge != Halfedge_handle())
    {
        if (m_rightTurnHalfedge->target()->degree() > 1)
            rightTurnViolated = !CGAL::right_turn(m_rightTurnHalfedge->source()->point(),
                                                   m_rightTurnHalfedge->target()->point(),
                                                   target);
    }

    bool ccwBetweenViolated = false;
    if (m_haveCcwBetween)
    {
        Kernel kernel;
        Kernel::Direction_2 d(Kernel::Segment_2(m_ccwBetweenD1.source(), target));
        Kernel::Direction_2 d1(Kernel::Segment_2(m_ccwBetweenD1.source(), m_ccwBetweenD1.target()));
        Kernel::Direction_2 d2(Kernel::Segment_2(m_ccwBetweenD2.source(), m_ccwBetweenD2.target()));
        auto ccw = kernel.counterclockwise_in_between_2_object();
        if (!ccw(d, d1, d2))
            ccwBetweenViolated = true;
    }

    bool leftToRightViolated = false;
    if (m_haveLeftToRightPoint)
    {
        if (arrangement()->traits()->equal_2_object()(m_leftToRightPoint,
                                                      target))
            leftToRightViolated = true;
        else if (!Segment(m_leftToRightPoint, target).is_directed_right())
            leftToRightViolated = true;
    }

    bool rightToLeftViolated = false;
    if (m_haveRightToLeftPoint)
    {
        if (arrangement()->traits()->equal_2_object()(m_rightToLeftPoint,
                                                      target))
            rightToLeftViolated = true;
        else if (Segment(m_rightToLeftPoint, target).is_directed_right())
            rightToLeftViolated = true;
    }

    bool violation =
            noDegenerateSegmentViolated ||
            inFaceViolated ||
            noIntersectVertexViolated ||
            noIntersectPointViolated ||
            leftTurnViolated ||
            rightTurnViolated ||
            ccwBetweenViolated ||
            leftToRightViolated ||
            rightToLeftViolated;
#if 1
    if (inFaceViolated)
        qDebug() << "Constraint: InFace violated";
    if (noIntersectVertexViolated)
        qDebug() << "Constraint: NoIntersect (Vertex) violated";
    if (noIntersectPointViolated)
        qDebug() << "Constraint: NoIntersect (Point) violated";
    if (leftTurnViolated)
        qDebug() << "Constraint: LeftTurn violated";
    if (rightTurnViolated)
        qDebug() << "Constraint: RightTurn violated";
    if (noDegenerateSegmentViolated)
        qDebug() << "Constraint: NoDegenerateSegment violated";
    if (ccwBetweenViolated)
        qDebug() << "Constraint: CcwBetween violated";
    if (rightToLeftViolated)
        qDebug() << "Constraint: LeftToRight violated";
    if (leftToRightViolated)
        qDebug() << "Constraint: RightToLeft violated";
    if (!violation)
        qDebug() << "Constraint: No violation";
#endif

    setConstrained(!violation);
}

void GraphicsTask::setConstrained(bool constrained)
{
    if (m_constrained == constrained)
        return;
    m_constrained = constrained;
    emit constrainedChanged(m_constrained);
}

bool GraphicsTask::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    checkConstraints(Point(event->scenePos().x(), event->scenePos().y()));
    return false;
}

bool GraphicsTask::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() != Qt::LeftButton)
        return false;

    auto item = static_cast<LeGraphicsItem*>(m_bridge->scene()->itemAt(event->scenePos(), m_view->transform()));
    if (m_bridge->isVertex(item))
    {
        auto v = m_bridge->vertex(item);
        qDebug() << QString("Vertex clicked: [%1, %2]")
                    .arg(CGAL::to_double(v->point().x()))
                    .arg(CGAL::to_double(v->point().y()));
        if (m_clickGoal == ClickVertex && isConstrained())
        {
            m_vertex = v;
            emit vertexClicked(m_vertex);
        }
    }
    else if (m_bridge->isHalfedge(item))
    {
        auto he = m_bridge->halfedge(item);
        qDebug() << QString("HalfEdge clicked: [%1, %2] -> [%3, %4]")
                    .arg(CGAL::to_double(he->source()->point().x()))
                    .arg(CGAL::to_double(he->source()->point().y()))
                    .arg(CGAL::to_double(he->target()->point().x()))
                    .arg(CGAL::to_double(he->target()->point().y()));
        if (m_clickGoal == ClickHalfedge && isConstrained())
        {
            m_halfedge = he;
            emit halfedgeClicked(m_halfedge);
        }
    }
    else if (m_clickGoal == ClickFace && item == nullptr && isConstrained())
    {
        m_face = m_bridge->unboundedFace();
        emit faceClicked(m_face);
    }
    else if (m_clickGoal == ClickFace && m_bridge->isFace(item) && isConstrained())
    {
        m_face = m_bridge->face(item);
        emit faceClicked(m_face);
    }
    else
    {
        auto p = Point(event->scenePos().x(), event->scenePos().y());
        qDebug() << QString("Point clicked: [%1, %2]")
                    .arg(CGAL::to_double(p.x()))
                    .arg(CGAL::to_double(p.y()));
        if (m_clickGoal == ClickPoint && isConstrained())
        {
            m_point = p;
            emit pointClicked(m_point);
        }
    }

    return true;
}

/**************************************************************************************
 * InsertPointInFaceTask
 *************************************************************************************/

InsertPointInFaceTask::InsertPointInFaceTask(QObject *parent)
    : GraphicsTask(parent)
    , m_selectFaceState(new QState())
    , m_selectPointState(new QState())
{
    addInitialState(m_selectFaceState);
    addState(m_selectPointState);

    addFaceClickedTransition("face", m_face,
                             m_selectFaceState, m_selectPointState);
    addPointClickedTransition("location", m_point,
                              m_selectPointState, finalState());

    connect(m_selectPointState, &QState::entered,
            this, [this]() {
        //        m_lineItem->setPath(QPainterPath(), 0); // pixels
        //        scene()->foregroundLayer()->addToLayer(m_lineItem);
        setInFaceConstraint(m_face);
    });
}

UndoCommand *InsertPointInFaceTask::createCommand()
{
    return new InsertInCommand(arrangement(),
                               m_point,
                               m_face);
}

/**************************************************************************************
 * InsertSegmentInFaceTask
 *************************************************************************************/

InsertSegmentInFaceTask::InsertSegmentInFaceTask(QObject *parent)
    : GraphicsTask(parent)
    , m_selectFaceState(new QState())
    , m_selectPoint1State(new QState())
    , m_selectPoint2State(new QState())
    , m_lineItem(new LeGraphicsStrokedPathItem(GftTrace))
{
    m_lineItem->setAcceptHoverEvents(false);
    m_lineItem->setFlag(LeGraphicsItem::ItemIsSelectable, false);
    m_lineItem->setStateFlag(LeGraphicsStyle::MarkedGood, true);
    m_lineItem->setStateFlag(LeGraphicsStyle::MarkedBad, false);

    addInitialState(m_selectFaceState);
    addState(m_selectPoint1State);
    addState(m_selectPoint2State);

    addFaceClickedTransition("face", m_face,
                             m_selectFaceState, m_selectPoint1State);
    addPointClickedTransition("first", m_point1,
                              m_selectPoint1State, m_selectPoint2State);
    addPointClickedTransition("second", m_point2,
                              m_selectPoint2State, finalState());

    connect(m_selectPoint1State, &QState::entered,
            this, [this]() {
        m_lineItem->setPath(QPainterPath(), 0); // pixels
        scene()->foregroundLayer()->addToLayer(m_lineItem);
        clearConstraints();
        setInFaceConstraint(m_face);
    });

    connect(m_selectPoint2State, &QState::entered,
            this, [this]() {
        clearConstraints();
        setNoIntersectPointConstraint(m_point1);
        setNoDegenerateSegmentConstraint(m_point1);
        checkConstraints(m_point1);
    });

    connect(m_selectPoint2State, &QState::exited,
            this, [this]() {
        scene()->foregroundLayer()->removeFromLayer(m_lineItem);
    });

    connect(this, &GraphicsTask::constrainedChanged,
            this, [this](bool constrained) {
        m_lineItem->setStateFlag(LeGraphicsStyle::MarkedGood, constrained);
        m_lineItem->setStateFlag(LeGraphicsStyle::MarkedBad, !constrained);
    });
}

bool InsertSegmentInFaceTask::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    GraphicsTask::mouseMoveEvent(event);

    if (!m_selectPoint2State->active())
        return false;

    QPainterPath path;
    path.moveTo(CGAL::to_double(m_point1.x()),
                CGAL::to_double(m_point1.y()));
    path.lineTo(event->scenePos());
    m_lineItem->setPath(path, 0);
    return false;
}

UndoCommand *InsertSegmentInFaceTask::createCommand()
{
    return new InsertInCommand(arrangement(),
                               Segment(m_point1, m_point2),
                               m_face);
}

/**************************************************************************************
 * InsertSegmentAtVerticesVertexVertexTask
 *************************************************************************************/

InsertSegmentAtVerticesVertexVertexTask::InsertSegmentAtVerticesVertexVertexTask(QObject *parent)
    : GraphicsTask(parent)
    , m_selectVertex1State(new QState())
    , m_selectVertex2State(new QState())
    , m_lineItem(new LeGraphicsStrokedPathItem(GftTrace))
{
    m_lineItem->setAcceptHoverEvents(false);
    m_lineItem->setFlag(LeGraphicsItem::ItemIsSelectable, false);
    m_lineItem->setStateFlag(LeGraphicsStyle::MarkedGood, true);
    m_lineItem->setStateFlag(LeGraphicsStyle::MarkedBad, false);

    addInitialState(m_selectVertex1State);
    addState(m_selectVertex2State);

    addVertexClickedTransition("first", m_v1,
                               m_selectVertex1State, m_selectVertex2State);
    addVertexClickedTransition("second", m_v2,
                               m_selectVertex2State, finalState());

    connect(m_selectVertex2State, &QState::entered,
            this, [this]() {
        m_lineItem->setPath(QPainterPath(), 0); // pixels
        scene()->foregroundLayer()->addToLayer(m_lineItem);
        clearConstraints();
        setNoIntersectVertexConstraint(m_v1);
        setNoDegenerateSegmentConstraint(m_v1->point());
        checkConstraints(m_v1->point());
    });

    connect(m_selectVertex2State, &QState::exited,
            this, [this]() {
        scene()->foregroundLayer()->removeFromLayer(m_lineItem);
    });

    connect(this, &GraphicsTask::constrainedChanged,
            this, [this](bool constrained) {
        m_lineItem->setStateFlag(LeGraphicsStyle::MarkedGood, constrained);
        m_lineItem->setStateFlag(LeGraphicsStyle::MarkedBad, !constrained);
    });
}

bool InsertSegmentAtVerticesVertexVertexTask::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    GraphicsTask::mouseMoveEvent(event);

    if (!m_selectVertex2State->active())
        return false;

    QPainterPath path;
    path.moveTo(CGAL::to_double(m_v1->point().x()),
                CGAL::to_double(m_v1->point().y()));
    path.lineTo(event->scenePos());
    m_lineItem->setPath(path, 0);
    return false;
}

UndoCommand *InsertSegmentAtVerticesVertexVertexTask::createCommand()
{
    return new InsertAtCommand(arrangement(),
                               Segment(m_v1->point(), m_v2->point()),
                               m_v1, m_v2);
}

/**************************************************************************************
 * InsertSegmentAtVerticesEdgeVertexTask
 *************************************************************************************/

InsertSegmentAtVerticesEdgeVertexTask::InsertSegmentAtVerticesEdgeVertexTask(QObject *parent)
    : GraphicsTask(parent)
    , m_selectPred1State(new QState())
    , m_selectVertex2State(new QState())
    , m_lineItem(new LeGraphicsStrokedPathItem(GftTrace))
{
    m_lineItem->setAcceptHoverEvents(false);
    m_lineItem->setFlag(LeGraphicsItem::ItemIsSelectable, false);
    m_lineItem->setStateFlag(LeGraphicsStyle::MarkedGood, true);
    m_lineItem->setStateFlag(LeGraphicsStyle::MarkedBad, false);

    addInitialState(m_selectPred1State);
    addState(m_selectVertex2State);

    addHalfedgeClickedTransition("predecessor", m_predecessor,
                                 m_selectPred1State, m_selectVertex2State);
    addVertexClickedTransition("target", m_v2,
                               m_selectVertex2State, finalState());

    connect(m_selectVertex2State, &QState::entered,
            this, [this]() {
        m_lineItem->setPath(QPainterPath(), 0); // pixels
        scene()->foregroundLayer()->addToLayer(m_lineItem);

        auto circ = m_predecessor->target()->incident_halfedges();
        while(circ != m_predecessor)
            ++circ;
        m_successor = ++circ;

        clearConstraints();
        setNoIntersectVertexConstraint(m_predecessor->target());
        setNoDegenerateSegmentConstraint(m_predecessor->target()->point());
        setCounterclockwiseBetweenConstraint(Segment(m_successor->target()->point(),
                                                     m_successor->source()->point()),
                                             Segment(m_predecessor->target()->point(),
                                                     m_predecessor->source()->point()));
        checkConstraints(m_predecessor->target()->point());
    });

    connect(m_selectVertex2State, &QState::exited,
            this, [this]() {
        scene()->foregroundLayer()->removeFromLayer(m_lineItem);
    });

    connect(this, &GraphicsTask::constrainedChanged,
            this, [this](bool constrained) {
        m_lineItem->setStateFlag(LeGraphicsStyle::MarkedGood, constrained);
        m_lineItem->setStateFlag(LeGraphicsStyle::MarkedBad, !constrained);
    });
}

UndoCommand *InsertSegmentAtVerticesEdgeVertexTask::createCommand()
{
    return new InsertAtCommand(arrangement(),
                               Segment(m_predecessor->target()->point(), m_v2->point()),
                               m_predecessor, m_v2);
}

bool InsertSegmentAtVerticesEdgeVertexTask::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    GraphicsTask::mouseMoveEvent(event);

    if (!m_selectVertex2State->active())
        return false;

    QPainterPath path;
    path.moveTo(CGAL::to_double(m_predecessor->target()->point().x()),
                CGAL::to_double(m_predecessor->target()->point().y()));
    path.lineTo(event->scenePos());
    m_lineItem->setPath(path, 0);
    return false;
}

/**************************************************************************************
 * InsertSegmentInFaceTask
 *************************************************************************************/

InsertSegmentAtVerticesEdgeEdgeTask::InsertSegmentAtVerticesEdgeEdgeTask(QObject *parent)
    : GraphicsTask(parent)
    , m_selectPred1State(new QState())
    , m_selectPred2State(new QState())
{
    addInitialState(m_selectPred1State);
    addState(m_selectPred2State);

    addHalfedgeClickedTransition("first predecessor", m_pred1,
                                 m_selectPred1State, m_selectPred2State);
    addHalfedgeClickedTransition("second predecessor", m_pred2,
                                 m_selectPred2State, finalState());
}

UndoCommand *InsertSegmentAtVerticesEdgeEdgeTask::createCommand()
{
    return new InsertAtCommand(arrangement(),
                               Segment(m_pred1->target()->point(), m_pred2->target()->point()),
                               m_pred1, m_pred2);
}

/**************************************************************************************
 * InsertSegmentFromLeftVertexTask
 *************************************************************************************/

InsertSegmentFromLeftVertexTask::InsertSegmentFromLeftVertexTask(QObject *parent)
    : GraphicsTask(parent)
    , m_selectVertex1State(new QState())
    , m_selectPoint2State(new QState())
    , m_lineItem(new LeGraphicsStrokedPathItem(GftTrace))
{
    m_lineItem->setAcceptHoverEvents(false);
    m_lineItem->setFlag(LeGraphicsItem::ItemIsSelectable, false);
    m_lineItem->setStateFlag(LeGraphicsStyle::MarkedGood, true);
    m_lineItem->setStateFlag(LeGraphicsStyle::MarkedBad, false);

    addInitialState(m_selectVertex1State);
    addState(m_selectPoint2State);

    addVertexClickedTransition("left", m_v1,
                               m_selectVertex1State, m_selectPoint2State);
    addPointClickedTransition("right", m_p2,
                              m_selectPoint2State, finalState());

    connect(m_selectPoint2State, &QState::entered,
            this, [this]() {
        m_lineItem->setPath(QPainterPath(), 0); // pixels
        scene()->foregroundLayer()->addToLayer(m_lineItem);

        clearConstraints();
        setNoIntersectVertexConstraint(m_v1);
        setNoDegenerateSegmentConstraint(m_v1->point());
        setLeftToRightConstraint(m_v1->point());
        checkConstraints(m_v1->point());
    });

    connect(m_selectPoint2State, &QState::exited,
            this, [this]() {
        scene()->foregroundLayer()->removeFromLayer(m_lineItem);
    });

    connect(this, &GraphicsTask::constrainedChanged,
            this, [this](bool constrained) {
        m_lineItem->setStateFlag(LeGraphicsStyle::MarkedGood, constrained);
        m_lineItem->setStateFlag(LeGraphicsStyle::MarkedBad, !constrained);
    });
}

bool InsertSegmentFromLeftVertexTask::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    GraphicsTask::mouseMoveEvent(event);

    if (!m_selectPoint2State->active())
        return false;

    QPainterPath path;
    path.moveTo(CGAL::to_double(m_v1->point().x()),
                CGAL::to_double(m_v1->point().y()));
    path.lineTo(event->scenePos());
    m_lineItem->setPath(path, 0);
    return false;
}

UndoCommand *InsertSegmentFromLeftVertexTask::createCommand()
{
    return new InsertFromLeftCommand(arrangement(),
                                     Segment(m_v1->point(), m_p2),
                                     m_v1);
}
