#pragma once

#include <QObject>

#include <LeGraphicsView/LeGraphicsSceneEventFilter.h>

class LeGraphicsView;
class LeGraphicsScene;

#include <LeGeometricArrangement/LeGeometricArrangement.h>
#include <LeGeometricArrangement/UndoCommand.h>

class GraphicsArrangementBridge;

class QAbstractState;
class QStateMachine;
class QState;

class LeGraphicsStrokedPathItem;

class GraphicsTask : public QObject, public LeGraphicsSceneEventFilter
{
    Q_OBJECT

    Q_PROPERTY(bool constrained READ isConstrained NOTIFY constrainedChanged)

public:
    explicit GraphicsTask(QObject *parent = 0);

    void start(GraphicsArrangementBridge *bridge, LeGraphicsView *view);

    virtual LGA::UndoCommand *createCommand() = 0;

    bool isConstrained() const { return m_constrained; }

signals:
    void finished();
    void constrainedChanged(bool constrained);

protected:signals:
    void pointClicked(const LGA::Point &point);
    void vertexClicked(LGA::Vertex_handle vertex);
    void halfedgeClicked(LGA::Halfedge_handle halfedge);
    void edgeClicked(LGA::Halfedge_handle edge);
    void faceClicked(LGA::Face_handle face);

protected:
    void addInitialState(QAbstractState *state);
    void addState(QAbstractState *state);
    QAbstractState *finalState() const;
    LGA::Arrangement *arrangement() const;
    LeGraphicsScene *scene() const;

    // FIXME: Use a custom transition?
    enum ClickGoal
    {
        ClickPoint,
        ClickVertex,
        ClickHalfedge,
        ClickFace
    };
    void setClickGoal(ClickGoal goal);
    ClickGoal m_clickGoal;
    LGA::Point m_point;
    LGA::Face_handle m_face;
    LGA::Halfedge_handle m_halfedge;
    LGA::Vertex_handle m_vertex;

    void addHalfedgeClickedTransition(const QString &text,
                                      LGA::Halfedge_handle &he,
                                      QState *source,
                                      QAbstractState *target);
    void addVertexClickedTransition(const QString &text,
                                    LGA::Vertex_handle &v,
                                    QState *source,
                                    QAbstractState *target);
    void addFaceClickedTransition(const QString &text,
                                  LGA::Face_handle &f,
                                  QState *source,
                                  QAbstractState *target);
    void addPointClickedTransition(const QString &text,
                                   LGA::Point &p,
                                   QState *source,
                                   QAbstractState *target);

    void clearConstraints();
    void setInFaceConstraint(LGA::Face_handle ref);
    void setNoIntersectVertexConstraint(LGA::Vertex_handle ref);
    void setNoIntersectPointConstraint(LGA::Point ref);
    void setLeftTurnConstraint(LGA::Halfedge_handle ref);
    void setRightTurnConstraint(LGA::Halfedge_handle ref);
    void setNoDegenerateSegmentConstraint(LGA::Point ref);
    void setCounterclockwiseBetweenConstraint(const LGA::Segment &d1,
                                              const LGA::Segment &d2);
    void setLeftToRightConstraint(LGA::Point ref);
    void setRightToLeftConstraint(LGA::Point ref);

//private:
    void checkConstraints(const LGA::Point &target);
    void setConstrained(bool constrained);
    bool m_constrained = true;
    LGA::Face_handle m_inFace;
    bool m_haveNoIntersectPoint = false;
    LGA::Point m_noIntersectPoint;
    LGA::Vertex_handle m_noIntersectVertex;
    LGA::Halfedge_handle m_leftTurnHalfedge;
    LGA::Halfedge_handle m_rightTurnHalfedge;
    bool m_haveNoDegenerateSegment = false;
    LGA::Point m_noDegenerateSegment;
    bool m_haveCcwBetween = false;
    LGA::Segment m_ccwBetweenD1;
    LGA::Segment m_ccwBetweenD2;
    bool m_haveLeftToRightPoint = false;
    LGA::Point m_leftToRightPoint;
    bool m_haveRightToLeftPoint = false;
    LGA::Point m_rightToLeftPoint;

//private:
    GraphicsArrangementBridge *m_bridge = nullptr;
    LeGraphicsView *m_view = nullptr;
    QStateMachine *m_stateMachine = nullptr;
    QAbstractState *m_finalState = nullptr;

    // LeGraphicsSceneEventFilter interface
public:
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
};

class InsertPointInFaceTask: public GraphicsTask
{
    Q_OBJECT

public:
    explicit InsertPointInFaceTask(QObject *parent = nullptr);

private:
    QState *m_selectFaceState = nullptr;
    QState *m_selectPointState = nullptr;
    LGA::Point m_point;
    LGA::Face_handle m_face;

    // GraphicsTask interface
public:
    virtual LGA::UndoCommand *createCommand() override;
};

class InsertSegmentInFaceTask: public GraphicsTask
{
    Q_OBJECT

public:
    explicit InsertSegmentInFaceTask(QObject *parent = nullptr);

public slots:

private:
    QState *m_selectFaceState = nullptr;
    QState *m_selectPoint1State = nullptr;
    QState *m_selectPoint2State = nullptr;
    LGA::Point m_point;
    LGA::Point m_point1;
    LGA::Point m_point2;
    LGA::Face_handle m_face;
    LeGraphicsStrokedPathItem *m_lineItem = nullptr;

    // GraphicsTask interface
public:
    virtual LGA::UndoCommand *createCommand() override;

    // LeGraphicsSceneEventFilter interface
public:
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
};


class InsertSegmentAtVerticesVertexVertexTask: public GraphicsTask
{
    Q_OBJECT

public:
    explicit InsertSegmentAtVerticesVertexVertexTask(QObject *parent = nullptr);

public slots:

private:
    QState *m_selectVertex1State = nullptr;
    QState *m_selectVertex2State = nullptr;
    LGA::Vertex_handle m_v1;
    LGA::Vertex_handle m_v2;
    LeGraphicsStrokedPathItem *m_lineItem = nullptr;

    // GraphicsTask interface
public:
    virtual LGA::UndoCommand *createCommand() override;

    // LeGraphicsSceneEventFilter interface
public:
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
};

class InsertSegmentAtVerticesEdgeVertexTask: public GraphicsTask
{
    Q_OBJECT

public:
    explicit InsertSegmentAtVerticesEdgeVertexTask(QObject *parent = nullptr);

public slots:

private:
    QState *m_selectPred1State = nullptr;
    QState *m_selectVertex2State = nullptr;
    LGA::Halfedge_handle m_predecessor;
    LGA::Vertex_handle m_v2;
    LGA::Halfedge_handle m_successor;
    LeGraphicsStrokedPathItem *m_lineItem = nullptr;

    // GraphicsTask interface
public:
    virtual LGA::UndoCommand *createCommand() override;

    // LeGraphicsSceneEventFilter interface
public:
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
};

class InsertSegmentAtVerticesEdgeEdgeTask: public GraphicsTask
{
    Q_OBJECT

public:
    explicit InsertSegmentAtVerticesEdgeEdgeTask(QObject *parent = nullptr);

public slots:

private:
    QState *m_selectPred1State = nullptr;
    QState *m_selectPred2State = nullptr;
    LGA::Halfedge_handle m_halfedge;
    LGA::Halfedge_handle m_pred1;
    LGA::Halfedge_handle m_pred2;

    // GraphicsTask interface
public:
    virtual LGA::UndoCommand *createCommand() override;
};

class InsertSegmentFromLeftVertexTask: public GraphicsTask
{
    Q_OBJECT

public:
    explicit InsertSegmentFromLeftVertexTask(QObject *parent = nullptr);

public slots:

private:
    QState *m_selectVertex1State = nullptr;
    QState *m_selectPoint2State = nullptr;
    LGA::Vertex_handle m_v1;
    LGA::Point m_p2;
    LeGraphicsStrokedPathItem *m_lineItem = nullptr;

    // GraphicsTask interface
public:
    virtual LGA::UndoCommand *createCommand() override;

    // LeGraphicsSceneEventFilter interface
public:
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
};
