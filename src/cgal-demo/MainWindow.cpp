#include "MainWindow.h"

#include "GraphicsArrangementBridge.h"
#include "ArrangementTreeModel.h"

#include <LeGeometricArrangement/LeGeometricArrangement.h>
#include <LeGeometricArrangement/UndoCommand.h>

#include "LeGraphicsView/LeGraphicsScene.h"
#include "LeGraphicsView/LeGraphicsView.h"
#include "LeGraphicsView/LeGraphicsPalette.h"
#include "LeGraphicsView/LeGraphicsFeatureOption.h"
#include "LeGraphicsView/LeGraphicsItem.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/Solarized.h"

#include <QDockWidget>
#include <QTimer>
#include <QToolBar>
#include <QTreeView>
#include <QUndoStack>
#include <QUndoView>
#include <QVBoxLayout>

// For demo
#include <CGAL/Timer.h>
#include <list>
#include <fstream>

using namespace LGA;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_graphicsScene(new LeGraphicsScene(this))
    , m_graphicsView(new LeGraphicsView())
{
    setupArrangement();
    setupUndoStack();
    populateArrangement(); // will use undo stack for demo/testing purpose
    setupGraphicsScene();
    setupArrangementTreeModel();
    setupArrangementBridge();
    setupGraphicsView();
    setupArrangementTreeView();
    setupToolBar();
    setupUndoView();
    setCentralWidget(m_graphicsView);
    setGeometry(0, 0, 1920, 540);
}

MainWindow::~MainWindow()
{

}

void MainWindow::setupGraphicsScene()
{
    m_graphicsScene->setObjectName("scene");

    LeGraphicsPalette palette;
    palette.setBrush(LeGraphicsPalette::Background, Solarized::backgroundHighlight);
    palette.setBrush(LeGraphicsPalette::Grid, Solarized::secondaryContent);
    palette.setBrush(LeGraphicsPalette::Origin, Solarized::foreground);
    palette.setBrush(LeGraphicsPalette::Cursor, Solarized::foreground);
    palette.setBrush(LeGraphicsPalette::Selection, Solarized::foregroundHighlight);
    palette.setBrush(LeGraphicsPalette::Highlight, Solarized::foreground);
    palette.setBrush(LeGraphicsPalette::Handle, Solarized::foreground);
    palette.setBrush(LeGraphicsPalette::RubberBand, Solarized::foregroundHighlight);
    palette.setBrush(LeGraphicsPalette::BadMark, Solarized::red);
    palette.setBrush(LeGraphicsPalette::GoodMark, Solarized::green);
    //palette.setBrush(LeGraphicsPalette::Feature, Solarized::primaryContent);
    m_graphicsScene->setGraphicsPalette(palette);

    auto featureOption = LeGraphicsFeatureOption();
    featureOption.setAllFeatureVisible(true);
    featureOption.setAllFeatureBrush(QBrush(Solarized::foreground, Qt::SolidPattern));
    m_graphicsScene->foregroundLayer()->setFeatureOption(featureOption);
    m_graphicsScene->foregroundLayer()->setVisible(true);

    auto layer = new LeGraphicsItemLayer();
    layer->setNames("Layer", "Layer", "Layer");
    featureOption = LeGraphicsFeatureOption();
    featureOption.setAllFeatureVisible(true);
    featureOption.setAllFeatureBrush(QBrush(Solarized::blue, Qt::SolidPattern));
    featureOption.setFeatureBrush(GftPad, QBrush(Solarized::red, Qt::SolidPattern));
    featureOption.setFeatureBrush(GftTrace, QBrush(Solarized::green, Qt::SolidPattern));
    layer->setFeatureOption(featureOption);
    layer->setVisible(true);
    m_graphicsScene->setLayers(QList<LeGraphicsItemLayer*>() << layer);
    m_graphicsScene->setCurrentLayer(layer);
}

#include "GraphicsTask.h"

void MainWindow::setupGraphicsView()
{
    m_graphicsView->setObjectName("view");
    m_graphicsView->setLeGraphicsScene(m_graphicsScene);
    m_graphicsView->setDrawGridInForeground(false);
    m_graphicsView->setInteractionMode(LeGraphicsView::SingleSelectionMode);
    m_graphicsView->setOrientation(LeGraphicsView::XRightYUp);
    m_graphicsView->setFrameStyle(QFrame::NoFrame);
    m_graphicsView->layout()->setMargin(0);
    m_graphicsView->layout()->setSpacing(0);
}

void MainWindow::setupArrangement()
{
    m_arrangement = new Arrangement();
}

void MainWindow::setupArrangementBridge()
{
    m_arrangementBridge = new GraphicsArrangementBridge(this);
    m_arrangementBridge->setup(m_arrangement, m_graphicsScene);
}

void MainWindow::setupArrangementTreeModel()
{
    m_arrangementTreeModel = new ArrangementTreeModel(this);
    m_arrangementTreeModel->setArrangement(m_arrangement);
}

void MainWindow::setupArrangementTreeView()
{
    auto dock = new QDockWidget();
    auto view = new QTreeView();
    view->setModel(m_arrangementTreeModel);
    view->setExpanded(QModelIndex(), true);
    dock->setWidget(view);
    addDockWidget(Qt::LeftDockWidgetArea, dock);
}

void MainWindow::addTool(QToolBar *toolBar, const QIcon &icon, const QString &text, GraphicsTask *task)
{
    if (task == nullptr)
        return;
    auto action = toolBar->addAction(icon, text);
    connect(action, &QAction::triggered,
            this, [this, task]() {
        task->start(m_arrangementBridge, m_graphicsView);
    });
    connect(task, &GraphicsTask::finished,
            this, [this, task]() {
        m_undoStack->push(task->createCommand());
    });
}

void MainWindow::setupToolBar()
{
    auto toolBar = addToolBar("Tools");

    addTool(toolBar,
            QIcon(":/icons/insert-at-vertices-edge-edge.svg"),
            "Insert a segment, by 2 half-edges",
            new InsertSegmentAtVerticesEdgeEdgeTask(this));
    addTool(toolBar,
            QIcon(":/icons/insert-at-vertices-edge-vertex.svg"),
            "Insert a segment, by half-edge and vertex",
            new InsertSegmentAtVerticesEdgeVertexTask(this));
    addTool(toolBar,
            QIcon(":/icons/insert-at-vertices-vertex-vertex.svg"),
            "Insert a segment, by 2 vertices",
            new InsertSegmentAtVerticesVertexVertexTask(this));
    addTool(toolBar,
            QIcon(":/icons/insert-from-left-vertex.svg"),
            "Insert a segment, by left vertex",
            new InsertSegmentFromLeftVertexTask(this));
    addTool(toolBar,
            QIcon(":/icons/insert-from-right-vertex.svg"),
            "Insert a segment, by right vertex",
            nullptr);
    addTool(toolBar,
            QIcon(":/icons/insert-from-left-edge.svg"),
            "Insert a segment, by left half-edge",
            nullptr);
    addTool(toolBar,
            QIcon(":/icons/insert-from-right-edge.svg"),
            "Insert a segment, by right half-edge",
            nullptr);
    addTool(toolBar,
            QIcon(":/icons/insert-point-in-face.svg"),
            "Insert a vertex, by face and point",
            new InsertPointInFaceTask(this));
    addTool(toolBar,
            QIcon(":/icons/insert-segment-in-face.svg"),
            "Insert a segment, by face and 2 points",
            new InsertSegmentInFaceTask(this));
    addTool(toolBar,
            QIcon(":/icons/remove-isolated-vertex.svg"),
            "Remove an isolated vertex",
            nullptr);
    addTool(toolBar,
            QIcon(":/icons/remove-edge.svg"),
            "Remove an edge",
            nullptr);
    addTool(toolBar,
            QIcon(":/icons/merge-edge.svg"),
            "Merge 2 edges",
            nullptr);
    addTool(toolBar,
            QIcon(":/icons/split-edge.svg"),
            "Split an edge by a point",
            nullptr);
}

void MainWindow::setupUndoStack()
{
    m_undoStack = new QUndoStack();
    auto action = m_undoStack->createUndoAction(this);
    action->setShortcut(QKeySequence(Qt::Key_Undo));
    action = m_undoStack->createRedoAction(this);
    action->setShortcut(QKeySequence(Qt::Key_Redo));
}

void MainWindow::setupUndoView()
{
    auto dock = new QDockWidget();
    auto view = new QUndoView();
    view->setStack(m_undoStack);
    dock->setWidget(view);
    addDockWidget(Qt::RightDockWidgetArea, dock);
}

void MainWindow::populateArrangement()
{
#if 1 // Empty
    m_graphicsScene->setSceneRect(-50, -50, 100, 100);
#elif 1 // Empty
    m_graphicsScene->setSceneRect(-50, -50, 100, 100);

    Point p00(0, 0), p10(1, 0), p20(2, 0), p30(3, 0), p01(0, 1), p02(0, 2), p03(0, 3), p0505(0.5, 0.5), p1010(1, 1), p2020(2, 2);
    QList<Segment> segments({Segment(p03, p00),
                             Segment(p00, p30),
                             Segment(p30, p03),
                             //                             Segment(p01, p00),
                             //                             Segment(p00, p10),
                             //                             Segment(p10, p01),
                             //                             Segment(p02, p20),
                             //                             Segment(p00, p20),
                             //                             Segment(p20, p02),
                             Segment(p0505, p2020),
                            });

    InsertInCommand *insertInCommand;
    InsertAtCommand *insertAtCommand;

    //    for (const Segment &segment: segments)
    //    {
    //        insertInCommand = new InsertInCommand(m_arrangement, segment.source(), m_arrangement->unbounded_face());
    //        m_undoStack->push(insertInCommand);
    //        Vertex_handle v1 = insertInCommand->vertexHandle();

    //        insertInCommand = new InsertInCommand(m_arrangement, segment.target(), m_arrangement->unbounded_face());
    //        m_undoStack->push(insertInCommand);
    //        Vertex_handle v2 = insertInCommand->vertexHandle();

    //        insertAtCommand = new InsertAtCommand(m_arrangement, segment, v1, v2);
    //        m_undoStack->push(insertAtCommand);
    //    }

    insertInCommand = new InsertInCommand(m_arrangement, p00, m_arrangement->unbounded_face());
    m_undoStack->push(insertInCommand);
    Vertex_handle v00 = insertInCommand->vertex();

    insertInCommand = new InsertInCommand(m_arrangement, p03, m_arrangement->unbounded_face());
    m_undoStack->push(insertInCommand);
    Vertex_handle v03 = insertInCommand->vertex();

    insertInCommand = new InsertInCommand(m_arrangement, p30, m_arrangement->unbounded_face());
    m_undoStack->push(insertInCommand);
    Vertex_handle v30 = insertInCommand->vertex();

    insertAtCommand = new InsertAtCommand(m_arrangement, Segment(p00, p03), v00, v03);
    m_undoStack->push(insertAtCommand);
    //Halfedge_handle e1 = insertAtCommand->halfedgeHandle();

    insertAtCommand = new InsertAtCommand(m_arrangement, Segment(p00, p30), v00, v30);
    m_undoStack->push(insertAtCommand);
    //Halfedge_handle e1 = insertAtCommand->halfedgeHandle();

    insertAtCommand = new InsertAtCommand(m_arrangement, Segment(p30, p03), v30, v03);
    m_undoStack->push(insertAtCommand);
    //Halfedge_handle e1 = insertAtCommand->halfedgeHandle();

    //    for (int i=0; i<100; i++)
    //    {
    //        while (m_undoStack->canUndo())
    //            m_undoStack->undo();
    //        while (m_undoStack->canRedo())
    //            m_undoStack->redo();
    //    }

#elif 0
    Point        p00(0, 0), p10(1, 0), p20(2, 0), p30(3, 0), p01(0, 1), p02(0, 2), p03(0, 3), p0505(0.5, 0.5), p1010(1, 1), p2020(2, 2);
    Segment      cv[] = {Segment(p03, p00),
                         Segment(p00, p30),
                         Segment(p30, p03),
                         Segment(p0505, p2020),
                        };
    CGAL::insert(*m_arrangement, &cv[0], &cv[sizeof(cv)/sizeof(Segment)]);
    m_graphicsScene->setSceneRect(-5, -5, 10, 10);

#elif 0 // Triangle
    Point        p1(10, 10), p2(10, 20), p3(20, 10);
    Segment      cv[] = {Segment(p1, p2), Segment(p2, p3), Segment(p3, p1)};
    insert(*m_arrangement, &cv[0], &cv[sizeof(cv)/sizeof(Segment)]);
    m_graphicsScene->setSceneRect(-50, -50, 100, 100);
#elif 0 // H
    Segment       s1 (Point(1, 3), Point(4, 3));
    Halfedge_handle e1 = m_arrangement->insert_in_face_interior (s1, m_arrangement->unbounded_face());
    Segment       s2 (Point(1, 4), Point(4, 4));
    Halfedge_handle e2 = m_arrangement->insert_in_face_interior (s2, m_arrangement->unbounded_face());
    insert(*m_arrangement, Segment(Point(1, 1), Point(1, 6)));
    insert(*m_arrangement, Segment(Point(4, 1), Point(4, 6)));
    m_graphicsScene->setSceneRect(-50, -50, 100, 100);
#elif 0 // 1 horizontal segment
    Segment       s1 (Point(1, 3), Point(4, 3));
    Halfedge_handle e1 = m_arrangement->insert_in_face_interior (s1, m_arrangement->unbounded_face());
    m_graphicsScene->setSceneRect(-50, -50, 100, 100);
#else
    // Get the name of the input file from the command line, or use the default
    // Europe.dat file if no command-line parameters are given.
    const char * filename = "/home/krys/Projects/cgal/Arrangement_on_surface_2/examples/Arrangement_on_surface_2/Europe.dat";

    // Open the input file.
    std::ifstream     in_file (filename);

    if (! in_file.is_open()) {
        std::cerr << "Failed to open " << filename << " ..." << std::endl;
        return;
    }
    // Read the segments from the file.
    // The input file format should be (all coordinate values are double
    // precision floating-point numbers):
    // <n>                                 // number of segments.
    // <sx_1> <sy_1>  <tx_1> <ty_1>        // source and target of segment #1.
    // <sx_2> <sy_2>  <tx_2> <ty_2>        // source and target of segment #2.
    //   :      :       :      :
    // <sx_n> <sy_n>  <tx_n> <ty_n>        // source and target of segment #n.
    std::list<Segment>  segments;

    unsigned int n;
    in_file >> n;
    unsigned int i;
    for (i = 0; i < n; ++i) {
        double sx, sy, tx, ty;
        in_file >> sx >> sy >> tx >> ty;
        segments.push_back (Segment (Point (Number_type(sx), Number_type(sy)),
                                     Point (Number_type(tx), Number_type(ty))));
    }
    in_file.close();

    // Construct the arrangement by aggregately inserting all segments.
    CGAL::Timer                    timer;
    std::cout << "Performing aggregated insertion of "
              << n << " segments." << std::endl;

    timer.start();
    CGAL::insert_non_intersecting_curves (*m_arrangement, segments.begin(), segments.end());
    CGAL::insert(*m_arrangement, Segment(Point (Number_type(-1000), Number_type(3000)),
                                         Point (Number_type(2500), Number_type(3000))));
    CGAL::insert(*m_arrangement, Segment(Point (Number_type(2500), Number_type(3000)),
                                         Point (Number_type(2500), Number_type(6000))));
    CGAL::insert(*m_arrangement, Segment(Point (Number_type(2500), Number_type(6000)),
                                         Point (Number_type(-1000), Number_type(6000))));
    CGAL::insert(*m_arrangement, Segment(Point (Number_type(-1000), Number_type(6000)),
                                         Point (Number_type(-1000), Number_type(3000))));
    timer.stop();

    // Print the arrangement dimensions.
    std::cout << "V = " << m_arrangement->number_of_vertices()
              << ",  E = " << m_arrangement->number_of_edges()
              << ",  F = " << m_arrangement->number_of_faces() << std::endl;

    std::cout << "Construction took " << timer.time()
              << " seconds." << std::endl;

    m_graphicsScene->setSceneRect(-4000, -0, 8000, 8000);
#endif

}
