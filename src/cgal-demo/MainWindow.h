#pragma once

#include <QMainWindow>

#include <LeGeometricArrangement/LeGeometricArrangement.h>

class QUndoStack;

class LeGraphicsScene;
class LeGraphicsView;

class GraphicsArrangementBridge;
class GraphicsTask;
class ArrangementTreeModel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    LeGraphicsScene *m_graphicsScene = nullptr;
    LeGraphicsView *m_graphicsView = nullptr;
    LGA::Arrangement *m_arrangement = nullptr;
    GraphicsArrangementBridge *m_arrangementBridge = nullptr;
    ArrangementTreeModel *m_arrangementTreeModel = nullptr;
    QUndoStack *m_undoStack = nullptr;

    void setupGraphicsScene();
    void setupGraphicsView();
    void setupArrangement();
    void setupArrangementBridge();
    void setupArrangementTreeModel();
    void setupArrangementTreeView();
    void addTool(QToolBar *toolBar, const QIcon &icon, const QString &text, GraphicsTask *task);
    void setupToolBar();
    void setupUndoStack();
    void setupUndoView();

    void populateArrangement();
};
