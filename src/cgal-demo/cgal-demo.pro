#-------------------------------------------------
#
# Project created by QtCreator 2017-02-28T15:24:34
#
#-------------------------------------------------
QT       += core gui widgets
CONFIG +=  c++11
TARGET = cgal-demo
TEMPLATE = app

include(../lib/LeGraphicsView/LeGraphicsView.pri)
include(../lib/LeGeometricArrangement/LeGeometricArrangement.pri)

QMAKE_CXXFLAGS += \
-Wshadow \
-Wredundant-decls \
-Wcast-align \
-Wmissing-declarations \
-Wmissing-include-dirs \
-Wextra \
-Wall \
-Winvalid-pch \
-Wredundant-decls \
-Wmissing-prototypes \
-Wformat=2 \
-Wmissing-format-attribute \
-Wformat-nonliteral \
-Wuninitialized \
-Wpedantic

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        MainWindow.cpp \
    GraphicsArrangementBridge.cpp \
    ArrangementTreeModel.cpp \
    GraphicsTask.cpp

HEADERS  += MainWindow.h \
    GraphicsArrangementBridge.h \
    ArrangementTreeModel.h \
    GraphicsTask.h

RESOURCES += \
    cgal-demo.qrc
