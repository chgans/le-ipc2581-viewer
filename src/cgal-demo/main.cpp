#if 1
#include "MainWindow.h"
#include <QApplication>

QMainWindow *g_mainWindow = nullptr;

#include <CGAL/assertions.h>
#include <CGAL/assertions_behaviour.h>
#include <QMessageBox>

static inline void cgal_warning_handler(
    const char *type,
    const char *expr,
    const char* file,
    int line,
    const char* msg)
{
    QMessageBox::warning(g_mainWindow,
                          "CGAL warning",
                          QString("<b>%1</b><br/><br/>"
                                  "Type:<pre><code>%2</code></pre>"
                                  "Expression:<pre><code>%3</code></pre>"
                                  "Location:<pre><code>%4:%5</code></pre>")
                          .arg(msg).arg(type).arg(expr)
                          .arg(file).arg(line));
}

static inline void cgal_error_handler(
    const char *type,
    const char *expr,
    const char* file,
    int line,
    const char* msg)
{
    QMessageBox::critical(g_mainWindow,
                          "CGAL error",
                          QString("<b>%1</b><br/><br/>"
                                  "Type:<pre><code>%2</code></pre>"
                                  "Expression:<pre><code>%3</code></pre>"
                                  "Location:<pre><code>%4:%5</code></pre>")
                          .arg(msg).arg(type).arg(expr)
                          .arg(file).arg(line));
}

int main(int argc, char *argv[])
{
    CGAL::set_warning_handler(cgal_warning_handler);
    CGAL::set_warning_behaviour(CGAL::CONTINUE);
    CGAL::set_error_handler(cgal_error_handler);
    CGAL::set_error_behaviour(CGAL::CONTINUE);

    QApplication a(argc, argv);
    MainWindow w;
    g_mainWindow = &w;
    w.show();

    return a.exec();
}
#else

#include <LeGeometricArrangement/LeGeometricArrangement.h>

int main (int /*argv**/, char **/*argv*/)
{
    Arrangement arr;
    Point p00(0,0), p10(1,0), p11(1,1);, p01(0,1);
    Segment s1(p00, p01), s2(p01, p11), s3(p11, p10), s4(p10, p01);
    auto v00 = arr.insert_in_face_interior(p00, arr.unbounded_face());
    auto v10 = arr.insert_in_face_interior(p10, arr.unbounded_face());
    auto v11 = arr.insert_in_face_interior(p11, arr.unbounded_face());
    auto v01 = arr.insert_in_face_interior(p01, arr.unbounded_face());
    arr.insert_at_vertices(s1, v00, v01);
    arr.insert_at_vertices(s2, v01, v11);
    arr.insert_at_vertices(s3, v11, v10);
    arr.insert_at_vertices(s4, v10, v00);

    Segment s(Point(-1, 0))
    return 0;
}

#endif
