#pragma once

#define CGAL_NO_TRAPEZOIDAL_DECOMPOSITION_2_OPTIMIZATION

#include <CGAL/Cartesian.h>
#include <CGAL/Arr_extended_dcel.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arr_non_caching_segment_traits_2.h>
#include <CGAL/Arr_observer.h>
#include <CGAL/Arrangement_2.h>
#include <CGAL/Arr_walk_along_line_point_location.h>
#include <CGAL/Arr_trapezoid_ric_point_location.h>

namespace LGA
{

#if 0
    typedef double                                          Number_type;
    typedef CGAL::Cartesian<Number_type>                    Kernel;
#else
    typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
    typedef Kernel::FT                                        Number_type;
#endif

    typedef CGAL::Arr_segment_traits_2<Kernel>  Traits;
    typedef Traits::Point_2                                 Point;
    // FIXME: Don't call this Segment, as this might not be the same as Kernel::Segment
    // Arr_segment_traits_2<Kernel> define it as Arr_segment_2<Kernel> which BTW can
    // be cast into a Kernel::Segment_2
    typedef Traits::X_monotone_curve_2                      Segment;
    typedef Traits::Curve_2                                 Curve;
    typedef CGAL::Arr_extended_dcel<Traits, unsigned int, unsigned int, unsigned int>
    Dcel;
    typedef CGAL::Arrangement_2<Traits, Dcel>               Arrangement;
    typedef CGAL::Arr_observer<Arrangement>                 ArrangementObserver;

    typedef CGAL::Arr_walk_along_line_point_location<Arrangement> WalkLocator;
    typedef CGAL::Arr_trapezoid_ric_point_location<Arrangement>   TrapezoidLocator;
    typedef CGAL::Arr_point_location_result<Arrangement>::Type     LocatorResult;

    /*
 * a bidirectional circulator over the halfedges of a CCB (connected component of the boundary).
 * Its value-type is Halfedge. Each bounded face has a single CCB representing it outer boundary,
 * and may have several inner CCBs representing its holes.
 */
    typedef Arrangement::Ccb_halfedge_circulator            Ccb_halfedge_circulator;
    typedef Arrangement::Ccb_halfedge_const_circulator      Ccb_halfedge_const_circulator;

    typedef Arrangement::Inner_ccb_iterator        Inner_ccb_iterator;
    typedef Arrangement::Inner_ccb_const_iterator  Inner_ccb_const_iterator;

    /*
 * a bidirectional iterator over the edges of the arrangement.
 * (That is, it skips every other halfedge.) Its value-type is Halfedge.
 */
    typedef Arrangement::Edge_iterator                      Edge_iterator;
    typedef Arrangement::Edge_const_iterator                Edge_const_iterator;

    /*
 * 	a handle for an arrangement face.
 */
    typedef Arrangement::Face_handle                        Face_handle;
    typedef Arrangement::Face_const_handle                  Face_const_handle;
    /*
 * a bidirectional iterator over the faces of arrangement.
 * Its value-type is Face.
 */
    typedef Arrangement::Face_iterator                      Face_iterator;
    typedef Arrangement::Face_const_iterator                Face_const_iterator;
    /*
 * a bidirectional circulator over the halfedges that have a given vertex as their target.
 * Its value-type is Halfedge.
 */
    typedef Arrangement::Halfedge_around_vertex_circulator
    Halfedge_around_vertex_circulator;
    typedef Arrangement::Halfedge_around_vertex_const_circulator
    Halfedge_around_vertex_const_circulator;
    /*
 * a handle for a halfedge.
 * The halfedge and its twin form together an arrangement edge.
 */
    typedef Arrangement::Halfedge_handle                    Halfedge_handle;
    typedef Arrangement::Halfedge_const_handle              Halfedge_const_handle;
    /*
 * a bidirectional iterator over the halfedges of the arrangement.
 * Its value-type is Halfedge.
 */
    typedef Arrangement::Halfedge_iterator                  Halfedge_iterator;
    typedef Arrangement::Halfedge_const_iterator            Halfedge_const_iterator;
    /*
 * a bidirectional iterator over the holes (i.e., inner CCBs) contained inside a given face.
 * Its value type is Ccb_halfedge_circulator.
 */
    typedef Arrangement::Hole_iterator                      Hole_iterator;
    typedef Arrangement::Hole_const_iterator                Hole_const_iterator;
    /*
 * a bidirectional iterator over the isolated vertices contained inside a given face.
 * Its value type is Vertex.
 */
    typedef Arrangement::Isolated_vertex_iterator           Isolated_vertex_iterator;
    typedef Arrangement::Isolated_vertex_const_iterator     Isolated_vertex_const_iterator;
    /*
 * a bidirectional iterator over the unbounded faces of arrangement.
 * Its value-type is Face.
 */
    typedef Arrangement::Unbounded_face_iterator            Unbounded_face_iterator;
    typedef Arrangement::Unbounded_face_const_iterator      Unbounded_face_const_iterator;
    /*
 * a handle for an arrangement vertex.
 */
    typedef Arrangement::Vertex_handle                      Vertex_handle;
    typedef Arrangement::Vertex_const_handle                Vertex_const_handle;
    /*
 * a bidirectional iterator over the vertices of the arrangement.
 * Its value-type is Vertex.
 */
    typedef Arrangement::Vertex_iterator                    Vertex_iterator;
    typedef Arrangement::Vertex_const_iterator              Vertex_const_iterator;

}
