INCLUDEPATH += $$PWD/..
PRECOMPILED_HEADER += $$PWD/LeGeometricArrangement.h

LIBS += -lCGAL -lgmp -lmpfr

win32:CONFIG(release, debug|release): LIBS += -L$$top_builddir/src/lib/LeGeometricArrangement/release/ -lLeGeometricArrangement
else:win32:CONFIG(debug, debug|release): LIBS += -L$$top_builddir/src/lib/LeGeometricArrangement/debug/ -lLeGeometricArrangement
else:unix: LIBS += -L$$top_builddir/src/lib/LeGeometricArrangement/ -lLeGeometricArrangement

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeGeometricArrangement/release/libLeGeometricArrangement.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeGeometricArrangement/debug/libLeGeometricArrangement.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeGeometricArrangement/release/LeGeometricArrangement.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeGeometricArrangement/debug/LeGeometricArrangement.lib
else:unix: PRE_TARGETDEPS += $$top_builddir/src/lib/LeGeometricArrangement/libLeGeometricArrangement.a
