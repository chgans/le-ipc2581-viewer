#include "UndoCommand.h"

#include <CGAL/Arr_naive_point_location.h>

using namespace LGA;

UndoCommand::UndoCommand(Arrangement *arr)
    : m_arrangement(arr)
{

}

Arrangement *UndoCommand::arrangement() const
{
    return m_arrangement;
}

int UndoCommand::giveVertex(Vertex_handle v)
{
    int index = 0;
    Vertex_iterator iter = arrangement()->vertices_begin();
    do
         ++index;
    while (++iter != v);
    return index;
}

Vertex_handle UndoCommand::takeVertex(int which)
{
    int i = 0;
    Vertex_iterator iter = arrangement()->vertices_begin();
    while (i++ != which)
        ++iter;
    return iter;
}

int UndoCommand::giveHalfedge(Halfedge_handle he)
{
    int index = 0;
    Halfedge_iterator iterator = arrangement()->halfedges_begin();
    do
         ++index;
    while (++iterator != he);
    return index;
}

Halfedge_handle UndoCommand::takeHalfedge(int which)
{
    int i = 0;
    Halfedge_iterator iterator = arrangement()->halfedges_begin();
    while (i++ != which)
        ++iterator;
    return iterator;
}

int UndoCommand::giveFace(Face_handle f)
{
    int index = 0;
    Face_iterator iterator = arrangement()->faces_begin();
    do
         ++index;
    while (++iterator != f);
    return index;
}

Face_handle UndoCommand::takeFace(int which)
{
    int i = 0;
    Face_iterator iterator = arrangement()->faces_begin();
    while (i++ != which)
        ++iterator;
    return iterator;
}

Halfedge_handle UndoCommand::predecessorHalfedgeAroundVertex(Halfedge_handle he, Vertex_handle v)
{
    Halfedge_around_vertex_circulator circulator = v->incident_halfedges();
    auto current = circulator;
    do
    {
        if (he == current)
            return --current;
    }
    while (++current != circulator);
    Q_UNREACHABLE();
}


InsertAtCommand::InsertAtCommand(Arrangement *arr, const Segment &c, Vertex_handle v1, Vertex_handle v2)
    : UndoCommand(arr)
    , m_mode(V1V2)
    , m_segment(c)
{
    setText("Insert segment, by 2 vertices");
    m_v1 = giveVertex(v1);
    m_v2 = giveVertex(v2);
}

InsertAtCommand::InsertAtCommand(Arrangement *arr, const Segment &c, Halfedge_handle pred1, Vertex_handle v2)
    : UndoCommand(arr)
    , m_mode(E1V2)
    , m_segment(c)
{
    setText("Insert segment, by half-edge and vertex");
    m_pred1 = giveHalfedge(pred1);
    m_v2 = giveVertex(v2);
}

InsertAtCommand::InsertAtCommand(Arrangement *arr, const Segment &c, Halfedge_handle pred1, Halfedge_handle pred2)
    : UndoCommand(arr)
    , m_mode(E1E2)
    , m_segment(c)
{
    setText("Insert segment, by 2 half-edges");
    m_pred1 = giveHalfedge(pred1);
    m_pred2 = giveHalfedge(pred2);
}

void InsertAtCommand::undo()
{
    Halfedge_handle he = takeHalfedge(m_he);
    switch (m_mode)
    {
        case V1V2:
            m_v1 = giveVertex(he->source());
            m_v2 = giveVertex(he->target());
            break;
        case E1V2:
            m_pred1 = giveHalfedge(predecessorHalfedgeAroundVertex(he->twin(), he->source()));
            m_v2 = giveVertex(he->target());
            break;
        case E1E2:
            m_pred1 = giveHalfedge(predecessorHalfedgeAroundVertex(he->twin(), he->source()));
            m_pred2 = giveHalfedge(predecessorHalfedgeAroundVertex(he, he->target()));
            break;
        default:
            Q_UNREACHABLE();
    }
    m_segment = he->curve();
    arrangement()->remove_edge(he, false, false);
}

void InsertAtCommand::redo()
{
    Halfedge_handle he;
    switch (m_mode)
    {
        case V1V2:
            he = arrangement()->insert_at_vertices(m_segment, takeVertex(m_v1), takeVertex(m_v2));
            break;
        case E1V2:
            he = arrangement()->insert_at_vertices(m_segment, takeHalfedge(m_pred1), takeVertex(m_v2));
            break;
        case E1E2:
            he = arrangement()->insert_at_vertices(m_segment, takeHalfedge(m_pred1), takeHalfedge(m_pred2));
            break;
        default:
            Q_UNREACHABLE();
    }
    m_he = giveHalfedge(he);
}


InsertInCommand::InsertInCommand(Arrangement *arr, const Point &p, Face_handle f)
    : UndoCommand(arr)
    , m_mode(PointFace)
    , m_point(p)
{
    setText("Insert vertex, in face");
    m_face = giveFace(f);
}

InsertInCommand::InsertInCommand(Arrangement *arr, const Segment &c, Face_handle f)
    : UndoCommand(arr)
    , m_mode(SegmentFace)
    , m_segment(c)
{
    setText("Insert segment, in face");
    m_face = giveFace(f);
}

Vertex_handle InsertInCommand::vertex()
{
    return takeVertex(m_v); // FIXME: do not take
}

Halfedge_handle InsertInCommand::halfedge()
{
    return takeHalfedge(m_he); // FIXME: do not take
}

void InsertInCommand::undo()
{
    switch (m_mode)
    {
        case PointFace:
        {
            auto v = takeVertex(m_v);
            m_point = v->point();
            m_face = giveFace(v->face());
            arrangement()->remove_isolated_vertex(v);
            break;
        }
        case SegmentFace:
        {
            auto he = takeHalfedge(m_he);
            //m_segment = he->curve();
            m_face = giveFace(he->face());
            arrangement()->remove_edge(he, true, true);
            break;
        }
        default:
            Q_UNREACHABLE();
    }
}

void InsertInCommand::redo()
{
    switch (m_mode)
    {
        case PointFace:
        {
            auto v = arrangement()->insert_in_face_interior(m_point, takeFace(m_face));
            m_v = giveVertex(v);
            break;
        }
        case SegmentFace:
        {
            // The returned half-edge is directed from left to right,
            // but we need to store the HE that has the same direction as m_segment
            auto he = arrangement()->insert_in_face_interior(m_segment, takeFace(m_face));
//            if (!m_segment.is_directed_right())
//                he = he->twin();
            m_he = giveHalfedge(he);
            break;
        }
        default:
            Q_UNREACHABLE();
    }
}

InsertFromLeftCommand::InsertFromLeftCommand(Arrangement *arr, const Segment &c, Vertex_handle v1)
    : UndoCommand(arr)
    , m_mode(SegmentVertex)
    , m_segment(c)
{
    setText("Insert a segment, from left vertex");
    m_v1 = giveVertex(v1);
}

InsertFromLeftCommand::InsertFromLeftCommand(Arrangement *arr, const Segment &c, Halfedge_handle he1)
    : UndoCommand(arr)
    , m_mode(SegmentEdge)
    , m_segment(c)
{
    setText("Insert a segment, from left half-edge");
    m_pred1 = giveHalfedge(he1);
}

void InsertFromLeftCommand::undo()
{
    switch (m_mode)
    {
        case SegmentVertex:
        {
            auto he = takeHalfedge(m_he);
            m_segment = he->curve();
            m_v1 = giveVertex(he->source());
            arrangement()->remove_edge(he, false, true);
            break;
        }
        case SegmentEdge:
        {
            auto he = takeHalfedge(m_he);
            m_segment = he->curve();
            auto pred = predecessorHalfedgeAroundVertex(he->twin(), he->twin()->target());
            m_pred1 = giveHalfedge(pred);
            arrangement()->remove_edge(he, false, true);
            break;
        }
        default:
            Q_UNREACHABLE();
    }
}

void InsertFromLeftCommand::redo()
{
    switch (m_mode)
    {
        case SegmentVertex:
        {
            auto he = arrangement()->insert_from_left_vertex(m_segment, takeVertex(m_v1));
            m_he = giveHalfedge(he);
            break;
        }
        case SegmentEdge:
        {
            auto he = arrangement()->insert_from_left_vertex(m_segment, takeHalfedge(m_pred1));
            m_he = giveHalfedge(he);
            break;
        }
        default:
            Q_UNREACHABLE();
    }
}
