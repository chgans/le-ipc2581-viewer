#pragma once

#include <QUndoCommand>

#include "LeGeometricArrangement.h"

namespace LGA
{

    class UndoCommand : public QUndoCommand
    {
    public:
        UndoCommand(Arrangement *arr);

        Arrangement *arrangement() const;

    protected:
        int giveVertex(Vertex_handle v);
        Vertex_handle takeVertex(int which);

        int giveHalfedge(Halfedge_handle he);
        Halfedge_handle takeHalfedge(int which);

        int giveFace(Face_handle f);
        Face_handle takeFace(int which);

        Halfedge_handle predecessorHalfedgeAroundVertex(Halfedge_handle he, Vertex_handle v);

    private:
        Arrangement *m_arrangement;
    };

    // low-level commands:
    //
    // => InsertAtCommand(), InsertFrom(), InsertInCommand(), RemoveCommand(), MergeCommand(), SplitCommand()
    // with different ctor signatures

    // insert, at-vertices: c, he1, he2 => he. Undo: remove(he)
    // insert, at-vertices: c, he, v => he. Undo: remove(he)
    // insert, at-vertices: c, v1, v2 => he. Undo: remove(he)
    class InsertAtCommand: public UndoCommand
    {
    public:
        explicit InsertAtCommand(Arrangement *arr,
                                 const Segment &c,
                                 Vertex_handle v1,
                                 Vertex_handle v2);
        explicit InsertAtCommand(Arrangement *arr,
                                 const Segment & c,
                                 Halfedge_handle pred1,
                                 Vertex_handle v2);
        explicit InsertAtCommand(Arrangement *arr,
                                 const Segment &c,
                                 Halfedge_handle pred1,
                                 Halfedge_handle pred2);

    private:
        enum Mode
        {
            V1V2,
            E1V2,
            E1E2
        };
        Mode m_mode;
        Segment m_segment;
        int m_v1 = -1;
        int m_pred1 = -1;
        int m_v2 = -1;
        int m_pred2 = -1;
        int m_he = -1;

        // QUndoCommand interface
    public:
        virtual void undo() override;
        virtual void redo() override;
    };

    // insert, from-left: c, he =>  he. Undo: remove(he)
    // insert, from-left: c, v => he. Undo: remove(he)
    // insert, from-right: c, he => he. Undo: remove(he)
    // insert, from-right: c, v => he. Undo: remove(he)
    class InsertFromLeftCommand: public UndoCommand
    {
    public:
        explicit InsertFromLeftCommand(Arrangement *arr,
                                       const Segment &c,
                                       Vertex_handle v1);
        explicit InsertFromLeftCommand(Arrangement *arr,
                                       const Segment &c,
                                       Halfedge_handle he1);

    private:
        enum Mode
        {
            SegmentVertex,
            SegmentEdge,
        };
        Mode m_mode;
        Segment m_segment;
        int m_v1 = -1;
        int m_pred1 = -1;
        int m_he = -1;

        // QUndoCommand interface
    public:
        virtual void undo() override;
        virtual void redo() override;

    };

    // insert: p, f => v (isolated). Undo: remove(v)
    // insert: c, f => he. Undo: remove(he), remove(he->source()), remove(he->target)
    class InsertInCommand: public UndoCommand
    {
    public:
        explicit InsertInCommand(Arrangement *arr,
                                 const Point &p,
                                 Face_handle f);
        explicit InsertInCommand(Arrangement *arr,
                                 const Segment &c,
                                 Face_handle f);

        Vertex_handle vertex();
        Halfedge_handle halfedge();

    private:
        enum Mode
        {
            PointFace,
            SegmentFace,
        };
        Mode m_mode;
        Point m_point;
        Segment m_segment;
        int m_face = -1;
        int m_v = -1;
        int m_he = -1;

        // QUndoCommand interface
    public:
        virtual void undo() override;
        virtual void redo() override;
    };

    // remove: e => mergedF or e->f. Undo:
    // remove: v (isolated) => v->f. Undo: insert(v->p, f)
    class RemoveCommand: public UndoCommand
    {
    public:
        RemoveCommand();

    };

    // merge: e1, e2, c => he. Undo: remove(he), insert(oldV->p, oldV->f), insert(...)
    class MergeCommand: public UndoCommand
    {
    public:
        MergeCommand();

    };

    // split: e, c1, c2
    class SplitCommand: public UndoCommand
    {
    public:
        SplitCommand();

    };

}
