#include "GraphicsGrid.h"

#include <QtMath>

GraphicsGrid::GraphicsGrid(QObject *parent) : QObject(parent),
    m_minimumSceneSize(1.0),
    m_minimumViewSize(10),
    m_multiplier(10),
    m_touched(true)
{
}

qreal GraphicsGrid::minimumSceneSize() const
{
    return m_minimumSceneSize;
}

int GraphicsGrid::minimumViewSize() const
{
    return m_minimumViewSize;
}

int GraphicsGrid::multiplier() const
{
    return m_multiplier;
}

QRectF GraphicsGrid::sceneRect() const
{
    return m_sceneRect;
}

QTransform GraphicsGrid::viewTransform() const
{
    return m_viewTransfrom;
}

void GraphicsGrid::setMinimumSceneSize(qreal minimumSceneSize)
{
    if (!qFuzzyCompare(m_minimumSceneSize, minimumSceneSize))
        return;
    if (minimumSceneSize <= 1E-9)
        return;
    m_minimumSceneSize = minimumSceneSize;
    m_touched = true;
    emit minimumSceneSizeChanged(minimumSceneSize);
}

void GraphicsGrid::setMinimumViewSize(int minimumViewSize)
{
    if (m_minimumViewSize == minimumViewSize)
        return;
    if (m_minimumViewSize < 10)
        return;
    m_minimumViewSize = minimumViewSize;
    m_touched = true;
    emit minimumViewSizeChanged(minimumViewSize);
}

void GraphicsGrid::setMultiplier(int multiplier)
{
    if (m_multiplier == multiplier)
        return;
    if (multiplier < 2)
        return;
    m_multiplier = multiplier;
    m_touched = true;
    emit multiplierChanged(multiplier);
}

void GraphicsGrid::setSceneRect(const QRectF &sceneRect)
{
    const QRectF rect = sceneRect.normalized();
    if (m_sceneRect == rect)
        return;

    m_sceneRect = rect;
    m_touched = true;
    emit sceneRectChanged(rect);
}

void GraphicsGrid::setViewTransform(const QTransform &transform)
{
    if (m_viewTransfrom == transform)
        return;

    m_viewTransfrom = transform;
    m_touched = true;
    emit viewTransformChanged(transform);
}

void GraphicsGrid::maybeUpdate() const
{
//    if (!m_touched)
//        return;

//    m_touched = false;

//    qreal minorSceneStep = m_viewTransfrom.inverted().map(QLine(0, 0, m_minimumViewSize, m_minimumViewSize)).manatanLength();


//    static const int sentinal = 1000;
//    int viewSize = 0;
//    sceneSize = 1;
//    int iteration = 0;
//    while (viewSize <= m_minimumViewSize)
//    {
//        viewSize = qFloor(m_viewTransfrom.inverted().map(QLine(0, 0, sceneSize, sceneSize)).manatanLength());
//        if (++iteration > sentinel)
//            return;
//    }

}
