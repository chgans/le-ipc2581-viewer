#ifndef GRAPHICSGRID_H
#define GRAPHICSGRID_H

#include <QObject>
#include <QLineF>
#include <QRectF>
#include <QTransform>

class GraphicsGrid : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QRectF sceneRect READ sceneRect WRITE setSceneRect NOTIFY sceneRectChanged)
    Q_PROPERTY(qreal minimumSceneSize READ minimumSceneSize WRITE setMinimumSceneSize NOTIFY minimumSceneSizeChanged)
    Q_PROPERTY(QTransform viewTransform READ viewTransform WRITE setViewTransform NOTIFY viewTransformChanged)
    Q_PROPERTY(int minimumViewSize READ minimumViewSize WRITE setMinimumViewSize NOTIFY minimumViewSizeChanged)
    Q_PROPERTY(int multiplier READ multiplier WRITE setMultiplier NOTIFY multiplierChanged)

public:
    explicit GraphicsGrid(QObject *parent = 0);

    qreal minimumSceneSize() const;
    int minimumViewSize() const;
    int multiplier() const;
    QRectF sceneRect() const;
    QTransform viewTransform() const;

    QLineF *rlines(int *count) const;

signals:
    void minimumSceneSizeChanged(qreal minimumSceneSize);
    void minimumViewSizeChanged(int minimumViewSize);
    void multiplierChanged(int multiplier);
    void sceneRectChanged(const QRectF &sceneRect);
    void viewTransformChanged(const QTransform &viewTransform);

public slots:
    void setMinimumSceneSize(qreal minimumSceneSize);
    void setMinimumViewSize(int minimumViewSize);
    void setMultiplier(int multiplier);
    void setSceneRect(const QRectF &sceneRect);
    void setViewTransform(const QTransform &viewTransform);

private:
    qreal m_minimumSceneSize;
    int m_minimumViewSize;
    int m_multiplier;
    QRectF m_sceneRect;
    QTransform m_viewTransfrom;
    mutable QVector<QLineF> m_lines;
    mutable int m_hLineCount;
    mutable int m_vLineCount;
    mutable bool m_touched;
    void maybeUpdate() const;
};

#endif // GRAPHICSGRID_H
