#include "GraphicsLayerBar.h"
#include "ItemModels/LayerSetModel.h"

#include <QMenu>
#include <QToolButton>

GraphicsLayerBar::GraphicsLayerBar(QWidget *parent) : QWidget(parent)
{

}

void GraphicsLayerBar::setModel(LayerSetModel *model)
{
    if (m_model == model)
        return;

    if (m_model != nullptr)
        onModelAboutToBeReset();

    m_model = model;

    if (m_model != nullptr)
        onModelReset();
}

void GraphicsLayerBar::onModelDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
{

}

void GraphicsLayerBar::onModelRowsInserted(const QModelIndex &parent, int first, int last)
{

}

void GraphicsLayerBar::onModelRowsRemoved(const QModelIndex &parent, int first, int last)
{

}

void GraphicsLayerBar::onModelRowsMoved(const QModelIndex &parent, int start, int end, const QModelIndex &destination)
{

}

void GraphicsLayerBar::onModelAboutToBeReset()
{
    m_model->disconnect(this);
    qDeleteAll(m_buttons);
    qDeleteAll(m_buttonContextMenus);
    m_extraButtonMenu->clear();
}

void GraphicsLayerBar::onModelReset()
{
    for (int row = 0; row < m_buttons.count(); row++)
    {
        const QString name = m_model->data(m_model->index(row, LayerSetModel::NameColumn)).toString();
        const QColor color = Qt::white; // FIXME: layer has feature color group but no layer color
        auto tab = makeTab(name, color);
        m_buttons.append(tab);
    }
}

QToolButton *GraphicsLayerBar::makeTab(const QString &text, const QColor &fgColor)
{
    QToolButton *button = new QToolButton;
    const QColor bgColor("#002b36");
    const QColor bghColor("#073642");
    button->setText(text);
    button->setCheckable(true);
    button->setAutoFillBackground(true);
    button->setStyleSheet(QString("QToolButton {"
                                  //" background-color: %1;"
                                  " border: 3px outset %1;"
                                  //" border-radius: 5px;"
                                  " color: %2;"
                                  "}"
                                  "QToolButton:checked {"
                                  //" background-color: %3;"
                                  " border: 3px inset %1;"
                                  "}")
                          .arg(bgColor.name()).arg(fgColor.name())
                          .arg(bghColor.name()));

    return button;
}
