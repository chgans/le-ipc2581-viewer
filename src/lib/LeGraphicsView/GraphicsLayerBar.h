#ifndef GRAPHICSLAYERBAR_H
#define GRAPHICSLAYERBAR_H

#include <QWidget>

class QToolButton;
class QMenu;

class LayerSetModel; // Should be a GraphicsLayerSetModel
class GraphicsLayerBar : public QWidget
{
    Q_OBJECT
public:
    explicit GraphicsLayerBar(QWidget *parent = 0);

    void setModel(LayerSetModel *model);

signals:

public slots:

    void onModelDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight,
                            const QVector<int> &roles = QVector<int>());
    void onModelRowsInserted(const QModelIndex &parent, int first, int last);
    void onModelRowsRemoved(const QModelIndex &parent, int first, int last);
    void onModelRowsMoved( const QModelIndex &parent, int start, int end, const QModelIndex &destination);
    void onModelAboutToBeReset();
    void onModelReset();

    QToolButton *makeTab(const QString &text, const QColor &fgColor);
private:
    LayerSetModel *m_model;
    QList<QToolButton *> m_buttons;
    QList<QMenu *> m_buttonContextMenus;
    QToolButton *m_extraButton;
    QMenu *m_extraButtonMenu;
};

#endif // GRAPHICSLAYERBAR_H
