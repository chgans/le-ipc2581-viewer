#pragma once

// From palette
//enum Role
//{
//    Origin,
//    Feature,
//    Highlight,
//    Selection,
//    Handle,
//    Background,
//    RubberBand,
//    GoodMark,
//    BadMark,
//    Grid,
//    Cursor,
//};

// From LeGraphicsStyle
//enum StateFlag
//{
//    // By order of precedence:
//    Edited = 0x01,
//    Highlighted = 0x02,
//    Selected = 0x04,

//    // Exclusive
//    MarkedGood = 0x10,
//    MarkedBad = 0x20,

//    NoStateFlag = 0x00
//};

enum LeGraphicsFeature
{
    GftPad      = 0x0001,
    GftPin      = 0x0002,
    GftFiducial = 0x0004,
    GftHole     = 0x0008,
    GftSlot     = 0x0010,
    GftFill     = 0x0020,
    GftTrace    = 0x0040,
    GftText     = 0x0080,

    GftNone     = 0x0000,
    GftAll      = 0xFFFF
};

// FIXME: Needs to know as well top vs bottom and left vs right
// to draw the handle the right way around
// Rename to Move{{Top|Center|Bottom}{Left|Center|Right}}HandleRole
// We wil need as well handles for corner radius and chamfer
enum LeGraphicsHandleRole
{
    TopHandleRole = 1,
    BottomHandleRole,
    LeftHandleRole,
    RightHandleRole,
    TopRightHandleRole,
    TopLeftHandleRole,
    BottomRightHandleRole,
    BottomLeftHandleRole,
    CenterHandleRole
};
