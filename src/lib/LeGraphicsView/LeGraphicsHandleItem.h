#ifndef LEGRAPHICSHANDLEITEM_H
#define LEGRAPHICSHANDLEITEM_H

#include "LeGraphicsItem.h"

class LeGraphicsHandleItem : public LeGraphicsItem
{
public:
    explicit LeGraphicsHandleItem(LeGraphicsHandleRole role = CenterHandleRole);

    void setHandleRole(LeGraphicsHandleRole role);
    LeGraphicsHandleRole handleRole() const;

private:
    LeGraphicsHandleRole m_role;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // LeGraphicsItem interface
public:
    virtual LeGraphicsItem *clone() const override;

    // QGraphicsItem interface
protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
};

#endif // LEGRAPHICSHANDLEITEM_H
