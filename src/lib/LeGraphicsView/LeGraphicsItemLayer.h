#pragma once

#include <QGraphicsObject>

#include "LeGraphics.h"
#include "LeGraphicsFeatureOption.h"

#include <QList>
#include <QMap>
#include <QString>

class LeGraphicsItem;
class LeGraphicsCompositeItem;
class LeGraphicsPalette;
class LeGraphicsScene;

class LeGraphicsItemLayer: public QGraphicsObject
{
    Q_OBJECT

    Q_PROPERTY(QString fullName READ fullName WRITE setFullName NOTIFY fullNameChanged)
    Q_PROPERTY(QString shortName READ shortName WRITE setShortName NOTIFY shortNameChanged)
    Q_PROPERTY(QString tinyName READ tinyName WRITE setTinyName NOTIFY tinyNameChanged)

public:
    enum
    {
        Type = UserType + 0x100
    };

    explicit LeGraphicsItemLayer(const QString &fullName = QString());
    ~LeGraphicsItemLayer();

    void setNames(const QString &fullName,
                  const QString &shortName,
                  const QString &tinyName);

    QString fullName() const;
    QString shortName() const;
    QString tinyName() const;

    void addToLayer(LeGraphicsItem *item);
    void addToLayer(const QList<LeGraphicsItem *> &items);
    LeGraphicsItem *removeFromLayer(LeGraphicsItem *item);
    QList<LeGraphicsItem *> items() const;
    void clear();

    void setFeatureOption(const LeGraphicsFeatureOption &option);
    LeGraphicsFeatureOption featureOption() const;
    LeGraphicsPalette graphicsPalette(LeGraphicsFeature feature) const;
    LeGraphicsScene *graphicsScene() const;

signals:
    void fullNameChanged(QString fullName);
    void shortNameChanged(QString shortName);
    void tinyNameChanged(QString tinyName);

public slots:
    void setFullName(QString fullName);
    void setShortName(QString shortName);
    void setTinyName(QString tinyName);

private:
    QString m_fullName;
    QString m_shortName;
    QString m_tinyName;
    LeGraphicsFeatureOption m_featureOption;
    QMap<LeGraphicsFeature, LeGraphicsCompositeItem*> m_featureItemMap;
    void setFeaturesVisible(LeGraphicsFeature type, bool visible);

    // QGraphicsItem interface
public:
    virtual int type() const override { return Type; }
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
};
