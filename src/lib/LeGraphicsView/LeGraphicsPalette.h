#pragma once

#include <QBrush>
#include <QMap>

class LeGraphicsPalette
{
public:
    LeGraphicsPalette();
    LeGraphicsPalette(const LeGraphicsPalette &other)
    { *this = other; }

    LeGraphicsPalette& operator=(const LeGraphicsPalette& rhs)
    {
        if (this != &rhs)
            m_brushMap = rhs.m_brushMap;
        return *this;
    }

    enum Role
    {
        Origin,
        Feature,
        Highlight,
        Selection,
        Handle,
        Background,
        RubberBand,
        GoodMark,
        BadMark,
        Grid,
        Cursor,
    };

    void setBrush(Role role, const QBrush &brush)
    {
        m_brushMap.insert(role, brush);
    }

    QBrush brush(Role role) const
    {
        return m_brushMap.value(role);
    }

    void setColor(Role role, const QColor &color)
    {
        m_brushMap[role].setColor(color);
    }

    QColor color(Role role) const
    {
        return m_brushMap.value(role).color();
    }

private:
    QMap<Role, QBrush> m_brushMap;

    friend bool operator==(const LeGraphicsPalette& lhs, const LeGraphicsPalette& rhs);
};

inline bool operator==(const LeGraphicsPalette& lhs, const LeGraphicsPalette& rhs)
{
    return lhs.m_brushMap == rhs.m_brushMap;
}

inline bool operator!=(const LeGraphicsPalette& lhs, const LeGraphicsPalette& rhs)
{
    return !(lhs == rhs);
}
