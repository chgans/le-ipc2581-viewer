#pragma once

#include <QWidget>

class LeGraphicsRuler;

class LeGraphicsRulerBar : public QWidget
{
    Q_OBJECT
    Q_ENUMS(Alignment)

public:
    enum Alignment { Horizontal, Vertical };
    static const int BREADTH;

    explicit LeGraphicsRulerBar(LeGraphicsRulerBar::Alignment alignment, QWidget *parent = nullptr);
    ~LeGraphicsRulerBar();

    void setAlignment(LeGraphicsRulerBar::Alignment alignment);
    LeGraphicsRulerBar::Alignment rulerType() const;

    void setRuler(const LeGraphicsRuler *ruler);
    const LeGraphicsRuler *ruler() const;

    void setCursorPosition(const QPointF &pos);
    QPointF cursorPosition() const;

    void setBackgroundColor(const QColor &color);
    QColor backgroundColor() const;

    void setForegroundColor(const QColor &color);
    QColor foregroundColor() const;

private:
    static const int FONT_SIZE;
    static const int MAJOR_TICK_HEIGHT;
    static const int MINOR_TICK_HEIGHT;

    // Store a Graduator GridModel::[x|y]Graduator() ?
    const LeGraphicsRuler *m_ruler;
    Alignment m_alignment;
    QColor m_backgroundColor;
    QColor m_foregroundColor;
    qreal m_currentPos;

    void drawMinorTick(QPainter &painter, int pixelPos);
    void drawMajorTick(QPainter &painter, int pixelPos, qreal logicalPos);
    void drawIndicator(QPainter &painter, int pixelPos);

    // QWidget interface
public:
    // TODO: get rid of this
    QSize minimumSizeHint() const override;

    // QWidget interface
protected:
    void paintEvent(QPaintEvent *event) override;
};
