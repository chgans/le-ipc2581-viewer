#pragma once

class QGraphicsSceneMouseEvent;
class QKeyEvent;
class QPainter;
class QRectF;

class LeGraphicsSceneEventFilter
{
public:
    LeGraphicsSceneEventFilter();
    virtual ~LeGraphicsSceneEventFilter();

    virtual void drawBackground(QPainter *painter, const QRectF &rect);
    virtual void drawForeground(QPainter *painter, const QRectF &rect);

    virtual bool keyPressEvent(QKeyEvent *event);
    virtual bool keyReleaseEvent(QKeyEvent *event);

    virtual bool mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    virtual bool mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
};
