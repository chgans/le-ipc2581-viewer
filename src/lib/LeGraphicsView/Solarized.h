#ifndef SOLARIZED_H
#define SOLARIZED_H

#include <QColor>

class Solarized
{
public:
    static const QColor background;
    static const QColor backgroundHighlight;

    static const QColor foreground;
    static const QColor foregroundHighlight;

    static const QColor primaryContent;
    static const QColor secondaryContent;
    static const QColor emphasisedContent;

    static const QColor base03;
    static const QColor base02;
    static const QColor base01;
    static const QColor base00;
    static const QColor base0;
    static const QColor base1;
    static const QColor base2;
    static const QColor base3;

    static const QColor yellow;
    static const QColor orange;
    static const QColor red;
    static const QColor magenta;
    static const QColor violet;
    static const QColor blue;
    static const QColor cyan;
    static const QColor green;

    static QList<QColor> colorList();
    static QColor accent(int index);
};

#endif // SOLARIZED_H
