#include "AssemblyDrawingParser.h"

#include "OutlineParser.h"
#include "MarkingParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

AssemblyDrawingParser::AssemblyDrawingParser(
    OutlineParser *&_outlineParser
    , MarkingParser *&_markingParser
):    m_outlineParser(_outlineParser)
    , m_markingParser(_markingParser)
{

}

bool AssemblyDrawingParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new AssemblyDrawing());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Outline"))
        {
            if (!m_outlineParser->parse(reader))
                return false;
            auto result = m_outlineParser->result();
            m_result->outline = result;
        }
        else if (name == QStringLiteral("Marking"))
        {
            if (!m_markingParser->parse(reader))
                return false;
            auto result = m_markingParser->result();
            m_result->markingList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

AssemblyDrawing *AssemblyDrawingParser::result()
{
    return m_result.take();
}

}