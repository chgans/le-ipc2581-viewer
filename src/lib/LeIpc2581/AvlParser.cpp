#include "AvlParser.h"


#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

AvlParser::AvlParser(
){

}

bool AvlParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Avl());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Avl *AvlParser::result()
{
    return m_result.take();
}

}