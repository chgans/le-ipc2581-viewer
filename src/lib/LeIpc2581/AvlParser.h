
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Avl.h"

namespace Ipc2581b
{


class AvlParser
{
public:
    AvlParser(
            );

    bool parse(QXmlStreamReader *reader);
    Avl *result();

private:
    QScopedPointer<Avl> m_result;
};

}