
#pragma once

#include <QString>

class QXmlStreamReader;

#include "BackdrillList.h"

namespace Ipc2581b
{

class BackdrillListParser
{
public:
    BackdrillListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    BackdrillList result();

private:
    BackdrillList m_result;
};

}