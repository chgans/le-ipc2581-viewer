
#pragma once

#include <QString>

class QXmlStreamReader;

#include "BoardTechnology.h"

namespace Ipc2581b
{

class BoardTechnologyParser
{
public:
    BoardTechnologyParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    BoardTechnology result();

private:
    BoardTechnology m_result;
};

}