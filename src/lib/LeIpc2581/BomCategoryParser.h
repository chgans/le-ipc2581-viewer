
#pragma once

#include <QString>

class QXmlStreamReader;

#include "BomCategory.h"

namespace Ipc2581b
{

class BomCategoryParser
{
public:
    BomCategoryParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    BomCategory result();

private:
    BomCategory m_result;
};

}