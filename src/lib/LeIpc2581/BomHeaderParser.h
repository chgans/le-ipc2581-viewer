
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "BomHeader.h"

namespace Ipc2581b
{

class BoolParser;
class NameRefParser;
class QStringParser;

class BomHeaderParser
{
public:
    BomHeaderParser(
            QStringParser*&
            , QStringParser*&
            , BoolParser*&
            , NameRefParser*&
            );

    bool parse(QXmlStreamReader *reader);
    BomHeader *result();

private:
    QStringParser *&m_assemblyParser;
    QStringParser *&m_revisionParser;
    BoolParser *&m_affectingParser;
    NameRefParser *&m_stepRefParser;
    QScopedPointer<BomHeader> m_result;
};

}