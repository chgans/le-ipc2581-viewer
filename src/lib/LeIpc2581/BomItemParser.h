
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "BomItem.h"

namespace Ipc2581b
{

class BomCategoryParser;
class BomDesParser;
class CharacteristicsParser;
class IntParser;
class QStringParser;

class BomItemParser
{
public:
    BomItemParser(
            QStringParser*&
            , QStringParser*&
            , IntParser*&
            , BomCategoryParser*&
            , QStringParser*&
            , QStringParser*&
            , BomDesParser*&
            , CharacteristicsParser*&
            );

    bool parse(QXmlStreamReader *reader);
    BomItem *result();

private:
    QStringParser *&m_oEMDesignNumberRefParser;
    QStringParser *&m_quantityParser;
    IntParser *&m_pinCountParser;
    BomCategoryParser *&m_categoryParser;
    QStringParser *&m_internalPartNumberParser;
    QStringParser *&m_descriptionParser;
    BomDesParser *&m_bomDesParser;
    CharacteristicsParser *&m_characteristicsParser;
    QScopedPointer<BomItem> m_result;
};

}