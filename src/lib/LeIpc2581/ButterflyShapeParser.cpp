#include "ButterflyShapeParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

ButterflyShapeParser::ButterflyShapeParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool ButterflyShapeParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, ButterflyShape> map =
    {
        {QStringLiteral("ROUND"), ButterflyShape::Round},
        {QStringLiteral("SQUARE"), ButterflyShape::Square},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum ButterflyShape").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

ButterflyShape ButterflyShapeParser::result()
{
    return m_result;
}

}