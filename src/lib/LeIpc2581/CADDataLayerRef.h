
#pragma once

#include <QList>
#include "Optional.h"


#include "QString.h"

namespace Ipc2581b
{

class CADDataLayerRef
{
public:
	virtual ~CADDataLayerRef() {}

    QString layerId;

};

}