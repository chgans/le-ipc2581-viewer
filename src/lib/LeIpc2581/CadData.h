
#pragma once

#include <QList>
#include "Optional.h"


#include "Layer.h"
#include "Stackup.h"
#include "Step.h"

namespace Ipc2581b
{

class CadData
{
public:
	virtual ~CadData() {}

    QList<Layer*> layerList;
    QList<Stackup*> stackupList;
    QList<Step*> stepList;

};

}