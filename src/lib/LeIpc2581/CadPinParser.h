
#pragma once

#include <QString>

class QXmlStreamReader;

#include "CadPin.h"

namespace Ipc2581b
{

class CadPinParser
{
public:
    CadPinParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    CadPin result();

private:
    CadPin m_result;
};

}