
#pragma once

#include <QString>

class QXmlStreamReader;

#include "CadProperty.h"

namespace Ipc2581b
{

class CadPropertyParser
{
public:
    CadPropertyParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    CadProperty result();

private:
    CadProperty m_result;
};

}