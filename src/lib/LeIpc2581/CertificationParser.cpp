#include "CertificationParser.h"

#include "CertificationStatusParser.h"
#include "CertificationCategoryParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

CertificationParser::CertificationParser(
    CertificationStatusParser *&_certificationStatusParser
    , CertificationStatusParser *&_certificationParser
    , CertificationCategoryParser *&_certificationCategoryParser
):    m_certificationStatusParser(_certificationStatusParser)
    , m_certificationParser(_certificationParser)
    , m_certificationCategoryParser(_certificationCategoryParser)
{

}

bool CertificationParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Certification());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("certificationStatus")))
    {
        data = reader->attributes().value(QStringLiteral("certificationStatus"));
        if (!m_certificationStatusParser->parse(reader, data))
            return false;
        m_result->certificationStatusOptional = Optional<CertificationStatus>(m_certificationStatusParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("certification")))
    {
        data = reader->attributes().value(QStringLiteral("certification"));
        if (!m_certificationParser->parse(reader, data))
            return false;
        m_result->certificationOptional = Optional<CertificationStatus>(m_certificationParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("certificationCategory")))
    {
        data = reader->attributes().value(QStringLiteral("certificationCategory"));
        if (!m_certificationCategoryParser->parse(reader, data))
            return false;
        m_result->certificationCategoryOptional = Optional<CertificationCategory>(m_certificationCategoryParser->result());
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Certification *CertificationParser::result()
{
    return m_result.take();
}

}