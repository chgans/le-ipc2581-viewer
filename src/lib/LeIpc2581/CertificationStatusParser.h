
#pragma once

#include <QString>

class QXmlStreamReader;

#include "CertificationStatus.h"

namespace Ipc2581b
{

class CertificationStatusParser
{
public:
    CertificationStatusParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    CertificationStatus result();

private:
    CertificationStatus m_result;
};

}