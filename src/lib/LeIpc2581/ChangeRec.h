
#pragma once

#include <QList>
#include "Optional.h"


#include "Approval.h"
#include "QString.h"

namespace Ipc2581b
{

class ChangeRec
{
public:
	virtual ~ChangeRec() {}

    QString dateTime;
    QString personRef;
    QString application;
    QString change;
    QList<Approval*> approvalList;

};

}