
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "ChangeRec.h"

namespace Ipc2581b
{

class ApprovalParser;
class QStringParser;

class ChangeRecParser
{
public:
    ChangeRecParser(
            QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , ApprovalParser*&
            );

    bool parse(QXmlStreamReader *reader);
    ChangeRec *result();

private:
    QStringParser *&m_dateTimeParser;
    QStringParser *&m_personRefParser;
    QStringParser *&m_applicationParser;
    QStringParser *&m_changeParser;
    ApprovalParser *&m_approvalParser;
    QScopedPointer<ChangeRec> m_result;
};

}