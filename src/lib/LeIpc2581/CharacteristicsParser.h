
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Characteristics.h"

namespace Ipc2581b
{


class CharacteristicsParser
{
public:
    CharacteristicsParser(
            );

    bool parse(QXmlStreamReader *reader);
    Characteristics *result();

private:
    QScopedPointer<Characteristics> m_result;
};

}