
#pragma once


namespace Ipc2581b
{

class Color;
class ColorRef;

class ColorGroup
{
public:
	virtual ~ColorGroup() {}

    enum class ColorGroupType
    {
        Color
        ,ColorRef
    };

    virtual ColorGroupType colorGroupType() const = 0;

    inline bool isColor() const
    {
        return colorGroupType() == ColorGroupType::Color;
    }

    inline Color *toColor()
    {
        return reinterpret_cast<Color*>(this);
    }

    inline const Color *toColor() const
    {
        return reinterpret_cast<const Color*>(this);
    }

    inline bool isColorRef() const
    {
        return colorGroupType() == ColorGroupType::ColorRef;
    }

    inline ColorRef *toColorRef()
    {
        return reinterpret_cast<ColorRef*>(this);
    }

    inline const ColorRef *toColorRef() const
    {
        return reinterpret_cast<const ColorRef*>(this);
    }


};

}

// For user convenience
#include "Color.h"
#include "ColorRef.h"
