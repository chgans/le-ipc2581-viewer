
#pragma once

#include <QList>
#include "Optional.h"

#include "ColorGroup.h"

#include "QString.h"

namespace Ipc2581b
{

class ColorRef: public ColorGroup
{
public:
	virtual ~ColorRef() {}

    QString id;

    virtual ColorGroupType colorGroupType() const override
    {
        return ColorGroupType::ColorRef;
    }
};

}