
#pragma once

namespace Ipc2581b
{

enum class ComplianceList
{
    Reach,
    HalogenFree,
    Weee,
    Rohs,
    Other,
    ConflictMinerals,
};

}