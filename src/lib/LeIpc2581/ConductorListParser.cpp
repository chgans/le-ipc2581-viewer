#include "ConductorListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

ConductorListParser::ConductorListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool ConductorListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, ConductorList> map =
    {
        {QStringLiteral("FINISHED_HEIGHT"), ConductorList::FinishedHeight},
        {QStringLiteral("SURFACE_ROUGHNESS_UPFACING"), ConductorList::SurfaceRoughnessUpfacing},
        {QStringLiteral("SURFACE_ROUGHNESS_DOWNFACING"), ConductorList::SurfaceRoughnessDownfacing},
        {QStringLiteral("OTHER"), ConductorList::Other},
        {QStringLiteral("SURFACE_ROUGHNESS_TREATED"), ConductorList::SurfaceRoughnessTreated},
        {QStringLiteral("CONDUCTIVITY"), ConductorList::Conductivity},
        {QStringLiteral("ETCH_FACTOR"), ConductorList::EtchFactor},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum ConductorList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

ConductorList ConductorListParser::result()
{
    return m_result;
}

}