
#pragma once

namespace Ipc2581b
{

enum class Context
{
    Documentation,
    Boardpanel,
    Assembly,
    Coupon,
    Board,
    Assemblypallet,
    Tooling,
    Miscellaneous,
};

}