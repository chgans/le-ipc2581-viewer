
#pragma once

#include <QString>

class QXmlStreamReader;

#include "Context.h"

namespace Ipc2581b
{

class ContextParser
{
public:
    ContextParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    Context result();

private:
    Context m_result;
};

}