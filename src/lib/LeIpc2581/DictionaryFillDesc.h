
#pragma once

#include <QList>
#include "Optional.h"


#include "Units.h"
#include "EntryFillDesc.h"

namespace Ipc2581b
{

class DictionaryFillDesc
{
public:
	virtual ~DictionaryFillDesc() {}

    Units units;
    QList<EntryFillDesc*> entryFillDescList;

};

}