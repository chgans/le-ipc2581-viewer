
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "DictionaryFont.h"

namespace Ipc2581b
{

class UnitsParser;
class EntryFontParser;

class DictionaryFontParser
{
public:
    DictionaryFontParser(
            UnitsParser*&
            , EntryFontParser*&
            );

    bool parse(QXmlStreamReader *reader);
    DictionaryFont *result();

private:
    UnitsParser *&m_unitsParser;
    EntryFontParser *&m_entryFontParser;
    QScopedPointer<DictionaryFont> m_result;
};

}