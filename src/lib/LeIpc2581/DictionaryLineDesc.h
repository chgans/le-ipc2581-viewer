
#pragma once

#include <QList>
#include "Optional.h"


#include "Units.h"
#include "EntryLineDesc.h"

namespace Ipc2581b
{

class DictionaryLineDesc
{
public:
	virtual ~DictionaryLineDesc() {}

    Units units;
    QList<EntryLineDesc*> entryLineDescList;

};

}