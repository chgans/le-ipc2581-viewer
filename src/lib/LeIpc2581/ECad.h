
#pragma once

#include <QList>
#include "Optional.h"


#include "CadHeader.h"
#include "CadData.h"
#include "QString.h"

namespace Ipc2581b
{

class ECad
{
public:
	virtual ~ECad() {}

    QString name;
    CadHeader *cadHeader;
    CadData *cadData;

};

}