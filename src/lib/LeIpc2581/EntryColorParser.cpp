#include "EntryColorParser.h"

#include "ColorParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

EntryColorParser::EntryColorParser(
    QStringParser *&_idParser
    , ColorParser *&_colorParser
):    m_idParser(_idParser)
    , m_colorParser(_colorParser)
{

}

bool EntryColorParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new EntryColor());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("id")))
    {
        data = reader->attributes().value(QStringLiteral("id"));
        if (!m_idParser->parse(reader, data))
            return false;
        m_result->id = m_idParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("id: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Color"))
        {
            if (!m_colorParser->parse(reader))
                return false;
            auto result = m_colorParser->result();
            m_result->color = result;
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

EntryColor *EntryColorParser::result()
{
    return m_result.take();
}

}