
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "EntryFont.h"

namespace Ipc2581b
{

class FontDefParser;
class QStringParser;

class EntryFontParser
{
public:
    EntryFontParser(
            QStringParser*&
            , FontDefParser*&
            );

    bool parse(QXmlStreamReader *reader);
    EntryFont *result();

private:
    QStringParser *&m_idParser;
    FontDefParser *&m_fontDefParser;
    QScopedPointer<EntryFont> m_result;
};

}