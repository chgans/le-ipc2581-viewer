
#pragma once

#include <QList>
#include "Optional.h"


#include "LineDesc.h"
#include "QString.h"

namespace Ipc2581b
{

class EntryLineDesc
{
public:
	virtual ~EntryLineDesc() {}

    QString id;
    LineDesc *lineDesc;

};

}