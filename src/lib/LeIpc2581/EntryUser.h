
#pragma once

#include <QList>
#include "Optional.h"


#include "UserPrimitive.h"
#include "QString.h"

namespace Ipc2581b
{

class EntryUser
{
public:
	virtual ~EntryUser() {}

    QString id;
    UserPrimitive *userPrimitive;

};

}