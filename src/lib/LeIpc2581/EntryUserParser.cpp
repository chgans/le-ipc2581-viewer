#include "EntryUserParser.h"

#include "UserPrimitiveParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

EntryUserParser::EntryUserParser(
    QStringParser *&_idParser
    , UserPrimitiveParser *&_userPrimitiveParser
):    m_idParser(_idParser)
    , m_userPrimitiveParser(_userPrimitiveParser)
{

}

bool EntryUserParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new EntryUser());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("id")))
    {
        data = reader->attributes().value(QStringLiteral("id"));
        if (!m_idParser->parse(reader, data))
            return false;
        m_result->id = m_idParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("id: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (m_userPrimitiveParser->isSubstitution(name))
        {
            if (!m_userPrimitiveParser->parse(reader))
                return false;
            auto result = m_userPrimitiveParser->result();
            m_result->userPrimitive = result;
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

EntryUser *EntryUserParser::result()
{
    return m_result.take();
}

}