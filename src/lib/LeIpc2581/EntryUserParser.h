
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "EntryUser.h"

namespace Ipc2581b
{

class UserPrimitiveParser;
class QStringParser;

class EntryUserParser
{
public:
    EntryUserParser(
            QStringParser*&
            , UserPrimitiveParser*&
            );

    bool parse(QXmlStreamReader *reader);
    EntryUser *result();

private:
    QStringParser *&m_idParser;
    UserPrimitiveParser *&m_userPrimitiveParser;
    QScopedPointer<EntryUser> m_result;
};

}