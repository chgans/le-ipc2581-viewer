#include "ExposureParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

ExposureParser::ExposureParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool ExposureParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, Exposure> map =
    {
        {QStringLiteral("COVERED_SECONDARY"), Exposure::CoveredSecondary},
        {QStringLiteral("EXPOSED"), Exposure::Exposed},
        {QStringLiteral("COVERED"), Exposure::Covered},
        {QStringLiteral("COVERED_PRIMARY"), Exposure::CoveredPrimary},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum Exposure").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

Exposure ExposureParser::result()
{
    return m_result;
}

}