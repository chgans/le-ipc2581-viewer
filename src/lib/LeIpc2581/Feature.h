
#pragma once


namespace Ipc2581b
{

class StandardShape;
class UserShape;

class Feature
{
public:
	virtual ~Feature() {}

    enum class FeatureType
    {
        StandardShape
        ,UserShape
    };

    virtual FeatureType featureType() const = 0;

    inline bool isStandardShape() const
    {
        return featureType() == FeatureType::StandardShape;
    }

    inline StandardShape *toStandardShape()
    {
        return reinterpret_cast<StandardShape*>(this);
    }

    inline const StandardShape *toStandardShape() const
    {
        return reinterpret_cast<const StandardShape*>(this);
    }

    inline bool isUserShape() const
    {
        return featureType() == FeatureType::UserShape;
    }

    inline UserShape *toUserShape()
    {
        return reinterpret_cast<UserShape*>(this);
    }

    inline const UserShape *toUserShape() const
    {
        return reinterpret_cast<const UserShape*>(this);
    }


};

}

// For user convenience
#include "StandardShape.h"
#include "UserShape.h"
