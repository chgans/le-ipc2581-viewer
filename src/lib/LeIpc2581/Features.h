
#pragma once

#include <QList>
#include "Optional.h"


#include "Location.h"
#include "Xform.h"
#include "Feature.h"

namespace Ipc2581b
{

class Features
{
public:
	virtual ~Features() {}

    Optional<Xform*> xformOptional;
    QList<Location*> locationList;
    Feature *feature;

};

}