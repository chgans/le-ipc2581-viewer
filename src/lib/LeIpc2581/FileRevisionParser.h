
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "FileRevision.h"

namespace Ipc2581b
{

class SoftwarePackageParser;
class QStringParser;

class FileRevisionParser
{
public:
    FileRevisionParser(
            QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , SoftwarePackageParser*&
            );

    bool parse(QXmlStreamReader *reader);
    FileRevision *result();

private:
    QStringParser *&m_fileRevisionIdParser;
    QStringParser *&m_commentParser;
    QStringParser *&m_labelParser;
    SoftwarePackageParser *&m_softwarePackageParser;
    QScopedPointer<FileRevision> m_result;
};

}