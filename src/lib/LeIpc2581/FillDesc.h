
#pragma once

#include <QList>
#include "Optional.h"

#include "FillDescGroup.h"

#include "FillProperty.h"
#include "ColorGroup.h"
#include "Double.h"

namespace Ipc2581b
{

class FillDesc: public FillDescGroup
{
public:
	virtual ~FillDesc() {}

    FillProperty fillProperty;
    Optional<Double> lineWidthOptional;
    Optional<Double> pitch1Optional;
    Optional<Double> pitch2Optional;
    Optional<Double> angle1Optional;
    Optional<Double> angle2Optional;
    Optional<ColorGroup*> colorOptional;

    virtual FillDescGroupType fillDescGroupType() const override
    {
        return FillDescGroupType::FillDesc;
    }
};

}