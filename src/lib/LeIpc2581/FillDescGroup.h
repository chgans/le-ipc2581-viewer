
#pragma once


namespace Ipc2581b
{

class FillDesc;
class FillDescRef;

class FillDescGroup
{
public:
	virtual ~FillDescGroup() {}

    enum class FillDescGroupType
    {
        FillDesc
        ,FillDescRef
    };

    virtual FillDescGroupType fillDescGroupType() const = 0;

    inline bool isFillDesc() const
    {
        return fillDescGroupType() == FillDescGroupType::FillDesc;
    }

    inline FillDesc *toFillDesc()
    {
        return reinterpret_cast<FillDesc*>(this);
    }

    inline const FillDesc *toFillDesc() const
    {
        return reinterpret_cast<const FillDesc*>(this);
    }

    inline bool isFillDescRef() const
    {
        return fillDescGroupType() == FillDescGroupType::FillDescRef;
    }

    inline FillDescRef *toFillDescRef()
    {
        return reinterpret_cast<FillDescRef*>(this);
    }

    inline const FillDescRef *toFillDescRef() const
    {
        return reinterpret_cast<const FillDescRef*>(this);
    }


};

}

// For user convenience
#include "FillDesc.h"
#include "FillDescRef.h"
