
#pragma once

#include <QString>

class QXmlStreamReader;

#include "FillProperty.h"

namespace Ipc2581b
{

class FillPropertyParser
{
public:
    FillPropertyParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    FillProperty result();

private:
    FillProperty m_result;
};

}