
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "FontDefExternal.h"

namespace Ipc2581b
{

class QStringParser;

class FontDefExternalParser
{
public:
    FontDefExternalParser(
            QStringParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    FontDefExternal *result();

private:
    QStringParser *&m_nameParser;
    QStringParser *&m_urnParser;
    QScopedPointer<FontDefExternal> m_result;
};

}