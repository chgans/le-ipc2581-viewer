
#pragma once

namespace Ipc2581b
{

enum class GeneralList
{
    Electrical,
    Thermal,
    Material,
    Other,
    Instruction,
    Standard,
};

}