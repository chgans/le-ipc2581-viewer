
#pragma once

#include <QString>

class QXmlStreamReader;

#include "GeneralList.h"

namespace Ipc2581b
{

class GeneralListParser
{
public:
    GeneralListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    GeneralList result();

private:
    GeneralList m_result;
};

}