
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "GoodPanelMark.h"

namespace Ipc2581b
{

class LocationParser;
class StandardShapeParser;
class XformParser;

class GoodPanelMarkParser
{
public:
    GoodPanelMarkParser(
            XformParser*&
            , LocationParser*&
            , StandardShapeParser*&
            );

    bool parse(QXmlStreamReader *reader);
    GoodPanelMark *result();

private:
    XformParser *&m_xformParser;
    LocationParser *&m_locationParser;
    StandardShapeParser *&m_standardShapeParser;
    QScopedPointer<GoodPanelMark> m_result;
};

}