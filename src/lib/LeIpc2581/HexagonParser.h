
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Hexagon.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class FillDescGroupParser;
class XformParser;
class DoubleParser;

class HexagonParser
{
public:
    HexagonParser(
            DoubleParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Hexagon *result();

private:
    DoubleParser *&m_lengthParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<Hexagon> m_result;
};

}