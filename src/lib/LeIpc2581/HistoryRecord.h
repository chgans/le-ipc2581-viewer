
#pragma once

#include <QList>
#include "Optional.h"


#include "ChangeRec.h"
#include "FileRevision.h"
#include "QString.h"

namespace Ipc2581b
{

class HistoryRecord
{
public:
	virtual ~HistoryRecord() {}

    QString number;
    QString origination;
    QString software;
    QString lastChange;
    Optional<QString> externalConfigurationEntryOptional;
    FileRevision *fileRevision;
    QList<ChangeRec*> changeRecList;

};

}