
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "IdRef.h"

namespace Ipc2581b
{

class QStringParser;

class IdRefParser
{
public:
    IdRefParser(
            QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    IdRef *result();

private:
    QStringParser *&m_idParser;
    QScopedPointer<IdRef> m_result;
};

}