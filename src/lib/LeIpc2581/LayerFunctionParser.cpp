#include "LayerFunctionParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

LayerFunctionParser::LayerFunctionParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool LayerFunctionParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, LayerFunction> map =
    {
        {QStringLiteral("COMPONENT"), LayerFunction::Component},
        {QStringLiteral("EMBEDDED_COMPONENT"), LayerFunction::EmbeddedComponent},
        {QStringLiteral("DIELBASE"), LayerFunction::Dielbase},
        {QStringLiteral("BOARD_OUTLINE"), LayerFunction::BoardOutline},
        {QStringLiteral("ASSEMBLY"), LayerFunction::Assembly},
        {QStringLiteral("STACKUP_COMPOSITE"), LayerFunction::StackupComposite},
        {QStringLiteral("PLANE"), LayerFunction::Plane},
        {QStringLiteral("SILKSCREEN"), LayerFunction::Silkscreen},
        {QStringLiteral("REWORK"), LayerFunction::Rework},
        {QStringLiteral("COURTYARD"), LayerFunction::Courtyard},
        {QStringLiteral("SOLDERBUMP"), LayerFunction::Solderbump},
        {QStringLiteral("BOARDFAB"), LayerFunction::Boardfab},
        {QStringLiteral("PROBE"), LayerFunction::Probe},
        {QStringLiteral("PIN"), LayerFunction::Pin},
        {QStringLiteral("V_CUT"), LayerFunction::VCut},
        {QStringLiteral("GRAPHIC"), LayerFunction::Graphic},
        {QStringLiteral("COATINGNONCOND"), LayerFunction::Coatingnoncond},
        {QStringLiteral("CONDFILM"), LayerFunction::Condfilm},
        {QStringLiteral("OTHER"), LayerFunction::Other},
        {QStringLiteral("FIXTURE"), LayerFunction::Fixture},
        {QStringLiteral("CAPACITIVE"), LayerFunction::Capacitive},
        {QStringLiteral("DIELADHV"), LayerFunction::Dieladhv},
        {QStringLiteral("CONDUCTIVE_ADHESIVE"), LayerFunction::ConductiveAdhesive},
        {QStringLiteral("SIGNAL"), LayerFunction::Signal},
        {QStringLiteral("COATINGCOND"), LayerFunction::Coatingcond},
        {QStringLiteral("CONDUCTOR"), LayerFunction::Conductor},
        {QStringLiteral("COMPONENT_TOP"), LayerFunction::ComponentTop},
        {QStringLiteral("DRILL"), LayerFunction::Drill},
        {QStringLiteral("RESISTIVE"), LayerFunction::Resistive},
        {QStringLiteral("SOLDERMASK"), LayerFunction::Soldermask},
        {QStringLiteral("DIELCORE"), LayerFunction::Dielcore},
        {QStringLiteral("GLUE"), LayerFunction::Glue},
        {QStringLiteral("MIXED"), LayerFunction::Mixed},
        {QStringLiteral("ROUT"), LayerFunction::Rout},
        {QStringLiteral("DOCUMENT"), LayerFunction::Document},
        {QStringLiteral("PASTEMASK"), LayerFunction::Pastemask},
        {QStringLiteral("CONDFOIL"), LayerFunction::Condfoil},
        {QStringLiteral("HOLEFILL"), LayerFunction::Holefill},
        {QStringLiteral("SOLDERPASTE"), LayerFunction::Solderpaste},
        {QStringLiteral("DIELPREG"), LayerFunction::Dielpreg},
        {QStringLiteral("LEGEND"), LayerFunction::Legend},
        {QStringLiteral("LANDPATTERN"), LayerFunction::Landpattern},
        {QStringLiteral("COMPONENT_BOTTOM"), LayerFunction::ComponentBottom},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum LayerFunction").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

LayerFunction LayerFunctionParser::result()
{
    return m_result;
}

}