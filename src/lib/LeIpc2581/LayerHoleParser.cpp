#include "LayerHoleParser.h"


#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

LayerHoleParser::LayerHoleParser(
){

}

bool LayerHoleParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new LayerHole());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

LayerHole *LayerHoleParser::result()
{
    return m_result.take();
}

}