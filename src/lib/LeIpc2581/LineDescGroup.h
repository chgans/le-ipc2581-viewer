
#pragma once


namespace Ipc2581b
{

class LineDesc;
class LineDescRef;

class LineDescGroup
{
public:
	virtual ~LineDescGroup() {}

    enum class LineDescGroupType
    {
        LineDesc
        ,LineDescRef
    };

    virtual LineDescGroupType lineDescGroupType() const = 0;

    inline bool isLineDesc() const
    {
        return lineDescGroupType() == LineDescGroupType::LineDesc;
    }

    inline LineDesc *toLineDesc()
    {
        return reinterpret_cast<LineDesc*>(this);
    }

    inline const LineDesc *toLineDesc() const
    {
        return reinterpret_cast<const LineDesc*>(this);
    }

    inline bool isLineDescRef() const
    {
        return lineDescGroupType() == LineDescGroupType::LineDescRef;
    }

    inline LineDescRef *toLineDescRef()
    {
        return reinterpret_cast<LineDescRef*>(this);
    }

    inline const LineDescRef *toLineDescRef() const
    {
        return reinterpret_cast<const LineDescRef*>(this);
    }


};

}

// For user convenience
#include "LineDesc.h"
#include "LineDescRef.h"
