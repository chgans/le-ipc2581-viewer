
#pragma once

#include <QList>
#include "Optional.h"

#include "LineDescGroup.h"

#include "QString.h"

namespace Ipc2581b
{

class LineDescRef: public LineDescGroup
{
public:
	virtual ~LineDescRef() {}

    QString id;

    virtual LineDescGroupType lineDescGroupType() const override
    {
        return LineDescGroupType::LineDescRef;
    }
};

}