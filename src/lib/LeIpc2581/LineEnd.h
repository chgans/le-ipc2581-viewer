
#pragma once

namespace Ipc2581b
{

enum class LineEnd
{
    Round,
    Square,
    None,
};

}