
#pragma once

#include <QString>

class QXmlStreamReader;

#include "LineProperty.h"

namespace Ipc2581b
{

class LinePropertyParser
{
public:
    LinePropertyParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    LineProperty result();

private:
    LineProperty m_result;
};

}