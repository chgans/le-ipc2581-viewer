
#pragma once

#include <QList>
#include "Optional.h"


#include "MarkingUsage.h"
#include "Location.h"
#include "Xform.h"
#include "Feature.h"

namespace Ipc2581b
{

class Marking
{
public:
	virtual ~Marking() {}

    Optional<MarkingUsage> markingUsageOptional;
    Optional<Xform*> xformOptional;
    Optional<Location*> locationOptional;
    Feature *feature;

};

}