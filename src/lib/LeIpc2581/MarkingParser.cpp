#include "MarkingParser.h"

#include "MarkingUsageParser.h"
#include "LocationParser.h"
#include "XformParser.h"
#include "FeatureParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

MarkingParser::MarkingParser(
    MarkingUsageParser *&_markingUsageParser
    , XformParser *&_xformParser
    , LocationParser *&_locationParser
    , FeatureParser *&_featureParser
):    m_markingUsageParser(_markingUsageParser)
    , m_xformParser(_xformParser)
    , m_locationParser(_locationParser)
    , m_featureParser(_featureParser)
{

}

bool MarkingParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Marking());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("markingUsage")))
    {
        data = reader->attributes().value(QStringLiteral("markingUsage"));
        if (!m_markingUsageParser->parse(reader, data))
            return false;
        m_result->markingUsageOptional = Optional<MarkingUsage>(m_markingUsageParser->result());
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Xform"))
        {
            if (!m_xformParser->parse(reader))
                return false;
            auto result = m_xformParser->result();
            m_result->xformOptional = Optional<Xform*>(result);
        }
        else if (name == QStringLiteral("Location"))
        {
            if (!m_locationParser->parse(reader))
                return false;
            auto result = m_locationParser->result();
            m_result->locationOptional = Optional<Location*>(result);
        }
        else if (m_featureParser->isSubstitution(name))
        {
            if (!m_featureParser->parse(reader))
                return false;
            auto result = m_featureParser->result();
            m_result->feature = result;
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Marking *MarkingParser::result()
{
    return m_result.take();
}

}