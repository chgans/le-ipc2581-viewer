
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "MaterialCut.h"

namespace Ipc2581b
{

class DoubleParser;

class MaterialCutParser
{
public:
    MaterialCutParser(
            DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            );

    bool parse(QXmlStreamReader *reader);
    MaterialCut *result();

private:
    DoubleParser *&m_depthParser;
    DoubleParser *&m_plusTolParser;
    DoubleParser *&m_minusTolParser;
    QScopedPointer<MaterialCut> m_result;
};

}