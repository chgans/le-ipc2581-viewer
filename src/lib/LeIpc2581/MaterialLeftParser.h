
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "MaterialLeft.h"

namespace Ipc2581b
{

class DoubleParser;

class MaterialLeftParser
{
public:
    MaterialLeftParser(
            DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            );

    bool parse(QXmlStreamReader *reader);
    MaterialLeft *result();

private:
    DoubleParser *&m_depthParser;
    DoubleParser *&m_plusTolParser;
    DoubleParser *&m_minusTolParser;
    QScopedPointer<MaterialLeft> m_result;
};

}