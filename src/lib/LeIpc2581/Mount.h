
#pragma once

namespace Ipc2581b
{

enum class Mount
{
    Smt,
    Other,
    Thmt,
};

}