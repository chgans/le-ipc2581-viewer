
#pragma once

namespace Ipc2581b
{

enum class NetPoint
{
    End,
    Middle,
};

}