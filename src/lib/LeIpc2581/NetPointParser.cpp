#include "NetPointParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

NetPointParser::NetPointParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool NetPointParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, NetPoint> map =
    {
        {QStringLiteral("END"), NetPoint::End},
        {QStringLiteral("MIDDLE"), NetPoint::Middle},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum NetPoint").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

NetPoint NetPointParser::result()
{
    return m_result;
}

}