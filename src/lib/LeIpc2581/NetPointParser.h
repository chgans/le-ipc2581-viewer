
#pragma once

#include <QString>

class QXmlStreamReader;

#include "NetPoint.h"

namespace Ipc2581b
{

class NetPointParser
{
public:
    NetPointParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    NetPoint result();

private:
    NetPoint m_result;
};

}