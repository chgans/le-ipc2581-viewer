
#pragma once

#include <QList>
#include "Optional.h"

#include "StandardPrimitive.h"

#include "LineDescGroup.h"
#include "FillDescGroup.h"
#include "Xform.h"
#include "Double.h"

namespace Ipc2581b
{

class Octagon: public StandardPrimitive
{
public:
	virtual ~Octagon() {}

    Double length;
    Optional<Xform*> xformOptional;
    Optional<LineDescGroup*> lineDescGroupOptional;
    Optional<FillDescGroup*> fillDescGroupOptional;

    virtual StandardPrimitiveType standardPrimitiveType() const override
    {
        return StandardPrimitiveType::Octagon;
    }
};

}