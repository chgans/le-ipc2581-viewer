
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Octagon.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class FillDescGroupParser;
class XformParser;
class DoubleParser;

class OctagonParser
{
public:
    OctagonParser(
            DoubleParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Octagon *result();

private:
    DoubleParser *&m_lengthParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<Octagon> m_result;
};

}