#pragma once

namespace Ipc2581b
{

template<class T>
struct Optional
{
    Optional(const T &value):
        hasValue(true), value(value)
    {}
    Optional():
        hasValue(false)
    {}
    bool hasValue;
    T value;
};

}