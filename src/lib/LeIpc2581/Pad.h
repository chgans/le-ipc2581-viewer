
#pragma once

#include <QList>
#include "Optional.h"


#include "Location.h"
#include "PinRef.h"
#include "Feature.h"
#include "Xform.h"
#include "QString.h"

namespace Ipc2581b
{

class Pad
{
public:
	virtual ~Pad() {}

    QString padstackDefRef;
    Optional<Xform*> xformOptional;
    Location *location;
    Feature *feature;
    Optional<PinRef*> pinRefOptional;

};

}