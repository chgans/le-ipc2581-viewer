
#pragma once

namespace Ipc2581b
{

enum class PadUsage
{
    Plane,
    Mask,
    Termination,
    None,
    ToolingHole,
    Via,
};

}