
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "PadstackDef.h"

namespace Ipc2581b
{

class PadstackPadDefParser;
class PadstackHoleDefParser;
class QStringParser;

class PadstackDefParser
{
public:
    PadstackDefParser(
            QStringParser*&
            , PadstackHoleDefParser*&
            , PadstackPadDefParser*&
            );

    bool parse(QXmlStreamReader *reader);
    PadstackDef *result();

private:
    QStringParser *&m_nameParser;
    PadstackHoleDefParser *&m_padstackHoleDefParser;
    PadstackPadDefParser *&m_padstackPadDefParser;
    QScopedPointer<PadstackDef> m_result;
};

}