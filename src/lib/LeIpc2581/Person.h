
#pragma once

#include <QList>
#include "Optional.h"


#include "QString.h"

namespace Ipc2581b
{

class Person
{
public:
	virtual ~Person() {}

    QString name;
    QString enterpriseRef;
    Optional<QString> titleOptional;
    Optional<QString> emailOptional;
    Optional<QString> phoneOptional;
    Optional<QString> faxOptional;
    Optional<QString> mailStopOptional;
    Optional<QString> publicKeyOptional;

};

}