
#pragma once

#include <QList>
#include "Optional.h"


#include "Xform.h"
#include "QString.h"
#include "NetPoint.h"
#include "Exposure.h"
#include "Feature.h"
#include "Double.h"
#include "Bool.h"

namespace Ipc2581b
{

class PhyNetPoint
{
public:
	virtual ~PhyNetPoint() {}

    Double x;
    Double y;
    QString layerRef;
    Optional<QString> secondaryLayerRefOptional;
    NetPoint netNode;
    Exposure exposure;
    Optional<QString> layerIndexOptional;
    Optional<QString> commentOptional;
    Optional<Bool> viaOptional;
    Optional<Bool> fiducialOptional;
    Optional<Bool> testOptional;
    Optional<Double> staggerXOptional;
    Optional<Double> staggerYOptional;
    Optional<Double> staggerRadiusOptional;
    Optional<Xform*> xformOptional;
    Feature *feature;

};

}