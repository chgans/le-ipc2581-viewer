
#pragma once

#include <QString>

class QXmlStreamReader;

#include "PinOneOrientation.h"

namespace Ipc2581b
{

class PinOneOrientationParser
{
public:
    PinOneOrientationParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    PinOneOrientation result();

private:
    PinOneOrientation m_result;
};

}