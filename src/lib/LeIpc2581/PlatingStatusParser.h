
#pragma once

#include <QString>

class QXmlStreamReader;

#include "PlatingStatus.h"

namespace Ipc2581b
{

class PlatingStatusParser
{
public:
    PlatingStatusParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    PlatingStatus result();

private:
    PlatingStatus m_result;
};

}