
#pragma once

#include <QString>

class QXmlStreamReader;

#include "Polarity.h"

namespace Ipc2581b
{

class PolarityParser
{
public:
    PolarityParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    Polarity result();

private:
    Polarity m_result;
};

}