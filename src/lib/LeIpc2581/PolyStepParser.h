
#pragma once

#include <QString>
class QXmlStreamReader;

#include "PolyStep.h"

namespace Ipc2581b
{

class PolyStepSegmentParser;
class PolyStepCurveParser;

class PolyStepParser
{
public:
    PolyStepParser(
            PolyStepSegmentParser*&
            , PolyStepCurveParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    PolyStep *result();

private:
    PolyStepSegmentParser *&m_polyStepSegmentParser;
    PolyStepCurveParser *&m_polyStepCurveParser;
    PolyStep *m_result;
};

}