#include "PolyStepSegmentParser.h"

#include "DoubleParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

PolyStepSegmentParser::PolyStepSegmentParser(
    DoubleParser *&_xParser
    , DoubleParser *&_yParser
):    m_xParser(_xParser)
    , m_yParser(_yParser)
{

}

bool PolyStepSegmentParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new PolyStepSegment());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("x")))
    {
        data = reader->attributes().value(QStringLiteral("x"));
        if (!m_xParser->parse(reader, data))
            return false;
        m_result->x = m_xParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("x: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("y")))
    {
        data = reader->attributes().value(QStringLiteral("y"));
        if (!m_yParser->parse(reader, data))
            return false;
        m_result->y = m_yParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("y: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

PolyStepSegment *PolyStepSegmentParser::result()
{
    return m_result.take();
}

}