#include "PolygonParser.h"

#include "LineDescGroupParser.h"
#include "XformParser.h"
#include "FillDescGroupParser.h"
#include "PolyBeginParser.h"
#include "PolyStepParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

PolygonParser::PolygonParser(
    PolyBeginParser *&_polyBeginParser
    , PolyStepParser *&_polyStepParser
    , XformParser *&_xformParser
    , LineDescGroupParser *&_lineDescGroupParser
    , FillDescGroupParser *&_fillDescGroupParser
):    m_polyBeginParser(_polyBeginParser)
    , m_polyStepParser(_polyStepParser)
    , m_xformParser(_xformParser)
    , m_lineDescGroupParser(_lineDescGroupParser)
    , m_fillDescGroupParser(_fillDescGroupParser)
{

}

bool PolygonParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Polygon());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("PolyBegin"))
        {
            if (!m_polyBeginParser->parse(reader))
                return false;
            auto result = m_polyBeginParser->result();
            m_result->polyBegin = result;
        }
        else if (m_polyStepParser->isSubstitution(name))
        {
            if (!m_polyStepParser->parse(reader))
                return false;
            auto result = m_polyStepParser->result();
            m_result->polyStepList.append(result);
        }
        else if (name == QStringLiteral("Xform"))
        {
            if (!m_xformParser->parse(reader))
                return false;
            auto result = m_xformParser->result();
            m_result->xformOptional = Optional<Xform*>(result);
        }
        else if (m_lineDescGroupParser->isSubstitution(name))
        {
            if (!m_lineDescGroupParser->parse(reader))
                return false;
            auto result = m_lineDescGroupParser->result();
            m_result->lineDescGroupOptional = Optional<LineDescGroup*>(result);
        }
        else if (m_fillDescGroupParser->isSubstitution(name))
        {
            if (!m_fillDescGroupParser->parse(reader))
                return false;
            auto result = m_fillDescGroupParser->result();
            m_result->fillDescGroupOptional = Optional<FillDescGroup*>(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Polygon *PolygonParser::result()
{
    return m_result.take();
}

}