
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Polygon.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class XformParser;
class FillDescGroupParser;
class PolyBeginParser;
class PolyStepParser;

class PolygonParser
{
public:
    PolygonParser(
            PolyBeginParser*&
            , PolyStepParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Polygon *result();

private:
    PolyBeginParser *&m_polyBeginParser;
    PolyStepParser *&m_polyStepParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<Polygon> m_result;
};

}