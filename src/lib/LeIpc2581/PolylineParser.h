
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Polyline.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class PolyBeginParser;
class PolyStepParser;

class PolylineParser
{
public:
    PolylineParser(
            PolyBeginParser*&
            , PolyStepParser*&
            , LineDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Polyline *result();

private:
    PolyBeginParser *&m_polyBeginParser;
    PolyStepParser *&m_polyStepParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    QScopedPointer<Polyline> m_result;
};

}