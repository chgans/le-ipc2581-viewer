
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Property.h"

namespace Ipc2581b
{

class QStringParser;
class BoolParser;
class PropertyUnitParser;
class DoubleParser;

class PropertyParser
{
public:
    PropertyParser(
            DoubleParser*&
            , QStringParser*&
            , PropertyUnitParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , BoolParser*&
            , DoubleParser*&
            , QStringParser*&
            , PropertyUnitParser*&
            , QStringParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Property *result();

private:
    DoubleParser *&m_valueParser;
    QStringParser *&m_textParser;
    PropertyUnitParser *&m_unitParser;
    DoubleParser *&m_tolPlusParser;
    DoubleParser *&m_tolMinusParser;
    DoubleParser *&m_plusTolParser;
    DoubleParser *&m_minusTolParser;
    BoolParser *&m_tolPercentParser;
    DoubleParser *&m_refValueParser;
    QStringParser *&m_refTextParser;
    PropertyUnitParser *&m_refUnitParser;
    QStringParser *&m_layerOrGroupRefParser;
    QStringParser *&m_commentParser;
    QScopedPointer<Property> m_result;
};

}