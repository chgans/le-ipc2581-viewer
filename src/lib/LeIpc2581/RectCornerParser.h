
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "RectCorner.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class FillDescGroupParser;
class XformParser;
class DoubleParser;

class RectCornerParser
{
public:
    RectCornerParser(
            DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    RectCorner *result();

private:
    DoubleParser *&m_lowerLeftXParser;
    DoubleParser *&m_lowerLeftYParser;
    DoubleParser *&m_upperRightXParser;
    DoubleParser *&m_upperRightYParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<RectCorner> m_result;
};

}