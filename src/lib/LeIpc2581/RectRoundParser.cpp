#include "RectRoundParser.h"

#include "LineDescGroupParser.h"
#include "BoolParser.h"
#include "FillDescGroupParser.h"
#include "XformParser.h"
#include "DoubleParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

RectRoundParser::RectRoundParser(
    DoubleParser *&_widthParser
    , DoubleParser *&_heightParser
    , DoubleParser *&_radiusParser
    , BoolParser *&_upperRightParser
    , BoolParser *&_upperLeftParser
    , BoolParser *&_lowerRightParser
    , BoolParser *&_lowerLeftParser
    , XformParser *&_xformParser
    , LineDescGroupParser *&_lineDescGroupParser
    , FillDescGroupParser *&_fillDescGroupParser
):    m_widthParser(_widthParser)
    , m_heightParser(_heightParser)
    , m_radiusParser(_radiusParser)
    , m_upperRightParser(_upperRightParser)
    , m_upperLeftParser(_upperLeftParser)
    , m_lowerRightParser(_lowerRightParser)
    , m_lowerLeftParser(_lowerLeftParser)
    , m_xformParser(_xformParser)
    , m_lineDescGroupParser(_lineDescGroupParser)
    , m_fillDescGroupParser(_fillDescGroupParser)
{

}

bool RectRoundParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new RectRound());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("width")))
    {
        data = reader->attributes().value(QStringLiteral("width"));
        if (!m_widthParser->parse(reader, data))
            return false;
        m_result->width = m_widthParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("width: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("height")))
    {
        data = reader->attributes().value(QStringLiteral("height"));
        if (!m_heightParser->parse(reader, data))
            return false;
        m_result->height = m_heightParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("height: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("radius")))
    {
        data = reader->attributes().value(QStringLiteral("radius"));
        if (!m_radiusParser->parse(reader, data))
            return false;
        m_result->radius = m_radiusParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("radius: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("upperRight")))
    {
        data = reader->attributes().value(QStringLiteral("upperRight"));
        if (!m_upperRightParser->parse(reader, data))
            return false;
        m_result->upperRightOptional = Optional<Bool>(m_upperRightParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("upperLeft")))
    {
        data = reader->attributes().value(QStringLiteral("upperLeft"));
        if (!m_upperLeftParser->parse(reader, data))
            return false;
        m_result->upperLeftOptional = Optional<Bool>(m_upperLeftParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("lowerRight")))
    {
        data = reader->attributes().value(QStringLiteral("lowerRight"));
        if (!m_lowerRightParser->parse(reader, data))
            return false;
        m_result->lowerRightOptional = Optional<Bool>(m_lowerRightParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("lowerLeft")))
    {
        data = reader->attributes().value(QStringLiteral("lowerLeft"));
        if (!m_lowerLeftParser->parse(reader, data))
            return false;
        m_result->lowerLeftOptional = Optional<Bool>(m_lowerLeftParser->result());
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Xform"))
        {
            if (!m_xformParser->parse(reader))
                return false;
            auto result = m_xformParser->result();
            m_result->xformOptional = Optional<Xform*>(result);
        }
        else if (m_lineDescGroupParser->isSubstitution(name))
        {
            if (!m_lineDescGroupParser->parse(reader))
                return false;
            auto result = m_lineDescGroupParser->result();
            m_result->lineDescGroupOptional = Optional<LineDescGroup*>(result);
        }
        else if (m_fillDescGroupParser->isSubstitution(name))
        {
            if (!m_fillDescGroupParser->parse(reader))
                return false;
            auto result = m_fillDescGroupParser->result();
            m_result->fillDescGroupOptional = Optional<FillDescGroup*>(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

RectRound *RectRoundParser::result()
{
    return m_result.take();
}

}