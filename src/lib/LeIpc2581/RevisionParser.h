
#pragma once

#include <QString>

class QXmlStreamReader;

#include "Revision.h"

namespace Ipc2581b
{

class RevisionParser
{
public:
    RevisionParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    Revision result();

private:
    Revision m_result;
};

}