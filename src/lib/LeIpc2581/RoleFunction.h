
#pragma once

namespace Ipc2581b
{

enum class RoleFunction
{
    Billto,
    Buyer,
    Receiver,
    Owner,
    Deliverto,
    Other,
    Sender,
    Engineer,
    Designer,
    Customerservice,
};

}