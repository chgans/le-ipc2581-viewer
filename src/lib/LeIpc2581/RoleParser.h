
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Role.h"

namespace Ipc2581b
{

class RoleFunctionParser;
class QStringParser;

class RoleParser
{
public:
    RoleParser(
            QStringParser*&
            , RoleFunctionParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Role *result();

private:
    QStringParser *&m_idParser;
    RoleFunctionParser *&m_roleFunctionParser;
    QStringParser *&m_descriptionParser;
    QStringParser *&m_publicKeyParser;
    QStringParser *&m_authorityParser;
    QScopedPointer<Role> m_result;
};

}