#include "SetParser.h"

#include "FiducialParser.h"
#include "ColorGroupParser.h"
#include "IdRefParser.h"
#include "QStringParser.h"
#include "SlotCavityParser.h"
#include "HoleParser.h"
#include "PolarityParser.h"
#include "LineDescGroupParser.h"
#include "PadUsageParser.h"
#include "PadParser.h"
#include "BoolParser.h"
#include "FeaturesParser.h"
#include "NonstandardAttributeParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

SetParser::SetParser(
    QStringParser *&_netParser
    , PolarityParser *&_polarityParser
    , PadUsageParser *&_padUsageParser
    , BoolParser *&_testPointParser
    , QStringParser *&_geometryParser
    , BoolParser *&_plateParser
    , QStringParser *&_componentRefParser
    , NonstandardAttributeParser *&_nonstandardAttributeParser
    , PadParser *&_padParser
    , FiducialParser *&_fiducialParser
    , HoleParser *&_holeParser
    , SlotCavityParser *&_slotCavityParser
    , IdRefParser *&_specRefParser
    , FeaturesParser *&_featuresParser
    , ColorGroupParser *&_colorGroupParser
    , LineDescGroupParser *&_lineDescGroupParser
):    m_netParser(_netParser)
    , m_polarityParser(_polarityParser)
    , m_padUsageParser(_padUsageParser)
    , m_testPointParser(_testPointParser)
    , m_geometryParser(_geometryParser)
    , m_plateParser(_plateParser)
    , m_componentRefParser(_componentRefParser)
    , m_nonstandardAttributeParser(_nonstandardAttributeParser)
    , m_padParser(_padParser)
    , m_fiducialParser(_fiducialParser)
    , m_holeParser(_holeParser)
    , m_slotCavityParser(_slotCavityParser)
    , m_specRefParser(_specRefParser)
    , m_featuresParser(_featuresParser)
    , m_colorGroupParser(_colorGroupParser)
    , m_lineDescGroupParser(_lineDescGroupParser)
{

}

bool SetParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Set());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("net")))
    {
        data = reader->attributes().value(QStringLiteral("net"));
        if (!m_netParser->parse(reader, data))
            return false;
        m_result->netOptional = Optional<QString>(m_netParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("polarity")))
    {
        data = reader->attributes().value(QStringLiteral("polarity"));
        if (!m_polarityParser->parse(reader, data))
            return false;
        m_result->polarityOptional = Optional<Polarity>(m_polarityParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("padUsage")))
    {
        data = reader->attributes().value(QStringLiteral("padUsage"));
        if (!m_padUsageParser->parse(reader, data))
            return false;
        m_result->padUsageOptional = Optional<PadUsage>(m_padUsageParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("testPoint")))
    {
        data = reader->attributes().value(QStringLiteral("testPoint"));
        if (!m_testPointParser->parse(reader, data))
            return false;
        m_result->testPointOptional = Optional<Bool>(m_testPointParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("geometry")))
    {
        data = reader->attributes().value(QStringLiteral("geometry"));
        if (!m_geometryParser->parse(reader, data))
            return false;
        m_result->geometryOptional = Optional<QString>(m_geometryParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("plate")))
    {
        data = reader->attributes().value(QStringLiteral("plate"));
        if (!m_plateParser->parse(reader, data))
            return false;
        m_result->plateOptional = Optional<Bool>(m_plateParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("componentRef")))
    {
        data = reader->attributes().value(QStringLiteral("componentRef"));
        if (!m_componentRefParser->parse(reader, data))
            return false;
        m_result->componentRefOptional = Optional<QString>(m_componentRefParser->result());
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("NonstandardAttribute"))
        {
            if (!m_nonstandardAttributeParser->parse(reader))
                return false;
            auto result = m_nonstandardAttributeParser->result();
            m_result->nonstandardAttributeList.append(result);
        }
        else if (name == QStringLiteral("Pad"))
        {
            if (!m_padParser->parse(reader))
                return false;
            auto result = m_padParser->result();
            m_result->padList.append(result);
        }
        else if (m_fiducialParser->isSubstitution(name))
        {
            if (!m_fiducialParser->parse(reader))
                return false;
            auto result = m_fiducialParser->result();
            m_result->fiducialList.append(result);
        }
        else if (name == QStringLiteral("Hole"))
        {
            if (!m_holeParser->parse(reader))
                return false;
            auto result = m_holeParser->result();
            m_result->holeList.append(result);
        }
        else if (name == QStringLiteral("SlotCavity"))
        {
            if (!m_slotCavityParser->parse(reader))
                return false;
            auto result = m_slotCavityParser->result();
            m_result->slotCavityList.append(result);
        }
        else if (name == QStringLiteral("SpecRef"))
        {
            if (!m_specRefParser->parse(reader))
                return false;
            auto result = m_specRefParser->result();
            m_result->specRefList.append(result);
        }
        else if (name == QStringLiteral("Features"))
        {
            if (!m_featuresParser->parse(reader))
                return false;
            auto result = m_featuresParser->result();
            m_result->featuresList.append(result);
        }
        else if (m_colorGroupParser->isSubstitution(name))
        {
            if (!m_colorGroupParser->parse(reader))
                return false;
            auto result = m_colorGroupParser->result();
            m_result->colorGroupList.append(result);
        }
        else if (m_lineDescGroupParser->isSubstitution(name))
        {
            if (!m_lineDescGroupParser->parse(reader))
                return false;
            auto result = m_lineDescGroupParser->result();
            m_result->lineDescGroupList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Set *SetParser::result()
{
    return m_result.take();
}

}