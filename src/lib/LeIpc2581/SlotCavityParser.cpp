#include "SlotCavityParser.h"

#include "SimpleParser.h"
#include "DoubleParser.h"
#include "PlatingStatusParser.h"
#include "ZAxisDimParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

SlotCavityParser::SlotCavityParser(
    QStringParser *&_nameParser
    , PlatingStatusParser *&_platingStatusParser
    , DoubleParser *&_plusTolParser
    , DoubleParser *&_minusTolParser
    , SimpleParser *&_simpleParser
    , ZAxisDimParser *&_z_AxisDimParser
):    m_nameParser(_nameParser)
    , m_platingStatusParser(_platingStatusParser)
    , m_plusTolParser(_plusTolParser)
    , m_minusTolParser(_minusTolParser)
    , m_simpleParser(_simpleParser)
    , m_z_AxisDimParser(_z_AxisDimParser)
{

}

bool SlotCavityParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new SlotCavity());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("name")))
    {
        data = reader->attributes().value(QStringLiteral("name"));
        if (!m_nameParser->parse(reader, data))
            return false;
        m_result->name = m_nameParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("name: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("platingStatus")))
    {
        data = reader->attributes().value(QStringLiteral("platingStatus"));
        if (!m_platingStatusParser->parse(reader, data))
            return false;
        m_result->platingStatus = m_platingStatusParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("platingStatus: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("plusTol")))
    {
        data = reader->attributes().value(QStringLiteral("plusTol"));
        if (!m_plusTolParser->parse(reader, data))
            return false;
        m_result->plusTol = m_plusTolParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("plusTol: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("minusTol")))
    {
        data = reader->attributes().value(QStringLiteral("minusTol"));
        if (!m_minusTolParser->parse(reader, data))
            return false;
        m_result->minusTol = m_minusTolParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("minusTol: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (m_simpleParser->isSubstitution(name))
        {
            if (!m_simpleParser->parse(reader))
                return false;
            auto result = m_simpleParser->result();
            m_result->simpleList.append(result);
        }
        else if (m_z_AxisDimParser->isSubstitution(name))
        {
            if (!m_z_AxisDimParser->parse(reader))
                return false;
            auto result = m_z_AxisDimParser->result();
            m_result->z_AxisDimOptional = Optional<ZAxisDim*>(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

SlotCavity *SlotCavityParser::result()
{
    return m_result.take();
}

}