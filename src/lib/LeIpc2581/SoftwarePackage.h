
#pragma once

#include <QList>
#include "Optional.h"


#include "Certification.h"
#include "QString.h"

namespace Ipc2581b
{

class SoftwarePackage
{
public:
	virtual ~SoftwarePackage() {}

    QString name;
    QString vendor;
    QString revision;
    Optional<QString> modelOptional;
    QList<Certification*> certificationList;

};

}