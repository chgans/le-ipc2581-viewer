
#pragma once


namespace Ipc2581b
{

class Backdrill;
class Compliance;
class Conductor;
class Dielectric;
class General;
class Impedance;
class Technology;
class Temperature;
class Tool;
class V_Cut;

class SpecificationType
{
public:
	virtual ~SpecificationType() {}

    enum class SpecificationTypeType
    {
        Backdrill
        ,Compliance
        ,Conductor
        ,Dielectric
        ,General
        ,Impedance
        ,Technology
        ,Temperature
        ,Tool
        ,V_Cut
    };

    virtual SpecificationTypeType specificationTypeType() const = 0;

    inline bool isBackdrill() const
    {
        return specificationTypeType() == SpecificationTypeType::Backdrill;
    }

    inline Backdrill *toBackdrill()
    {
        return reinterpret_cast<Backdrill*>(this);
    }

    inline const Backdrill *toBackdrill() const
    {
        return reinterpret_cast<const Backdrill*>(this);
    }

    inline bool isCompliance() const
    {
        return specificationTypeType() == SpecificationTypeType::Compliance;
    }

    inline Compliance *toCompliance()
    {
        return reinterpret_cast<Compliance*>(this);
    }

    inline const Compliance *toCompliance() const
    {
        return reinterpret_cast<const Compliance*>(this);
    }

    inline bool isConductor() const
    {
        return specificationTypeType() == SpecificationTypeType::Conductor;
    }

    inline Conductor *toConductor()
    {
        return reinterpret_cast<Conductor*>(this);
    }

    inline const Conductor *toConductor() const
    {
        return reinterpret_cast<const Conductor*>(this);
    }

    inline bool isDielectric() const
    {
        return specificationTypeType() == SpecificationTypeType::Dielectric;
    }

    inline Dielectric *toDielectric()
    {
        return reinterpret_cast<Dielectric*>(this);
    }

    inline const Dielectric *toDielectric() const
    {
        return reinterpret_cast<const Dielectric*>(this);
    }

    inline bool isGeneral() const
    {
        return specificationTypeType() == SpecificationTypeType::General;
    }

    inline General *toGeneral()
    {
        return reinterpret_cast<General*>(this);
    }

    inline const General *toGeneral() const
    {
        return reinterpret_cast<const General*>(this);
    }

    inline bool isImpedance() const
    {
        return specificationTypeType() == SpecificationTypeType::Impedance;
    }

    inline Impedance *toImpedance()
    {
        return reinterpret_cast<Impedance*>(this);
    }

    inline const Impedance *toImpedance() const
    {
        return reinterpret_cast<const Impedance*>(this);
    }

    inline bool isTechnology() const
    {
        return specificationTypeType() == SpecificationTypeType::Technology;
    }

    inline Technology *toTechnology()
    {
        return reinterpret_cast<Technology*>(this);
    }

    inline const Technology *toTechnology() const
    {
        return reinterpret_cast<const Technology*>(this);
    }

    inline bool isTemperature() const
    {
        return specificationTypeType() == SpecificationTypeType::Temperature;
    }

    inline Temperature *toTemperature()
    {
        return reinterpret_cast<Temperature*>(this);
    }

    inline const Temperature *toTemperature() const
    {
        return reinterpret_cast<const Temperature*>(this);
    }

    inline bool isTool() const
    {
        return specificationTypeType() == SpecificationTypeType::Tool;
    }

    inline Tool *toTool()
    {
        return reinterpret_cast<Tool*>(this);
    }

    inline const Tool *toTool() const
    {
        return reinterpret_cast<const Tool*>(this);
    }

    inline bool isV_Cut() const
    {
        return specificationTypeType() == SpecificationTypeType::V_Cut;
    }

    inline V_Cut *toV_Cut()
    {
        return reinterpret_cast<V_Cut*>(this);
    }

    inline const V_Cut *toV_Cut() const
    {
        return reinterpret_cast<const V_Cut*>(this);
    }


};

}

// For user convenience
#include "Backdrill.h"
#include "Compliance.h"
#include "Conductor.h"
#include "Dielectric.h"
#include "General.h"
#include "Impedance.h"
#include "Technology.h"
#include "Temperature.h"
#include "Tool.h"
#include "V_Cut.h"
