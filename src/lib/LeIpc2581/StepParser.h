
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Step.h"

namespace Ipc2581b
{

class ComponentParser;
class LayerFeatureParser;
class PackageParser;
class QStringParser;
class StepRepeatParser;
class LogicalNetParser;
class RouteParser;
class PadStackParser;
class PhyNetGroupParser;
class PadstackDefParser;
class DfxMeasurementsParser;
class NonstandardAttributeParser;
class ContourParser;
class LocationParser;

class StepParser
{
public:
    StepParser(
            QStringParser*&
            , NonstandardAttributeParser*&
            , PadStackParser*&
            , PadstackDefParser*&
            , RouteParser*&
            , LocationParser*&
            , ContourParser*&
            , StepRepeatParser*&
            , PackageParser*&
            , ComponentParser*&
            , LogicalNetParser*&
            , PhyNetGroupParser*&
            , LayerFeatureParser*&
            , DfxMeasurementsParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Step *result();

private:
    QStringParser *&m_nameParser;
    NonstandardAttributeParser *&m_nonstandardAttributeParser;
    PadStackParser *&m_padStackParser;
    PadstackDefParser *&m_padStackDefParser;
    RouteParser *&m_routeParser;
    LocationParser *&m_datumParser;
    ContourParser *&m_profileParser;
    StepRepeatParser *&m_stepRepeatParser;
    PackageParser *&m_packageParser;
    ComponentParser *&m_componentParser;
    LogicalNetParser *&m_logicalNetParser;
    PhyNetGroupParser *&m_phyNetGroupParser;
    LayerFeatureParser *&m_layerFeatureParser;
    DfxMeasurementsParser *&m_dfxMeasurementListParser;
    QScopedPointer<Step> m_result;
};

}