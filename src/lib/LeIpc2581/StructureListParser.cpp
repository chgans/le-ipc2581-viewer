#include "StructureListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

StructureListParser::StructureListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool StructureListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, StructureList> map =
    {
        {QStringLiteral("MICROSTRIP_EMBEDDED"), StructureList::MicrostripEmbedded},
        {QStringLiteral("MICROSTRIP_NO_MASK"), StructureList::MicrostripNoMask},
        {QStringLiteral("OTHER"), StructureList::Other},
        {QStringLiteral("PLANE_LESS_STRIPLINE"), StructureList::PlaneLessStripline},
        {QStringLiteral("COPLANAR_WAVEGUIDE_NO_MASK"), StructureList::CoplanarWaveguideNoMask},
        {QStringLiteral("MICROSTRIP_DUAL_MASKED_COVERED"), StructureList::MicrostripDualMaskedCovered},
        {QStringLiteral("COPLANAR_WAVEGUIDE_MASK_COVERED"), StructureList::CoplanarWaveguideMaskCovered},
        {QStringLiteral("COPLANAR_WAVEGUIDE_DUAL_MASKED_COVERED"), StructureList::CoplanarWaveguideDualMaskedCovered},
        {QStringLiteral("COPLANAR_WAVEGUIDE_STRIPLINE"), StructureList::CoplanarWaveguideStripline},
        {QStringLiteral("COPLANAR_WAVEGUIDE_EMBEDDED"), StructureList::CoplanarWaveguideEmbedded},
        {QStringLiteral("MICROSTRIP_MASK_COVERED"), StructureList::MicrostripMaskCovered},
        {QStringLiteral("STRIPLINE"), StructureList::Stripline},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum StructureList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

StructureList StructureListParser::result()
{
    return m_result;
}

}