
#pragma once

#include <QList>
#include "Optional.h"

#include "SpecificationType.h"

#include "TemperatureList.h"
#include "Property.h"
#include "QString.h"

namespace Ipc2581b
{

class Temperature: public SpecificationType
{
public:
	virtual ~Temperature() {}

    TemperatureList type;
    Optional<QString> commentOptional;
    QList<Property*> propertyList;

    virtual SpecificationTypeType specificationTypeType() const override
    {
        return SpecificationTypeType::Temperature;
    }
};

}