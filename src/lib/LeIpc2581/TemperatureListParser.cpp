#include "TemperatureListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

TemperatureListParser::TemperatureListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool TemperatureListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, TemperatureList> map =
    {
        {QStringLiteral("EXPANSION_X_Y_AXIS"), TemperatureList::ExpansionXYAxis},
        {QStringLiteral("EXPANSION_Z_AXIS"), TemperatureList::ExpansionZAxis},
        {QStringLiteral("THERMAL_DELAMINATION"), TemperatureList::ThermalDelamination},
        {QStringLiteral("OTHER"), TemperatureList::Other},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum TemperatureList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

TemperatureList TemperatureListParser::result()
{
    return m_result;
}

}