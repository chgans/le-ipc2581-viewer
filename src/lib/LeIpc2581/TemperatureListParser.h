
#pragma once

#include <QString>

class QXmlStreamReader;

#include "TemperatureList.h"

namespace Ipc2581b
{

class TemperatureListParser
{
public:
    TemperatureListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    TemperatureList result();

private:
    TemperatureList m_result;
};

}