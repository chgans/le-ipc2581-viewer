
#pragma once

#include <QString>

class QXmlStreamReader;

#include "ToolList.h"

namespace Ipc2581b
{

class ToolListParser
{
public:
    ToolListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    ToolList result();

private:
    ToolList m_result;
};

}