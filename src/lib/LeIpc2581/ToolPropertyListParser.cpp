#include "ToolPropertyListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

ToolPropertyListParser::ToolPropertyListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool ToolPropertyListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, ToolPropertyList> map =
    {
        {QStringLiteral("FINISHED_SIZE"), ToolPropertyList::FinishedSize},
        {QStringLiteral("OTHER"), ToolPropertyList::Other},
        {QStringLiteral("DRILL_SIZE"), ToolPropertyList::DrillSize},
        {QStringLiteral("BIT_ANGLE"), ToolPropertyList::BitAngle},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum ToolPropertyList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

ToolPropertyList ToolPropertyListParser::result()
{
    return m_result;
}

}