
#pragma once

#include <QString>

class QXmlStreamReader;

#include "ToolPropertyList.h"

namespace Ipc2581b
{

class ToolPropertyListParser
{
public:
    ToolPropertyListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    ToolPropertyList result();

private:
    ToolPropertyList m_result;
};

}