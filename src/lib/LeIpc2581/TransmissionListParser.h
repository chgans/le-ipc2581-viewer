
#pragma once

#include <QString>

class QXmlStreamReader;

#include "TransmissionList.h"

namespace Ipc2581b
{

class TransmissionListParser
{
public:
    TransmissionListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    TransmissionList result();

private:
    TransmissionList m_result;
};

}