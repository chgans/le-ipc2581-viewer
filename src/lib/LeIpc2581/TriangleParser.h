
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Triangle.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class FillDescGroupParser;
class XformParser;
class DoubleParser;

class TriangleParser
{
public:
    TriangleParser(
            DoubleParser*&
            , DoubleParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Triangle *result();

private:
    DoubleParser *&m_baseParser;
    DoubleParser *&m_heightParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<Triangle> m_result;
};

}