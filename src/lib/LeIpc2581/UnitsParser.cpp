#include "UnitsParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

UnitsParser::UnitsParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool UnitsParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, Units> map =
    {
        {QStringLiteral("MILLIMETER"), Units::Millimeter},
        {QStringLiteral("MICRON"), Units::Micron},
        {QStringLiteral("INCH"), Units::Inch},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum Units").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

Units UnitsParser::result()
{
    return m_result;
}

}