
#pragma once

#include "UserShape.h"

namespace Ipc2581b
{

class Simple;
class Text;
class UserSpecial;

class UserPrimitive: public UserShape
{
public:
	virtual ~UserPrimitive() {}

    enum class UserPrimitiveType
    {
        Simple
        ,Text
        ,UserSpecial
    };

    virtual UserPrimitiveType userPrimitiveType() const = 0;

    inline bool isSimple() const
    {
        return userPrimitiveType() == UserPrimitiveType::Simple;
    }

    inline Simple *toSimple()
    {
        return reinterpret_cast<Simple*>(this);
    }

    inline const Simple *toSimple() const
    {
        return reinterpret_cast<const Simple*>(this);
    }

    inline bool isText() const
    {
        return userPrimitiveType() == UserPrimitiveType::Text;
    }

    inline Text *toText()
    {
        return reinterpret_cast<Text*>(this);
    }

    inline const Text *toText() const
    {
        return reinterpret_cast<const Text*>(this);
    }

    inline bool isUserSpecial() const
    {
        return userPrimitiveType() == UserPrimitiveType::UserSpecial;
    }

    inline UserSpecial *toUserSpecial()
    {
        return reinterpret_cast<UserSpecial*>(this);
    }

    inline const UserSpecial *toUserSpecial() const
    {
        return reinterpret_cast<const UserSpecial*>(this);
    }

    virtual UserShapeType userShapeType() const override
    {
        return UserShapeType::UserPrimitive;
    }

};

}

// For user convenience
#include "Simple.h"
#include "Text.h"
#include "UserSpecial.h"
