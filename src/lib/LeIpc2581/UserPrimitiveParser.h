
#pragma once

#include <QString>
class QXmlStreamReader;

#include "UserPrimitive.h"

namespace Ipc2581b
{

class SimpleParser;
class UserSpecialParser;
class TextParser;

class UserPrimitiveParser
{
public:
    UserPrimitiveParser(
            SimpleParser*&
            , TextParser*&
            , UserSpecialParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    UserPrimitive *result();

private:
    SimpleParser *&m_simpleParser;
    TextParser *&m_textParser;
    UserSpecialParser *&m_userSpecialParser;
    UserPrimitive *m_result;
};

}