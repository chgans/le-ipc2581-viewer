
#pragma once

#include <QList>
#include "Optional.h"

#include "UserShape.h"

#include "QString.h"

namespace Ipc2581b
{

class UserPrimitiveRef: public UserShape
{
public:
	virtual ~UserPrimitiveRef() {}

    QString id;

    virtual UserShapeType userShapeType() const override
    {
        return UserShapeType::UserPrimitiveRef;
    }
};

}