
#pragma once

#include <QString>
class QXmlStreamReader;

#include "ZAxisDim.h"

namespace Ipc2581b
{

class MaterialLeftParser;
class MaterialCutParser;

class ZAxisDimParser
{
public:
    ZAxisDimParser(
            MaterialCutParser*&
            , MaterialLeftParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    ZAxisDim *result();

private:
    MaterialCutParser *&m_materialCutParser;
    MaterialLeftParser *&m_materialLeftParser;
    ZAxisDim *m_result;
};

}