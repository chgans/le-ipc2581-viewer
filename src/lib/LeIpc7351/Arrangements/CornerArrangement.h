#pragma once

#include "LeIpc7351/Arrangement.h"

#include <QSizeF>

class CornerArrangement : public Arrangement
{
    Q_OBJECT

    Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged)

public:
    CornerArrangement();
    ~CornerArrangement();

    QSizeF size() const;

signals:
    void arrangementChanged();
    void sizeChanged(QSizeF size);

public slots:
    void setSize(QSizeF size);

private:
    QSizeF m_size;

    // Arrangement interface
protected:
    virtual void calculate() override;
};
