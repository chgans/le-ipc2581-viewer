#include "DualArrangement.h"

DualArrangement::DualArrangement()
    : Arrangement()
    , m_distance(1.0)
    , m_orientation(Qt::Horizontal)
{

}

DualArrangement::~DualArrangement()
{

}

qreal DualArrangement::distance() const
{
    return m_distance;
}

Qt::Orientation DualArrangement::orientation() const
{
    return m_orientation;
}

void DualArrangement::setDistance(qreal distance)
{
    if (qFuzzyCompare(m_distance, distance))
        return;

    _IDO_SET_PROPERTY(distance);
    calculate();
}

void DualArrangement::setOrientation(Qt::Orientation orientation)
{
    if (m_orientation == orientation)
        return;

    _IDO_SET_PROPERTY(orientation);
    calculate();
}

void DualArrangement::calculate()
{
    QList<Transform> xforms;
    if (m_orientation == Qt::Horizontal)
    {
        Transform xform;
        xform.rotation = 0;
        xform.mirror = true;
        xform.translation = QPointF(-m_distance/2.0, 0);
        xforms.append(xform);
        xform.rotation = 0;
        xform.mirror = false;
        xform.translation = QPointF(m_distance/2.0, 0);
        xforms.append(xform);
    }
    else
    {
        Transform xform;
        xform.rotation = -90;
        xform.mirror = false;
        xform.translation = QPointF(0, -m_distance/2.0);
        xforms.append(xform);
        xform.rotation = -90;
        xform.mirror = true;
        xform.translation = QPointF(0, m_distance/2.0);
        xforms.append(xform);
    }

    setTransformations(xforms);
}
