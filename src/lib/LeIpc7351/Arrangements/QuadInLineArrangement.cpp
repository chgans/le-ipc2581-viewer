#include "QuadInLineArrangement.h"

QuadInLineArrangement::QuadInLineArrangement()
    : Arrangement()
    , m_size(QSizeF())
    , m_terminalCount(0)
    , m_terminalPitch(0)
{
}

QuadInLineArrangement::~QuadInLineArrangement()
{

}

QSizeF QuadInLineArrangement::size() const
{
    return m_size;
}

int QuadInLineArrangement::terminalCount() const
{
    return m_terminalCount;
}

qreal QuadInLineArrangement::terminalPitch() const
{
    return m_terminalPitch;
}

void QuadInLineArrangement::setSize(QSizeF size)
{
    if (m_size == size)
        return;

    _IDO_SET_PROPERTY2(size, QSizeF(qMax(size.width(), (m_terminalCount-1)*m_terminalPitch),
                                    qMax(size.height(), (m_terminalCount-1)*m_terminalPitch)));
}

void QuadInLineArrangement::setTerminalCount(int terminalCount)
{
    if (m_terminalCount == terminalCount)
        return;

    _IDO_SET_PROPERTY(terminalCount);
}

void QuadInLineArrangement::setTerminalPitch(qreal terminalPitch)
{
    if (m_terminalPitch == terminalPitch)
        return;

    _IDO_SET_PROPERTY(terminalPitch);
}


void QuadInLineArrangement::calculate()
{
    QList<Transform> xforms;

    auto width = (m_terminalCount-1)*m_terminalPitch;
    auto height = (m_terminalCount-1)*m_terminalPitch;
    auto hspan = m_size.width();
    auto vspan = m_size.height();

    // Bottom
    for (int i = 0; i < m_terminalCount; i++)
    {
        Transform xform;
        xform.rotation = -90;
        xform.mirror = false;
        xforms.append(xform);
        xform.translation = QPointF(-width/2.0 + i*m_terminalPitch,
                                    -vspan/2.0);
    }
    // Right
    for (int i = 0; i < m_terminalCount; i++)
    {
        Transform xform;
        xform.rotation = 0;
        xform.mirror = false;
        xforms.append(xform);
        xform.translation = QPointF(hspan/2.0,
                                    -height/2.0 + i*m_terminalPitch);
    }
    // Top
    for (int i = 0; i < m_terminalCount; i++)
    {
        Transform xform;
        xform.rotation = 90;
        xform.mirror = false;
        xforms.append(xform);
        xform.translation = QPointF(width/2.0 - i*m_terminalPitch,
                                    vspan/2.0);
    }
    // Left
    for (int i = 0; i < m_terminalCount; i++)
    {
        Transform xform;
        xform.rotation = 180;
        xform.mirror = false;
        xforms.append(xform);
        xform.translation = QPointF(-hspan/2.0,
                                    height/2.0 - i*m_terminalPitch);
    }

    setTransformations(xforms);
}
