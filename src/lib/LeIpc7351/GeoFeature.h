#pragma once

#include "IDocumentObject.h"


// TBD: BoundingBox, to/from Rect, size
// to/from Rect would make it easy to morth a GeoFeature into another one
// BBox/size could be used for easy shape manipulation (eg hamdles)
// would need generic modifier like move{Top|Center|Bottom}{Left|Center|Right}

class GeoFeature : public IDocumentObject
{
    Q_OBJECT

    Q_PROPERTY(Transform placement READ placement WRITE setPlacement NOTIFY placementChanged)
    Q_PROPERTY(QPointF position READ position WRITE setPosition NOTIFY positionChanged STORED false)
    Q_PROPERTY(qreal rotation READ rotation WRITE setRotation NOTIFY rotationChanged STORED false)
    Q_PROPERTY(bool mirrored READ isMirrored WRITE setMirrored NOTIFY mirroredChanged STORED false)

public:
    GeoFeature();
    ~GeoFeature();

    Transform placement() const;
    QPointF position() const;
    qreal rotation() const;
    bool isMirrored() const;

public slots:
    void setPlacement(Transform placement);
    void setPosition(QPointF position);
    void setRotation(qreal rotation);
    void setMirrored(bool mirrored);

signals:
    void placementChanged(Transform placement);
    void positionChanged(QPointF position);
    void rotationChanged(qreal rotation);
    void mirroredChanged(bool mirrored);

private:
    Transform m_placement;
};

