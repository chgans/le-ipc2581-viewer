#include "IDocumentObjectFactory.h"

IDocumentObjectFactory::IDocumentObjectFactory(QObject *parent)
    : QObject(parent)
{

}

IDocumentObjectFactory::~IDocumentObjectFactory()
{

}

QString IDocumentObjectFactory::enumIconName(int typeId, int value) const
{
    Q_UNUSED(typeId);
    Q_UNUSED(value);
    return QString();
}

QString IDocumentObjectFactory::iconName(int typeId, const IDocumentObject *object) const
{
    Q_UNUSED(typeId);
    Q_UNUSED(object);
    return QString();
}
