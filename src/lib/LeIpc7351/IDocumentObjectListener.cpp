#include "IDocumentObjectListener.h"

#include "IDocumentObject.h"

#include <QDebug>

IDocumentObjectListener::IDocumentObjectListener()
{

}

IDocumentObjectListener::~IDocumentObjectListener()
{

}

void IDocumentObjectListener::documentObjectAboutToBeInserted(IDocumentObject *parent, IDocumentObject *child, int index)
{
    Q_UNUSED(parent)
    Q_UNUSED(child)
    Q_UNUSED(index)
}

void IDocumentObjectListener::documentObjectInserted(IDocumentObject *parent, IDocumentObject *child, int index)
{
    Q_UNUSED(parent)
    Q_UNUSED(child)
    Q_UNUSED(index)
}

void IDocumentObjectListener::documentObjectAboutToBeRemoved(IDocumentObject *parent, IDocumentObject *child, int index)
{
    Q_UNUSED(parent)
    Q_UNUSED(child)
    Q_UNUSED(index)
}

void IDocumentObjectListener::documentObjectRemoved(IDocumentObject *parent, IDocumentObject *child, int index)
{
    Q_UNUSED(parent)
    Q_UNUSED(child)
    Q_UNUSED(index)
}

void IDocumentObjectListener::documentObjectAboutToChangeProperty(const IDocumentObject *object, const QString &name, const QVariant &oldValue)
{
    Q_UNUSED(object)
    Q_UNUSED(name)
    Q_UNUSED(oldValue)
}

void IDocumentObjectListener::documentObjectPropertyChanged(const IDocumentObject *object, const QString &name, const QVariant &newValue)
{
    Q_UNUSED(object)
    Q_UNUSED(name)
    Q_UNUSED(newValue)
}

void IDocumentObjectListener::beginListeningToDocumentObject(IDocumentObject *object)
{
    if (object == nullptr)
    {
        qWarning() << "IDocumentObjectListener::beginListeningToDocumentObject: object is nullptr";
        return;
    }
    object->registerListener(this);
}

void IDocumentObjectListener::endListeningToDocumentObject(IDocumentObject *object)
{
    if (object == nullptr)
    {
        qWarning() << "IDocumentObjectListener::endListeningToDocumentObject: object is nullptr";
        return;
    }
    object->unregisterListener(this);
}
