#pragma once

#include <QString>
#include <QVariant>

class IDocumentObject;

class IDocumentObjectListener
{
public:
    IDocumentObjectListener();
    virtual ~IDocumentObjectListener();

    virtual void documentObjectAboutToBeInserted(IDocumentObject *parent,
                                                 IDocumentObject *child,
                                                 int index);
    virtual void documentObjectInserted(IDocumentObject *parent,
                                        IDocumentObject *child,
                                        int index);
    virtual void documentObjectAboutToBeRemoved(IDocumentObject *parent,
                                                IDocumentObject *child,
                                                int index);
    virtual void documentObjectRemoved(IDocumentObject *parent,
                                       IDocumentObject *child,
                                       int index);
    virtual void documentObjectAboutToChangeProperty(const IDocumentObject *object,
                                                     const QString &name,
                                                     const QVariant &oldValue);
    virtual void documentObjectPropertyChanged(const IDocumentObject *object,
                                               const QString &name,
                                               const QVariant &newValue);

    void beginListeningToDocumentObject(IDocumentObject *object);
    void endListeningToDocumentObject(IDocumentObject *object);
};
