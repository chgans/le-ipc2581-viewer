#include "LandPattern.h"

LandPattern::LandPattern()
{

}

LandPattern::~LandPattern()
{

}

QList<PadStackInstance *> LandPattern::pads() const
{
    return m_pads;
}

void LandPattern::setPads(QList<PadStackInstance *> pads)
{
    if (m_pads == pads)
        return;

    m_pads = pads;
    emit padsChanged(pads);
}
