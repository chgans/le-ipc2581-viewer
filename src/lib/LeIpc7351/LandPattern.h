#pragma once

#include "IDocumentObject.h"

#include "PadStackInstance.h"

#include <QList>

class LandPattern: public IDocumentObject
{
    Q_OBJECT

    Q_PROPERTY(QList<PadStackInstance*> pads READ pads WRITE setPads NOTIFY padsChanged)

    QList<PadStackInstance*> m_pads;

public:
    enum Type
    {
        Type = FeatureBaseType + 4
    };

    LandPattern();
    ~LandPattern();

    QList<PadStackInstance*> pads() const;

public slots:
    void setPads(QList<PadStackInstance*> pads);

signals:
    void padsChanged(QList<PadStackInstance*> pads);

    // IDocumentObject interface
public:
    virtual int objectType() const override { return Type; }
    virtual bool hasEditor() const override { return true; }
};
