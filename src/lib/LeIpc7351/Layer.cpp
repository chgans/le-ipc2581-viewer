#include "Layer.h"

Layer::Layer()
    : IDocumentObject()
    , m_function(L7::LayerFunction::Copper)
    , m_side(L7::LayerSide::Top)
    , m_polarity(L7::PositiveLayer)
{

}

Layer::~Layer()
{

}

L7::LayerFunction Layer::function() const
{
    return m_function;
}

L7::LayerSide Layer::side() const
{
    return m_side;
}

L7::LayerPolarity Layer::polarity() const
{
    return m_polarity;
}

void Layer::setFunction(L7::LayerFunction function)
{
    if (m_function == function)
        return;

    m_function = function;
    emit functionChanged(function);
}

void Layer::setSide(L7::LayerSide side)
{
    if (m_side == side)
        return;

    m_side = side;
    emit sideChanged(side);
}

void Layer::setPolarity(L7::LayerPolarity polarity)
{
    if (m_polarity == polarity)
        return;

    m_polarity = polarity;
    emit polarityChanged(polarity);
}
