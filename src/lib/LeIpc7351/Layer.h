#pragma once

#include "IDocumentObject.h"

#include "LeIpc7351.h"

class Layer : public IDocumentObject
{
    Q_OBJECT

    Q_PROPERTY(L7::LayerFunction function READ function WRITE setFunction NOTIFY functionChanged)
    Q_PROPERTY(L7::LayerSide side READ side WRITE setSide NOTIFY sideChanged)
    Q_PROPERTY(L7::LayerPolarity polarity READ polarity WRITE setPolarity NOTIFY polarityChanged)

public:
    Layer();
    ~Layer();

    L7::LayerFunction function() const;
    L7::LayerSide side() const;
    L7::LayerPolarity polarity() const;

signals:

    void functionChanged(L7::LayerFunction function);
    void sideChanged(L7::LayerSide side);
    void polarityChanged(L7::LayerPolarity polarity);

public slots:
    void setFunction(L7::LayerFunction function);
    void setSide(L7::LayerSide side);
    void setPolarity(L7::LayerPolarity polarity);

private:
    L7::LayerFunction m_function;
    L7::LayerSide m_side;
    L7::LayerPolarity m_polarity;
};
