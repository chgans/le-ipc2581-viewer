#include "PackageFeature.h"

#include <QtGlobal>

PackageFeature::PackageFeature()
    : IDocumentObject()
{

}

PackageFeature::~PackageFeature()
{

}

L7::Anchor PackageFeature::anchoringPoint() const
{
    return m_anchoringPoint;
}

qreal PackageFeature::rotation() const
{
    return m_rotation;
}

QPointF PackageFeature::position() const
{
    return m_position;
}

void PackageFeature::setAnchoringPoint(L7::Anchor anchoringPoint)
{
    if (m_anchoringPoint == anchoringPoint)
        return;

    m_anchoringPoint = anchoringPoint;
    emit anchoringPointChanged(anchoringPoint);
}

void PackageFeature::setRotation(qreal rotation)
{
    if (m_rotation == rotation)
        return;

    m_rotation = rotation;
    emit rotationChanged(rotation);
}

void PackageFeature::setPosition(QPointF position)
{
    if (m_position == position)
        return;

    m_position = position;
    emit positionChanged(position);
}

PackageElectricalFeature::PackageElectricalFeature()
    : PackageFeature()
{

}

PackageElectricalFeature::~PackageElectricalFeature()
{

}

PackageSurfaceTerminal::PackageSurfaceTerminal()
    : PackageElectricalFeature()
{

}

PackageSurfaceTerminal::~PackageSurfaceTerminal()
{

}

PackageThroughTerminal::PackageThroughTerminal()
    : PackageElectricalFeature()
{

}

PackageThroughTerminal::~PackageThroughTerminal()
{

}

PackageMechanicalFeature::PackageMechanicalFeature()
    : PackageFeature()
{

}

PackageMechanicalFeature::~PackageMechanicalFeature()
{

}

PackageBody::PackageBody()
    : PackageMechanicalFeature()
{

}

PackageBody::~PackageBody()
{

}

PackageSurfaceLeadTerminal::PackageSurfaceLeadTerminal()
    : PackageSurfaceTerminal()
{

}

PackageSurfaceLeadTerminal::~PackageSurfaceLeadTerminal()
{

}

qreal PackageSurfaceLeadTerminal::landingLength() const
{
    return m_landingLength;
}

qreal PackageSurfaceLeadTerminal::overallLength() const
{
    return m_overallLength;
}

qreal PackageSurfaceLeadTerminal::width() const
{
    return m_width;
}

qreal PackageSurfaceLeadTerminal::thickness() const
{
    return m_thickness;
}

void PackageSurfaceLeadTerminal::setLandingLength(qreal landingLength)
{
    if (qFuzzyCompare(m_landingLength, landingLength))
        return;

    m_landingLength = landingLength;
    emit landingLengthChanged(landingLength);

    if (m_landingLength > m_overallLength)
        setOverallLength(m_landingLength);
}

void PackageSurfaceLeadTerminal::setOverallLength(qreal overallLength)
{
    if (m_overallLength == overallLength)
        return;

    m_overallLength = overallLength;
    emit overallLengthChanged(m_overallLength);

    if (m_overallLength < m_landingLength)
        setLandingLength(m_overallLength);
}

void PackageSurfaceLeadTerminal::setWidth(qreal width)
{
    if (m_width == width)
        return;

    m_width = width;
    emit widthChanged(width);
}

void PackageSurfaceLeadTerminal::setThickness(qreal thickness)
{
    if (m_thickness == thickness)
        return;

    m_thickness = thickness;
    emit thicknessChanged(thickness);
}

PackageInwardLLeadTerminal::PackageInwardLLeadTerminal()
    : PackageSurfaceLeadTerminal()
{

}

PackageInwardLLeadTerminal::~PackageInwardLLeadTerminal()
{

}

PackageGullLeadTerminal::PackageGullLeadTerminal()
    : PackageSurfaceLeadTerminal()
{

}

PackageGullLeadTerminal::~PackageGullLeadTerminal()
{

}

PackageInwardJLeadTerminal::PackageInwardJLeadTerminal()
    : PackageSurfaceLeadTerminal()
{

}

PackageInwardJLeadTerminal::~PackageInwardJLeadTerminal()
{

}

PackageOutwardLLeadTerminal::PackageOutwardLLeadTerminal()
    : PackageSurfaceLeadTerminal()
{

}

PackageOutwardLLeadTerminal::~PackageOutwardLLeadTerminal()
{

}

PackageUnderOutwardLLeadTerminal::PackageUnderOutwardLLeadTerminal()
    : PackageSurfaceLeadTerminal()
{

}

PackageUnderOutwardLLeadTerminal::~PackageUnderOutwardLLeadTerminal()
{

}
