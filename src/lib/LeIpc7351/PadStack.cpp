#include "PadStack.h"
#include "PadStackElement.h"

PadStack::PadStack()
    : IDocumentObject()
{

}

PadStack::~PadStack()
{

}

QList<PadStackElement *> PadStack::elements() const
{
    return m_elements;
}

void PadStack::setRole(L7::PadStackRole role)
{
    if (m_role == role)
        return;

    _IDO_SET_PROPERTY(role);
}

void PadStack::setElements(QList<PadStackElement *> elements)
{
    if (m_elements == elements)
        return;

    for (auto element: m_elements)
        removeChildObject(element);

    for (auto element: elements)
        addChildObject(element);

    _IDO_SET_PROPERTY(elements);
}

L7::PadStackRole PadStack::role() const
{
    return m_role;
}

void PadStack::addElement(PadStackElement *element)
{
    setElements(elements() << element);
}
