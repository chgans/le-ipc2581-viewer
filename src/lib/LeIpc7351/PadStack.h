#pragma once

#include "IDocumentObject.h"

#include "LeIpc7351.h"

// FIXME: Needed for Q_PROPERTY
#include "PadStackElement.h"

#include <QList>

class PadStack: public IDocumentObject
{
    Q_OBJECT
    Q_DISABLE_COPY(PadStack)

    Q_PROPERTY(L7::PadStackRole role READ role WRITE setRole NOTIFY roleChanged)
    L7::PadStackRole m_role = L7::UnkownPadStackRole;
    Q_PROPERTY(QList<PadStackElement*> elements READ elements WRITE setElements NOTIFY elementsChanged)
    QList<PadStackElement*> m_elements;

public:
    enum
    {
        Type = FeatureBaseType + 1
    };
    PadStack();
    ~PadStack();

    QList<PadStackElement *> elements() const;
    L7::PadStackRole role() const;

    void addElement(PadStackElement *element);

signals:
    void roleChanged(L7::PadStackRole role);
    void elementsChanged(QList<PadStackElement*> elements);

public slots:
    void setRole(L7::PadStackRole role);
    void setElements(QList<PadStackElement*> elements);

    // IDocumentObject interface
public:
    virtual int objectType() const override { return Type; }
    virtual bool hasEditor() const override { return true; }
};
//Q_DECLARE_METATYPE(PadStack*)
//Q_DECLARE_METATYPE(const PadStack*)
//Q_DECLARE_METATYPE(QList<PadStack*>)
//Q_DECLARE_METATYPE(QList<const PadStack*>)
