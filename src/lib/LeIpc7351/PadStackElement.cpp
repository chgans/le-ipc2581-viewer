#include "PadStackElement.h"
#include "Primitive.h"

PadStackElement::PadStackElement()
    : IDocumentObject()
{

}

PadStackElement::~PadStackElement()
{

}

Primitive *PadStackElement::primitive() const
{
    return m_primitive;
}

L7::PadStackElementFunction PadStackElement::function() const
{
    return m_function;
}

void PadStackElement::setFunction(L7::PadStackElementFunction function)
{
    if (m_function == function)
        return;

    _IDO_SET_PROPERTY(function);
}

void PadStackElement::setPrimitive(Primitive *primitive)
{
    if (m_primitive == primitive)
        return;

    if (m_primitive != nullptr)
        removeChildObject(m_primitive);
    addChildObject(primitive);
    _IDO_SET_PROPERTY(primitive);
}
