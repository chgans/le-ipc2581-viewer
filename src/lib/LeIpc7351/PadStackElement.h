#pragma once

#include "IDocumentObject.h"

#include "LeIpc7351.h"
#include "Primitive.h"

class Primitive;

class PadStackElement: public IDocumentObject
{
    Q_OBJECT
    Q_DISABLE_COPY(PadStackElement)

    Q_PROPERTY(L7::PadStackElementFunction function READ function WRITE setFunction NOTIFY functionChanged)
    Q_PROPERTY(Primitive* primitive READ primitive WRITE setPrimitive NOTIFY primitiveChanged)

    L7::PadStackElementFunction m_function = L7::UnknownElementFunction;
    Primitive* m_primitive = nullptr;

public:
    enum
    {
        Type = FeatureBaseType + 2
    };

    PadStackElement();
    ~PadStackElement();

    Primitive *primitive() const;
    L7::PadStackElementFunction function() const;

public slots:
    void setFunction(L7::PadStackElementFunction function);
    void setPrimitive(Primitive* primitive);

signals:
    void functionChanged(L7::PadStackElementFunction function);
    void primitiveChanged(Primitive* primitive);

    // IDocumentObject interface
public:
    virtual int objectType() const override { return Type; }
};
//Q_DECLARE_METATYPE(PadStackElement*)
//Q_DECLARE_METATYPE(const PadStackElement*)
//Q_DECLARE_METATYPE(QList<PadStackElement*>)
//Q_DECLARE_METATYPE(QList<const PadStackElement*>)
