#include "PadStackInstance.h"

PadStackInstance::PadStackInstance()
    : IDocumentObject()
{

}

PadStackInstance::~PadStackInstance()
{

}

QPointF PadStackInstance::position() const
{
    return m_position;
}

qreal PadStackInstance::rotation() const
{
    return m_rotation;
}

void PadStackInstance::setPosition(QPointF position)
{
    if (m_position == position)
        return;

    m_position = position;
    emit positionChanged(position);
}

void PadStackInstance::setRotation(qreal rotation)
{
    if (m_rotation == rotation)
        return;

    m_rotation = rotation;
    emit rotationChanged(rotation);
}
