#pragma once

#include "IDocumentObject.h"

#include <QPointF>

class PadStackInstance : public IDocumentObject
{
    Q_OBJECT

    Q_PROPERTY(QPointF position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(qreal rotation READ rotation WRITE setRotation NOTIFY rotationChanged)

    QPointF m_position;

    qreal m_rotation;

public:
    enum
    {
        Type = FeatureBaseType + 3
    };

    PadStackInstance();
    ~PadStackInstance();

    QPointF position() const;
    qreal rotation() const;

public slots:
    void setPosition(QPointF position);
    void setRotation(qreal rotation);

signals:
    void positionChanged(QPointF position);
    void rotationChanged(qreal rotation);

    // IDocumentObject interface
public:
    virtual int objectType() const override { return Type; }
};
