#include "RectanglePrimitive.h"

RectanglePrimitive::RectanglePrimitive()
{

}

RectanglePrimitive::~RectanglePrimitive()
{

}

QSizeF RectanglePrimitive::size() const
{
    return m_size;
}

qreal RectanglePrimitive::cornerRadius() const
{
    return m_cornerRadius;
}

qreal RectanglePrimitive::chamferSize() const
{
    return m_chamferSize;
}

L7::CornerLocations RectanglePrimitive::roundedCorners() const
{
    return m_roundedCorners;
}

L7::CornerLocations RectanglePrimitive::chamferedCorners() const
{
    return m_chamferedCorners;
}

void RectanglePrimitive::setSize(QSizeF size)
{
    if (m_size == size)
        return;

    const qreal min = qMin(m_size.width()/2.0, m_size.height()/2.0);
    if (m_cornerRadius > min)
        setCornerRadius(min);
    if (chamferSize() > min)
        setChamferSize(min);

    // TODO: adjust corners?
    _IDO_SET_PROPERTY(size);
}

void RectanglePrimitive::setCornerRadius(qreal cornerRadius)
{
    if (m_cornerRadius == cornerRadius)
        return;

    if (cornerRadius > qMin(m_size.width()/2.0, m_size.height()/2.0))
        return;

    if (cornerRadius < 0)
        return;

    _IDO_SET_PROPERTY(cornerRadius);
}

void RectanglePrimitive::setChamferSize(qreal chamferSize)
{
    if (m_chamferSize == chamferSize)
        return;

    if (chamferSize > qMin(m_size.width()/2.0, m_size.height()/2.0))
        return;

    if (chamferSize < 0)
        return;

    _IDO_SET_PROPERTY(chamferSize);
}

void RectanglePrimitive::setRoundedCorners(L7::CornerLocations roundedCorners)
{
    if (m_roundedCorners == roundedCorners)
        return;

    _IDO_SET_PROPERTY(roundedCorners);
}

void RectanglePrimitive::setChamferedCorners(L7::CornerLocations chamferedCorners)
{
    if (m_chamferedCorners == chamferedCorners)
        return;

    _IDO_SET_PROPERTY(chamferedCorners);
}
