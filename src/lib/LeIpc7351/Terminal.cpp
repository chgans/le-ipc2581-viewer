#include "Terminal.h"

Terminal::Terminal()
    : IDocumentObject()
{
}

Terminal::~Terminal()
{

}

QSizeF Terminal::size() const
{
    return m_size;
}

L7::Anchor Terminal::anchor() const
{
    return m_anchor;
}

void Terminal::setSize(QSizeF size)
{
    if (m_size == size)
        return;

    m_size = size;
    emit sizeChanged(size);
}

void Terminal::setAnchor(L7::Anchor anchor)
{
    if (m_anchor == anchor)
        return;

    m_anchor = anchor;
    emit anchorChanged(anchor);
}
