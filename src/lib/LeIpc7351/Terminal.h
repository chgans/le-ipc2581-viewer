#pragma once

#include "IDocumentObject.h"
#include "LeIpc7351.h"

#include <QSizeF>
#include <QRectF>

class Terminal : public IDocumentObject
{
    Q_OBJECT

    Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged)
    QSizeF m_size;
    Q_PROPERTY(L7::Anchor anchor READ anchor WRITE setAnchor NOTIFY anchorChanged)
    L7::Anchor m_anchor;

public:
    enum
    {
        Type = FeatureBaseType + 5
    };
    Terminal();
    ~Terminal();

    QSizeF size() const;
    L7::Anchor anchor() const;
    QRectF boundingBox() const;

public slots:
    void setSize(QSizeF size);
    void setAnchor(L7::Anchor anchor);

signals:
    void sizeChanged(QSizeF size);
    void anchorChanged(L7::Anchor anchor);

    // IDocumentObject interface
public:
    virtual int objectType() const override { return Type; }
};
//Q_DECLARE_METATYPE(Terminal*)
//Q_DECLARE_METATYPE(const Terminal*)
//Q_DECLARE_METATYPE(QList<Terminal*>)
//Q_DECLARE_METATYPE(QList<const Terminal*>)
