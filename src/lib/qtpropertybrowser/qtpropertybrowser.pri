INCLUDEPATH += $$PWD/..

win32:CONFIG(release, debug|release): LIBS += -L$$top_builddir/src/lib/qtpropertybrowser/release/ -lqtpropertybrowser
else:win32:CONFIG(debug, debug|release): LIBS += -L$$top_builddir/src/lib/qtpropertybrowser/debug/ -lqtpropertybrowser
else:unix: LIBS += -L$$top_builddir/src/lib/qtpropertybrowser/ -lqtpropertybrowser

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/qtpropertybrowser/release/libqtpropertybrowser.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/qtpropertybrowser/debug/libqtpropertybrowser.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/qtpropertybrowser/release/qtpropertybrowser.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/qtpropertybrowser/debug/qtpropertybrowser.lib
else:unix: PRE_TARGETDEPS += $$top_builddir/src/lib/qtpropertybrowser/libqtpropertybrowser.a
