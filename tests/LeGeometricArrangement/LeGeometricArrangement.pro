TEMPLATE = app

TARGET = test-le-geometric-arrangement

CONFIG += console c++11
QT += widgets testlib

SOURCES += main.cpp \
    TestUndoCommand.cpp

HEADERS += \
    TestUndoCommand.h

include(../../src/lib/LeGeometricArrangement/LeGeometricArrangement.pri)
