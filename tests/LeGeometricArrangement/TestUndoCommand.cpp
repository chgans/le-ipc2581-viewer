#include "TestUndoCommand.h"

#include <CGAL/IO/Arr_text_formatter.h>

#include <QDebug>
#include <QList>
#include <QMap>
#include <QString>
#include <QStringList>
#include <QTest>
#include <QTextStream>

#include <iostream>
#include <sstream>

using namespace LGA;

static inline bool equivalent(const Point p1, const Point &p2)
{
    return p1 == p2;
}

static inline bool equivalent(Vertex_const_handle v1, Vertex_const_handle v2)
{
    return equivalent(v1->point(), v2->point());
}

static inline bool equivalent(Halfedge_const_handle e1, Halfedge_const_handle e2)
{
    return equivalent(e1->source(), e2->source()) &&
            equivalent(e1->target(), e2->target());
}

static inline bool equivalent(Ccb_halfedge_const_circulator circ1, Ccb_halfedge_const_circulator circ2)
{
    auto curr1 = circ1;
    auto curr2 = circ2;
    // Align the circulators
    while (curr2->source()->point() != curr1->source()->point())
        if (++curr2 == circ2)
            return false; // cannot find a common point to start with

    // Check both edge sources are equivalent for each point in the chain
    do
    {
        if (!equivalent(Halfedge_const_handle(curr1), Halfedge_const_handle(curr2)))
            return false;
        ++curr1;
        ++curr2;
    }
    while (curr1 != circ1);
    return true;
}

static inline bool find(Hole_const_iterator iter, Hole_const_iterator begin, Hole_const_iterator end)
{
    for (auto hIter = begin; hIter != end; ++hIter)
        if (equivalent(*iter, *hIter))
            return true;
    return false;
}

static inline bool find(Isolated_vertex_const_iterator iter, Isolated_vertex_const_iterator begin, Isolated_vertex_const_iterator end)
{
    for (auto vIter = begin; vIter != end; ++vIter)
        if (equivalent(iter, vIter))
            return true;
    return false;
}

static inline bool find(Vertex_const_iterator iter, Vertex_const_iterator begin, Vertex_const_iterator end)
{
    for (auto vIter = begin; vIter != end; ++vIter)
        if (equivalent(iter, vIter))
            return true;
    return false;
}

static inline bool find(Halfedge_const_iterator iter, Halfedge_const_iterator begin, Halfedge_const_iterator end)
{
    for (auto hIter = begin; hIter != end; ++hIter)
        if (equivalent(iter, hIter))
            return true;
    return false;
}

static inline bool equivalent(Face_const_handle f1, Face_const_handle f2)
{
    if (f1->number_of_outer_ccbs() != f2->number_of_outer_ccbs())
        return false;

    if (f1->number_of_holes() != f2->number_of_holes())
        return false;

    if (f1->number_of_isolated_vertices() != f2->number_of_isolated_vertices())
        return false;

    if (f1->number_of_outer_ccbs() != 0 &&
            !equivalent(f1->outer_ccb(), f2->outer_ccb()))
        return false;

    for (auto hIter = f1->holes_begin(); hIter != f1->holes_end(); ++hIter)
        if (!find(hIter, f2->holes_begin(), f2->holes_end()))
            return false;

    for (auto vIter = f1->isolated_vertices_begin(); vIter != f1->isolated_vertices_end(); ++vIter)
        if (!find(vIter, f2->isolated_vertices_begin(), f2->isolated_vertices_end()))
            return false;

    return true;
}

static inline bool find(Face_const_iterator iter, Face_const_iterator begin, Face_const_iterator end)
{
    for (auto fIter = begin; fIter != end; ++fIter)
        if (equivalent(Face_const_handle(fIter), Face_const_handle(iter)))
            return true;
    return false;
}

static inline bool equivalent(const Arrangement &a1, const Arrangement &a2)
{
    if (a1.number_of_vertices() != a2.number_of_vertices())
        return false;
    if (a1.number_of_edges() != a2.number_of_edges())
        return false;
    if (a1.number_of_faces() != a2.number_of_faces())
        return false;

    for (auto vIter = a1.vertices_begin(); vIter != a1.vertices_end(); ++vIter)
        if (!find(vIter, a2.vertices_begin(), a2.vertices_end()))
            return false;

    for (auto hIter = a1.halfedges_begin(); hIter != a1.halfedges_end(); ++hIter)
        if (!find(hIter, a2.halfedges_begin(), a2.halfedges_end()))
            return false;

    for (auto fIter = a1.faces_begin(); fIter != a1.faces_end(); ++fIter)
        if (!find(fIter, a2.faces_begin(), a2.faces_end()))
            return false;

    return true;
}

bool operator == (const Arrangement &a1, const Arrangement &a2)
{
    return equivalent(a1, a2);
}

static inline QString serialise(const Arrangement &arrangement)
{
    std::stringstream sstream;
    CGAL::Arr_text_formatter<Arrangement> formatter;
    CGAL::write(arrangement, sstream, formatter);
    return QString::fromStdString(sstream.str());
}

static inline void dumpDiff(const QString &s1, const QString &s2)
{
    if (s1 == s2)
    {
        return;
    }

    QStringList l1 = s1.split('\n');
    QStringList l2 = s2.split('\n');

    const int min = qMin(l1.count(), l2.count());
    for (int i = 0; i < qMin(l1.count(), l2.count()); ++i)
    {
        if (l1.value(i) == l2.value(i))
            qDebug() << l1.value(i);
        else
            qDebug() << l1.value(i) << " == VS ==" << l2.value(i);
    }
    for (int i = min; i < l1.count(); ++i)
        qDebug() << "1: " << l1.value(i);
    for (int i = min; i < l2.count(); ++i)
        qDebug() << "2: " << l2.value(i);
}
#define DUMP_DIFF(a, b) qDebug() << #a << "VS" << #b; dumpDiff(a, b)

TestUndoCommand::TestUndoCommand(QObject *parent)
    : QObject(parent)
{

}

Arrangement TestUndoCommand::makeSquare()
{
    Arrangement arrangement;
    m_v1 = arrangement.insert_in_face_interior(Point(0, 0), arrangement.unbounded_face());
    m_v2 = arrangement.insert_in_face_interior(Point(0, 1), arrangement.unbounded_face());
    m_v3 = arrangement.insert_in_face_interior(Point(1, 1), arrangement.unbounded_face());
    m_v4 = arrangement.insert_in_face_interior(Point(1, 0), arrangement.unbounded_face());
    m_e1 = arrangement.insert_at_vertices(Segment(m_v1->point(), m_v2->point()), m_v1, m_v2);
    m_e2 = arrangement.insert_at_vertices(Segment(m_v2->point(), m_v3->point()), m_v2, m_v3);
    m_e3 = arrangement.insert_at_vertices(Segment(m_v3->point(), m_v4->point()), m_v3, m_v4);
    m_e4 = arrangement.insert_at_vertices(Segment(m_v4->point(), m_v1->point()), m_v4, m_v1);
    return arrangement;
}

void TestUndoCommand::testArrangementComparaison()
{
    Arrangement arrangement1;
    Arrangement arrangement2;
    QVERIFY(equivalent(arrangement1, arrangement2));

    arrangement1 = makeSquare();
    arrangement2 = makeSquare();
    QVERIFY(equivalent(arrangement1, arrangement2));

    arrangement1 = Arrangement();
    arrangement2 = makeSquare();
    QVERIFY(!equivalent(arrangement1, arrangement2));

    arrangement1 = makeSquare();
    arrangement2 = Arrangement();
    QVERIFY(!equivalent(arrangement1, arrangement2));

    arrangement1 = makeSquare();
    arrangement1.insert_in_face_interior(Point(10, 10), arrangement1.unbounded_face());
    arrangement2 = makeSquare();
    QVERIFY(!equivalent(arrangement1, arrangement2));
    arrangement2.insert_in_face_interior(Point(10, 10), arrangement2.unbounded_face());
    QVERIFY(equivalent(arrangement1, arrangement2));

    arrangement1.insert_in_face_interior(Segment(Point(3, 3), Point(3, 4)), arrangement1.unbounded_face());
    arrangement2.insert_in_face_interior(Segment(Point(4, 4), Point(4, 5)), arrangement2.unbounded_face());
    QVERIFY(!equivalent(arrangement1, arrangement2));
    arrangement1.insert_in_face_interior(Segment(Point(4, 4), Point(4, 5)), arrangement1.unbounded_face());
    arrangement2.insert_in_face_interior(Segment(Point(3, 3), Point(3, 4)), arrangement2.unbounded_face());
    QVERIFY(equivalent(arrangement1, arrangement2));

    // Split the face
    CGAL::insert(arrangement1, Segment(Point(0, 1), Point(1, 0)));
    QVERIFY(!equivalent(arrangement1, arrangement2));
    CGAL::insert(arrangement2, Segment(Point(0, 1), Point(1, 0)));
    QVERIFY(equivalent(arrangement1, arrangement2));

    // Insert a hole in upper face
    CGAL::insert(arrangement1, Segment(Point(0.6, .6), Point(0.7, 0.7)));
    QVERIFY(!equivalent(arrangement1, arrangement2));
    CGAL::insert(arrangement2, Segment(Point(0.6, .6), Point(0.7, 0.7)));
    QVERIFY(equivalent(arrangement1, arrangement2));

    // Include the face in a bigger face
    CGAL::insert(arrangement1, Segment(Point(-1, -1), Point(-1, 2)));
    CGAL::insert(arrangement1, Segment(Point(-1, 2), Point(2, 2)));
    CGAL::insert(arrangement1, Segment(Point(2, 2), Point(2, -1)));
    CGAL::insert(arrangement1, Segment(Point(2, -1), Point(-1, -1)));
    QVERIFY(!equivalent(arrangement1, arrangement2));
    CGAL::insert(arrangement2, Segment(Point(-1, -1), Point(-1, 2)));
    CGAL::insert(arrangement2, Segment(Point(-1, 2), Point(2, 2)));
    CGAL::insert(arrangement2, Segment(Point(2, 2), Point(2, -1)));
    CGAL::insert(arrangement2, Segment(Point(2, -1), Point(-1, -1)));
    QVERIFY(equivalent(arrangement1, arrangement2));
}

void TestUndoCommand::testInsertInCommand_pf_1()
{
    Arrangement arrangement = makeSquare();
    InsertInCommand command(&arrangement,
                            Point(2, 2),
                            arrangement.unbounded_face());
    exerciseCommand(arrangement, &command);
    QCOMPARE(m_initial, m_afterUndo1);
    QVERIFY(arrangement.number_of_faces() == 2);
    QVERIFY(arrangement.number_of_edges() == 4);
    QVERIFY(arrangement.number_of_vertices() == 5);
    QCOMPARE(m_afterRedo1, m_afterRedo2);
    QCOMPARE(m_afterUndo1, m_afterUndo2);
    QCOMPARE(m_afterRedo2, m_afterRedo3);
}

void TestUndoCommand::testInsertInCommand_pf_2()
{
    Arrangement arrangement = makeSquare();
    InsertInCommand command(&arrangement,
                            Point(0.5, 0.5),
                            m_e2->twin()->face());
    exerciseCommand(arrangement, &command);
    QVERIFY(arrangement.number_of_faces() == 2);
    QVERIFY(arrangement.number_of_edges() == 4);
    QVERIFY(arrangement.number_of_vertices() == 5);
    QCOMPARE(m_initial, m_afterUndo1);
    QCOMPARE(m_afterRedo1, m_afterRedo2);
    QCOMPARE(m_afterUndo1, m_afterUndo2);
    QCOMPARE(m_afterRedo2, m_afterRedo3);
}

void TestUndoCommand::testInsertInCommand_cf_1()
{
    Arrangement arrangement = makeSquare();
    InsertInCommand command(&arrangement,
                            Segment(Point(2.1, 2.1), Point(2.2, 2.2)),
                            arrangement.unbounded_face());
    exerciseCommand(arrangement, &command);
    QVERIFY(arrangement.number_of_faces() == 2);
    QVERIFY(arrangement.number_of_edges() == 5);
    QVERIFY(arrangement.number_of_vertices() == 6);
    QCOMPARE(m_initial, m_afterUndo1);
    QCOMPARE(m_afterRedo1, m_afterRedo2);
    QCOMPARE(m_afterUndo1, m_afterUndo2);
    QCOMPARE(m_afterRedo2, m_afterRedo3);
}

void TestUndoCommand::testInsertInCommand_cf_2()
{
    Arrangement arrangement = makeSquare();
    InsertInCommand command(&arrangement,
                            Segment(Point(0.1, 0.1), Point(0.2, 0.2)),
                            m_e2->twin()->face());
    exerciseCommand(arrangement, &command);
    QVERIFY(arrangement.number_of_faces() == 2);
    QVERIFY(arrangement.number_of_edges() == 5);
    QVERIFY(arrangement.number_of_vertices() == 6);
    QCOMPARE(m_initial, m_afterUndo1);
    QCOMPARE(m_afterRedo1, m_afterRedo2);
    QCOMPARE(m_afterUndo1, m_afterUndo2);
    QCOMPARE(m_afterRedo2, m_afterRedo3);
}

void TestUndoCommand::testInsertInCommand_cf_3()
{
    Arrangement arrangement = makeSquare();
    InsertInCommand command(&arrangement,
                            Segment(Point(0.2, 0.2), Point(0.1, 0.1)),
                            m_e2->twin()->face());
    exerciseCommand(arrangement, &command);
    QVERIFY(arrangement.number_of_faces() == 2);
    QVERIFY(arrangement.number_of_edges() == 5);
    QVERIFY(arrangement.number_of_vertices() == 6);
    QCOMPARE(m_initial, m_afterUndo1);
    QCOMPARE(m_afterRedo1, m_afterRedo2);
    QCOMPARE(m_afterUndo1, m_afterUndo2);
    QCOMPARE(m_afterRedo2, m_afterRedo3);
}


void TestUndoCommand::testInsertAtCommand_vv()
{
    Arrangement arrangement = makeSquare();
    InsertAtCommand command(&arrangement,
                            Segment(m_v2->point(), m_v4->point()),
                            m_v2, m_v4);
    exerciseCommand(arrangement, &command);
    QVERIFY(arrangement.number_of_faces() == 3);
    QVERIFY(arrangement.number_of_edges() == 5);
    QVERIFY(arrangement.number_of_vertices() == 4);
    QCOMPARE(m_initial, m_afterUndo1);
    QCOMPARE(m_afterRedo1, m_afterRedo2);
    QCOMPARE(m_afterUndo1, m_afterUndo2);
    QCOMPARE(m_afterRedo2, m_afterRedo3);
}

void TestUndoCommand::testInsertAtCommand_hv()
{
    Arrangement arrangement = makeSquare();
    InsertAtCommand command(&arrangement,
                            Segment(m_v4->point(), m_v2->point()),
                            m_e4->twin(), m_v2);
    exerciseCommand(arrangement, &command);
    QVERIFY(arrangement.number_of_faces() == 3);
    QVERIFY(arrangement.number_of_edges() == 5);
    QVERIFY(arrangement.number_of_vertices() == 4);
    QCOMPARE(m_initial, m_afterUndo1);
    QCOMPARE(m_afterRedo1, m_afterRedo2);
    QCOMPARE(m_afterUndo1, m_afterUndo2);
    QCOMPARE(m_afterRedo2, m_afterRedo3);
}

void TestUndoCommand::testInsertAtCommand_hh()
{
    Arrangement arrangement = makeSquare();
    arrangement.remove_edge(m_e2, false, false);
    InsertAtCommand command(&arrangement,
                            Segment(m_v2->point(), m_v3->point()),
                            m_e1, m_e3->twin());
    exerciseCommand(arrangement, &command);
    QCOMPARE(arrangement, makeSquare());
    QCOMPARE(m_initial, m_afterUndo1);
    QCOMPARE(m_afterRedo1, m_afterRedo2);
    QCOMPARE(m_afterUndo1, m_afterUndo2);
    QCOMPARE(m_afterRedo2, m_afterRedo3);
}

void TestUndoCommand::testBug1()
{
    Arrangement arrangement;
    //WalkLocator locator(arrangement);
    TrapezoidLocator locator(arrangement);
    m_v1 = arrangement.insert_in_face_interior(Point(0, 0), arrangement.unbounded_face());
    m_v2 = arrangement.insert_in_face_interior(Point(0, 1), arrangement.unbounded_face());
    m_v3 = arrangement.insert_in_face_interior(Point(1, 1), arrangement.unbounded_face());
    m_v4 = arrangement.insert_in_face_interior(Point(1, 0), arrangement.unbounded_face());
    m_e1 = arrangement.insert_at_vertices(Segment(m_v1->point(), m_v2->point()), m_v1, m_v2);
    //m_e2 = arrangement.insert_at_vertices(Segment(m_v2->point(), m_v3->point()), m_v2, m_v3);
    m_e3 = arrangement.insert_at_vertices(Segment(m_v3->point(), m_v4->point()), m_v3, m_v4);
    //m_e4 = arrangement.insert_at_vertices(Segment(m_v4->point(), m_v1->point()), m_v4, m_v1);

    m_e2 = arrangement.insert_at_vertices(Segment(m_v2->point(), m_v3->point()), m_v2, m_v3);
    arrangement.remove_edge(m_e2, false, false);
//    m_e4 = arrangement.insert_at_vertices(Segment(m_v4->point(), m_v1->point()), m_v4, m_v1);
//    arrangement.remove_edge(m_e4, false, false);

//    m_e2 = arrangement.insert_at_vertices(Segment(m_v2->point(), m_v3->point()), m_v2, m_v3);
//    m_e4 = arrangement.insert_at_vertices(Segment(m_v4->point(), m_v1->point()), m_v4, m_v1);
//    arrangement.remove_edge(m_e2, false, false);
//    arrangement.remove_edge(m_e4, false, false);
}

void TestUndoCommand::exerciseCommand(const Arrangement &arrangement, UndoCommand *command)
{
    m_initial = arrangement;

    command->redo();
    m_afterRedo1 = arrangement;

    command->undo();
    m_afterUndo1 = arrangement;

    command->redo();
    m_afterRedo2 = arrangement;

    command->undo();
    m_afterUndo2 = arrangement;

    command->redo();
    m_afterRedo3 = arrangement;
}
