#ifndef TESTUNDOCOMMAND_H
#define TESTUNDOCOMMAND_H

#include <QObject>

#include <LeGeometricArrangement/LeGeometricArrangement.h>
#include <LeGeometricArrangement/UndoCommand.h>

class TestUndoCommand : public QObject
{
    Q_OBJECT

public:
    explicit TestUndoCommand(QObject *parent = 0);


private slots:
    void testArrangementComparaison();
    void testInsertInCommand_pf_1();
    void testInsertInCommand_pf_2();
    void testInsertInCommand_cf_1();
    void testInsertInCommand_cf_2();
    void testInsertInCommand_cf_3();
    void testInsertAtCommand_vv();
    void testInsertAtCommand_hv();
    void testInsertAtCommand_hh();

    void testBug1();

private:
    LGA::Arrangement makeSquare();
    void exerciseCommand(const LGA::Arrangement &arrangement, LGA::UndoCommand *command);
    LGA::Arrangement m_initial, m_afterRedo1, m_afterUndo1, m_afterRedo2, m_afterUndo2, m_afterRedo3;
    LGA::Vertex_handle m_v1, m_v2, m_v3, m_v4;
    LGA::Halfedge_handle m_e1, m_e2, m_e3, m_e4;
};

#endif // TESTUNDOCOMMAND_H
