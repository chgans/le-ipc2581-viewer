
#include "TestUndoCommand.h"

#include <CGAL/assertions_behaviour.h>

#include <QtTest>

#include <iostream>

int main(int argc, char *argv[])
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)


    CGAL::set_warning_behaviour(CGAL::CONTINUE);
    CGAL::set_error_behaviour(CGAL::CONTINUE);

    int failures = 0;

    {
      TestUndoCommand test;
      failures += QTest::qExec(&test);
    }

    std::cout << std::endl;
    std::cout << "Total failures: " << failures;
    std::cout << std::endl;
    std::cout << std::endl;
    return failures;
}
