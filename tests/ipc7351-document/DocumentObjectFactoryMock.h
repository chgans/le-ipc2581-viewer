#pragma once

#include "LeIpc7351/IDocumentObjectFactory.h"

class DocumentObjectFactoryMock: public IDocumentObjectFactory
{
    Q_OBJECT

public:
    DocumentObjectFactoryMock();

    // IDocumentObjectFactory interface
public:
    virtual QList<int> objectTypeIds() const override;
    virtual IDocumentObject *createObject(int typeId) const override;
    virtual QString typeName(int typeId) const override;
};
