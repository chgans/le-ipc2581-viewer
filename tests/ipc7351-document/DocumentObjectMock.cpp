#include "DocumentObjectMock.h"


DocumentObjectMock0::DocumentObjectMock0()
    : IDocumentObject()
{

}

DocumentObjectMock1::DocumentObjectMock1()
    : IDocumentObject()
{

}

QString DocumentObjectMock1::string() const
{
    return m_string;
}

qreal DocumentObjectMock1::real() const
{
    return m_real;
}

int DocumentObjectMock1::integer() const
{
    return m_integer;
}

void DocumentObjectMock1::setString(QString string)
{
    if (m_string == string)
        return;

    _IDO_SET_PROPERTY(string);
}

void DocumentObjectMock1::setReal(qreal real)
{
    if (m_real == real)
        return;

    _IDO_SET_PROPERTY(real);
}

void DocumentObjectMock1::setInteger(int integer)
{
    if (m_integer == integer)
        return;

    _IDO_SET_PROPERTY(integer);
}

void DocumentObjectMock1::setEnumeration(DocumentObjectMock1::MockEnum enumeration)
{
    if (m_enumeration == enumeration)
        return;

    m_enumeration = enumeration;
    emit enumerationChanged(enumeration);
}

void DocumentObjectMock1::setFlags(MockFlags flags)
{
    if (m_flags == flags)
        return;

    m_flags = flags;
    emit flagsChanged(flags);
}

void DocumentObjectMock1::setStringList(QStringList stringList)
{
    if (m_stringList == stringList)
        return;

    m_stringList = stringList;
    emit stringListChanged(stringList);
}

int DocumentObjectMock1::objectType() const
{
    return Type;
}

QStringList DocumentObjectMock1::stringList() const
{
    return m_stringList;
}

DocumentObjectMock1::MockEnum DocumentObjectMock1::enumeration() const
{
    return m_enumeration;
}

DocumentObjectMock1::MockFlags DocumentObjectMock1::flags() const
{
    return m_flags;
}

DocumentObjectMock2::DocumentObjectMock2()
    : IDocumentObject()
{

}

DocumentObjectMock1 *DocumentObjectMock2::object() const
{
    return m_object;
}

void DocumentObjectMock2::setObject(DocumentObjectMock1 *object)
{
    if (m_object == object)
        return;

    m_object = object;
    emit objectChanged(object);
}

DocumentObjectMock3::DocumentObjectMock3():
    IDocumentObject()
{

}

QList<DocumentObjectMock1 *> DocumentObjectMock3::objects() const
{
    return m_objects;
}

void DocumentObjectMock3::setObjects(QList<DocumentObjectMock1 *> objects)
{
    if (m_objects == objects)
        return;

    m_objects = objects;
    emit objectsChanged(objects);
}

DocumentObjectMock4::DocumentObjectMock4()
    : IDocumentObject()
{

}

QList<QString> DocumentObjectMock4::strings() const
{
    return m_strings;
}

QList<double> DocumentObjectMock4::doubles() const
{
    return m_doubles;
}

void DocumentObjectMock4::setStrings(const QList<QString> &strings)
{
    if (m_strings == strings)
        return;
    m_strings = strings;
    emit stringsChanged(m_strings);
}

void DocumentObjectMock4::setDoubles(const QList<double> &doubles)
{
    if (m_doubles == doubles)
        return;
    m_doubles = doubles;
    emit doublesChanged(m_doubles);
}

void DocumentObjectMock4::setPoints(QList<QPointF> points)
{
    if (m_points == points)
        return;

    m_points = points;
    emit pointsChanged(points);
}

void DocumentObjectMock4::setStringLists(QList<QStringList> stringLists)
{
    if (m_stringLists == stringLists)
        return;

    m_stringLists = stringLists;
    emit stringListsChanged(stringLists);
}

QList<QStringList> DocumentObjectMock4::stringLists() const
{
    return m_stringLists;
}

QList<QPointF> DocumentObjectMock4::points() const
{
    return m_points;
}
