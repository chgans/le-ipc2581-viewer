#include "TestDocument.h"

#include <QtTest>
#include <QScopedPointer>

#include "LeIpc7351/Document.h"
#include "LeIpc7351/IDocumentObject.h"
#include "LeIpc7351/IDocumentObjectFactory.h"

void TestDocument::testEmptyDocument()
{
    QScopedPointer<Document> doc(new Document(this));
    QVERIFY(doc->childObjectCount() == 0);
    QVERIFY(doc->objectId().isNull() == false);
    QVERIFY(doc->objectName().startsWith("NoName"));
    QVERIFY(doc->objectUserName() == doc->objectName());
    QVERIFY(doc->parentObjectIndex() == -1);
    QVERIFY(doc->childObject(0) == nullptr);
    QVERIFY(doc->childObject(-1) == nullptr);
    QVERIFY(doc->childObject(42) == nullptr);
    //QVERIFY(doc->object(QUuid()) == nullptr);
    //QVERIFY(doc->object(QUuid::createUuid()) == nullptr);
    QVERIFY(doc->addObject("toto") == nullptr);
    QVERIFY(doc->childObjectCount() == 0);
}

void TestDocument::testObjectNames()
{
    QScopedPointer<Document> doc(new Document(this));
    doc->setObjectName("toto");
    QVERIFY(doc->objectName() == "toto");
    QVERIFY(doc->objectUserName() == "toto");
    doc->setObjectUserName("tata");
    QVERIFY(doc->objectName() == "toto");
    QVERIFY(doc->objectUserName() == "tata");
    doc->setObjectUserName("");
    QVERIFY(doc->objectName() == "toto");
    QVERIFY(doc->objectUserName() == "toto");
}

void TestDocument::testObjectFactory()
{

}
