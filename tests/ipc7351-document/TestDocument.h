#ifndef TESTDOCUMENT_H
#define TESTDOCUMENT_H

#include <QObject>

class TestDocument : public QObject
{
    Q_OBJECT

private slots:
    void testEmptyDocument();
    void testObjectNames();
    void testObjectFactory();
};

#endif // TESTDOCUMENT_H
