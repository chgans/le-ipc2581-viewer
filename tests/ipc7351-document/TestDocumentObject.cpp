#include "TestDocumentObject.h"
#include "DocumentObjectMock.h"

#include <QSignalSpy>
#include <QScopedPointer>
#include <QTest>

void TestDocumentObject::testProperties()
{
    QScopedPointer<DocumentObjectMock1> object(new DocumentObjectMock1());
    QSignalSpy signalSpy(object.data(), SIGNAL(stringChanged(QString)));
    QCOMPARE(object->string(), QString(""));
    object->setString("toto");
    QCOMPARE(object->string(), QString("toto"));
    QCOMPARE(signalSpy.count(), 1);
    QCOMPARE(signalSpy.at(0).at(0), QVariant("toto"));
    object->setString("toto");
    QCOMPARE(object->string(), QString("toto"));
    QCOMPARE(signalSpy.count(), 1);
    object->setString("tata");
    QCOMPARE(object->string(), QString("tata"));
    QCOMPARE(signalSpy.count(), 2);
    QCOMPARE(signalSpy.at(1).at(0), QVariant("tata"));
}
