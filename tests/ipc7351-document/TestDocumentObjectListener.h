#ifndef TESTDOCUMENTOBJECTLISTENER_H
#define TESTDOCUMENTOBJECTLISTENER_H

#include <QObject>
#include "LeIpc7351/IDocumentObjectListener.h"

class TestDocumentObjectListener : public QObject, public IDocumentObjectListener
{
    Q_OBJECT
public:
    explicit TestDocumentObjectListener(QObject *parent = 0);

private slots:
    void init();
    void cleanup();

    void testInsertChildObject();
    void testRemoveChildObject();
    void testChangeStaticProperty();
    void testChangeDynamicProperty();

    void testCannotListenTwice();
    void testCannotInsertTwice();
    void testCannotRemoveTwice();

private:
    void clear();

    QString firstCalled;

    int objectAboutToBeInsertedCallCount;
    const IDocumentObject *objectAboutToBeInsertedParent;
    const IDocumentObject *objectAboutToBeInsertedChild;
    int objectAboutToBeInsertedIndex;

    int objectInsertedCallCount;
    const IDocumentObject *objectInsertedParent;
    const IDocumentObject *objectInsertedChild;
    int objectInsertedIndex;

    int objectAboutToBeRemovedCallCount;
    const IDocumentObject *objectAboutToBeRemovedParent;
    const IDocumentObject *objectAboutToBeRemovedChild;
    int objectAboutToBeRemovedIndex;

    int objectRemovedCallCount;
    const IDocumentObject *objectRemovedParent;
    const IDocumentObject *objectRemovedChild;
    int objectRemovedIndex;

    int objectPropertyAboutToChangeCallCount;
    const IDocumentObject *objectPropertyAboutToChangeObject;
    QString objectPropertyAboutToChangeProperty;
    QVariant objectPropertyAboutToChangeValue;

    int objectPropertyChangedCallCount;
    const IDocumentObject *objectPropertyChangedObject;
    QString objectPropertyChangedProperty;
    QVariant objectPropertyChangedValue;

    // IDocumentObjectListener interface
public:
    virtual void documentObjectAboutToBeInserted(IDocumentObject *parent,
                                                 IDocumentObject *child,
                                                 int index) override;
    virtual void documentObjectInserted(IDocumentObject *parent,
                                        IDocumentObject *child,
                                        int index) override;
    virtual void documentObjectAboutToBeRemoved(IDocumentObject *parent,
                                                IDocumentObject *child,
                                                int index) override;
    virtual void documentObjectRemoved(IDocumentObject *parent,
                                       IDocumentObject *child,
                                       int index) override;
    virtual void documentObjectAboutToChangeProperty(const IDocumentObject *object,
                                                     const QString &name,
                                                     const QVariant &value) override;
    virtual void documentObjectPropertyChanged(const IDocumentObject *object,
                                               const QString &name,
                                               const QVariant &value) override;
};

#endif // TESTDOCUMENTOBJECTLISTENER_H
