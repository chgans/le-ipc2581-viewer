#pragma once

#include <QObject>

class TestDocumentStreamReader : public QObject
{
    Q_OBJECT
    bool m_debug = false;

private slots:
    void testReadStringValue_data();
    void testReadStringValue();
    void testReadIntValue_data();
    void testReadIntValue();
    void testReadDoubleValue_data();
    void testReadDoubleValue();
    void testReadBoolValue_data();
    void testReadBoolValue();
    void testReadEnumValue();
    void testReadFlagValue();
    void testReadPointValue();
    void testReadSizeValue();
    void testReadStringListValue();

    void testReadListOfStringValue();
    void testReadListOfIntValue();
    void testReadListOfDoubleValue();
    void testReadListOfBoolValue();
    void testReadListOfEnumValue();
    void testReadListOfFlagValue();
    void testReadListOfPointValue();
    void testReadListOfSizeValue();
    void testReadListOfStringListValue();

    void testReadStringPropertyValue();
    void testReadIntPropertyValue();
    void testReadDoublePropertyValue();
    void testReadBoolPropertyValue();
    void testReadEnumPropertyValue();
    void testReadFlagPropertyValue();
    void testReadPointPropertyValue();
    void testReadSizePropertyValue();
    void testReadStringListPropertyValue();

    void testReadListOfStringPropertyValue();
    void testReadListOfIntPropertyValue();
    void testReadListOfDoublePropertyValue();
    void testReadListOfBoolPropertyValue();
    void testReadListOfEnumPropertyValue();
    void testReadListOfFlagPropertyValue();
    void testReadListOfPointPropertyValue();
    void testReadListOfSizePropertyValue();
    void testReadListOfStringListPropertyValue();

    void testReadMockObject0(); // No properties
    void testReadMockObject1(); // Simple type properties

    void testReadObjectStarValue();
    void testReadObjectStarPropertyValue();
    void testReadListOfObjectStarValue();
    void testReadListOfObjectStarPropertyValue();

    void testReadMockObject2(); // object* properties
    void testReadMockObject3(); // list<object*> properties
    void testReadMockObject4(); // list<simple-type> properties

    void testReadEmptyDocument();
    void testReadComplexDocument();
};
