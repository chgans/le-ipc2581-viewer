#ifndef TESTDOCUMENTSTREAMWRITER_H
#define TESTDOCUMENTSTREAMWRITER_H

#include <QObject>

class TestDocumentStreamWriter : public QObject
{
    Q_OBJECT

    bool m_debug = false;

private slots:

    void testWriteNullObject();
    void testWriteSimpleObject();
    void testWriteSimpleDocument();
    void testWriteNullSubObjectProperty();
    void testWriteSubObjectProperty();

    void testWriteSubObjectListProperty();


private:
    void dumpXml(const QByteArray &data);
};

#endif // TESTDOCUMENTSTREAMWRITER_H
